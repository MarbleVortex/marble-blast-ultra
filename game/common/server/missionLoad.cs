//-----------------------------------------------------------------------------
// Torque Shader Engine 
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Server mission loading
//-----------------------------------------------------------------------------

// On every mission load except the first, there is a pause after
// the initial mission info is downloaded to the client.
// JMQ: not for MB2
//$MissionLoadPause = 5000;

//-----------------------------------------------------------------------------

function loadMission( %missionName, %isFirstMission ) 
{
   $missionLoadStart = getRealTime();

   endMission();
   echo("*** LOADING MISSION: " @ %missionName);
   echo("*** Stage 1 load");
   
   // Reset all of these
   clearCenterPrintAll();
   clearBottomPrintAll();
   clearServerPaths();

   // increment the mission sequence (used for ghost sequencing)
   $missionSequence++;
   $missionRunning = false;
   $Server::MissionFile = %missionName;

   // Extract mission info from the mission file,
   // including the display name and stuff to send
   // to the client.
   buildLoadInfo( %missionName );

   // Download mission info to the clients
   %count = ClientGroup.getCount();
   for( %cl = 0; %cl < %count; %cl++ ) {
      %client = ClientGroup.getObject( %cl );
      if (!%client.isAIControlled())
         sendLoadInfoToClient(%client);
   }

   // if this isn't the first mission, allow some time for the server
   // to transmit information to the clients:
   if( %isFirstMission || $Server::ServerType $= "SinglePlayer" )
      loadMissionStage2();
   else
      schedule( $MissionLoadPause, ServerGroup, loadMissionStage2 );
}

//-----------------------------------------------------------------------------

function loadMissionStage2() 
{
   // Create the mission group off the ServerGroup
   echo("*** Stage 2 load");
   $instantGroup = ServerGroup;

   // Make sure the mission exists
   %file = $Server::MissionFile;
   echo( "--Trying to find mission " @ %file );
   
   if( !isFile( %file ) ) {
      error( "Could not find mission " @ %file );
      return;
   }
	
	if (isFile( %file) ) {
		// Calculate the mission CRC.  The CRC is used by the clients
		// to caching mission lighting.
		$missionCRC = getFileCRC( %file );

		// Exec the mission, objects are added to the ServerGroup
		exec(%file);
	
	}
   
   // If there was a problem with the load, let's try another mission
   if( !isObject(MissionGroup) ) {
      error( "No 'MissionGroup' found in mission \"" @ $missionName @ "\"." );
      schedule( 3000, ServerGroup, CycleMissions );
      return;
   }
   
   $Mission::Name = MissionInfo.name;
   $Mission::Mode = MissionInfo.gameMode;
   
   // For some reason it doesn't always remove everything
   $addedFlag = false;
   $addedTeleportParticles = false;
   for (%i = 0; %i < 100; %i++)
   {
      removePreviewStuff();
   }

   // populate id variables for the mission we loaded
   $Server::MissionId = MissionInfo.level;
   $Server::GameModeId = GameMissionInfo.getGameModeIdFromString(MissionInfo.gameMode);

   // Mission cleanup group
   new SimGroup( MissionCleanup );
   $instantGroup = MissionCleanup;
   
   // Construct MOD paths
   pathOnMissionLoadDone();

   // Mission loading done...
   echo("*** Mission loaded");
   
   // Start all the clients in the mission
   $missionRunning = true;
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
      ClientGroup.getObject(%clientIndex).loadMission();

   // Go ahead and launch the game
   onMissionLoaded();
   purgeResources();
}

function removePreviewStuff()
{
   if (!$Client::showPreviews && !$addedFlag && MissionInfo.gameMode $= "race")
   {
      for (%i = 0; %i < MissionGroup.getCount(); %i++)
      {
         %obj = MissionGroup.getObject(%i);
         if (%obj.getClassName() $= "Trigger" && %obj.getDataBlock().getName() $= "FinishTrigger")
         {
            FinishTrigger.addFlag(%obj);
            $addedFlag = true;
         }
      }
   }
   if (!$Client::showPreviews && !$addedTeleportParticles)
   {
      for (%i = 0; %i < MissionGroup.getCount(); %i++)
      {
         %obj = MissionGroup.getObject(%i);
         if (%obj.getClassName() $= "Trigger" && %obj.getDataBlock().getName() $= "TeleportTrigger")
         {
            TeleportTrigger.addParticles(%obj);
            $addedTeleportParticles = true;
         }
      }
   }
   if ($Client::showPreviews)
   {
      for (%i = 0; %i < MissionGroup.getCount(); %i++)
      {
         %obj = MissionGroup.getObject(%i);
                  
         if (%obj !$= "" && (%obj.getClassName() $= "SpawnSphere" ||
             %obj.getClassName() $= "InteriorInstance" ||
             %obj.getClassName() $= "Sky" ||
             %obj.getClassName() $= "Sun" || (%obj.isMethod("getDataBlock") &&
             (%obj.getDatablock().getName() $= "astrolabeShape" ||
             %obj.getDatablock().getName() $= "astrolabeCloudsBeginnerShape" ||
             %obj.getDatablock().getName() $= "astrolabeCloudsIntermediateShape" ||
             %obj.getDatablock().getName() $= "astrolabeCloudsAdvancedShape" ||
             %obj.getDatablock().getName() $= "glass_3shape" ||
             %obj.getDatablock().getName() $= "glass_6shape" ||
             %obj.getDatablock().getName() $= "glass_9shape" ||
             %obj.getDatablock().getName() $= "glass_12shape" ||
             %obj.getDatablock().getName() $= "glass_15shape" ||
             %obj.getDatablock().getName() $= "glass_18shape" ||
             %obj.getDatablock().getName() $= "glass_flat" ||
			 %obj.getDatablock().getName() $= "tritwist_glass" ||
             %obj.getDatablock().getName() $= "Horizon_0_Glass" ||
             %obj.getDatablock().getName() $= "HopSkipJump" ||
             %obj.getDatablock().getName() $= "RoyaleFull" ||
             %obj.getDatablock().getName() $= "HalfPipe0" ||
			    %obj.getDatablock().getName() $= "WaterShape" ||
			    %obj.getDatablock().getName() $= "Tree" ||
			    %obj.getDatablock().getName() $= "Flower" ||
			    %obj.getDatablock().getName() $= "TallGrass"))))
            continue;
         
         if (MissionInfo.showPlatforms && %obj !$= "" && %obj.getName() $= "MustChange_g")
         {
            continue;
         }
         
         if (MissionInfo.keepStatic && %obj !$= "" && (%obj.getClassName() $= "StaticShape" || %obj.getClassName() $= "TSStatic"))
         {
            continue;
         }
            //if (%obj.getClassName() $= "SimGroup")
            //{
               //for (%j = 0; %j < %obj.getCount(); %j++)
               //{
                  //%obj2 = %obj.getObject(%j);
                  //if (%obj2 !$= "" && (%obj2.getClassName() $= "PathedInterior" || %obj2.getClassName() $= "Path"))
                  //{
                     ////echo("Keeping Platforms");
                     ////continue;
                  //} else {
                     ////   
                     ////echo("Thing: " @ %obj2.getClassName());
                     //%obj2.delete();
                  //}
               //}
            //}
         //}
         
         //if (%obj !$= "" && %obj.getClassName() !$= "SimGroup")
            %obj.delete();
      }
   }
}


//-----------------------------------------------------------------------------

function endMission()
{
   if (!isObject( MissionGroup ))
      return;

   echo("*** ENDING MISSION");
   
   // Inform the game code we're done.
   onMissionEnded();

   // Inform the clients
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      // clear ghosts and paths from all clients
      %cl = ClientGroup.getObject( %clientIndex );
      if (%cl.dummy)
      {
         %cl.delete();
         continue;
      }
      %cl.endMission();
      %cl.resetGhosting();
      %cl.clearPaths();
   }
   
   // Delete everything
   MissionGroup.delete();
   MissionCleanup.delete();

   $ServerGroup.delete();
   $ServerGroup = new SimGroup(ServerGroup);
}


//-----------------------------------------------------------------------------

function resetMission()
{
   echo("*** MISSION RESET");

   // Remove any temporary mission objects
   MissionCleanup.delete();
   $instantGroup = ServerGroup;
   new SimGroup( MissionCleanup );
   $instantGroup = MissionCleanup;

   //
   onMissionReset();
}
