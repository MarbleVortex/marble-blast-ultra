//-----------------------------------------------------------------------------
// Torque Shader Engine 
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

function portInit(%port)
{
   $currentPort = %port;
   error("Port Init: " @ %port);
   %failCount = 0;
   while(%failCount < 10 && !setNetPort(%port)) {
      echo("Port init failed on port " @ %port @ " trying next port.");
      %port++; %failCount++;
   }
}
 
function createServer(%serverType, %mission)
{
   if (%mission $= "") {
      error("createServer: mission name unspecified");
      return;
   }

   destroyServer();

   //
   $missionSequence = 0;
   $Server::PlayerCount = 0;
   $Server::PrivatePlayerCount = 0;
   $Server::ServerType = %serverType;
   $Server::ArbRegTimeout = 10000;
  // Setup for multi-player, the network must have been
  // initialized before now.
  if (%serverType $= "MultiPlayer") 
  {
	 startMultiplayerServer();
  }

   // Load the mission
   $ServerGroup = new SimGroup(ServerGroup);
   onServerCreated();
   $doShowEditor = false;
   loadMission(%mission, true);
}

function startMultiplayerServer()
{
   if (doesAllowConnect())
   {
      error("startMultiplayerServer(): Multiplayer mode is already active");
      return;
   }
   echo("Starting multiplayer mode");
   $Server::ServerType = "MultiPlayer";
   $Server::Hosting = true;
   $Server::BlockJIP = false;
   
   // Make sure the network port is set to the correct pref.
   if ($hiddenServer)
   {
      portInit($Pref::Server::Port);
      schedule(0, 0, "portInit", $Pref::Server::Port);
   } else
   {
      portInit(0);
      schedule(0, 0, "portInit", 0);
   }
   allowConnect(true);
   stopVortexNet();
   hostVortexNet(getUserName(), !$hiddenServer);

   //if ($pref::Server::DisplayOnMaster !$= "Never" )
      //schedule(0,0,startHeartbeat);
	  
	//MasterConnection.startServer();
	
	  //MasterConnection.startHeartbeat();
      
   // we are now considered to be connected to a multiplayer server (our own)
   $Client::connectedMultiplayer = true;
   
   // update the data for the local client connection
   // the third value is whether or not we are talking which, since we can't record, is always false!
   LocalClientConnection.updateClientData(getUserName(), $Player::XBLiveId, false, false);
}

function stopMultiplayerServer()
{
   MasterConnection.stopServer();
//   if (!doesAllowConnections())
//      return;

   if (!$Server::Hosting)
      return;
   
   echo("Stopping multiplayer mode");
   $Server::ServerType = "SinglePlayer";
   $Server::Hosting = false;
   $Server::UsingLobby  = false;
   $Server::BlockJIP = false;
   
   stopVortexNet();
   
   portInit(0);
   allowConnect(false);
   //stopHeartbeat();
   
   MasterConnection.stopHeartbeat();
   
   cancel($Server::ArbSched);
   
   if (isObject(ServerConnection))
      ServerConnection.ready = false;
   
   $Client::connectedMultiplayer = false;
}

//-----------------------------------------------------------------------------

function destroyServer()
{
    $Server::Hosting = false;
    allowConnect(false);
    MasterConnection.stopServer();
    //stopHeartbeat();
   
   $Server::ServerType = "";
   $missionRunning = false;
   
   // Clean up the game scripts
   onServerDestroyed();

   // Delete all the server objects
   if (isObject(MissionGroup))
      MissionGroup.delete();
   if (isObject(MissionCleanup))
      MissionCleanup.delete();
   if (isObject($ServerGroup))
      $ServerGroup.delete();
   if (isObject(MissionInfo))
      MissionInfo.delete();

      // Delete all the connections:
      while (ClientGroup.getCount())
      {
         %client = ClientGroup.getObject(0);
         %client.delete();
      }

   $Server::GuidList = "";
   $Server::MissionFile = "";

   // Delete all the data blocks...
   // JMQ: we don't delete datablocks in this version of MB
   //deleteDataBlocks();
   
   // Save any server settings - Not on the XBox -pw
   //echo( "Exporting server prefs..." );
   //export( "$Pref::Server::*", "~/prefs.cs", false );

   // Dump anything we're not using
   purgeResources();
}


//--------------------------------------------------------------------------

function resetServerDefaults()
{
   echo( "Resetting server defaults..." );
   
   // Override server defaults with prefs:   
   exec( "~/defaults.cs" );
   exec( "~/prefs.cs" );

   $doShowEditor = false;
   loadMission( $Server::MissionFile );
}


//------------------------------------------------------------------------------
// Guid list maintenance functions:
function addToServerGuidList( %guid )
{
   %count = getFieldCount( $Server::GuidList );
   for ( %i = 0; %i < %count; %i++ )
   {
      if ( getField( $Server::GuidList, %i ) == %guid )
         return;
   }

   $Server::GuidList = $Server::GuidList $= "" ? %guid : $Server::GuidList TAB %guid;
}

function removeFromServerGuidList( %guid )
{
   %count = getFieldCount( $Server::GuidList );
   for ( %i = 0; %i < %count; %i++ )
   {
      if ( getField( $Server::GuidList, %i ) == %guid )
      {
         $Server::GuidList = removeField( $Server::GuidList, %i );
         return;
      }
   }

   // Huh, didn't find it.
}


//-----------------------------------------------------------------------------

function onServerInfoQuery()
{
   // When the server is queried for information, the value
   // of this function is returned as the status field of
   // the query packet.  This information is accessible as
   // the ServerInfo::State variable.
   return "Doing Ok";
}

