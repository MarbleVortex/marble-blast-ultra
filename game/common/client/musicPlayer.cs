if (!isObject(DjAnson))
	new SimSet(DjAnson);
	
DjAnson.playOnActivate = 0;
DjAnson.isActive = false;
DjAnson.currTrack = 0;

function DjAnson::activate(%this)
{
	if (!%this.isPlaying && %this.playOnActivate)
		playNext();
}

function DjAnson::stop(%this)
{
   //%this.currTrack = %this.getObject(%this.currTrackIndex);   
   
	sfxStop($musicSound);
	%this.isPlaying = false;
	//%this.delete();
}

function DjAnson::addTrack(%this, %trackSfxProfile)
{
	if (!%this.isMember(%trackSfxProfile))
		%this.add(%trackSfxProfile);
}

$pref::SFX::fadeInTime = 3000;
$pref::SFX::fadeOutTime = 3000;

function DjAnson::playNextTwo(%this) {
	%this.stop();
	
	// the following switch statement finds, based on how many songs are in the list of tracks,
	// which song is being played, and picks the appropriate icon. - Anthony
	switch ($pref::MusicTrack) {
	   case 0:
	   // Marble Fantasy
	   $pref::MusicTrack = 1;
	   MusicFrame.setBitmap("marble/client/ui/music/" @ $pref::MusicTrack+1, true);
		case 1:
	   // Anthony Trance
	   $pref::MusicTrack = 2;
	   MusicFrame.setBitmap("marble/client/ui/music/" @ $pref::MusicTrack+1, true);
	   case 2:
	   // Skail - MBX
	   $pref::MusicTrack = 3;
	   MusicFrame.setBitmap("marble/client/ui/music/" @ $pref::MusicTrack+1, true);
	   default:
	   // Tim Trance
	   $pref::MusicTrack = 0;
	   MusicFrame.setBitmap("marble/client/ui/music/" @ $pref::MusicTrack+1, true);
	}
	
	Canvas.pushDialog(MusicDlg);
   $closeMusic = Canvas.schedule(3000, "popDialog", MusicDlg);
   %this.play();
   fadeInAudio();
}

function DjAnson::playNext(%this)
{
   // prevent TAB'ing on the esrbGui before music is even playing.
   if (%this.isPlaying)
   {
      fadeOutAudio();
      // wait until the previous song has faded out to start the next track. - Anthony
      %this.schedule($pref::SFX::fadeOutTime, "playNextTwo");
   }
}

function DjAnson::play(%this)
{	
	%this.currTrack = %this.getObject($pref::MusicTrack);
	
	$musicSound = sfxPlay(%this.currTrack);
	%this.isPlaying = true;
}