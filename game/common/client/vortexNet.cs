$VortexNet::Address = "marblevortex.com"; //"163.172.161.196";
$VortexNet::TCPPort = "24000";
$VortexNet::UDPPort = "24001";
$VortexNet::GameName = "MBUltra_v1.9.1";

function allowConnect(%flag)
{
   allowConnections(%flag);
}

function doesAllowConnect()
{
	return doesAllowConnections();
}

function onVortexNetBroadcastSuccess()
{
	echo("Broadcasting to VortexNet...");
}

function onVortexNetBroadcastFailed()
{
	error("Failed to broadcast to VortexNet");
}

function onVortexNetQueryResponse(%response)
{
	//echo ("Query Response: " @ %response);
	
	//if (%response $= "none")
	//{
	//	MessageBoxOK("VortexNet", "There are no matches available to join");
	//} else {
		//%resp = strreplace(strreplace(%response, " ", ","), ":", " ");
		//%id = getWord(%resp, 0);
		
		//joinVortexNet(%id);
		
		FindGameGui.onSearchComplete("pc", %response);
	//}
}

function onVortexNetJoinSuccess(%addr, %port)
{
	//echo("onJoinSuccess: " @ %address @ ":" @ %port);
	Canvas.setContent(MissionLoadingGui);
	//connect(%address @ ":" @ %port, "", getUserName(), $pref::MarbleIndex);
	
	%address = %addr @ ":" @ %port;
	
	if (isObject(ServerConnection))
	   ServerConnection.delete();
	
	// clear the scores gui here so that our the client id from the preview server doesn't
	// show up in the scores list.
	PlayerListGui.clear();
	// reset client Id since we are connecting to a different server
	$Player::ClientId = 0;

	%conn = new GameConnection(ServerConnection);
	RootGroup.add(ServerConnection);

	%xbLiveVoice = 0; // XBLiveGetVoiceStatus();

	if ($UserAchievements::GamerScore $= "")
	$UserAchievements::GamerScore = 0;

	%score = $UserAchievements::GamerScore;

	// we expect $Player:: variables to be properly populated at this point   
	//%isDemoLaunch = isDemoLaunch();
	
   //error("Name is: " @ $Player::Name);
   // the fifth argument is whether or not we're a demo - but we're on the free PC version
	%conn.setConnectArgs(getUserName(), $Player::XBLiveId, %xbLiveVoice, %invited, false, %score, $VersionCode);
	%conn.setJoinPassword($Client::Password); 

	if (%address $= "")
	{
		// local connections are not considered to be multiplayer connections initially.  
		// they become multiplayer connections when multiplayer mode is started
		$Client::connectedMultiplayer = false;
		%conn.connectLocal();
		setNetPort(0);
	}
	else
	{
		// connecting as a client to remote server
		// need to open up the port so that we can get voice data
		if (!isPCBuild())
		{
			setNetPort(0);
			//portInit($Pref::Server::Port);
		}
		$Client::connectedMultiplayer = true;
		%conn.connect(%address);
	}
}

function onVortexNetJoinFailed()
{
	error("VortexNet Join Failed");
}

function onVortexNetClientJoin(%address, %port)
{
	//echo("onClientJoin: " @ %address @ ":" @ %port);
}

function onVortexNetDNSResolved()
{
   //echo("onDNSResolved");
}

function onVortexNetDNSFailed()
{
   error("Failed to resolve DNS");
}

function onVortexNetConnectionRequest()
{
   //echo("onConnectionRequest");
}

function onVortexNetConnected()
{
   //echo("onConnected");
}

function onVortexNetConnectFailed()
{
   error("VortexNet Connection Failed");
}

function onVortexNetDisconnect()
{
   //echo("onDisconnect");
}

function onVortexNetServerOutdated()
{
   //echo("onServerOutdated");
   
   MessageBoxOK("VortexNet", "Error: The VortexNet server is out of date.");
}

function onVortexNetClientOutdated()
{
   //echo("onClientOutdated");
   
   MessageBoxOK("VortexNet", "Error: Your game is out of date. Please download the latest update from www.vortexgames.org");
}
