//-----------------------------------------------------------------------------
// arrayObject.cs
// Copyright (c) The Platinum Team
// Portions Copyright (c) GarageGames and TGE
//-----------------------------------------------------------------------------


// Jeff: make the array group
if (!isObject(ArrayGroup)) {
	new SimGroup(ArrayGroup);
	RootGroup.add(ArrayGroup);
}

// Jeff: create a new array
function Array(%name, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg0, %arga, %argb, %argc, %argd, %arge, %argf) {
	%array = new ScriptObject(%name) {
		class = "Array";
		size = 0;
	};
	ArrayGroup.add(%array);

	//HiGuy: You can now specify up to 16 args for the array to have
	%array.addArgs(%arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg0, %arga, %argb, %argc, %argd, %arge, %argf);
	return %array;
}

//HiGuy: Duplicate the array
function Array::duplicate(%this) {
	%new = Array(%this.getName());
	for (%i = 0; %i < %this.size; %i ++)
		%new.addEntry(%this.val[%i]);
	return %new;
}

// Jeff: fill an array all at once
function Array::fillArray(%this, %list) {
	%this.clear();
	%size = getFieldCount(%list);
	for (%i = 0; %i < %size; %i ++)
		%this.val[%i] = getField(%list, %i);
	%this.size = %size;
	return %this;
}

// Jeff: add an entry to the array object
function Array::addEntry(%this, %entry) {
	%this.val[%this.size] = %entry;
	%this.size ++;
	return %this;
}

// Jeff: replaces the value of an entry by index
function Array::replaceEntryByIndex(%this, %index, %entry) {
	%this.val[%index] = %entry;
	if (%index >= %this.size)
		%this.size = %index + 1;
	return %this;
}

//HiGuy: In case you want to insert an entry before
function Array::insertEntryBefore(%this, %entry, %before) {
	if (%before > %this.size || %before < 0) {
		error("Array::insertEntryBefore() Index" SPC %before SPC "out of bounds [0 .." SPC %this.size @ "]");
		return;
	}
	for (%i = %this.size; %i > %before; %i --)
		%this.val[%i] = %this.val[%i - 1];
	%this.val[%before] = %entry;
	%this.size ++;
	return %this;
}

//HiGuy: More fun for inserting
function Array::insertEntryAfter(%this, %entry, %after) {
	if (%this.size >= %after || %after < 0) {
		error("Array::insertEntryAfter() Index" SPC %after SPC "out of bounds [0 .." SPC %this.size - 1 @ "]");
		return;
	}
	for (%i = %this.size; %i > %after + 1; %i --)
		%this.val[%i] = %this.val[%i - 1];
	%this.val[%after + 1] = %entry;
	%this.size ++;
	return %this;
}

// Jeff: remove an entry from the array object by specifing the index
function Array::removeEntryByIndex(%this, %index) {
	%this.size --;
	for (%i = %index; %i < %this.size; %i ++)
		%this.val[%i] = %this.val[%i + 1];
	%this.val[%this.size] = "";
	return %this;
}

//HiGuy: My brain wants this to be shorter
function Array::removeEntry(%this, %index) {
	%this.removeEntryByIndex(%index);
	return %this;
}

//HiGuy: Removes all entries that are the same as %conts
function Array::removeEntriesByContents(%this, %conts) {
	for (%i = 0; %i < %this.size; %i ++) {
		%entry = %this.val[%i];
		if (%entry $= %conts) {
			%this.removeEntryByIndex(%i);
			%i --;
		}
	}
	return %this;
}

//HiGuy: Shorthand
function Array::removeMatching(%this, %conts) {
	%this.removeEntriesByContents(%conts);
	return %this;
}

// Jeff: get a value from the specified index of the array
function Array::getEntryByIndex(%this, %index) {
	return %this.val[%index];
}

//HiGuy: Shorthand
function Array::getEntry(%this, %index) {
	return %this.getEntryByIndex(%index);
}

//HiGuy: Get the index of the first object with %value at field %field
function Array::getIndexByField(%this, %value, %field) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (getField(%this.val[%i], %field) $= %value)
			return %i;
	}
	return -1;
}

//HiGuy: Get the index of the first object with %value at record %record
function Array::getIndexByRecord(%this, %value, %record) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (getRecord(%this.val[%i], %record) $= %value)
			return %i;
	}
	return -1;
}

//HiGuy: Get the first object with %value at field %field
function Array::getEntryByField(%this, %value, %field) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (getField(%this.val[%i], %field) $= %value)
			return %this.val[%i];
	}
	return "";
}

//HiGuy: Get the first object with %value at record %record
function Array::getEntryByRecord(%this, %value, %record) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (getRecord(%this.val[%i], %record) $= %value)
			return %this.val[%i];
	}
	return "";
}

//HiGuy: Get the first object with %value at variable %var
function Array::getEntryByVariable(%this, %var, %value) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (isObject(%this.val[%i]) && %this.val[%i].getFieldValue(%var) $= %value)
			return %this.val[%i];
	}
	return "";
}

// Jeff: get the index of the specified entry
function Array::getIndexByEntry(%this, %value) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (%this.val[%i] $= %value)
			return %i;
	}
	return -1;
}

//HiGuy: Shorthand again!
function Array::getIndex(%this, %value) {
	return %this.getIndexByEntry(%value);
}

// Jeff: replace an index with null
function Array::replaceEntryByIndexWithNull(%this, %index) {
	%this.val[%index] = "";
	return %this;
}

//HiGuy: Wow, that is a long name
function Array::nullifyEntry(%this, %index) {
	%this.replaceEntryByIndexWithNull(%index);
	return %this;
}

//HiGuy: Returns true if the array contains an entry equal to "entry"
function Array::containsEntry(%this, %entry) {
	for (%i = 0; %i < %this.size; %i ++) {
		%ientry = %this.val[%i];
		if (%ientry $= %entry)
			return true;
	}
	return false;
}

//HiGuy: Shorthand
function Array::contains(%this, %entry) {
	return %this.containsEntry(%entry);
}

function Array::containsEntryAtField(%this, %entry, %field) {
	for (%i = 0; %i < %this.size; %i ++) {
		%ientry = getField(%this.val[%i], %field);
		if (%ientry $= %entry)
			return true;
	}
	return false;
}

//HiGuy: I like shortness
function Array::containsField(%this, %entry, %field) {
	return %this.containsEntryAtField(%entry, %field);
}

function Array::containsEntryAtRecord(%this, %entry, %record) {
	for (%i = 0; %i < %this.size; %i ++) {
		%ientry = getRecord(%this.val[%i], %record);
		if (%ientry $= %entry)
			return true;
	}
	return false;
}

//HiGuy: I like shortness
function Array::containsRecord(%this, %entry, %record) {
	return %this.containsEntryAtRecord(%entry, %record);
}


function Array::containsEntryAtVariable(%this, %var, %entry) {
	for (%i = 0; %i < %this.size; %i ++) {
		if (!isObject(%this.val[%i]))
			continue;
		%ientry = %this.val[%i].getFieldValue(%var);
		if (%ientry $= %entry)
			return true;
	}
	return false;
}

//HiGuy: I like shortness
function Array::containsVariable(%this, %var, %entry) {
	return %this.containsEntryAtVariable(%var, %entry);
}

// Jeff: get all the values of the array in a TAB based list
function Array::listValues(%this) {
	%list = %this.val[0];
	for (%i = 1; %i < %this.size; %i ++)
		%list = %list TAB %this.val[%i];
	return %list;
}

//HiGuy: Print all the values of the array to console
function Array::dumpValues(%this, %indent) {
	//echo(strRepeat("   ", %indent) @ "{");

	//HiGuy: Format and print, don't even try to understand :)
	for (%i = 0; %i < %this.size; %i ++) {
		%print = strRepeat("   ", %indent);
		%print = %print @ "   ";
		%print = %print @ strRepeat(" ", mFloor(mLog10(%this.size)) - mFloor(mLog10(%i)));
		%print = %print @ "[" @ %i @ "]";
		//%print = %print SPC (%this.val[%i] * 1 $= %this.val[%i] ? (isObject(%this.val[%i]) ? "<" : "") : "\"") @ %this.val[%i] @ (%this.val[%i] * 1 $= %this.val[%i] ? (isObject(%this.val[%i]) ? ">" : "") : "\"") @ (%i == %this.size - 1 ? "" : ","));
		//echo(%print);
	}
	//echo(strRepeat("   ", %indent) @ "}");
}

// Jeff: clear array
function Array::clear(%this) {
	for (%i = 0; %i < %this.size; %i ++)
		%this.val[%i] = "";
	%this.size = 0;
	return %this;
}

// Jeff: get size of the array
function Array::getSize(%this) {
	return %this.size;
}

//HiGuy: For single-value arrays
function Array::removeDuplicates(%this) {
	for (%i = 0; %i < %this.size; %i ++) {
		for (%j = %i + 1; %j < %this.size; %j ++) {
			if (%this.val[%i] $= %this.val[%j]) {
				%this.removeEntryByIndex(%j);
				%j --;
			}
		}
	}
	return %this;
}

//HiGuy: Duplicate + remove
function Array::removedDuplicatesArray(%this) {
	%new = %this.duplicate();
	%new.removeDuplicates();
	return %new;
}

//HiGuy: Swap two items (useful for sorting)
function Array::swap(%this, %index1, %index2) {
	%temp = %this.val[%index1];
	%this.val[%index1] = %this.val[%index2];
	%this.val[%index2] = %temp;
}

function qsortpartition(%this, %compareFn, %start, %end) {
	//Find a pivot (using the last one for easy sorting)
	%pivot = %end;
	%pivotItem = %this.val[%pivot];

	//How many items are less than the pivot (for swapping)
	%lower = %start;

	//Now split the list up
	for (%i = %start; %i < %end; %i ++) {
		//Item in the list
		%item = %this.val[%i];

		//Check if it's less
		if (call(%compareFn, %item, %pivotItem)) {
			//And move it
			%this.swap(%i, %lower);
			%lower ++;
		}
	}
	//Move the pivot to the center
	%this.swap(%pivot, %lower);
	//Let us know where the split is
	return %lower;
}

function qsortpart(%this, %compareFn, %start, %end) {
	//If the sub-list is of length 0 or 1, it's sorted
	if (%end - %start <= 1)
		return;

	//Partition the list into pieces
	%pivot = qsortpartition(%this, %compareFn, %start, %end - 1);

	//Now sort the parts by diving and conquering

	//Sort everything below the pivot
	qsortpart(%this, %compareFn, %start, %pivot);
	//Sort everything above the pivot, but not the pivot itself
	qsortpart(%this, %compareFn, %pivot + 1, %end);
}

function compareLesser(%a, %b) {
	return %a < %b;
}

function compareStringLesser(%a, %b) {
	return stricmp(%a, %b) < 0;
}

//HiGuy: Sort an array (using quicksort) with an optional compare function
function Array::sort(%this, %compareFn) {
	if (%compareFn $= "") {
		%compareFn = compareLesser;
	}
	//From 0 -> len(list)
	qsortpart(%this, %compareFn, 0, %this.getSize());
}

//This is really ugly, but the only way to support engine methods that need a fixed number of arguments
function SimObject::_call(%this, %func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9) {
	if (%arg9 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
	if (%arg8 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8);
	if (%arg7 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7);
	if (%arg6 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6);
	if (%arg5 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5);
	if (%arg4 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3, %arg4);
	if (%arg3 !$= "") return %this.call(%func, %arg0, %arg1, %arg2, %arg3);
	if (%arg2 !$= "") return %this.call(%func, %arg0, %arg1, %arg2);
	if (%arg1 !$= "") return %this.call(%func, %arg0, %arg1);
	if (%arg0 !$= "") return %this.call(%func, %arg0);
	return %this.call(%func);
}

function funccall(%func, %obj, %i, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9) {
	//Member functions have a "%this." at the front
	if (strpos(%func, "%this.") == 0) {
		//Strip off the "%this."
		%func = getSubStr(%func, strlen("%this."), strlen(%func));
		//Call it
		return %obj._call(%func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
	} else {
		//Normal call
		return call(%func, %obj, %i, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
	}
}

//HiGuy: Call a function on each element in a list
function Array::forEach(%this, %func, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9) {
	for (%i = 0; %i < %this.size; %i ++) {
		funccall(%func, %this.val[%i], %i, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
	}
}

//HiGuy: Return a new list with the result of calling a function on each element
function Array::map(%this, %func) {
	%newList = Array();
	%newList.schedule(1000, delete); //Clean up if we don't need it
	for (%i = 0; %i < %this.size; %i ++) {
		%newList.addEntry(funccall(%func, %this.val[%i], %i));
	}
	return %newList;
}

//HiGuy: Return a new list with the entries from this list which match a given function
function Array::filter(%this, %func) {
	%newList = Array();
	%newList.schedule(1000, delete); //Clean up if we don't need it
	for (%i = 0; %i < %this.size; %i ++) {
		if (funccall(%func, %this.val[%i], %i)) {
			%newList.addEntry(%this.val[%i]);
		}
	}
	return %newList;
}

//HiGuy: Calls a function over every value in the array, but accumulated left-to-right and stored in a parameter.
function Array::reduce(%this, %func, %initial) {
	%val = %initial;
	for (%i = 0; %i < %this.size; %i ++) {
		%val = call(%func, %val, %this.val[%i], %i);
	}
	return %val;
}

//HiGuy: Stick and stuff
function Array::addArgs(%this, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg0, %arga, %argb, %argc, %argd, %arge, %argf) {
	//HiGuy: If any arg is non-blank, add all args before it
	%go = false;

	if (%argf  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%argf,  0);
	}
	if (%arge  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arge,  0);
	}
	if (%argd  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%argd,  0);
	}
	if (%argc  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%argc,  0);
	}
	if (%argb  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%argb,  0);
	}
	if (%arga  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arga,  0);
	}
	if (%arg0  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg0,  0);
	}
	if (%arg9  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg9,  0);
	}
	if (%arg8  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg8,  0);
	}
	if (%arg7  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg7,  0);
	}
	if (%arg6  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg6,  0);
	}
	if (%arg5  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg5,  0);
	}
	if (%arg4  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg4,  0);
	}
	if (%arg3  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg3,  0);
	}
	if (%arg2  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg2,  0);
	}
	if (%arg1  !$= "" || %go) {
		%go = true;
		%this.insertEntryBefore(%arg1,  0);
	}

	return %this;
}

function strRepeat(%str, %times) {
	%fin = "";
	for (%i = 0; %i < %times; %i ++)
		%fin = %fin @ %str;
	return %fin;
}
