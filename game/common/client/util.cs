function round(%n) {  
   if(%n < 0) {  
      %t = -1;  
      %n = -%n;  
   }      
   else if(%n >= 0) {  
      %t = 1;  
   }  
  
   %f = mfloor(%n);  
   %a = %n - %f;  
   if(%a < 0.5) {  
      %b = 0;  
   }  
   else if(%a >= 0.5) {  
      %b = 1;  
   }  
  
   return mfloor((%f + %b) * %t);  
}  