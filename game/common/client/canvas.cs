//-----------------------------------------------------------------------------
// Torque Shader Engine
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function to construct and initialize the default canvas window
// used by the games

function createCanvas(%windowTitle)
{
   // Create the Canvas
   %foo = new GuiCanvas(Canvas);
   
   // Set our window title
   Canvas.setWindowTitle(%windowTitle);
   
   return true;
}

function initCanvas(%windowName)
{
	// No such function or anything like it exists in the engine...
	//videoSetGammaCorrection($pref::OpenGL::gammaCorrection);
   
   
   if (isShippingBuild())
   {
      $canvasCreated = createCanvas("Marble Blast Ultra");
   } else {
      $canvasCreated = createCanvas("Marble Blast Ultra (Designer's Build)");
   }   
   
   if (!$canvasCreated) {
      quit();
      return;
   }

   // Declare default GUI Profiles.
   //exec("~/ui/defaultProfiles.cs");
   
   exec("~/ui/defaultProfiles.cs");
   
   // exec platform specific profiles
   // there are none
   //exec("~/ui/" @ $platform @ "DefaultProfiles.cs");

   // Common GUI's
   //exec("~/ui/ConsoleDlg.gui");
   exec("~/ui/LoadFileDlg.gui");
   exec("~/ui/SaveFileDlg.gui");
   exec("~/ui/MessageBoxOkDlg.gui");
   exec("~/ui/MessageBoxYesNoDlg.gui");
   exec("~/ui/MessageBoxOKCancelDlg.gui");
   exec("~/ui/MessagePopupDlg.gui");
   exec("~/ui/HelpDlg.gui");
   exec("~/ui/RecordingsDlg.gui");
   exec("~/ui/NetGraphGui.gui");
   
   // Commonly used helper scripts
   exec("./metrics.cs");
   exec("./messageBox.cs");
   exec("./screenshot.cs");
   exec("./cursor.cs");
   exec("./help.cs");
   //exec("./recordings.cs"); // use the marble blast one
   
   echo("The following console messages vary from machine to machine based on the supported pixel shader version. They originate from shader.cs");
   exec("./shaders.cs");
   exec("./materials.cs");
   
   
    if (isShippingBuild())
        exec("common/ui/ConsoleDlgHack.gui");
    else
        exec("common/ui/ConsoleDlg.gui");
   
   //exec("./postFx.cs");

   // Init the audio system
   //OpenALInit();
   sfxStartup();
   
   // Start resize checker
   $CanvasResizeSched = Canvas.schedule(1000, checkResize);
}

function GuiCanvas::checkResize(%this)
{
   $CurrentVideoMode = Canvas.getVideoMode();
   
   if ($LastVideoMode $= "")
      $LastVideoMode = $CurrentVideoMode;
   
   if ($CurrentVideoMode !$= $LastVideoMode)
   {
      onWindowResized();
   }
   
   $LastVideoMode = $CurrentVideoMode;
   
   $CanvasResizeSched = Canvas.schedule(1000, checkResize);
}

function onWindowResized()
{
	initCanvasSize();
	if (isObject(RootGui) && RootGui.isAwake())
	{
		%content = RootGui.contentGui;
		reloadAllGuis();
		glogo.visible = 0;
		RootGui.contentGui = %content;
		RootGui.redisplay();
		RootGui.redisplayContent();
	}
}

function resetCanvas()
{
   if (isObject(Canvas))
   {
      Canvas.repaint(); 
   }
}