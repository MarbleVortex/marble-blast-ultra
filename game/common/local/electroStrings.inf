﻿[Strings]
;-------------------------------------------------------------------------------
; English String Table
;
; Note, any line that begins with a comment (;) is not used and does not 
; need to be translated
;-------------------------------------------------------------------------------
$Text::Title = "Marble Blast Ultra";

;-------------------------------------------------------------------------------
; General
;-------------------------------------------------------------------------------
$Text::Loading = "Loadinando...";
$Text::Exit = "Salir";
$Text::Previous = "Atras";
$Text::Next = "Siguiente";
$Text::Retry = "Reintentar";
$Text::Back = "Atras";
$Text::More = "Mas";
$Text::Level = "Level";
$Text::Start = "Start";
$Text::PressAToStart = "Presiona Enter.";
$Text::ClickToContinue = "Click Para continuar";
$Text::Chat = "Chat";

;-------------------------------------------------------------------------------
; Live
;-------------------------------------------------------------------------------
$Text::SignIn = "Entrar";
$Text::SignedIn = "Signed In";
$Text::SignInFailed = "Sign-In Failed";
$Text::PassCodeNeeded = "Pass code needed";
$Text::NotSignedIn = "Not Signed In";
$Text::SigningIn = "Signing In";

$Text::Friends = "Amigos";
$Text::Offline = "Offline";
$Text::PlayOffline = "Jugar Offline";
$Text::ConnectionLost = "Your connection to the Xbox Live service was lost. Please sign in again.";
$Text::DupLogin = "You were signed out of the Xbox Live service because another person signed in using your account.";
$Text::ESRBWarning = "Electro536: La experiencia del juego es excelente en modo multijugador";
$Text::NewContent = "Get New Content!";

;-------------------------------------------------------------------------------
; Live Leaderboards
;-------------------------------------------------------------------------------
$Text::Leaderboard = "Leaderboard"; // used for Y button label on single player level preview gui, bug 14993
$Text::LeaderBoards = "Leaderboards";
$Text::CLeaderBoard = "Change View";
$Text::NobodyRanked = "No one is ranked on this leaderboard yet.";
$Text::YouNotRanked = "You are not yet ranked on this leaderboard.";
$Text::FriendNotRanked = "Your friends are not yet ranked on this leaderboard.";
$Text::Overall = "Overall";
$Text::MyScore = "Mi record";
$Text::MBLeaderBoard = "Marble Blast leaderboard";


;-------------------------------------------------------------------------------
; Controler
;-------------------------------------------------------------------------------
$Text::ButtonA = "A";
$Text::ButtonB = "B";
$Text::ButtonX = "X";
$Text::ButtonY = "Y";
$Text::ButtonRB = "Right Bumper"; // capitalized because it is a label on pause menu
$Text::ButtonLB = "Left Bumper"; // capitalized because it is a label on pause menu
$Text::ButtonStart = "START";
$Text::ButtonBack = "BACK";
$Text::LTrigger = "left trigger";
$Text::RTrigger = "right trigger";
$Text::LStickBtn = "left stick";
$Text::RStickBtn = "right stick";
$Text::PullToUse = "Press the left mouse button to use the %1"; // %1 will be replaced with name of powerup

;-------------------------------------------------------------------------------
; Demo Mode
;-------------------------------------------------------------------------------
$Text::PlayAllN1 = "Play all 60 levels when you unlock the full version!";
$Text::SaveAndRank = "Save your score and post to the leaderboards when you unlock the full version!";
$Text::NoSaveInDemo = "You can post your high score only if you have unlocked the full version of this game. Press <bitmap:marble/client/ui/xbox/pad_button_a.png> to continue without saving your score online.";
$Text::DemoEgg = "Get lots of great features-including easter eggs-when you unlock the full version!";
$Text::InviteBuyNow = "Unlock the full version now to play in this match!";
$Text::DemoNoRanked = "Unlock the full version to play ranked matches!";
$Text::DemoUpsellLevel = "Unlock the full version to play this level!";

$Text::DemoUpsell00 = "Unlock Marble Blast Ultra now, and...";
$Text::DemoUpsell01 = "- Take on all 60 single-player levels!\n\n";
$Text::DemoUpsell02 = "- See how your best times match up with the best in the world!\n\n";
$Text::DemoUpsell03 = "- Compete on Xbox Live in all 10 multiplayer arenas!\n\n";
$Text::DemoUpsell04 = "- Post your best scores for bragging rights on the leaderboards!\n\n";
$Text::DemoUpsell05 = "- Play for keeps in ranked matches!\n\n"
$Text::DemoUpsell06 = "- Earn achievements and find hidden easter eggs!\n\n";
$Text::DemoUpsell07 = "Unlock the Full Game";
$Text::DemoUpsell08 = "Unlock these great features:\n\n";
$Text::DemoUpsell09 = "Back to Game";
$Text::DemoUpsell10 = "Quit";

$Text::DemoUpsellMP0 = "Bienvenido a Marble Blast Ultra modo multijugador!\n\n";
$Text::DemoUpsellMP1 = "You can try out Multiplayer mode for 7 minutes each time you play the Marble Blast Ultra trial game.\n\n";
$Text::DemoUpsellMP2 = "In the trial game, you cannot invite other players to your game or join other Marble Blast Ultra games by invitation.\n\n";
$Text::DemoUpsellMP3 = "Only one Level is available in the trial, but the full game contains all 10 multiplayer levels.\n\n";
$Text::DemoUpsellMP4 = "Unlock the full version for uninterrupted and unlimited multiplayer play!";

$Text::DemoUpsellMP5 = "Time's up!  Unlock the full game now and...\n\n";
$Text::DemoUpsellMP6 = "The host ran out of time!  Unlock the full game now and...\n\n";
$Text::DemoUpsellMP7 = "You are out of time!  Exit the match to unlock the full version.";

$Text::DemoTimerLabel = "Trial Time:"; // demo timer label

$Text::PurchasedThanks = "Thank you for purchasing Marble Blast Ultra!";

;-------------------------------------------------------------------------------
; Level Time Stuff
;-------------------------------------------------------------------------------
$Text::None = "None";
$Text::BestTime = "Mi mejor tiempo:";
$Text::BestScore = "Mi mejor record:";
$Text::EndTime = "Tiempo:";
$Text::EndScore = "Rating:";
$Text::EndScore2 = "Record:";
$Text::LevelComplete = "NIVEL COMPLETO!";
$Text::NewBestTime = "Nuevo Mejor Tiempo!";
$Text::NewHighScore = "Nuevo Record!";
$Text::ParTime = "Par Time:";
$Text::Pro = "Pro";
$Text::UnknownArtist = "Unknown Artist";
$Text::Improvement = "Improvement";
$Text::MyRecord = "Mi Record";
$Text::OldRecord = "Anterior Record";
$Text::NextLevel = "Sig. Nivel";
$Text::LoadingBest = "Cargando tus mejores tiempos";
$Text::Completion = "Completion";
$Text::TimeBonus = "Tiempo extra";
$Text::ChangeLevels = "Cambiar Nivel";
$Text::Rank = "Rank";
$Text::PlayerName = "Gamertag";
$Text::Time = "Tiempo";
$Text::Points = "Puntos";
$Text::Score = "Rating";
$Text::TrueSkill = "TrueSkill(TM) Rank";
$Text::Difficulty = "Dificultad";

;-------------------------------------------------------------------------------
; Tagged Strings
;-------------------------------------------------------------------------------
$Text::Tagged::Congratulations = '\c0Congratulations! You\'ve finished!';
$Text::Tagged::NotAllGems = '\c0You can\'t finish without all the gems!';
$Text::Tagged::HaveAllGems = '\c0You have all the gems. Head for the finish!';
$Text::Tagged::OneGemLeft = '\c0You picked up a gem. Only one gem to go!';
$Text::Tagged::NGemsLeft = '\c0You picked up a gem. %1 gems to go!';

$Text::Tagged::AnEasterEgg = '\c0You picked up an Easter Egg!';
$Text::Tagged::GemPickupOnePoint = '\c0You picked up a Gem worth 1 point!';
$Text::Tagged::GemPickupNPoints = '\c0You picked up a Gem worth %1 points!';
$Text::Tagged::GameOver = '\c0Game over!';

;-------------------------------------------------------------------------------
; Non-tagged powerup strings
;-------------------------------------------------------------------------------
$Text::ASuperJump = "Ahora tienes super salto!";
$Text::ASuperSpeed = "Ahora tienes Super Velocidad!";
$Text::AGyrocopter = "Ahora tienes el Elicoptero!";
$Text::AGravityMod = "You picked up a Gravity Modifier!";
$Text::AMegaMarble = "Ahora haste enorme con la Mega-bola!";
$Text::AnAnvil = "You picked up an Anvil powerup!";
$Text::APowerCube = "You picked up a Power Cube powerup!";
$Text::ABlast = "Tienes explocion batea a tus enemigos :p!";
$Text::ATimeTravelBonus = "You picked up %1 seconds of Time Travel bonus!";

;-------------------------------------------------------------------------------
; Non-tagged game messages 
;-------------------------------------------------------------------------------
$Text::Msg::Tied = "Game tied!"; // tie
$Text::Msg::WonMP1 = "%1 won with %2 point!"; // 1 point version
$Text::Msg::WonMPN = "%1 won with %2 points!"; // 0 or 2+ point version
$Text::Msg::PlayerJoin = "%1 ha ingresado al juego";
$Text::Msg::PlayerDrop =  "%1 ha salido del juego";

;-------------------------------------------------------------------------------
; Various gui strings
;-------------------------------------------------------------------------------
$Text::SearchingForGames  = "Buscando Juegos by electro...";
$Text::LobbyRankFormat = " [%1]"; whether the game is ranked or standard is displayed in the Lobby in this format
                                ; after the level name.  %1 will be $Text::Ranked or $Text::Standard
$Text::Ranked = "Ranked Match";
$Text::Standard = "Player Match";
$Text::ModeRanked = "Type: Ranked Match";
$Text::ModeStandard = "Type: Player Match";
$Text::PoweredBy = "POWERED BY"; // In context: "POWERED BY Electro536." 

;-------------------------------------------------------------------------------
; Gui titles
;-------------------------------------------------------------------------------
$Text::MainMenu = "Menu Principal";
$Text::DifficultyMenu = "SELECIONAR DIFICULTAD";
$Text::CustomDifficultyMenu = "SELECIONAR DIFICULTAD";
$Text::SelectBeginner = "NOVATO: ";
$Text::SelectIntermediate = "INTERMEDIO: ";
$Text::SelectAdvanced = "AVANZADO: ";
$Text::LevelPreview = "SELECCIONA NIVEL";
$Text::CreateMatchTitle = "CREAR JUEGO";
$Text::Lobby = "LOBBY";
$Text::JoinGame = "UNIRSE";
$Text::LeaderBoardScreen = "CLASIFICACIONES";
$Text::AchievementsScreen = "ACHIEVEMENTS";
$Text::MultiplayerMenu = "JUEGO MULTIPLAYER";
$Text::OptimatchTitle = "JUEGO PERZONALIZADO";
$Text::AboutMenu = "ACERCA DE MARBLE BLAST";
$Text::SelectLeaderboard = "SELECCION DE LIDERES";

;-------------------------------------------------------------------------------
; Menu Options
;-------------------------------------------------------------------------------
$Text::SinglePlayer = "Jugar Solo";
$Text::Multiplayer = "Juego Multijugador";
$Text::Achievements = "Logros";
$Text::Play = "Jugar";
;$Text::XbLiveOptions = "Xbox Live Options";
;$Text::Options = "Opciones";
$Text::HelpAndOptions = "Opciones y ayuda";
$Text::UnlockFullGame = "Unlock Full Game";
$Text::RTArcadeMenu = "Salir";
$Text::LevelsUltra = "Niveles para Ultra";
$Text::LevelsGold = "Niveles para Gold";
$Text::LevelsMobile = "Niveles para Mobile";
$Text::LevelsEvolved = "Niveles de Evolved";
$Text::LevelsCustom = "Niveles Personalizados";
$Text::LevelsBeginner = "Niveles para Novatos";
$Text::LevelsIntermediate = "Niveles Intermedios";
$Text::LevelsAdvanced = "Niveles Avanzados";
$Text::SPGemHunt = "Gem Hunt";
$Text::LevelsCustomGeneralYeah = "Niveles Generales";
$Text::LevelsCustomMatt = "Niveles de Matt";
$Text::LevelsCustomElectro = "Niveles de Electro";
;I prefer the term Climate Change to Global Warming
$Text::LevelsCustomNature = "Niveles de Naturaleza";
$Text::LevelsCustomRemix = "Niveles Reciclados";
$Text::LevelsCustomTroll = "Niveles \"Troll\"";
$Text::Go = "Go";
$Text::OK = "OK";
$Text::Cancel = "Cancelar";
$Text::Refresh = "Actualizar";
$Text::Select = "Select";
;$Text::ReturningToArcade = "Regresar al menu del juego";
;$Text::PlayMarbleBlast = "Jugar Marble Blast";
$Text::QuickMatch = "Buscar Juego";
$Text::OptiMatch = "Partida Personalizada";
$Text::CreateMatch = "Crear Juego";
$Text::Players = "Jugadores";
$Text::Ready = "Listo";
$Text::ExitGame = "Salir";

;-------------------------------------------------------------------------------
; Join Game Gui
;-------------------------------------------------------------------------------
$Text::Join = "Entrar";
$Text::Create = "Crear";
$Text::JoiningGame = "Ingresando al juego";
$Text::ZoneLabel = "Xbox Live Gamer Zone";
$Text::ZoneOptions = "Any\tMine";
$Text::LanguageLabel = "Lenguaje";
$Text::LangOptions = "Any\tMine";
$Text::AnyOption = "Any";
$Text::MaxPlayersOption = "Jugadores Max."; // $Text::AnyOption, 2, 3, 4 ... 8
$Text::HiddenServerOption = "Hidden Server";
$Text::MissionOption = "Nivel"; // $Text::AnyOption, $Text::LevelNameMP1, $Text::LevelNameMP2 ...

$Text::FGG_Host = "Host";
$Text::FGG_Mission = "Level";
$Text::FGG_NumPlayers = "Num Jugadores";
$Text::FGG_Players = "Jugadores";

;-------------------------------------------------------------------------------
; Create Game Gui
;-------------------------------------------------------------------------------
$Text::Mission = "Nivel";
$Text::MaxPlayers = "Jugadores max.";
$Text::PrivateSlots = "Slots Privados";

;-------------------------------------------------------------------------------
; Leaderboard Select Menu
;-------------------------------------------------------------------------------
$Text::LBSinglePlayer = "Jugar Solo";
$Text::LBMPScrum = "Multijugador";

;-------------------------------------------------------------------------------
; Lobby stuff
;-------------------------------------------------------------------------------
$Text::LobbyHostName = "Host:"; 
$Text::LobbyHostLevel = "Level:"; 
$Text::LobbyHostPrivateSlots = "Private Slots"; 
$Text::LobbyHostPublicSlots = "Public Slots"; 

; separators
$Text::Colon  = ":";
$Text::Comma  = ",";
$Text::Slash  = "/";
$Text::Plus   = "+";
$Text::Minus  = "-";
$Text::Period  = ".";

$Text::SubmitPlayerReview = "Submit Player Review";
$Text::Kick = "Kick";
$Text::GamerCard = "Show Gamer Card";
$Text::SendFriendRequest = "Send Friend Request";
$Text::NoOptionsAvailable  = "No options available.";
$Text::SelectPlayer = "Select Player";
$Text::Player = "Player:";

;-------------------------------------------------------------------------------
; Pause Menu
;-------------------------------------------------------------------------------
$Text::UseItems = "Powerup";
$Text::Blast = "Blast";
$Text::ShowScores = "Scores";
$Text::Jump = "Salto";
$Text::Camera = "Camara";
$Text::MoveMarble = "Move Marble";
;$Text::ShowNames = "Toggle Name Visibility";
$Text::HighScores = "Mejores Records";

$Text::Paused = "Pausado :p";
$Text::Resume = "Regresar";
$Text::Restart = "Reiniciar";
$Text::MainMenuOption = "Menu Principal";
$Text::ExitLevelOption = "Salir de Nivel";
$Text::ResumePlay = "Resume Play";

;-------------------------------------------------------------------------------
; Options
;-------------------------------------------------------------------------------
$Text::GameOptions = "Opciones de juego";
$Text::Adjust = "Adjust";
$Text::Toggle = "Toggle";
$Text::MusicVol = "Music Volume";
$Text::FXVol = "Effects Volume";
$Text::InvX = "Camera X-Axis";
$Text::InvY = "Camera Y-Axis";
$Text::MouseSensitivity = "Mouse Sensitivity";
$Text::OOBSound = "OOB Sound";
$Text::SecretSound = "Secret Sound";
$Text::Fullscreen = "Fullscreen";
$Text::ShowBuildNumberDisplay = "Display Build Number";
$Text::InvOnOff = "Normal\tInverted";
$Text::On = "On";
$Text::Off = "Off";
$Text::YesNo = "No\tYes";
$Text::MarbleSkin = "Marble Type";

;This string is no longer used $Text::MarbleOptions = "one\ttwo\tthree\tfour\tfive\tsix\tseven\teight\tnine\tten\televen\ttwelve\tthirteen\tfourteen\tfifteen\tsixteen\tseventeen\teighteen\tnineteen\ttwenty";
;$Text::MarbleColor = "Marble Color";

;-------------------------------------------------------------------------------
; Object Strings
;-------------------------------------------------------------------------------
$Text::FoundNewEgg = "Has encontrado un huevoo! Yeah!";
$Text::AlreadyFoundEgg = "You've already found this egg!";

$Text::UseSuperJump = "Super Jump powerup";
$Text::UseSuperSpeed = "YA tienes cohete";
$Text::UseGyrocopter = "Gyrocopter powerup";
$Text::UseMegaMarble = "Mega-Marble powerup";
$Text::UseBlast = "Blast powerup";
$Text::UseAnvil = "an Anvil powerup";
$Text::UsePowerCube = "a Power Cube powerup";

;-------------------------------------------------------------------------------
; Multiplayer Game Modes
;-------------------------------------------------------------------------------
$Text::GameModeScrum = "Scrum";

;-------------------------------------------------------------------------------
; Multiplayer Overall leaderboard titles
;-------------------------------------------------------------------------------
$Text::LB_SPOverall = "Single Player Total"
$Text::LB_MPScrumOverall = "Multiplayer Total";
$Text::LB_MPScrumSkill = "Ranked Match Total";

;-------------------------------------------------------------------------------
; Errors
;-------------------------------------------------------------------------------
;$Text::SettingsHosed = "Ignore this false error. Press A to start game";
;$Text::SettingsHosedTitle = "Settings Damaged";
;$Text::CtrlrReconnect = "Please reconnect the controller to port %1 and press START to continue.";
;$Text::CtrlrDisconnectTitle = "Controller Disconnected";
;$Text::ReconnectAnyPort = "Please reconnect a controller to any controller port and press START to continue.";
;$Text::UploadOnlyIfSignedIn = "You can upload your high score only if you are signed in to the Xbox Live service.";
$Text::Continue = "Continue";
;$Text::UpdateRequired = "An update is required to play this game online. Please return to the Xbox Live Arcade Menu, sign in to Xbox Live, and select this game again to download the update.";
; $Text::ArbRegister = "Registering with Arbitration service"; // using $Text::TestingNetwork for this now
$Text::SignInForMultiplayer = "You must sign in to Xbox Live to play a multiplayer game.";
$Text::MPNotAllowed = "You are not allowed to play multiplayer games on Xbox Live.";
;$Text::SignInToPurchase = "You must Sign In to Live in order to unlock the full game.  Press A to Sign In or B to cancel.";
$Text::SignInLeaderboard = "You must sign in to Xbox Live to view the leaderboards.";
;$Text::SignInForSP = "Please sign in to play.";
;$Text::SignInToPlay = "Please sign in to play or press B to Return to Arcade.";
$Text::ExitLevelConfirm = "Are you sure you want to exit this level?  You will lose your current level progress.";
$Text::HostMMConfirm = "Are you sure you want to return to the Main Menu?  This will end the game for all players.";
$Text::HostMMInviteConfirm = "Are you sure you want to accept this game invite?  This will end your game for all players.";
$Text::ClientMMConfirm = "Are you sure you want to end the level and return to the Main Menu?";
$Text::RankedHostConfirm = "Are you sure you want to exit this game?  This will end the game for all players, and you may lose points by quitting early.";
$Text::RankedClientConfirm = "Are you sure you want to exit this game?  You may lose points by quitting early.";

$Text::ErrorCannotConnect   = "Cannot connect to the session.";
$Text::ErrorSessionEnded    = "The session has ended unexpectedly.";
$Text::ErrorConnectionLost  = "You have lost your connection to the session.";
$Text::ErrorHostKilled      = "El host se ha desconectado.";
$Text::ErrorSessionGone     = "The game session is no longer available.";
$Text::ErrorYouBanned       = "You are not allowed to play in this session.";
$Text::ErrorNoGames         = "No encontre ningun juego :p";
$Text::ErrorHostKickedYou   = "The host has kicked you from the game.";
$Text::ErrorBandwidthWarn   = "Network conditions may affect this game if you allow this number of players.";
$Text::ErrorClientsNotReady = "You cannot start the game until all players are ready.";
$Text::ErrorClientsWillDrop = "If you exit now you will disconnect all players.";
$Text::ErrorTooFewPlayers   = "You cannot start a ranked match with only one player.";
$Text::ErrorSignedOff       = "You have been signed out of Xbox Live.  Returning to Main Menu.";
$Text::TestingNetwork       = "Espera un Momento";
;$Text::ErrorArbFailed       = "Session ended due to failed registration with Arbitration service."; // using $Text::ErrorCannotConnect now
$Text::ErrorDemoUpgrade     = "Unlock the full version to play in this match!";
;$Text::ErrorDemoReject      = "Full version players can not play in demo sessions."; // using $Text::ErrorCannotConnect, should never happen anyway
$Text::ErrorKickSure        = "Are you sure you want to kick this player?";
$Text::ErrorNoSwitchMarble  = "You may not switch your marble while in a game.";


;-------------------------------------------------------------------------------
; Help & Options Menu Options
;-------------------------------------------------------------------------------
$Text::HOMarble = "Apariencia de la canica";
$Text::HOOptions = "Input and Sound Options";
$Text::HORemapOptions = "Control Options";
$Text::HOVideoOptions = "Video Options";
$Text::HOChangeUserKey = "Change User Key";
$Text::HOMultiplayerSetupHowto = "How To Set Up Multiplayer";
$Text::HOInstructions = "Como Jugar";
$Text::HOCredits = "Creditos";

;-------------------------------------------------------------------------------
; About Menu Options
;-------------------------------------------------------------------------------
$Text::AboutBasics = "Marble Controls";
$Text::AboutPowerups = "Powerups";
$Text::AboutBlast = "Blast Meter";
$Text::AboutSP = "Single Player Mode";
$Text::AboutMP = "Multiplayer Mode";

;-------------------------------------------------------------------------------
; About Page
;-------------------------------------------------------------------------------
$Text::HOTitle = "Ayuda & Opciones";
$Text::About0Title = "APRIENCIA DE LA CANICA";
$Text::About1Title = "INPUT & SOUND OPTIONS";
$Text::About1aTitle = "VIDEO OPTIONS";
$Text::About1bTitle = "CHANGE KEY";
$Text::About2Title = "COMO JUGAR!";
$Text::About3Title = "CONTROLES DE LA CANICA :P";
$Text::About3aTitle = "POWERUPS";
$Text::About3bTitle = "BLAST METER";
$Text::About4Title = "MODO UN JUGADOR";
$Text::About5Title = "MODO MULTIJUGADOR";
$Text::About6Title = "THE MARBLE BLAST TEAM";

;About Marble Blast Basics
$Text::About1 = "Usa A S D W para mover la bola. Presiona barra espaciadora para saltar.";

;About Powerups
$Text::About2 = "There are many powerups to use. ultra blast, mega marble, gyrocopter, super speed, super jump, time travel.";
$Text::About3 = "Press left button on mouse to activate";

;About Blast
$Text::About4 = "Ultra blast is an power that can be used for combat or for jump asistant.";
$Text::About5 = "The blast meter show you when is ultra blast ready to use.";
$Text::About6 = "Press right button on mouse to use it. Ultra blast power up will charge blast meter and its soo much powerfull to blast players and so much powerfull to make little super jump";

;About Single Player Mode
$Text::AboutSP1 = "Single player is that you need to collect gems or race to finish pad";
$Text::AboutSP2 = "in single player you dont have oponets. And you can fight with time to be fast!";

;About Multiplayer
$Text::AboutMP1 = "Multiplayer (aka: Gem hunt) is an match were you need collect gems to get points. blue:+5 Green:+2 red:+1";
$Text::AboutMP2 = "player with most points will win the round!";

; Credits
$Text::MBCredits = "\n<spush><font:Arial Bold:%1>Marble Blast Ultra [PC]<spop>\n<spush><font:Arial Bold:%2>Version ";
$Text::GGCredits = "<spush><font:Arial Bold:%1>Creditos<spop>\n\n<spush><font:Arial Bold:%2>JUEGO CONVERTIDO A PC POR: David184/roblox184<spop>\n\n<spush>Beta Testers: Claudio,TheCZGamers,Davidrohusch,David184.<spop>\n\n<spush>ESPECIAL AGRADECIMIENTO A Xd3ATHp0nyX POR LA AYUDA ;(";
$Text::MSCredits = "Big thanks to: Claudio and TheCZGamers TRADUCCION AL ESPAÑOL POR ELECTRO536"; 
$Text::LocCredits = "<spush><font:Arial Bold:%1>Localization<spop>\n\n<spush><font:Arial Bold:%2>Japan Localization Team<spop>\nShinya Muto\nJunya Chiba\nGo Komatsu\nMayumi Koike\nTakehiro Kuga\nMasao Okamoto\nYutaka Hasegawa\nMunetaka Fuse\nTakashi Sasaki\nShinji Komiyama\n\n<spush><font:Arial Bold:%2>Korea Localization Team<spop>\nEun Hee Lee\nIn Goo Kwon\nWhi Young Yoon\nJi Young Kim\n\n<spush><font:Arial Bold:%2>Taiwan Localization Team<spop>\nRobert Lin\nCarole Lin\nLilia Lee\nJason Cheng";
$Text::PCCredits = "<spush><font:Arial Bold:%1>PC Port<spop>\n\n<spush><font:Arial Bold:%2>Team Members<spop>\nMatthieu Parizeau\nAnthony Kleine\nErik Martin\nJames Rudolph\nDavid Orel\nDavid Luo\n\n<spush><font:Arial Bold:%2>Special Thanks<spop>\nDaniel Hernandez\nHiGuy and Jeff\nWhirligig231\nRoziGames and Ender\nLee Zi Xian\nFairytale5353\nComputer Clan";
$Text::SpeechCredits = "We\'re all in this\nfor the same reason.\nWe love Marble Blast.\nSome people demonstrate\ntheir love for Marble Blast\nby giving their suggestions.\nSome people demonstrate\ntheir love for Marble Blast\nby making custom levels or mods.\nSome people demonstrate\ntheir love for Marble Blast\nby simply playing the game.\nWe\'re all in this\nfor the same reason though.\nIt\'s because we love Marble Blast,\nand by extension, we love GarageGames.\nThis PC Port is how we demonstrate\nour love for Marble Blast\nand is our tribute to GarageGames.\nThank you so much\nfor giving us motivation\nto do what we love to do.\n\n<spush><font:Arial Bold:%2>Sincerely,<spop>\n<spush><font:Arial Bold:%1>-The Marble Blast Ultra PC Port Team<spop>";

;-------------------------------------------------------------------------------
; Achievement Strings
;-------------------------------------------------------------------------------
;$Text::SPSignin = "Please Sign In so that your game is saved.  If you sign in to Live you can store your High Scores online.";
;$Text::SelectSaveDevice = "Please select a device to store your Achievements.  Press A to select or B to cancel.";

$Text::AchievementName01 = "Timely Marble";
$Text::AchievementName02 = "Apprentice's Badge";
$Text::AchievementName03 = "Journeyman's Badge";
$Text::AchievementName04 = "Adept's Badge";
$Text::AchievementName05 = "Marble-fu Initiate";
$Text::AchievementName06 = "Marble-fu Master";
$Text::AchievementName07 = "Marble-fu Transcendent";
$Text::AchievementName08 = "Egg Seeker";
$Text::AchievementName09 = "Egg Basket";
$Text::AchievementName10 = "First Place";
$Text::AchievementName11 = "Gem Collector";
$Text::AchievementName12 = "Veteran Battler";

$Text::AchievementDescription01 = "Finish any level under par time.";
$Text::AchievementDescription02 = "Complete all Beginner levels.";
$Text::AchievementDescription03 = "Complete all Intermediate levels.";
$Text::AchievementDescription04 = "Complete all Advanced levels.";
$Text::AchievementDescription05 = "Finish all Beginner levels under par time.";
$Text::AchievementDescription06 = "Finish all Intermediate levels under par time.";
$Text::AchievementDescription07 = "Finish all Advanced levels under par time.";
$Text::AchievementDescription08 = "Find any hidden easter egg.";
$Text::AchievementDescription09 = "Find all twenty easter eggs.";
$Text::AchievementDescription10 = "Get first place in a multiplayer match.";
$Text::AchievementDescription11 = "Get 75 points in a multiplayer match.";
$Text::AchievementDescription12 = "Collect 2,000 total points in multiplayer.";

;-------------------------------------------------------------------------------
; Singleplayer Game Strings
;-------------------------------------------------------------------------------

$Text::InGameReady = "Ready";
$Text::InGameGo = "Go!";
$Text::InGameOutofBounds = "Out of Bounds";

;-------------------------------------------------------------------------------
; Level Strings
;-------------------------------------------------------------------------------
$Text::LevelName1 = "Learning to Roll";
$Text::LevelStartHelp1 = "Bienvenido a Marble Blast Ultra!\nUsa teclas W, A, S, D Para mover la canica :p";
$Text::TriggerText1_0 = "Mueve el mause para cambiar la perspectiva de la camara";
$Text::TriggerText1_1 = "Presiona lbarra espaciadora para saltar!";
$Text::TriggerText1_2 = "Be careful not to fall off the ledge!";
$Text::TriggerText1_3 = "Roll to the finish pad to complete the level!";
$Text::LevelName2 = "Moving Up";
$Text::LevelStartHelp2 = "The Super Jump powerup can be used to vault taller barriers.";
$Text::TriggerText2_0 = "Use the Super Speed powerup to move quickly, especially up steep slopes!";
$Text::TriggerText2_1 = "Ride the elevator to reach the uppermost platform.";
$Text::LevelName3 = "Gem Collection";
$Text::LevelStartHelp3 = "You must collect the hidden gems before you may finish.";
$Text::TriggerText3_0 = "Time Travel powerups stop the clock, letting you get faster times!";
$Text::TriggerText3_1 = "Time Travel powerups stop the clock, letting you get faster times!";

$Text::LevelName4 = "Frictional Concerns";
$Text::LevelStartHelp4 = "Not all surfaces are the same!";
$Text::TriggerText4_0 = "Rubber mats give you better traction.\nUse them to scale difficult angles!";
$Text::TriggerText4_1 = "Ice is slippery!\nApproach it with caution.";

$Text::LevelName5 = "Triple Gravity";
$Text::LevelStartHelp5 = "Touch Gravity Modifiers to change which way is down!";
$Text::TriggerText5_0 = "Collect one gem from each of the three sides.";

$Text::LevelName6 = "Bridge Crossing";
$Text::LevelStartHelp6 = "Be careful not to fall off open edges!";
$Text::TriggerText6_0 = "Watch your step on this narrow bridge!\nIf you fall off you will lose time!";
$Text::TriggerText6_1 = "Moving platforms can bring you to hard-to-reach places.";

$Text::LevelName7 = "Bunny Slope";
$Text::LevelStartHelp7 = "Build up momentum to reach the bottom quickly!";

$Text::LevelName8 = "First Flight";
$Text::LevelStartHelp8 = "Use the Gyrocopter powerup to float across the gap safely.";

$Text::LevelName9 = "Hazardous Climb";
$Text::LevelStartHelp9 = "Don't let these hazards throw you off the track to the finish!";

$Text::LevelName10 = "Marble Melee Primer";
$Text::LevelStartHelp10 = "Press the right mouse button or 'E' to use the Blast power when your Blast Meter is lit.";
$Text::TriggerText10_0 = "A smaller marble would fall through the gaps...";
$Text::TriggerText10_1 = "The blast can be used in the air to jump higher.  It also shoves other marbles away in Multiplayer games.";
$Text::TriggerText10_2 = "The Ultra Blast powerup refills your blast meter instantly - and then some!";
$Text::TriggerText10_3 = "The Mega Marble powerup makes your marble enormous!";
$Text::TriggerText10_4 = "In a multiplayer game, the goal is to collect more gems than the other players.";
$Text::LevelName36 = "Hop Skip and a Jump";
$Text::LevelName39 = "Half-Pipe";
$Text::LevelStartHelp39 = "Speed around the half-pipe and collect the gems!";
$Text::LevelName45 = "Great Divide";
$Text::LevelStartHelp45 = "Climb over the pass and speed down the other side!";
$Text::LevelName40 = "Gauntlet";
$Text::LevelStartHelp40 = "Don't get clobbered!";
$Text::LevelName37 = "Fork in the Road";
$Text::LevelStartHelp37 = "See how quickly you can find all the gems!";
$Text::LevelName106 = "Endurance";
$Text::LevelStartHelp106 = "Don't fall behind the platform - there's no going back!";
$Text::LevelName104 = "Divergence";
$Text::LevelName105 = "Cube Root";
$Text::LevelName103 = "Black Diamond";
$Text::LevelName102 = "Timely Ascent";
$Text::LevelName107 = "Urban Jungle";
$Text::LevelStartHelp107 = "Welcome to the jungle!";
$Text::LevelName48 = "Upward Spiral";
$Text::LevelStartHelp48 = "Climb upward to reach the goal!";
$Text::LevelName28 = "Ramp Matrix";
$Text::LevelStartHelp28 = "Find 10 gems!";
$Text::LevelName108 = "Mountaintop Retreat";
$Text::LevelName101 = "The Road Less Traveled";
$Text::LevelName27 = "Skate Park";
$Text::LevelStartHelp27 = "Show off some of your crazy moves!";
$Text::LevelName35 = "Aim High";
$Text::LevelStartHelp35 = "Go for the top hole!";
$Text::LevelName34 = "Early Frost";
$Text::LevelStartHelp34 = "Caution: Ice Ahead!";
$Text::LevelName109 = "Skate to the Top";
$Text::LevelName72 = "Survival of the Fittest";
$Text::LevelStartHelp72 = "Stay on the platform to survive!";
$Text::LevelName77 = "Skyscraper";
$Text::LevelStartHelp77 = "Find all the gems on your way to the top!";
$Text::LevelName50 = "Tree House";
$Text::LevelName58 = "Natural Selection";
$Text::LevelStartHelp58 = "Don't fall behind!";
$Text::LevelName80 = "Whirl";
$Text::LevelName70 = "Will o\' Wisp";
$Text::LevelName55 = "Obstacle Course";
$Text::LevelName81 = "Sledding";
$Text::LevelStartHelp81 = "Jumping in slippery areas can help your navigation.";
$Text::LevelName86 = "Duality";
$Text::LevelStartHelp86 = "Which path is the quickest?";
$Text::LevelName100 = "King of the Mountain";
$Text::LevelStartHelp100 = "Can you face the mountain?";
$Text::LevelName53 = "Three-Fold Race";
$Text::LevelStartHelp53 = "Look both ways!";
$Text::LevelName73 = "Spelunking";
$Text::LevelStartHelp73 = "Recolecta todos los diamantes antes de que te los ganen :p";
$Text::TriggerText73_0 = "Use the super-jump to hop back out of the caves!";
$Text::LevelName98 = "Schadenfreude";
$Text::LevelStartHelp98 = "Pay careful attention to the pattern of the bumpers.";
$Text::LevelName57 = "Three-Fold Maze";
$Text::LevelName76 = "Ramps Reloaded";
$Text::LevelStartHelp76 = "Be careful when crossing between platforms.";
$Text::LevelName79 = "Acrobat";
$Text::LevelStartHelp79 = "Control your spin when you are about to land.";
$Text::LevelName83 = "Scaffold";
$Text::LevelStartHelp83 = "Keep moving to avoid falling through the trap doors!";
$Text::LevelName59 = "Slick Slide";
$Text::LevelStartHelp59 = "Watch out for bumpers!";
$Text::LevelName75 = "Extreme Skiing";
$Text::LevelStartHelp75 = "Beginning skiers are advised to use caution.";
$Text::LevelName88 = "Ordeal";
$Text::LevelStartHelp88 = "Strength, speed and stealth are the keys to the trials ahead!";
$Text::LevelName61 = "Half Pipe Elite";
$Text::LevelName87 = "Daedalus";
$Text::LevelName97 = "Under Construction";
$Text::LevelStartHelp97 = "Be very cautious on this framework.";
$Text::LevelName56 = "Points of the Compass";
$Text::LevelName89 = "Battlements";
$Text::LevelStartHelp89 = "Have fun storming the castle!";
$Text::LevelName69 = "Around the World";
$Text::LevelName21 = "Pitfalls";
$Text::LevelStartHelp21 = "Practice your rolling skills by avoiding the gaps in the floor!";
$Text::LevelName12 = "Gravity Helix";
$Text::LevelStartHelp12 = "Pick up the Gravity Modifiers to roll to the finish!";
$Text::LevelName23 = "Winding Road";
$Text::LevelStartHelp23 = "Follow the winding road, using the powerups to cross the gaps!";
$Text::LevelName22 = "Platform Party";
$Text::LevelStartHelp22 = "Ride the moving platforms to reach the finish!";
$Text::LevelName82 = "Jump Jump Jump!";
$Text::LevelName110 = "Laberinto Infinito :S";
$Text::LevelName111 = "Point Soft MBU";
$Text::LevelName112 = "The Great Divide";

;-------------------------------------------------------------------------------
; Multiplayer Level Strings
;-------------------------------------------------------------------------------

$Text::LevelNameMP1 = "All Angles";
$Text::LevelNameMP2 = "Battle Cube";
$Text::LevelNameMP3 = "Sprawl";
$Text::LevelNameMP4 = "Skate Battle Royale";
$Text::LevelNameMP5 = "Zenith";
$Text::LevelNameMP6 = "King of the Marble";
$Text::LevelNameMP7 = "Marble City";
$Text::LevelNameMP8 = "Horizon";
$Text::LevelNameMP9 = "Gems in the Road";
$Text::LevelNameMP10 = "Epicenter";
$Text::LevelNameMP11 = "Marble It Up!";
$Text::LevelNameMP12 = "Playground";
$Text::LevelNameMP13 = "Bowl";
$Text::LevelNameMP14 = "Concentric";
$Text::LevelNameMP15 = "Vortex Effect";
$Text::LevelNameMP16 = "Blast Club";
$Text::LevelNameMP17 = "Core";
$Text::LevelNameMP18 = "Triumvirate";
$Text::LevelNameMP19 = "Ziggurat";
$Text::LevelNameMP20 = "Promontory";
$Text::LevelNameMP21 = "Spires";
$Text::LevelNameMP22 = "Marbletown";
$Text::LevelNameMP23 = "Ramps Trampose";
$Text::LevelNameMP24 = "ProPhetic";
$Text::LevelNameMP25 = "Nicks Spraw!";
$Text::LevelNameMP26 = "Sprawl Evolved";
$Text::LevelNameMP27 = "Greeds Map";
$Text::LevelNameMP28 = "BlockParty";
$Text::LevelNameMP29 = "Mario Kart :p";
$Text::LevelNameMP30 = "Bunker";
$Text::LevelNameMP31 = "Pinnacle";
$Text::LevelNameMP32 = "GoodToBeKing";
$Text::LevelNameMP33 = "Glacier";
$Text::LevelNameMP34 = "Bunker";

;-------------------------------------------------------------------------------
; Additional Help Strings
;-------------------------------------------------------------------------------

$Text::TriggerTextFirstSuperJump = "Use the Super Jump powerup to leap across the gap.";
$Text::TriggerTextFirstGyrocopter = "Use the Gyrocopter powerup to float across the gap safely.";
$Text::TriggerTextFirstMines = "Beware of mines!";
$Text::TriggerTextIce = "Be careful--the ice is slippery!";
$Text::TriggerTextRubber = "The rubber-coated surface gives you more traction!";
$Text::TriggerTextTornado = "Beware of the tornado's pull!";
$Text::TriggerTextFans = "Beware of the wind from the fans.";
$Text::TriggerTextBumpers = "Bumpers push back when you collide with them.";
$Text::TriggerTextAirMove = "You can change the direction of your spin even while in the air.";
$Text::MultiplayerStartup = "Recolecta todos los diamantes para ganar :p!";
