    struct VertData  
    {  
       float4 texCoord        : TEXCOORD0;  
       float3 normal          : NORMAL;  
       float4 position        : POSITION;  
    };  
      
      
    struct ConnectData  
    {  
       float4 hpos            : POSITION;  
       float2 TexCoord        : TEXCOORD0;  
       float3 Eye             : TEXCOORD1;  
       float4 Position        : TEXCOORD2;  
    };  
      
    //-----------------------------------------------------------------------------  
    // Main                                                                          
    //-----------------------------------------------------------------------------  
    ConnectData main( VertData IN,  
                      uniform float4x4 modelview       : register(C0),  
                      uniform float3   eyePos          : register(C20)  
    )  
    {  
       ConnectData OUT;  
      
       OUT.hpos = mul(modelview, IN.position);  
       OUT.TexCoord = IN.texCoord;  
       float3 tangent = float3(0,1,0);  
       float3 binormal = cross(tangent,IN.normal);  
       float3x3 TBNMatrix = float3x3(tangent,binormal,IN.normal);   
       float3 eyeDirO = -(eyePos-IN.position) ; //eye vector in object space  
       OUT.Eye = normalize(mul(TBNMatrix,eyeDirO));  
       OUT.Position = OUT.hpos;  
      
       return OUT;  
    }  