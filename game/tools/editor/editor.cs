//-----------------------------------------------------------------------------
// Torque Game Engine 
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Hard coded images referenced from C++ code
//------------------------------------------------------------------------------

//   editor/SelectHandle.png
//   editor/DefaultHandle.png
//   editor/LockedHandle.png


//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Mission Editor 
//------------------------------------------------------------------------------

function Editor::create()
{
   // Not much to do here, build it and they will come...
   // Only one thing... the editor is a gui control which
   // expect the Canvas to exist, so it must be constructed
   // before the editor.
   new EditManager(Editor)
   {
      profile = "GuiContentProfile";
      horizSizing = "right";
      vertSizing = "top";
      position = "0 0";
      extent = "640 480";
      minExtent = "8 8";
      visible = "1";
      setFirstResponder = "0";
      modal = "1";
      helpTag = "0";
      open = false;
   };
}


function Editor::onAdd(%this)
{
   // Basic stuff
   exec("./cursors.cs");

   // Tools
   exec("./editor.bind.cs");
   exec("./ObjectBuilderGui.gui");

   // New World Editor
   exec("./EditorGui.gui");
   exec("./EditorGui.cs");

   // World Editor
   exec("./WorldEditorSettingsDlg.gui");

   // Terrain Editor
   exec("./TerrainEditorVSettingsGui.gui");

   // Ignore Replicated fxStatic Instances.
   EWorldEditor.ignoreObjClass("fxShapeReplicatedStatic");

   // do gui initialization...
   EditorGui.init();

   //
   exec("./editorRender.cs");
}

function Editor::checkActiveLoadDone()
{
   if(isObject(EditorGui) && EditorGui.loadingMission)
   {
      Canvas.setContent(EditorGui);
      EditorGui.loadingMission = false;
      return true;
   }
   return false;
}

//------------------------------------------------------------------------------
function toggleEditor(%make)
{
   if (!$Client::connectedMultiplayer){
	   if (%make && $testcheats)
	   {
		  // do not abuse the editor to get awesome impossible times. Soon as it's activated, bye bye times.
	      $useLB = 0;
		  //if (!$Game::Running) 
		  //{
			// // just in case ...
			// disconnect();
			// Editor.close();
		  //}
		  //else
		  //{
	      if (isShippingBuild())
         {
            XMessagePopupDlg.show(0, "The Mission Editor is buggy in this build! Please do not use the Shipping Build to edit the missions.", $Text::OK);
			// do not uncomment this next line if you do not know what you're doing!
			// in our tests it may cause crashes, system instabilities or even BSoDs if you are not using a properly configured AMD/ATI Radeon graphics card. It performs especially poorly on Intel Pentium graphics cards.
			// you may wish to use the included Designer's Build instead.
			// if you know what you're doing, feel free to uncomment this but don't say we didn't tell you!
            //XMessagePopupDlg.show(0, "The Mission Editor is buggy in this build! Please do not use the Shipping Build to edit the missions.", "Just Do It Nike", "if($Game::Running){if(!isObject(Editor)){Editor::create();MissionCleanup.add(Editor);}if(Canvas.getContent()==EditorGui.getId()){$infinitetime=1;toggleCamera(1);Editor.close(\"PlayGui\");Canvas.setContent(RootGui);RootGui.setContent(PlayGui);moveMap.push();}else{$infinitetime = 1;toggleCamera(1);Editor.open();}}", $Text::Cancel, "");
         } else {
		  if ($Game::Running){
				 if (!isObject(Editor))
				 {
					Editor::create();
					MissionCleanup.add(Editor);
				 }
				 
				 if (Canvas.getContent() == EditorGui.getId())
				 {
					   //$infinitetime = 0;
					   // no clean way to find out if the camera is free, so just toggle it. It's most likely not in use.
					   //toggleCamera(1);
					   Editor.close("PlayGui");
					   Canvas.setContent(RootGui);
					   RootGui.setContent(PlayGui);
					   moveMap.push();
				 } else 
				 {
					//$infinitetime = 1;
					//toggleCamera(1);
					Editor.open();
					//moveMap.pop();
				 }
			 }
		 }
		  //}
	   }
   }
}

function startEditor()
{
	RootGui.show();
	RootGui.setContent(MissionLoadingGui);
	
	//buildMissionList();	
	
	setSinglePlayerMode(false);
	
	createServer("SinglePlayer", "marble/data/missions/beginner/Level One/levelone.mis");
	
	connectToServer("");
	
	waitForEditor();
	
   //createPreviewServer($pref::Client::AutoStartMission);
   //waitForPreviewLevel();
	
	
	//RootGui.show();
	// RootGui.setContent(MissionLoadingGui);
	
	// setSinglePlayerMode(false);
         
   // // Create a new server
   // createServer("SinglePlayer", "marble/data/missions/beginner/Level One/levelone.mis");
	// commandToServer('JoinGame');
}

function waitForEditor()
{
	cancel($Client::EditorWaitSched);
	
	if (ServerConnection.getControlObject() == 0)
	{
		$Client::EditorWaitSched = schedule(100,0,waitForEditor);
		return;
	}
	
	if (!isObject(Editor))
   {
      Editor::create();
      MissionCleanup.add(Editor);
   }
   
   Editor.open();
}

function testMission(%val)
{
   if (%val)
   {
      if (Canvas.getContent() == EditorGui.getId())
      {
         //GameMissionInfo.setCurrentMission(MissionInfo.file);
         //$Server::MissionFile = MissionInfo.file;
         $Game::Running = false;
         $Game::SPGemHunt = false;
         if (MissionInfo.gameType $= "MultiPlayer" || MissionInfo.gameMode $= "Scrum")
            $Game::SPGemHunt = true;
            
         $Game::ParTime = MissionInfo.time;
         $Game::ParScore = MissionInfo.parScore;
         $Game::GoldTime = MissionInfo.goldTime;
         $Game::Difficulty = MissionInfo.difficulty;
         
         onMissionLoaded();
         //$Game::GemCount = countGems(MissionGroup);
         //commandToClient(ClientGroup.getObject(0), 'setGemCount', 0, MissionInfo.maxGems);
         
         Editor.close("PlayGui");
         Canvas.setContent(RootGui);
         RootGui.setContent(PlayGui);
      
         commandToServer('JoinGame');
      }
   }
}

function EditorFinishTest()
{
   commandToServer('SetWaitState');
   
   Editor.open();
}

function State::isClient()
{
   return true;
}

function Game::checkOwnership()
{
   return true;
}

function openDevMenu(%val)
{
   if (%val && $testcheats && !isShippingBuild())
   {
      if (Canvas.getContent() == EditorGui.getId())
      {
         toggleEditor(1);
      }
      
      Canvas.pushDialog(DevMenuGui);
   }
}

//------------------------------------------------------------------------------
//  The editor action maps are defined in editor.bind.cs
GlobalActionMap.bind(keyboard, "f11", toggleEditor);
//GlobalActionMap.bind(keyboard, "f11", openDevMenu);
//GlobalActionMap.bind(keyboard, "f11", testMission);
