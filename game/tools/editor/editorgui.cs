//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------

function EditorGui::getPrefs()
{
   EWorldEditor.dropType = getPrefSetting($Pref::WorldEditor::dropType, "atCamera");

   // same defaults as WorldEditor ctor
   EWorldEditor.planarMovement = getPrefSetting($pref::WorldEditor::planarMovement, true);
   EWorldEditor.undoLimit = getPrefSetting($pref::WorldEditor::undoLimit, 40);
   EWorldEditor.dropType = getPrefSetting($pref::WorldEditor::dropType, "screenCenter");
   EWorldEditor.projectDistance = getPrefSetting($pref::WorldEditor::projectDistance, 2000);
   EWorldEditor.boundingBoxCollision = getPrefSetting($pref::WorldEditor::boundingBoxCollision, true);
   EWorldEditor.renderPlane = getPrefSetting($pref::WorldEditor::renderPlane, true);
   EWorldEditor.renderPlaneHashes = getPrefSetting($pref::WorldEditor::renderPlaneHashes, true);
   EWorldEditor.gridColor = getPrefSetting($pref::WorldEditor::gridColor, "255 255 255 20");
   EWorldEditor.planeDim = getPrefSetting($pref::WorldEditor::planeDim, 500);
   EWorldEditor.gridSize = getPrefSetting($pref::WorldEditor::gridSize, "10 10 10");
   EWorldEditor.renderPopupBackground = getPrefSetting($pref::WorldEditor::renderPopupBackground, true);
   EWorldEditor.popupBackgroundColor = getPrefSetting($pref::WorldEditor::popupBackgroundColor, "100 100 100");
   EWorldEditor.popupTextColor = getPrefSetting($pref::WorldEditor::popupTextColor, "255 255 0");
   EWorldEditor.selectHandle = getPrefSetting($pref::WorldEditor::selectHandle, "gui/Editor_SelectHandle.png");
   EWorldEditor.defaultHandle = getPrefSetting($pref::WorldEditor::defaultHandle, "gui/Editor_DefaultHandle.png");
   EWorldEditor.lockedHandle = getPrefSetting($pref::WorldEditor::lockedHandle, "gui/Editor_LockedHandle.png");
   EWorldEditor.objectTextColor = getPrefSetting($pref::WorldEditor::objectTextColor, "255 255 255");
   EWorldEditor.objectsUseBoxCenter = getPrefSetting($pref::WorldEditor::objectsUseBoxCenter, true);
   EWorldEditor.axisGizmoMaxScreenLen = getPrefSetting($pref::WorldEditor::axisGizmoMaxScreenLen, 200);
   EWorldEditor.axisGizmoActive = getPrefSetting($pref::WorldEditor::axisGizmoActive, true);
   EWorldEditor.mouseMoveScale = getPrefSetting($pref::WorldEditor::mouseMoveScale, 0.2);
   EWorldEditor.mouseRotateScale = getPrefSetting($pref::WorldEditor::mouseRotateScale, 0.01);
   EWorldEditor.mouseScaleScale = getPrefSetting($pref::WorldEditor::mouseScaleScale, 0.01);
   EWorldEditor.minScaleFactor = getPrefSetting($pref::WorldEditor::minScaleFactor, 0.1);
   EWorldEditor.maxScaleFactor = getPrefSetting($pref::WorldEditor::maxScaleFactor, 4000);
   EWorldEditor.objSelectColor = getPrefSetting($pref::WorldEditor::objSelectColor, "255 0 0");
   EWorldEditor.objMouseOverSelectColor = getPrefSetting($pref::WorldEditor::objMouseOverSelectColor, "0 0 255");
   EWorldEditor.objMouseOverColor = getPrefSetting($pref::WorldEditor::objMouseOverColor, "0 255 0");
   EWorldEditor.showMousePopupInfo = getPrefSetting($pref::WorldEditor::showMousePopupInfo, true);
   EWorldEditor.dragRectColor = getPrefSetting($pref::WorldEditor::dragRectColor, "255 255 0");
   EWorldEditor.renderObjText = getPrefSetting($pref::WorldEditor::renderObjText, true);
   EWorldEditor.renderObjHandle = getPrefSetting($pref::WorldEditor::renderObjHandle, true);
   EWorldEditor.faceSelectColor = getPrefSetting($pref::WorldEditor::faceSelectColor, "0 0 100 100");
   EWorldEditor.renderSelectionBox = getPrefSetting($pref::WorldEditor::renderSelectionBox, true);
   EWorldEditor.selectionBoxColor = getPrefSetting($pref::WorldEditor::selectionBoxColor, "255 255 0");
   EWorldEditor.snapToGrid = getPrefSetting($pref::WorldEditor::snapToGrid, false);
   EWorldEditor.snapRotations = getPrefSetting($pref::WorldEditor::snapRotations, false);
   EWorldEditor.rotationSnap = getPrefSetting($pref::WorldEditor::rotationSnap, "15");

   ETerrainEditor.softSelecting = 1;
   ETerrainEditor.currentAction = "raiseHeight";
   ETerrainEditor.currentMode = "select";
}

function EditorGui::setPrefs()
{
   $Pref::WorldEditor::dropType = EWorldEditor.dropType;
   $pref::WorldEditor::planarMovement = EWorldEditor.planarMovement;
   $pref::WorldEditor::undoLimit = EWorldEditor.undoLimit;
   $pref::WorldEditor::dropType = EWorldEditor.dropType;
   $pref::WorldEditor::projectDistance = EWorldEditor.projectDistance;
   $pref::WorldEditor::boundingBoxCollision = EWorldEditor.boundingBoxCollision;
   $pref::WorldEditor::renderPlane = EWorldEditor.renderPlane;
   $pref::WorldEditor::renderPlaneHashes = EWorldEditor.renderPlaneHashes;
   $pref::WorldEditor::gridColor = EWorldEditor.GridColor;
   $pref::WorldEditor::planeDim = EWorldEditor.planeDim;
   $pref::WorldEditor::gridSize = EWorldEditor.GridSize;
   $pref::WorldEditor::renderPopupBackground = EWorldEditor.renderPopupBackground;
   $pref::WorldEditor::popupBackgroundColor = EWorldEditor.PopupBackgroundColor;
   $pref::WorldEditor::popupTextColor = EWorldEditor.PopupTextColor;
   $pref::WorldEditor::selectHandle = EWorldEditor.selectHandle;
   $pref::WorldEditor::defaultHandle = EWorldEditor.defaultHandle;
   $pref::WorldEditor::lockedHandle = EWorldEditor.lockedHandle;
   $pref::WorldEditor::objectTextColor = EWorldEditor.ObjectTextColor;
   $pref::WorldEditor::objectsUseBoxCenter = EWorldEditor.objectsUseBoxCenter;
   $pref::WorldEditor::axisGizmoMaxScreenLen = EWorldEditor.axisGizmoMaxScreenLen;
   $pref::WorldEditor::axisGizmoActive = EWorldEditor.axisGizmoActive;
   $pref::WorldEditor::mouseMoveScale = EWorldEditor.mouseMoveScale;
   $pref::WorldEditor::mouseRotateScale = EWorldEditor.mouseRotateScale;
   $pref::WorldEditor::mouseScaleScale = EWorldEditor.mouseScaleScale;
   $pref::WorldEditor::minScaleFactor = EWorldEditor.minScaleFactor;
   $pref::WorldEditor::maxScaleFactor = EWorldEditor.maxScaleFactor;
   $pref::WorldEditor::objSelectColor = EWorldEditor.objSelectColor;
   $pref::WorldEditor::objMouseOverSelectColor = EWorldEditor.objMouseOverSelectColor;
   $pref::WorldEditor::objMouseOverColor = EWorldEditor.objMouseOverColor;
   $pref::WorldEditor::showMousePopupInfo = EWorldEditor.showMousePopupInfo;
   $pref::WorldEditor::dragRectColor = EWorldEditor.dragRectColor;
   $pref::WorldEditor::renderObjText = EWorldEditor.renderObjText;
   $pref::WorldEditor::renderObjHandle = EWorldEditor.renderObjHandle;
   $pref::WorldEditor::raceSelectColor = EWorldEditor.faceSelectColor;
   $pref::WorldEditor::renderSelectionBox = EWorldEditor.renderSelectionBox;
   $pref::WorldEditor::selectionBoxColor = EWorldEditor.selectionBoxColor;
   $pref::WorldEditor::snapToGrid = EWorldEditor.snapToGrid;
   $pref::WorldEditor::snapRotations = EWorldEditor.snapRotations;
   $pref::WorldEditor::rotationSnap = EWorldEditor.rotationSnap;

}

function EditorGui::onSleep(%this)
{
   %this.setPrefs();
}

function EditorGui::init(%this)
{
   %this.getPrefs();
   $SelectedOperation = -1;
   $NextOperationId   = 1;
   $HeightfieldDirtyRow = -1;

   EditorMenuBar.clearMenus();
   EditorMenuBar.addMenu("File", 0);
   EditorMenuBar.addMenuItem("File", "New Mission...", 1);
   EditorMenuBar.addMenuItem("File", "Open Mission...", 2, "Ctrl O");
   EditorMenuBar.addMenuItem("File", "Save Mission...", 3, "Ctrl S");
   EditorMenuBar.addMenuItem("File", "Save Mission As...", 4);
   EditorMenuBar.addMenuItem("File", "-", 0);
   EditorMenuBar.addMenuItem("File", "Exit", 5);

   EditorMenuBar.addMenu("Edit", 1);
   EditorMenuBar.addMenuItem("Edit", "Undo", 1, "Ctrl Z");
   EditorMenuBar.setMenuItemBitmap("Edit", "Undo", 1);
   EditorMenuBar.addMenuItem("Edit", "Redo", 2, "Ctrl R");
   EditorMenuBar.setMenuItemBitmap("Edit", "Redo", 2);
   EditorMenuBar.addMenuItem("Edit", "-", 0);
   EditorMenuBar.addMenuItem("Edit", "Cut", 3, "Ctrl X");
   EditorMenuBar.setMenuItemBitmap("Edit", "Cut", 3);
   EditorMenuBar.addMenuItem("Edit", "Copy", 4, "Ctrl C");
   EditorMenuBar.setMenuItemBitmap("Edit", "Copy", 4);
   EditorMenuBar.addMenuItem("Edit", "Paste", 5, "Ctrl V");
   EditorMenuBar.setMenuItemBitmap("Edit", "Paste", 5);
   EditorMenuBar.addMenuItem("Edit", "Duplicate", 6, "Ctrl D");
   EditorMenuBar.setMenuItemBitmap("Edit", "Duplicate", 6);
   EditorMenuBar.addMenuItem("Edit", "-", 0);
   EditorMenuBar.addMenuItem("Edit", "Select All", 6, "Ctrl A");
   EditorMenuBar.addMenuItem("Edit", "Select None", 7, "Ctrl N");
   EditorMenuBar.addMenuItem("Edit", "-", 0);
   //EditorMenuBar.addMenuItem("Edit", "Relight Scene", 14, "Alt L");
   //EditorMenuBar.addMenuItem("Edit", "-", 0);
   EditorMenuBar.addMenuItem("Edit", "World Editor Settings...", 12);

   EditorMenuBar.addMenu("Camera", 7);
   EditorMenuBar.addMenuItem("Camera", "Drop Camera at Player", 1, "Alt Q");
   EditorMenuBar.addMenuItem("Camera", "Drop Player at Camera", 2, "Alt W");
   EditorMenuBar.addMenuItem("Camera", "Toggle Camera", 10, "Alt C");
   EditorMenuBar.addMenuItem("Camera", "-", 0);
   EditorMenuBar.addMenuItem("Camera", "Slowest", 3, "Shift 1", 1);
   EditorMenuBar.addMenuItem("Camera", "Very Slow", 4, "Shift 2", 1);
   EditorMenuBar.addMenuItem("Camera", "Slow", 5, "Shift 3", 1);
   EditorMenuBar.addMenuItem("Camera", "Medium Pace", 6, "Shift 4", 1);
   EditorMenuBar.addMenuItem("Camera", "Fast", 7, "Shift 5", 1);
   EditorMenuBar.addMenuItem("Camera", "Very Fast", 8, "Shift 6", 1);
   EditorMenuBar.addMenuItem("Camera", "Fastest", 9, "Shift 7", 1);

   EditorMenuBar.addMenu("World", 6);
   EditorMenuBar.addMenuItem("World", "Lock Selection", 10, "Ctrl L");
   EditorMenuBar.addMenuItem("World", "Unlock Selection", 11, "Ctrl Shift L");
   EditorMenuBar.addMenuItem("World", "-", 0);
   EditorMenuBar.addMenuItem("World", "Hide Selection", 12, "Ctrl H");
   EditorMenuBar.addMenuItem("World", "Show Selection", 13, "Ctrl Shift H");
   EditorMenuBar.addMenuItem("World", "-", 0);
   EditorMenuBar.addMenuItem("World", "Delete Selection", 17, "Delete");
   EditorMenuBar.addMenuItem("World", "Camera To Selection", 14);
   EditorMenuBar.addMenuItem("World", "Reset Transforms", 15);
   EditorMenuBar.addMenuItem("World", "Drop Selection", 16, "Ctrl D");
   EditorMenuBar.addMenuItem("World", "Add Selection to Instant Group", 17);
   EditorMenuBar.addMenuItem("World", "-", 0);
   EditorMenuBar.addMenuItem("World", "Drop at Origin", 0, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop at Camera", 1, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop at Camera w/Rot", 2, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop below Camera", 3, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop at Screen Center", 4, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop at Centroid", 5, "", 1);
   EditorMenuBar.addMenuItem("World", "Drop to Ground", 6, "", 1);

   EditorMenuBar.addMenu("Window", 2);
   EditorMenuBar.addMenuItem("Window", "World Editor", 2, "F2", 1);
   EditorMenuBar.addMenuItem("Window", "World Editor Inspector", 3, "F3", 1);
   EditorMenuBar.addMenuItem("Window", "World Editor Creator", 4, "F4", 1);
   
   EditorMenuBar.addMenu("MBU", 8);
   EditorMenuBar.addMenuItem("MBU", "Set Preview Camera", 2, "", 1);

   EditorMenuBar.onCameraMenuItemSelect(6, "Medium Pace");
   EditorMenuBar.onWorldMenuItemSelect(0, "Drop at Screen Center");

   EWorldEditor.init();

   //
   Creator.init();
   EditorTree.init();
   ObjectBuilderGui.init();

   EWorldEditor.isDirty = false;
   EditorGui.saveAs = false;
}

function EditorNewMission()
{
   if(ETerrainEditor.isMissionDirty || ETerrainEditor.isDirty || EWorldEditor.isDirty)
   {
      MessageBoxYesNo("Mission Modified", "Would you like to save changes to the current mission \"" @
         $Server::MissionFile @ "\" before creating a new mission?", "EditorDoNewMission(true);", "EditorDoNewMission(false);");
   }
   else
      EditorDoNewMission(false);
}

function EditorSaveMissionMenu()
{
   if(EditorGui.saveAs)
      EditorSaveMissionAs();
   else
      EditorSaveMission();
}

function EditorSaveMission()
{
   // just save the mission without renaming it

   // first check for dirty and read-only files:
   if((EWorldEditor.isDirty || ETerrainEditor.isMissionDirty) && !isWriteableFileName($Server::MissionFile))
   {
      MessageBoxOK("Error", "Mission file \""@ $Server::MissionFile @ "\" is read-only.");
      return false;
   }
   if(ETerrainEditor.isDirty && !isWriteableFileName(Terrain.terrainFile))
   {
      MessageBoxOK("Error", "Terrain file \""@ Terrain.terrainFile @ "\" is read-only.");
      return false;
   }
  
   // now write the terrain and mission files out:

   if(EWorldEditor.isDirty || ETerrainEditor.isMissionDirty)
      MissionGroup.save($Server::MissionFile);
   if(ETerrainEditor.isDirty)
      Terrain.save(Terrain.terrainFile);
   EWorldEditor.isDirty = false;
   ETerrainEditor.isDirty = false;
   ETerrainEditor.isMissionDirty = false;
   EditorGui.saveAs = false;

   return true;
}

function EditorDoSaveAs(%missionName)
{
   ETerrainEditor.isDirty = true;
   EWorldEditor.isDirty = true;
   %saveMissionFile = $Server::MissionFile;
   %saveTerrName = Terrain.terrainFile;

   $Server::MissionFile = %missionName;
   Terrain.terrainFile = filePath(%missionName) @ "/" @ fileBase(%missionName) @ ".ter";

   if(!EditorSaveMission())
   {
      $Server::MissionFile = %saveMissionFile;
      Terrain.terrainFile = %saveTerrName;
   }
}

function EditorSaveMissionAs()
{
   getSaveFilename("*.mis", "EditorDoSaveAs", $Server::MissionFile);

}

function EditorDoLoadMission(%file)
{
   // close the current editor, it will get cleaned up by MissionCleanup
   Editor.close();
   
   //Editor.close("MissionLoadingGui");
   //Canvas.setContent(RootGui);
   //RootGui.setContent(MissionLoadingGui);
   
   //MissionGroup.delete();
   destroyServer();

   //loadMission( %file, true ) ;
   
   createServer("SinglePlayer", %file);
   connectToServer("");
   
   // recreate and open the editor
   Editor::create();
   MissionCleanup.add(Editor);
   EditorGui.loadingMission = true;
   Editor.open();
}

function EditorSaveBeforeLoad()
{
   if(EditorSaveMission())
      getLoadFilename("*.mis", "EditorDoLoadMission");
}

function EditorDoNewMission(%saveFirst)
{
   if(%saveFirst)
      EditorSaveMission();

   %file = "marble/data/missions/newMission.mis";
   EditorDoLoadMission(%file);
   EditorGui.saveAs = true;
   EWorldEditor.isDirty = true;
   ETerrainEditor.isDirty = true;
}

function EditorOpenMission()
{
   if(ETerrainEditor.isMissionDirty || ETerrainEditor.isDirty || EWorldEditor.isDirty)
   {
      MessageBoxYesNo("Mission Modified", "Would you like to save changes to the current mission \"" @
         $Server::MissionFile @ "\" before opening a new mission?", "EditorSaveBeforeLoad();", "getLoadFilename(\"*.mis\", \"EditorDoLoadMission\");");
   }
   else
      getLoadFilename("*.mis", "EditorDoLoadMission");
}

function EditorMenuBar::onMenuSelect(%this, %menuId, %menu)
{
   if(%menu $= "File")
   {
      EditorMenuBar.setMenuItemEnable("File", "Save Mission...", ETerrainEditor.isDirty || ETerrainEditor.isMissionDirty || EWorldEditor.isDirty);
   }
   else if(%menu $= "Edit")
   {
      // enable/disable undo, redo, cut, copy, paste depending on editor settings

      if(EWorldEditor.isVisible())
      {
         // do actions based on world editor...
         EditorMenuBar.setMenuItemEnable("Edit", "Select All", true);
         EditorMenuBar.setMenuItemEnable("Edit", "Paste", EWorldEditor.canPasteSelection());
         %canCutCopy = EWorldEditor.getSelectionSize() > 0;

         EditorMenuBar.setMenuItemEnable("Edit", "Cut", %canCutCopy);
         EditorMenuBar.setMenuItemEnable("Edit", "Copy", %canCutCopy);
         EditorMenuBar.setMenuItemEnable("Edit", "Duplicate", %canCutCopy);

      }
      else if(ETerrainEditor.isVisible())
      {
         EditorMenuBar.setMenuItemEnable("Edit", "Cut", false);
         EditorMenuBar.setMenuItemEnable("Edit", "Copy", false);
         EditorMenuBar.setMenuItemEnable("Edit", "Paste", false);
         EditorMenuBar.setMenuItemEnable("Edit", "Select All", false);
      }
   }
   else if(%menu $= "World")
   {
      %selSize = EWorldEditor.getSelectionSize();
      %lockCount = EWorldEditor.getSelectionLockCount();
      %hideCount = EWorldEditor.getSelectionHiddenCount();

      EditorMenuBar.setMenuItemEnable("World", "Lock Selection", %lockCount < %selSize);
      EditorMenuBar.setMenuItemEnable("World", "Unlock Selection", %lockCount > 0);
      EditorMenuBar.setMenuItemEnable("World", "Hide Selection", %hideCount < %selSize);
      EditorMenuBar.setMenuItemEnable("World", "Show Selection", %hideCount > 0);

      EditorMenuBar.setMenuItemEnable("World", "Add Selection to Instant Group", %selSize > 0);
      EditorMenuBar.setMenuItemEnable("World", "Camera To Selection", %selSize > 0);
      EditorMenuBar.setMenuItemEnable("World", "Reset Transforms", %selSize > 0 && %lockCount == 0);
      EditorMenuBar.setMenuItemEnable("World", "Drop Selection", %selSize > 0 && %lockCount == 0);
      EditorMenuBar.setMenuItemEnable("World", "Delete Selection", %selSize > 0 && %lockCount == 0);
   }
}

function EditorMenuBar::onMenuItemSelect(%this, %menuId, %menu, %itemId, %item)
{
   switch$(%menu)
   {
      case "File":
         %this.onFileMenuItemSelect(%itemId, %item);
      case "Edit":
         %this.onEditMenuItemSelect(%itemId, %item);
      case "World":
         %this.onWorldMenuItemSelect(%itemId, %item);
      case "Window":
         %this.onWindowMenuItemSelect(%itemId, %item);
      case "Action":
         %this.onActionMenuItemSelect(%itemId, %item);
      case "Brush":
         %this.onBrushMenuItemSelect(%itemId, %item);
      case "Camera":
         %this.onCameraMenuItemSelect(%itemId, %item);
      case "MBU":
         %this.onMBUMenuItemSelect(%itemId, %item);
   }
}

function EditorMenuBar::onFileMenuItemSelect(%this, %itemId, %item)
{
   switch$(%item)
   {
      case "New Mission...":
         EditorNewMission();
      case "Open Mission...":
         EditorOpenMission();
      case "Save Mission...":
         EditorSaveMissionMenu();
      case "Save Mission As...":
         EditorSaveMissionAs();
      case "Import Texture Data...":
         Texture::import();
      case "Import Terraform Data...":
         Heightfield::import();
      case "Export Terraform Bitmap...":
         Heightfield::saveBitmap("");
      case "Exit":
         quit();
   }
}

function EditorMenuBar::onMBUMenuItemSelect(%this, %itemId, %item)
{
   switch$(%item)
   {
      case "Set Preview Camera":
         EditorSetPreviewCamera();
   }
}

function EditorSetPreviewCamera()
{
	CameraObj.setTransform(LocalClientConnection.camera.getTransform());
	EWorldEditor.isDirty = true;
	
	echo("Preview Camera has been moved!");
}

function EditorMenuBar::onCameraMenuItemSelect(%this, %itemId, %item)
{
   switch$(%item)
   {
      case "Drop Camera at Player":
         commandToServer('dropCameraAtPlayer');
      case "Drop Player at Camera":
         commandToServer('DropPlayerAtCamera');
      case "Toggle Camera":
         commandToServer('ToggleCamera');
      default:
         // all the rest are camera speeds:
         // item ids go from 3 (slowest) to 9 (fastest)
         %this.setMenuItemChecked("Camera", %itemId, true);
         // camera movement speed goes from 5 to 200:
         $Camera::movementSpeed = ((%itemId - 3) / 6.0) * 195 + 5;
   }
}

function EditorMenuBar::onWorldMenuItemSelect(%this, %itemId, %item)
{
   // edit commands for world editor...
   switch$(%item)
   {
      case "Lock Selection":
         EWorldEditor.lockSelection(true);
      case "Unlock Selection":
         EWorldEditor.lockSelection(false);
      case "Hide Selection":
         EWorldEditor.hideSelection(true);
      case "Show Selection":
         EWorldEditor.hideSelection(false);
      case "Camera To Selection":
         EWorldEditor.dropCameraToSelection();
      case "Reset Transforms":
         EWorldEditor.resetTransforms();
      case "Drop Selection":
         EWorldEditor.dropSelection();
      case "Delete Selection":
         EWorldEditor.deleteSelection();
      case "Add Selection to Instant Group":
         EWorldEditor.addSelectionToAddGroup();
      default:
         EditorMenuBar.setMenuItemChecked("World", %item, true);
         switch$(%item)
         {
            case "Drop at Origin":
               EWorldEditor.dropType = "atOrigin";
            case "Drop at Camera":
               EWorldEditor.dropType = "atCamera";
            case "Drop at Camera w/Rot":
               EWorldEditor.dropType = "atCameraRot";
            case "Drop below Camera":
               EWorldEditor.dropType = "belowCamera";
            case "Drop at Screen Center":
               EWorldEditor.dropType = "screenCenter";
            case "Drop to Ground":
               EWorldEditor.dropType = "toGround";
            case "Drop at Centroid":
               EWorldEditor.dropType = "atCentroid";
         }
   }
}

function EditorMenuBar::onEditMenuItemSelect(%this, %itemId, %item)
{
   if(%item $= "World Editor Settings...")
      Canvas.pushDialog(WorldEditorSettingsDlg);
   else if(%item $= "Terrain Editor Settings...")
      Canvas.pushDialog(TerrainEditorValuesSettingsGui, 99);
   else if(%item $= "Relight Scene")
      lightScene("", forceAlways);
   else if(EWorldEditor.isVisible())
   {
      // edit commands for world editor...
      switch$(%item)
      {
         case "Undo":
            EWorldEditor.undo();
         case "Redo":
            EWorldEditor.redo();
         case "Copy":
            EWorldEditor.copySelection();
         case "Cut":
            EWorldEditor.copySelection();
            EWorldEditor.deleteSelection();
         case "Paste":
            EWorldEditor.pasteSelection();
         case "Duplicate":
            EWorldEditor.duplicateSelection();
         case "Select All":
         case "Select None":
      }
   }
}

function EditorMenuBar::onWindowMenuItemSelect(%this, %itemId, %item)
{
   EditorGui.setEditor(%item);
}

function EditorGui::setWorldEditorVisible(%this)
{
   EWorldEditor.setVisible(true);
   EditorMenuBar.setMenuVisible("World", true);
   EWorldEditor.makeFirstResponder(true);
}

function EditorGui::setEditor(%this, %editor)
{
   EditorMenuBar.setMenuItemBitmap("Window", %this.currentEditor, -1);
   EditorMenuBar.setMenuItemBitmap("Window", %editor, 0);
   %this.currentEditor = %editor;

   switch$(%editor)
   {
      case "World Editor":
         EWFrame.setVisible(false);
         EWMissionArea.setVisible(false);
         %this.setWorldEditorVisible();
      case "World Editor Inspector":
         EWFrame.setVisible(true);
         EWMissionArea.setVisible(false);
         EWCreatorPane.setVisible(false);
         EWInspectorPane.setVisible(true);
         %this.setWorldEditorVisible();
      case "World Editor Creator":
         EWFrame.setVisible(true);
         EWMissionArea.setVisible(false);
         EWCreatorPane.setVisible(true);
         EWInspectorPane.setVisible(false);
         %this.setWorldEditorVisible();

   }
}

function EditorGui::getHelpPage(%this)
{
   switch$(%this.currentEditor)
   {
      case "World Editor" or "World Editor Inspector" or "World Editor Creator":
         return "5. World Editor";
      case "Mission Area Editor":
         return "6. Mission Area Editor";
      case "Terrain Editor":
         return "7. Terrain Editor";
      case "Terrain Terraform Editor":
         return "8. Terrain Terraform Editor";
      case "Terrain Texture Editor":
         return "9. Terrain Texture Editor";
      case "Terrain Texture Painter":
         return "10. Terrain Texture Painter";
   }
}

function EditorGui::onWake(%this)
{
   MoveMap.push();
   EditorMap.push();
   %this.setEditor(%this.currentEditor);
}

function EditorGui::onSleep(%this)
{
   EditorMap.pop();
   MoveMap.pop();
}

function AreaEditor::onUpdate(%this, %area)
{
   AreaEditingText.setValue( "X: " @ getWord(%area,0) @ " Y: " @ getWord(%area,1) @ " W: " @ getWord(%area,2) @ " H: " @ getWord(%area,3));
}

function AreaEditor::onWorldOffset(%this, %offset)
{
}

function EditorTree::init(%this)
{
   cancel(%this.initSched);
   if (!isObject(MissionGroup))
   {
      %this.initSched = %this.schedule(100, init);
      return;
   }
   %this.open(MissionGroup);

   // context menu
   new GuiControl(ETContextPopupDlg)
   {
      profile = "GuiModelessDialogProfile";
	   horizSizing = "width";
	   vertSizing = "height";
	   position = "0 0";
	   extent = "640 480";
	   minExtent = "8 8";
	   visible = "1";
	   setFirstResponder = "0";
	   modal = "1";
      
      new GuiPopUpMenuCtrl(ETContextPopup)
      {
         profile = "GuiScrollProfile";
         position = "0 0";
         extent = "0 0";
         minExtent = "0 0";
         maxPopupHeight = "200";
         command = "canvas.popDialog(ETContextPopupDlg);";
      };
   };
   ETContextPopup.setVisible(false);
}

function EditorTree::onInspect(%this, %obj)
{
   Inspector.inspect(%obj);
   ECreateSubsBtn.setVisible(%obj.getClassName() $= "InteriorInstance");
   InspectorNameEdit.setValue(%obj.getName());
}

function EditorTree::onSelect(%this, %obj)
{
   if($AIEdit)   
      aiEdit.selectObject(%obj);
   else
      EWorldEditor.selectObject(%obj);

}

function EditorTree::onUnselect(%this, %obj)
{
   if($AIEdit)
      aiEdit.unselectObject(%obj);
   else
      EWorldEditor.unselectObject(%obj);
}

function ETContextPopup::onSelect(%this, %index, %value)
{
   switch(%index)
   {
      case 0:
         EditorTree.contextObj.delete();
   }
}

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

function WorldEditor::createSubs(%this)
{
   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      if(%obj.getClassName() $= "InteriorInstance")
         %obj.magicButton();
   }
}

function WorldEditor::init(%this)
{
   // add objclasses which we do not want to collide with
   %this.ignoreObjClass(TerrainBlock, Sky, AIObjective);

   // editing modes
   %this.numEditModes = 3;
   %this.editMode[0]    = "move";
   %this.editMode[1]    = "rotate";
   %this.editMode[2]    = "scale";

   // context menu
   new GuiControl(WEContextPopupDlg)
   {
      profile = "GuiModelessDialogProfile";
	   horizSizing = "width";
	   vertSizing = "height";
	   position = "0 0";
	   extent = "640 480";
	   minExtent = "8 8";
	   visible = "1";
	   setFirstResponder = "0";
	   modal = "1";
      
      new GuiPopUpMenuCtrl(WEContextPopup)
      {
         profile = "GuiScrollProfile";
         position = "0 0";
         extent = "0 0";
         minExtent = "0 0";
         maxPopupHeight = "200";
         command = "canvas.popDialog(WEContextPopupDlg);";
      };
   };
   WEContextPopup.setVisible(false);
}

//------------------------------------------------------------------------------

function WorldEditor::onDblClick(%this, %obj)
{
   // Commented out because making someone double click to do this is stupid
   // and has the possibility of moving hte object
   
   //Inspector.inspect(%obj);
   //InspectorNameEdit.setValue(%obj.getName());
}

function WorldEditor::onClick( %this, %obj )
{
   Inspector.inspect( %obj );
   //ECreateSubsBtn.setVisible(%obj.getClassName() $= "InteriorInstance");
   InspectorNameEdit.setValue( %obj.getName() );
}

//------------------------------------------------------------------------------

function WorldEditor::export(%this)
{
   getSaveFilename("~/editor/*.mac", %this @ ".doExport", "selection.mac");
}

function WorldEditor::doExport(%this, %file)
{
   missionGroup.save("~/editor/" @ %file, true);
}

function WorldEditor::import(%this)
{
   getLoadFilename("~/editor/*.mac", %this @ ".doImport");
}

function WorldEditor::doImport(%this, %file)
{
   exec("~/editor/" @ %file);
}

function WorldEditor::onGuiUpdate(%this, %text)
{
}

function WorldEditor::getSelectionLockCount(%this)
{
   %ret = 0;
   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      if(%obj.locked $= "true")
         %ret++;
   }
   return %ret;
}

function WorldEditor::getSelectionHiddenCount(%this)
{
   %ret = 0;
   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      if(%obj.hidden $= "true")
         %ret++;
   }
   return %ret;
}

function WorldEditor::dropCameraToSelection(%this)
{
   if(%this.getSelectionSize() == 0)
      return;

   %pos = %this.getSelectionCentroid();
   %cam = LocalClientConnection.camera.getTransform();

   // set the pnt
   %cam = setWord(%cam, 0, getWord(%pos, 0));
   %cam = setWord(%cam, 1, getWord(%pos, 1));
   %cam = setWord(%cam, 2, getWord(%pos, 2));

   LocalClientConnection.camera.setTransform(%cam);
}

// * pastes the selection at the same place (used to move obj from a group to another)
function WorldEditor::moveSelectionInPlace(%this)
{
   %saveDropType = %this.dropType;
   %this.dropType = "atCentroid";
   %this.copySelection();
   %this.deleteSelection();
   %this.pasteSelection();
   %this.dropType = %saveDropType;
}

function WorldEditor::duplicateSelection(%this)
{
   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      %thetransform[%i] = %obj.getTransform();
   }
   %this.copySelection();
   %this.pasteSelection();
   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      
      %obj.setTransform(%thetransform[%i]);
   }
}

function WorldEditor::addSelectionToAddGroup(%this)
{
   for(%i = 0; %i < %this.getSelectionSize(); %i++) {
      %obj = %this.getSelectedObject(%i);
      $InstantGroup.add(%obj);
   }

}   
// resets the scale and rotation on the selection set
function WorldEditor::resetTransforms(%this)
{
   %this.addUndoState();

   for(%i = 0; %i < %this.getSelectionSize(); %i++)
   {
      %obj = %this.getSelectedObject(%i);
      %transform = %obj.getTransform();

      %transform = setWord(%transform, 3, "0");
      %transform = setWord(%transform, 4, "0");
      %transform = setWord(%transform, 5, "1");
      %transform = setWord(%transform, 6, "0");
         
      //
      %obj.setTransform(%transform);
      %obj.setScale("1 1 1");
   }
}


function WorldEditorToolbarDlg::init(%this)
{
   WorldEditorInspectorCheckBox.setValue(WorldEditorToolFrameSet.isMember("EditorToolInspectorGui"));
   WorldEditorMissionAreaCheckBox.setValue(WorldEditorToolFrameSet.isMember("EditorToolMissionAreaGui"));
   WorldEditorTreeCheckBox.setValue(WorldEditorToolFrameSet.isMember("EditorToolTreeViewGui"));
   WorldEditorCreatorCheckBox.setValue(WorldEditorToolFrameSet.isMember("EditorToolCreatorGui"));
}

function Creator::init( %this ) 
{
   %this.clear();

   $InstantGroup = "MissionGroup";

   // ---------- INTERIORS    
   %base = %this.addGroup( 0, "Interiors" );

   // walk all the interiors and add them to the correct group
   %interiorId = "";
   %file = findFirstFile( "*.dif" );
   
   while( %file !$= "" ) 
   {
      %file = makeRelativePath(%file, getMainDotCSDir());
      // Determine which group to put the file in
      // and build the group heirarchy as we go
      %split    = strreplace(%file, "/", " ");
      %dirCount = getWordCount(%split)-1;
      %parentId = %base;
      
      for(%i=0; %i<%dirCount; %i++)
      {
         %parent = getWords(%split, 0, %i);
         // if the group doesn't exist create it
         if ( !%interiorId[%parent] )
            %interiorId[%parent] = %this.addGroup( %parentId, getWord(%split, %i));
         %parentId = %interiorId[%parent];
      }
      // Add the file to the group
      %create = "createInterior(" @ "\"" @ %file @ "\"" @ ");";
      %this.addItem( %parentId, fileBase( %file ), %create );
   
      %file = findNextFile( "*.dif" );
   }


   // ---------- SHAPES - add in all the shapes now...
   %base = %this.addGroup(0, "Shapes");
   %dataGroup = "DataBlockGroup";
   
   for(%i = 0; %i < %dataGroup.getCount(); %i++)
   {
      %obj = %dataGroup.getObject(%i);
      echo ("Obj: " @ %obj.getName() @ " - " @ %obj.category );
      if(%obj.category !$= "" || %obj.category != 0)
      {
         %grp = %this.addGroup(%base, %obj.category);
         %this.addItem(%grp, %obj.getName(), %obj.getClassName() @ "::create(" @ %obj.getName() @ ");");
      }
   }


   // ---------- Static Shapes    
   %base = %this.addGroup( 0, "Static Shapes" );

   // walk all the statics and add them to the correct group
   %staticId = "";
   %file = findFirstFile( "*.dts" );
   while( %file !$= "" ) 
   {
      %file = makeRelativePath(%file, getMainDotCSDir());
      // Determine which group to put the file in
      // and build the group heirarchy as we go
      %split    = strreplace(%file, "/", " ");
      %dirCount = getWordCount(%split)-1;
      %parentId = %base;
      
      for(%i=0; %i<%dirCount; %i++)
      {
         %parent = getWords(%split, 0, %i);
         // if the group doesn't exist create it
         if ( !%staticId[%parent] )
            %staticId[%parent] = %this.addGroup( %parentId, getWord(%split, %i));
         %parentId = %staticId[%parent];
      }
      // Add the file to the group
      %create = "TSStatic::create(\"" @ %file @ "\");";
      %this.addItem( %parentId, fileBase( %file ), %create );
   
      %file = findNextFile( "*.dts" );
   }


   // *** OBJECTS - do the objects now...
   %objGroup[0] = "Environment";
   %objGroup[1] = "Mission";
   %objGroup[2] = "System";
   //%objGroup[3] = "AI";

   //%Environment_Item[0] = "Sky";
   //%Environment_Item[1] = "Sun";
   //%Environment_Item[2] = "Lightning";
   //%Environment_Item[3] = "Water";
   //%Environment_Item[0] = "Terrain";
   //%Environment_Item[0] = "AudioEmitter";
   //%Environment_Item[1] = "Precipitation";
   %Environment_Item[0] = "ParticleEmitter";
   //%Environment_Item[3] = "Light";

   %Mission_Item[0] = "MissionArea";
   %Mission_Item[1] = "Marker";
   %Mission_Item[2] = "Trigger";
   //%Mission_Item[3] = "PhysicalZone";
   //%Mission_Item[4] = "Camera";
   //%Mission_Item[5] = "GameType";
   //%Mission_Item[6] = "Forcefield";

   %System_Item[0] = "SimGroup";

   //%AI_Item[0] = "Objective";
   //%AI_Item[1] = "NavigationGraph";

   // objects group
   %base = %this.addGroup(0, "Mission Objects");

   // create 'em
   for(%i = 0; %objGroup[%i] !$= ""; %i++)
   {
      %grp = %this.addGroup(%base, %objGroup[%i]);

      %groupTag = "%" @ %objGroup[%i] @ "_Item";

      %done = false;
      for(%j = 0; !%done; %j++)
      {
         eval("%itemTag = " @ %groupTag @ %j @ ";");
         if(%itemTag $= "")
            %done = true;
         else
            %this.addItem(%grp, %itemTag, "ObjectBuilderGui.build" @ %itemTag @ "();");
      }
   }
}

function createInterior(%name)
{
   %obj = new InteriorInstance()
   {
      position = "0 0 0";
      rotation = "0 0 0";
      interiorFile = %name;
   };
   
   return(%obj);
}

function Creator::onAction(%this)
{
//   %this.currentSel = -1;
//   %this.currentRoot = -1;
//   %this.currentObj = -1;

   %sel = %this.getSelected();
   if(%sel == -1 || %this.isGroup(%sel) || !$missionRunning)
      return;
      
   // the value is the callback function..
   if(%this.getValue(%sel) $= "")
      return;

//   %this.currentSel = %sel;
//   %this.currentRoot = %this.getRootGroup(%sel);

   %this.create(%sel);
}

function Creator::create(%this, %sel)
{
   // create the obj and add to the instant group
   %obj = eval(%this.getValue(%sel));

   if(%obj == -1 || %obj == 0)
      return;

//   %this.currentObj = %obj;

   $InstantGroup.add(%obj);
      
   // drop it from the editor - only SceneObjects can be selected...
   EWorldEditor.clearSelection();
   EWorldEditor.selectObject(%obj);
   EWorldEditor.dropSelection();
}


function TSStatic::create(%shapeName)
{
   %obj = new TSStatic() 
   {
      shapeName = %shapeName;
   };
   return(%obj);
}

//--------------------------------------
function strip(%stripStr, %strToStrip)
{
   %len = strlen(%stripStr);
   if(strcmp(getSubStr(%strToStrip, 0, %len), %stripStr) == 0)
      return getSubStr(%strToStrip, %len, 100000);
   return %strToStrip;
}

function Heightfield::doLoadHeightfield(%name)
{
   // ok, we're getting a terrain file...

   %newTerr = new TerrainBlock() // unnamed - since we'll be deleting it shortly:
   {
      position = "0 0 -1000";
      terrainFile = strip("terrains/", %name);
      squareSize = 8;
      visibleDistance = 100;
   };
   if(isObject(%newTerr))
   {
      %script = %newTerr.getHeightfieldScript();
      if(%script !$= "")
         Heightfield::loadFromScript(%script);
      %newTerr.delete();
   }   
}   


//--------------------------------------
function Heightfield::setBitmap()
{
   getLoadFilename($TerraformerHeightfieldDir @ "/*.png", "Heightfield::doSetBitmap");
}   

//--------------------------------------
function Heightfield::doSetBitmap(%name)
{
   bitmap_name.setValue(%name);
   Heightfield::saveTab();
   Heightfield::preview($selectedOperation);
}   


//--------------------------------------
function Heightfield::hideTab()
{
   tab_terrainFile.setVisible(false);
   tab_fbm.setvisible(false);
   tab_rmf.setvisible(false);
   tab_canyon.setvisible(false);
   tab_smooth.setvisible(false);
   tab_smoothWater.setvisible(false);
   tab_smoothRidge.setvisible(false);
   tab_filter.setvisible(false);
   tab_turbulence.setvisible(false);
   tab_thermal.setvisible(false);
   tab_hydraulic.setvisible(false);
   tab_general.setvisible(false);
   tab_bitmap.setvisible(false);
   tab_blend.setvisible(false);
   tab_sinus.setvisible(false);
}


//--------------------------------------
function Heightfield::showTab(%id)
{
   Heightfield::hideTab();
   %data = restWords(Heightfield_operation.getRowTextById(%id));
   %tab  = getField(%data,1);
   echo("Tab data: " @ %data @ " tab: " @ %tab);
   %tab.setVisible(true);
}


//--------------------------------------
function Heightfield::center()
{
   %camera = terraformer.getCameraPosition(); 
   %x = getWord(%camera, 0);
   %y = getWord(%camera, 1);

   HeightfieldPreview.setOrigin(%x, %y);

   %origin = HeightfieldPreview.getOrigin();
   %x = getWord(%origin, 0);
   %y = getWord(%origin, 1);

   %root = HeightfieldPreview.getRoot();
   %x += getWord(%root, 0);
   %y += getWord(%root, 1);

   general_centerx.setValue(%x);
   general_centery.setValue(%y);
   Heightfield::saveTab();
}   

function ExportHeightfield::onAction()
{
   error("Time to export the heightfield...");
   if (Heightfield_operation.getSelectedId() != -1) {
      $TerraformerSaveRegister = getWord(Heightfield_operation.getValue(), 0);
      Heightfield::saveBitmap("");
   }
}

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

function TerrainEditor::onGuiUpdate(%this, %text)
{
   %mouseBrushInfo = " (Mouse Brush) #: " @ getWord(%text, 0) @ "  avg: " @ getWord(%text, 1);
   %selectionInfo = " (Selection) #: " @ getWord(%text, 2) @ "  avg: " @ getWord(%text, 3);
   
   TEMouseBrushInfo.setValue(%mouseBrushInfo);
   TEMouseBrushInfo1.setValue(%mouseBrushInfo);
   TESelectionInfo.setValue(%selectionInfo);
   TESelectionInfo1.setValue(%selectionInfo);
}

function TerrainEditor::offsetBrush(%this, %x, %y)
{
   %curPos = %this.getBrushPos();
   %this.setBrushPos(getWord(%curPos, 0) + %x, getWord(%curPos, 1) + %y);
}

function TerrainEditor::swapInLoneMaterial(%this, %name)
{
   // swapped?
   if(%this.baseMaterialsSwapped $= "true")
   {
      %this.baseMaterialsSwapped = "false";
      tEditor.popBaseMaterialInfo();
   }
   else
   {
      %this.baseMaterialsSwapped = "true";
      %this.pushBaseMaterialInfo();
      %this.setLoneBaseMaterial(%name);
   }
      
   //
   flushTextureCache();
}

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------


function TELoadTerrainButton::onAction(%this)
{
   getLoadFilename("terrains/*.ter", %this @ ".gotFileName");
}

function TELoadTerrainButton::gotFileName(%this, %name)
{
   //
   %pos = "0 0 0";
   %squareSize = "8";
   %visibleDistance = "1200";

   // delete current
   if(isObject(terrain))
   {
      %pos = terrain.position;
      %squareSize = terrain.squareSize;
      %visibleDistance = terrain.visibleDistance;

      terrain.delete();
   }

   // create new
   new TerrainBlock(terrain)
   {
      position = %pos;
      terrainFile = %name;
      squareSize = %squareSize;
      visibleDistance = %visibleDistance;
   };

   ETerrainEditor.attachTerrain();
}

function TerrainEditorSettingsGui::onWake(%this)
{
   TESoftSelectFilter.setValue(ETerrainEditor.softSelectFilter);
}

function TerrainEditorSettingsGui::onSleep(%this)
{
   ETerrainEditor.softSelectFilter = TESoftSelectFilter.getValue();
}

function TESettingsApplyButton::onAction(%this)
{
   ETerrainEditor.softSelectFilter = TESoftSelectFilter.getValue();
   ETerrainEditor.resetSelWeights(true);
   ETerrainEditor.processAction("softSelect");
}

function getPrefSetting(%pref, %default)
{
   // 
   if(%pref $= "")
      return(%default);
   else
      return(%pref);
}

//------------------------------------------------------------------------------

function Editor::open(%this)
{
   %this.prevContent = Canvas.getContent();

   Canvas.setContent(EditorGui);
   
   onMissionReset();
   
   EditorTree.init();
   //schedule(1000, 0, "EditorTree.init();");
}

function Editor::close(%this)
{
   if(%this.prevContent == -1 || %this.prevContent $= "")
      %this.prevContent = "PlayGui";

   Canvas.setContent(%this.prevContent);
}

//------------------------------------------------------------------------------

function GuiInspector::apply(%this, %name)
{
   %this.getInspectObject().setName(%name);
}
