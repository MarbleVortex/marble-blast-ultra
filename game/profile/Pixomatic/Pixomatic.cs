// Pixomatic Renderer Profile Script
if (resHigherThan("640 480"))
{
   $pref::Video::windowedRes = "640 480 32";
   $GFXCardProfiler::Retry = true;
}
   
$pref::Video::noRenderAstrolabe = true; 
$pref::TextureManager::maxSize = 128;

$GFXCardProfiler::AllowDevice= false;