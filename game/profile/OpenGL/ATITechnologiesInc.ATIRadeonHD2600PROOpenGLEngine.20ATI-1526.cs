// 10.5.2 Radeon HD 2600 profile script (seems to only be for iMacs shipped after 10.5.2)
//
// The HD 2600 drivers have been significantly improved (as has our implementation of various things) and we only need
// to disable GL_EXT_framebuffer_blit

GFXCardProfiler::setCapability("GL::suppRTBlit", false);
