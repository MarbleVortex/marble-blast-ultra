// This card does *MUCH* better with full MSAA then the default half MSAA (nearly 1.7x better)
GFXCardProfiler::SetCapability("GL::useMaxMSAA", true);
