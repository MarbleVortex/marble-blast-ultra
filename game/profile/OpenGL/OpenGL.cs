// OpenGL Renderer Profile Script
$pref::TextureManager::maxSize = 1024;
$GFXCardProfiler::AllowDevice = true;

$pref::Video::disableMultisampling = false;

GFXCardProfiler::SetCapability("GL::useMaxMSAA", true);