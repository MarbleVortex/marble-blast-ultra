// Intel cards shouldn't use shaders
GFXCardProfiler::setCapability("GL::suppFragmentShader", true);
GFXCardProfiler::setCapabiility("GL::Workaround::useSpecisfx3100Shaders", true);
GFXCardProfiler::setCapability("GL::Workaround::noManualMips", true);

// Shared RAM is terrible, bump down texture resolution
$pref::TextuerManager::maxSize = 256;