function resHigherThan(%resString)
{
   if (%resString $= "")
      return false;
   %width = getWord($pref::Video::windowedRes,0);
   %height = getWord($pref::Video::windowedRes,1);
   %inputWidth = getWord(%resString,0);
   %inputHeight = getWord(%resString,1);
   if (%width > %inputWidth || %height > %inputHeight)
      return true;
   return false;
}

function applyProfile()
{
   // export the detected pref vars to the detectedVideo file 
   // (which will probably be in %USER_HOME%\Application Data\GarageGames\Gamename)
   export("$GFXCardProfiler::Detected::*", "detectedVideo.cs");
   
   // the profile scripts can force the canvas to abort with the current device
   // initialization would then continue with the next device in the fallback 
   // chain, or if there are no more devices, it would fail.  this is useful
   // for aborting on devices where the game would init but not run properly
   // (e.g. as D3D8 on ancient video hardware).
   // by default we do not abort.
   $GFXCardProfiler::AllowDevice = true;
   
   // if auto profiling is disabled, we're done
   //if (!$pref::Video::autoProfile)
   //{
      // doesn't hurt to spew
   //   echo("$pref::Video::autoProfile is false, won't apply video profiles");
   //   return;
   //}

   %basePath = "./" @ $GFXCardProfiler::Detected::Renderer;
   // first exec renderer profile
   %script = %basePath @ "/" @ $GFXCardProfiler::Detected::Renderer @ ".cs";
   exec(%script);   
   // Crazy renderer/version string profile, because I said so
   %script = %basePath @ "/" @ $GFXCardProfiler::Detected::Renderer @ "." @ $GFXCardProfiler::Detected::VersionString @ ".cs";
   exec(%script);
   // exec vender profile
   %script = %basePath  @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ ".cs";
   exec(%script);   
   // exec vender product profile
   %script = %basePath @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ "." @
      $GFXCardProfiler::Detected::Product @ ".cs";
   exec(%script);   
   // exec vender product version profile
   %script = %basePath @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ "." @
      $GFXCardProfiler::Detected::Product @ "." @ 
      $GFXCardProfiler::Detected::VersionString @ ".cs";
   exec(%script);   
   %script = %basePath @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ "." @
      $GFXCardProfiler::Detected::Product @ "." @ 
      $GFXCardProfiler::Detected::Version @ ".cs";
   exec(%script); 
   // exec vender product version subversion profile
   %script = %basePath @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ "." @
      $GFXCardProfiler::Detected::Product @ "." @ 
      $GFXCardProfiler::Detected::Version @ "." @
      $GFXCardProfiler::Detected::Subversion @ ".cs";
   exec(%script);   
   // exec vender product version subversion build profile (whoa!)
   %script = %basePath @ "/" @ 
      $GFXCardProfiler::Detected::Vendor @ "." @
      $GFXCardProfiler::Detected::Product @ "." @ 
      $GFXCardProfiler::Detected::Version @ "." @
      $GFXCardProfiler::Detected::Subversion @ "." @
      $GFXCardProfiler::Detected::Build @ ".cs";
   exec(%script);   
   if ($GFXCardProfiler::Detected::DeviceId !$= "")
   {
      // exec vender product version subversion build deviceid profile (whoa!!)
      %script = %basePath @ "/" @ 
         $GFXCardProfiler::Detected::Vendor @ "." @
         $GFXCardProfiler::Detected::Product @ "." @ 
         $GFXCardProfiler::Detected::Version @ "." @
         $GFXCardProfiler::Detected::Subversion @ "." @
         $GFXCardProfiler::Detected::Build @ "." @
         $GFXCardProfiler::Detected::DeviceId @ ".cs" ;
      exec(%script);   
   }
   
   if (!$GFXCardProfiler::AllowDevice)
   {
      error("Video configuration disabled by profile");
   }
   
   return $GFXCardProfiler::AllowDevice;
}

applyProfile();
