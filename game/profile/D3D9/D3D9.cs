// Direct3D 9 Renderer Profile Script
$requiredPixVersion = 2.0;

%pixVer = getPixelShaderVersion();

// if they have non-zero pixel version, make sure it meets our min spec
// for shader rendering.
if (%pixVer > 0.0 && %pixVer < $requiredPixVersion )
{
   %displayInfo = getDisplayDeviceInformation();
   error("Display device does not meet requirements for d3d9 shader rendering (pixel version detected:" SPC
      %pixVer @ ", required:" SPC $requiredPixVersion @ ").  Video card: " @ %displayInfo);
   
   $GFXCardProfiler::AllowDevice = false;
   return;
}
     
if (%pixVer < 0.00001)
{
   // no zpass in fixed function
   $pref::video::useZPass = false;
}

// in DX9 we should be able to accurately get physical memory
validateTextureManagerMemory();
%mem = getTotalTexMemoryAtValidation();
%mem = %mem / 1024 / 1024;
echo("(DX9) Video memory size:" SPC %mem);
if (%mem > 0)
{
   if (%mem >= 60)
      $pref::TextureManager::maxSize = 1024;
   else if (%mem >= 35)
      $pref::TextureManager::maxSize = 256;
   else if (%mem >= 20)
      $pref::TextureManager::maxSize = 128;
   else 
      $pref::TextureManager::maxSize = 64;
}      

