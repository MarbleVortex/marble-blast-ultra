//-----------------------------------------------------------------------------
// Shader defs
//-----------------------------------------------------------------------------
new ShaderData( BumpMarb )
{
   DXVertexShaderFile 	= "shaders/marbles/marbBumpCubeDiffuseV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbBumpCubeDiffuseP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarb )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicRefRefMarb )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicGlassV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicGlassP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( DiffMarb )
{
   DXVertexShaderFile 	= "shaders/marbles/marbDiffCubeV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbDiffCubeP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarb2 )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassic2V.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassic2P.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarb2PureSphere )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassic2PureSphereV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassic2PureSphereP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarbGlass )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicGlassPerturbV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicGlassPerturbP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarbGlassPureSphere )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicGlassPerturbPureSphereV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicGlassPerturbPureSphereP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarbMetal )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicMetalPerturbV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicMetalPerturbP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarbGlass2 )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassicGlassPerturb2V.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassicGlassPerturb2P.hlsl";
   pixVersion = 2.0;
};

new ShaderData( CrystalMarb )
{
   DXVertexShaderFile 	= "shaders/marbles/marbCrystalV.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbCrystalP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( ClassicMarb3 )
{
   DXVertexShaderFile 	= "shaders/marbles/marbClassic3V.hlsl";
   DXPixelShaderFile 	= "shaders/marbles/marbClassic3P.hlsl";
   pixVersion = 2.0;
};

//-----------------------------------------------------------------------------
// Material defs
//-----------------------------------------------------------------------------
%mat = new Material(Material_Base_Marble)
{
   baseTex[0] = "marble/data/shapes/balls/base.marble";

   pixelSpecular[0] = true;
   specular[0] = "0.5 0.6 0.5 0.6";
   specularPower[0] = 12.0;
   //cubemap = sky_environment;

};

//function defineBasicMarbleMaterial(%id, %shader, %bump, %diff, %mapTo)
//{
//   %tmp = "new CustomMaterial(Material_Marble" @ %id @ ") " @
//        "{\n" @
//        "    mapTo = \"" @ %mapTo @ "\";\n" @
//        "    texture[0] = \"marble/data/shapes/balls/" @ %bump @ "\";\n" @
//        "    texture[1] = \"marble/data/shapes/balls/" @ %diff @ "\";\n" @
//        "    texture[3] = \"$dynamicCubemap\";\n" @
//        "    dynamicCubemap = true;\n" @ 
//        "    shader = \"" @ %shader @ "\";\n" @
//        "    version = 2.0;\n" @ 
//        "};\n";
        
//   echo(%tmp); // <-- I'm good for debugging.
//   eval(%tmp);
//}

function defineBasicMarbleMaterial(%id, %shader, %bump, %diff, %mapTo, %specular, %specPow)
{
   %tmp = "new CustomMaterial(Material_Marble" @ %id @ ") " @
        "{\n" @
        "    mapTo = \"" @ %mapTo @ "\";\n" @
		  "    baseTex[0] = \"marble/data/shapes/balls/" @ %diff @ "\";\n" @
        "    texture[0] = \"marble/data/shapes/balls/" @ %bump @ "\";\n" @
        "    texture[1] = \"marble/data/shapes/balls/" @ %diff @ "\";\n" @
        "    texture[3] = \"$dynamicCubemap\";\n" @
        "    specular[0] = \"" @ %specular @ "\";\n" @
        "    specularPower[0] = " @ %specPow @ ";\n" @
        "    dynamicCubemap = true;\n" @ 
        "    shader = \"" @ %shader @ "\";\n" @
        "    version = 2.0;\n" @ 
        
        // LOL
        //"    glow[0] = true;\n" @
        //"    emissive[0] = true;\n" @
        
        
        "};\n";
        
//   echo(%tmp); // <-- I'm good for debugging.
   eval(%tmp);
}


// Shaders with PureSphere after them do their own normal calculations and won't have seams.

// Official
// MBU-Style Default Blue Marble
defineBasicMarbleMaterial("01", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble1.skin",  "marble1.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("02", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble2.skin",  "marble2.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("03", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble3.skin",  "marble3.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("04", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble4.skin",  "marble4.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("05", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble5.skin",  "marble5.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("06", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble6.skin",  "marble6.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("07", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble7.skin",  "marble7.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("08", "ClassicMarbGlassPureSphere", "official/marble1.normal",  "official/marble8.skin",  "marble8.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("09", "CrystalMarb",                "official/marble9.normal",  "official/marble9.skin",  "marble9.skin",  "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("10", "CrystalMarb",                "official/marble9.normal",  "official/marble10.skin", "marble10.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("11", "CrystalMarb",                "official/marble9.normal",  "official/marble11.skin", "marble11.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("12", "CrystalMarb",                "official/marble9.normal",  "official/marble12.skin", "marble12.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("13", "CrystalMarb",                "official/marble9.normal",  "official/marble13.skin", "marble13.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("14", "CrystalMarb",                "official/marble9.normal",  "official/marble14.skin", "marble14.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("15", "ClassicMarbMetal",           "official/marble16.normal", "official/marble15.skin", "marble15.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("16", "ClassicMarbGlass",           "official/marble16.normal", "official/marble16.skin", "marble16.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("17", "ClassicMarbGlass",           "official/marble17.normal", "official/marble17.skin", "marble17.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("18", "ClassicMarbMetal",           "official/marble16.normal", "official/marble18.skin", "marble18.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("19", "ClassicMarb2",               "marble.normal",            "official/marble19.skin", "marble19.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("20", "ClassicMarb3",               "marble.normal",            "official/marble20.skin", "marble20.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("21", "ClassicMarb3",               "marble.normal",            "official/marble21.skin", "marble21.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("22", "ClassicMarb3",               "marble.normal",            "official/marble22.skin", "marble22.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("23", "ClassicMarb3",               "marble.normal",            "official/marble23.skin", "marble23.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("24", "ClassicMarb3",               "marble.normal",            "official/marble24.skin", "marble24.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("25", "ClassicMarb3",               "marble.normal",            "official/marble25.skin", "marble25.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("26", "ClassicMarb3",               "marble.normal",            "official/marble26.skin", "marble26.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("27", "ClassicMarb3",               "marble.normal",            "official/marble27.skin", "marble27.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("28", "ClassicMarb3",               "marble.normal",            "official/marble28.skin", "marble28.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("29", "ClassicMarb3",               "marble.normal",            "official/marble29.skin", "marble29.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("30", "ClassicMarb3",               "marble.normal",            "official/marble30.skin", "marble30.skin", "0.3 0.3 0.3 0.3", 24 );
defineBasicMarbleMaterial("31", "ClassicMarb3",               "marble.normal",            "official/marble31.skin", "marble31.skin", "0.6 0.6 0.6 0.6", 12 );
// MBG-Style Default Rainbow Marble
defineBasicMarbleMaterial("32", "ClassicMarb",                "marblefx.normal",          "official/marble32.skin", "marble32.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("33", "ClassicMarb2",               "marble.normal",            "official/marble33.skin", "marble33.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("34", "ClassicMarb3",               "marble.normal",            "official/marble34.skin", "marble34.skin", "0.2 0.2 0.2 0.2", 6  );
defineBasicMarbleMaterial("35", "ClassicMarb3",               "marble.normal",            "official/marble35.skin", "marble35.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("36", "ClassicMarb3",               "official/marble36.normal", "official/marble36.skin", "marble36.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("37", "ClassicMarbGlassPureSphere", "marble.normal",            "official/marble37.skin", "marble37.skin", "0.6 0.6 0.6 0.6", 12 );

// Custom
defineBasicMarbleMaterial("38", "ClassicMarb",                "marblefx.normal",        "custom/marble38.skin", "marble38.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("39", "ClassicMarb",                "marblefx.normal",        "custom/marble39.skin", "marble39.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("40", "ClassicMarb",                "marblefx.normal",        "custom/marble40.skin", "marble40.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("41", "ClassicMarb",                "marblefx.normal",        "custom/marble41.skin", "marble41.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("42", "ClassicMarb",                "marblefx.normal",        "custom/marble42.skin", "marble42.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("43", "ClassicMarb",                "marblefx.normal",        "custom/marble43.skin", "marble43.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("44", "ClassicMarb",                "marblefx.normal",        "custom/marble44.skin", "marble44.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("45", "ClassicMarb",                "marblefx.normal",        "custom/marble45.skin", "marble45.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("46", "ClassicMarb",                "marblefx.normal",        "custom/marble46.skin", "marble46.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("47", "ClassicMarb",                "marblefx.normal",        "custom/marble47.skin", "marble47.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("48", "ClassicMarbGlassPureSphere", "custom/marble48.normal", "custom/marble48.skin", "marble48.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("49", "ClassicMarb",                "marblefx.normal",        "custom/marble49.skin", "marble49.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("50", "ClassicMarb",                "marblefx.normal",        "custom/marble50.skin", "marble50.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("51", "ClassicMarb",                "marblefx.normal",        "custom/marble51.skin", "marble51.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("52", "ClassicMarb",                "marblefx.normal",        "custom/marble52.skin", "marble52.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("53", "ClassicMarb",                "marblefx.normal",        "custom/marble53.skin", "marble53.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("54", "ClassicMarb",                "marblefx.normal",        "custom/marble54.skin", "marble54.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("55", "ClassicMarb",                "marblefx.normal",        "custom/marble55.skin", "marble55.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("56", "ClassicMarb",                "marblefx.normal",        "custom/marble56.skin", "marble56.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("57", "ClassicMarb",                "marblefx.normal",        "custom/marble57.skin", "marble57.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("58", "ClassicMarb",                "marblefx.normal",        "custom/marble58.skin", "marble58.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("59", "ClassicMarb",                "marblefx.normal",        "custom/marble59.skin", "marble59.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("60", "ClassicMarb",                "marblefx.normal",        "custom/marble60.skin", "marble60.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("61", "ClassicMarb",                "marblefx.normal",        "custom/marble61.skin", "marble61.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("62", "ClassicMarbGlassPureSphere", "custom/marble62.normal", "custom/marble62.skin", "marble62.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("63", "ClassicMarbGlassPureSphere", "custom/marble63.normal", "custom/marble63.skin", "marble63.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("64", "ClassicMarb",                "marblefx.normal",        "custom/marble64.skin", "marble64.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("65", "ClassicMarb",                "marblefx.normal",        "custom/marble65.skin", "marble65.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("66", "ClassicMarb",                "marblefx.normal",        "custom/marble66.skin", "marble66.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("67", "ClassicMarb",                "marblefx.normal",        "custom/marble67.skin", "marble67.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("68", "ClassicMarbGlassPureSphere", "custom/marble68.normal", "custom/marble68.skin", "marble68.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("69", "ClassicMarb",                "marblefx.normal",        "custom/marble69.skin", "marble69.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("70", "ClassicMarb",                "marblefx.normal",        "custom/marble70.skin", "marble70.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("71", "ClassicMarb",                "marblefx.normal",        "custom/marble71.skin", "marble71.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("72", "ClassicMarb",                "marblefx.normal",        "custom/marble72.skin", "marble72.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("73", "ClassicMarb",                "marblefx.normal",        "custom/marble73.skin", "marble73.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("74", "ClassicMarb",                "marblefx.normal",        "custom/marble74.skin", "marble74.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("75", "ClassicMarbGlassPureSphere", "custom/marble75.normal", "custom/marble75.skin", "marble75.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("76", "ClassicMarb",                "marblefx.normal",        "custom/marble76.skin", "marble76.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("77", "ClassicMarb",                "marblefx.normal",        "custom/marble77.skin", "marble77.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("78", "ClassicMarb",                "marblefx.normal",        "custom/marble78.skin", "marble78.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("79", "ClassicMarb",                "marblefx.normal",        "custom/marble79.skin", "marble79.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("80", "ClassicMarb",                "marblefx.normal",        "custom/marble80.skin", "marble80.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("81", "ClassicMarb",                "marblefx.normal",        "custom/marble81.skin", "marble81.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("82", "ClassicMarb",                "marblefx.normal",        "custom/marble82.skin", "marble82.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("83", "ClassicMarb",                "custom/marble83.normal", "custom/marble83.skin", "marble83.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("84", "ClassicMarbGlass",           "custom/marble84.normal", "custom/marble84.skin", "marble84.skin", "0.6 0.6 0.6 0.6", 12 );

// Templates (MBG-Style because easier to implement)
defineBasicMarbleMaterial("87", "ClassicMarb", "marblefx.normal", "templates/marbletemplate1.skin", "marbletemplate1.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("88", "ClassicMarb", "marblefx.normal", "templates/marbletemplate2.skin", "marbletemplate2.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("89", "ClassicMarb", "marblefx.normal", "templates/marbletemplate3.skin", "marbletemplate3.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("90", "ClassicMarb", "marblefx.normal", "templates/marbletemplate4.skin", "marbletemplate4.skin", "0.6 0.6 0.6 0.6", 12 );
defineBasicMarbleMaterial("91", "ClassicMarb", "marblefx.normal", "templates/marbletemplate5.skin", "marbletemplate5.skin", "0.6 0.6 0.6 0.6", 12 );

// 3D Marbles

new CustomMaterial(Marble85Ring)
{
   mapTo = "marble85.ring.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble85.ring.skin";
   texture[0] = "~/data/shapes/balls/custom/marble85.ring.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble85Center)
{
   mapTo = "marble85.center.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble85.center.skin";
   texture[0] = "~/data/shapes/balls/custom/marble85.center.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;
   glow[0] = true;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble86Inner)
{
   mapTo = "marble86.inner.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble86.inner.skin";
   texture[0] = "~/data/shapes/balls/custom/marble86.inner.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;
   glow[0] = true;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

//new CustomMaterial(Marble86Outer)
//{
   //mapTo = "marble95.outer.skin";
//
   //texture[0] = "~/data/shapes/balls/custom/marble95.outer.skin";
   ////texture[1] = "$dynamicCubemap";
//
   //pixelSpecular[0] = true;
   //specular[0] = "0.8 0.9 0.8 0.8";
   //specularPower[0] = 40;
   //
   //translucent[0] = true;
//
   ////dynamicCubemap = true;
   //
   //shader = HalfTileSmall; 
   //version = 2.0;
//};

// Cannot hex edit this number. Must stay at 95. Refer to as 86 in code only.
new Material(Marble86Outer)
{
   mapTo = "marble95.outer.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble95.outer.skin";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 40;
   
   emissive[0] = true;
   
   dynamicCubemap[0] = true;
   
   translucent[0] = true;
};

new CustomMaterial(Marble86Spike)
{
   mapTo = "marble86.spike.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble86.spike.skin";
   texture[0] = "~/data/shapes/balls/custom/marble86.spike.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble87Outer)
{
   mapTo = "marble87.outer.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble87.outer.skin";
   texture[0] = "~/data/shapes/balls/custom/marble87.outer.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble87Inner)
{
   mapTo = "marble87.inner.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble87.inner.skin";
   texture[0] = "~/data/shapes/balls/custom/marble87.inner.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;
   glow[0] = true;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble88Cage)
{
   mapTo = "marble88.cage.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble88.cage.skin";
   texture[0] = "~/data/shapes/balls/custom/marble88.cage.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble89Ring)
{
   mapTo = "marble89.ring.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble89.ring.skin";
   texture[0] = "~/data/shapes/balls/custom/marble89.ring.skin";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new CustomMaterial(Marble89Core)
{
   mapTo = "marble89.core.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble89.core.skin";
   texture[0] = "~/data/shapes/balls/custom/marble89.core.skin";
   //texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;
   //emissive[0] = true;
	glow[0] = true;

   //dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

new Material(Rico_ball)
{
   mapTo = "marble90.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble90.skin";

   pixelSpecular[0] = false;
   specular[0] = "1 1 1 1";
   specularPower[0] = 80;
};

new Material(Rico_ball_glow)
{
   mapTo = "marble90.glow.skin";

   baseTex[0] = "~/data/shapes/balls/custom/marble90.glow.skin";

   pixelSpecular[0] = true;
   specular[0] = "1 1 1 1";
   specularPower[0] = 40;

   emissive[0] = true;
};

//-----------------------------------------------------------------------------------------------------------------------
// CUSTOM MARBLES
//-----------------------------------------------------------------------------------------------------------------------

new CustomMaterial(Reflectmarble1)
{
//   mapTo = "marble1.skin";

   baseTex[0] = "~/data/shapes/balls/plainNormal";
   texture[0] = "~/data/shapes/balls/plainNormal";
   texture[1] = "~/data/shapes/balls/marble1.skin";
   texture[3] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = BumpMarb; 
   version = 2.0;
};

new CustomMaterial(Reflectmarble2)
{
//   mapTo = "marble1.skin";

   baseTex[0] = "~/data/shapes/balls/plainNormal";
   texture[0] = "~/data/shapes/balls/plainNormal";
   texture[1] = "~/data/shapes/balls/marble1.skin";
   texture[3] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   shader = BumpMarb; 
   version = 2.0;
};

new CustomMaterial(Reflectmarble3)
{
//   mapTo = "marble1.skin";

   baseTex[0] = "~/data/shapes/balls/marble1.skin";
   texture[0] = "~/data/shapes/balls/marble1.skin";
   texture[1] = "$dynamicCubemap";

   dynamicCubemap = true;
   
   shader = DiffMarb; 
   version = 2.0;
};

