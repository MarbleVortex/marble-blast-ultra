// Skies
$sky_beginner = "marble/data/skies/sky_beginner.dml";
$sky_intermediate = "marble/data/skies/sky_intermediate.dml";
$sky_advanced = "marble/data/skies/sky_advanced.dml";

// leaderboard ids
$Leaderboard::SPOverall = 99;
$Leaderboard::SPCompletion = 95;
$Leaderboard::MPScrumOverall = 98;
// skill leaderboard variables are defined in engine code (since they may be changed by xlast)
// variables are:
// $Leaderboard::MPScrumSkill

// titles for the overall leaderboards
$LeaderboardTitles[$Leaderboard::SPOverall]     = $Text::LB_SPOverall;
// not displayed --> //$LeaderboardTitles[$Leaderboard::SPCompletion]  = $Text::LB_SPCompletion; 
$LeaderboardTitles[$Leaderboard::MPScrumOverall] = $Text::Standard;
$LeaderboardTitles[$Leaderboard::MPScrumSkill] = $Text::Ranked;

// overall leaderboards for each game mode
$OverallLeaderboards[0] = $Leaderboard::MPScrumOverall;

new ScriptObject(GameMissionInfo)
{
	currentSPIndex = -1;
	currentMPIndex = -1;
	currentMPCIndex = -1;
	currentCustomIndex = -1;
	currentCustomGeneralYeahIndex = -1;
	currentCustomMattIndex = -1;
	// enable Electro
	//currentCustomElectroIndex = -1;
	currentCustomNatureIndex = -1;
	currentCustomRemixIndex = -1;
	currentCustomTrollIndex = -1;
	currentCustomMattBeginnerIndex = -1;
	currentCustomMattIntermediateIndex = -1;
	currentCustomMattAdvancedIndex = -1;
	currentCustomMattBonusIndex = -1;
	//currentGigaBeginnerIndex = -1;
	//currentGigaIntermediateIndex = -1;
	//currentGigaAdvancedIndex = -1;
	//currentGigaBonusIndex = -1;
	currentGoldBeginnerIndex = -1;
	currentGoldIntermediateIndex = -1;
	currentGoldAdvancedIndex = -1;
	currentGoldBonusIndex = -1;
	currentMobileIndex = -1;	
	currentEvolvedBeginnerIndex = -1;
	currentEvolvedIntermediateIndex = -1;
	currentEvolvedAdvancedIndex = -1;
	currentEvolvedBonusIndex = -1;

	// various "constants"
	MissionFileSpec = "*/missions/*.mis";
	SPMode = "SinglePlayer";
	MPMode = "MultiPlayer";
	MPCMode = "MultiPlayerCustom";
	CMode = "Custom";
	//GigaMode = "Giga";
	GoldMode = "Gold";
	MobileMode = "Mobile";
	EvolvedMode = "Evolved";
	SpecialMode = "Special";
};

function GameMissionInfo::filterDifficulty(%this)
{
	return %this.filterDifficulty;
}

function GameMissionInfo::filterGame(%this)
{
	return %this.filterGame;
}

function GameMissionInfo::filterCustom(%this)
{
	return %this.filterCustom;
}

function GameMissionInfo::filterCustomDifficulty(%this)
{
	return %this.filterCustomDifficulty;
}

function GameMissionInfo::setFilterDifficulty(%this, %filter)
{
	%this.filterDifficulty = %filter;
}

function GameMissionInfo::setFilterGame(%this, %game)
{
	%this.filterGame = %game;
}

function GameMissionInfo::setFilterCustom(%this, %custom)
{
	%this.filterCustom = %custom;
}

function GameMissionInfo::setFilterCustomDifficulty(%this, %customdif)
{
	%this.filterCustomDifficulty = %customdif;
}

function GameMissionInfo::init(%this)
{
	buildMissionList();
	%this.setMode(GameMissionInfo.SPMode);
}

function GameMissionInfo::getMode(%this)
{
	return %this.mode;
}

function GameMissionInfo::setMode(%this,%mode)
{
	// Since we are switching modes we need to hide the current mission

	if (%mode $= "")
	%mode = GameMissionInfo.SPMode;

	// if our mode is changing clear the difficulty filter
	if (%mode !$= %this.mode)
	%this.setFilterDifficulty("");
	
	%this.mode = %mode;
}

function GameMissionInfo::getCurrentMissionGroup(%this)
{
	if (%this.mode $= GameMissionInfo.MPMode)
	%missionGroup = MultiPlayMissionGroup;
	else if (%this.mode $= GameMissionInfo.MPCMode)
	%missionGroup = MultiPlayCustomMissionGroup;
	else if (%this.mode $= GameMissionInfo.SpecialMode)
	%missionGroup = SpecialMissionGroup;
	else if (%this.mode $= GameMissionInfo.CMode) {
	   if(%this.filterCustom() $= "matt") {
	      if (%this.filterCustomDifficulty() $= "beginner") {
	         %missionGroup = CustomMattBeginnerMissionGroup;
	      } else if (%this.filterCustomDifficulty() $= "intermediate") {
	         %missionGroup = CustomMattIntermediateMissionGroup;
	      } else if (%this.filterCustomDifficulty() $= "advanced") {
	         %missionGroup = CustomMattAdvancedMissionGroup;
	      } else {
	         %missionGroup = CustomMattBonusMissionGroup;
	      }
	      
	   }
	   // enable Electro
	   //else if(%this.filterCustom() $= "electro") {
	      //%missionGroup = CustomElectroMissionGroup;
	   //}
	   else if(%this.filterCustom() $= "nature") {
	      %missionGroup = CustomNatureMissionGroup;
	   }
	   else if(%this.filterCustom() $= "remix") {
	      %missionGroup = CustomRemixMissionGroup;
	   }
	   else if(%this.filterCustom() $= "troll") {
	      %missionGroup = CustomTrollMissionGroup;
	   }
	   else if (%this.filterCustom() $= "generalyeah") {
	      %missionGroup = CustomGeneralYeahMissionGroup;
	   }
	}
   //else if (%this.mode $= GameMissionInfo.GigaMode)
   //{
      //if (%this.filterDifficulty() $= "beginner")
         //%missionGroup = GigaBeginnerMissionGroup;
      //else if (%this.filterDifficulty() $= "intermediate")
         //%missionGroup = GigaIntermediateMissionGroup;
      //else if (%this.filterDifficulty() $= "advanced")
         //%missionGroup = GigaAdvancedMissionGroup;
      //else   
	      //%missionGroup = GigaBonusMissionGroup;
   //}
   else if (%this.mode $= GameMissionInfo.GoldMode)
   {
      if (%this.filterDifficulty() $= "beginner")
         %missionGroup = GoldBeginnerMissionGroup;
      else if (%this.filterDifficulty() $= "intermediate")
         %missionGroup = GoldIntermediateMissionGroup;
      else if (%this.filterDifficulty() $= "advanced")
         %missionGroup = GoldAdvancedMissionGroup;
      else   
	      %missionGroup = GoldBonusMissionGroup;
   }
   else if (%this.mode $= GameMissionInfo.MobileMode)
   {
      %missionGroup = MobileMissionGroup;
   }
	else if (%this.mode $= GameMissionInfo.EvolvedMode)
   {
      if (%this.filterDifficulty() $= "beginner")
         %missionGroup = EvolvedBeginnerMissionGroup;
      else if (%this.filterDifficulty() $= "intermediate")
         %missionGroup = EvolvedIntermediateMissionGroup;
      else if (%this.filterDifficulty() $= "advanced")
         %missionGroup = EvolvedAdvancedMissionGroup;
      else   
	      %missionGroup = EvolvedBonusMissionGroup;
   }
   else
	   %missionGroup = SinglePlayMissionGroup;
	
	return %missionGroup;
}

function GameMissionInfo::getLeaderboardTitle(%this,%lbid)
{
	return $LeaderboardTitles[%lbid];
}

function GameMissionInfo::getOverallLeaderboard(%this,%gameModeId)
{
	return $OverallLeaderboards[%gameModeId];
}

function GameMissionInfo::findIndexByPath(%this, %missionFile)
{
	%index = -1;

	%missionGroup = GameMissionInfo.getCurrentMissionGroup();

	for (%i = 0; %i < %missionGroup.getCount(); %i++)
	{
		if (%missionGroup.getObject(%i).file $= %missionFile)
		{
			%index = %i;
			break;
		}
	}

	return %index;
}

function GameMissionInfo::findMissionById(%this,%missionId)
{
	%group = %this.getCurrentMissionGroup();
	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mission = %group.getObject(%i);
		//if (%mission.level
		if (%i + 1 == %missionId)
		return %mission;
	}
	return 0;
}

function GameMissionInfo::findMissionByPath(%this,%missionPath)
{
	%group = %this.getCurrentMissionGroup();
	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mission = %group.getObject(%i);
		if (%mission.file $= %missionPath)
		return %mission;
	}
	return 0;
}

function GameMissionInfo::setCurrentMission(%this,%missionFile)
{
	// find the index of the mission and update the appropriate index variable
	%index = %this.findIndexByPath(%missionFile);
	if (%index == -1)
	{
		error("setCurrentMission: idx should not be -1");
		return;
	}
	%this.setCurrentIndex(%index);
}

function GameMissionInfo::getCurrentMission(%this)
{
	if (%this.getCurrentIndex() == -1)
	   return 0;

	%mission = %this.getCurrentMissionGroup().getObject(%this.getCurrentIndex());
	if (!isObject(%mission))
	error("getCurrentMission: no current mission");
	%this.setBitmap(%mission);
	return %mission;
}

function GameMissionInfo::setCurrentIndex(%this,%index)
{
	if (%this.mode $= GameMissionInfo.MPMode)
	   %this.currentMPIndex = %index;
   else if (%this.mode $= GameMissionInfo.MPCMode)
	   %this.currentMPCIndex = %index;
	else if (%this.mode $= GameMissionInfo.SpecialMode)
	   %this.currentSpecialIndex = %index;
	else if (%this.mode $= GameMissionInfo.CMode)
	{
	   if (%this.filterCustom() $= "matt")
	   {
	      if (%this.filterCustomDifficulty() $= "beginner") {
	         %this.currentCustomMattBeginnerIndex = %index;
	      } else if (%this.filterCustomDifficulty() $= "intermediate") {
	         %this.currentCustomMattIntermediateIndex = %index;
	      } else if (%this.filterCustomDifficulty() $= "advanced") {
	         %this.currentCustomMattAdvancedIndex = %index;
	      } else {
	         %this.currentCustomMattBonusIndex = %index;
	      }
	// enable Electro
	   //} else if (%this.filterCustom() $= "electro") {
	      //%this.currentCustomElectroIndex = %index;
	   } else if (%this.filterCustom() $= "nature") {
	      %this.currentCustomNatureIndex = %index;
	   } else if (%this.filterCustom() $= "remix") {
	      %this.currentCustomRemixIndex = %index;
	   } else if (%this.filterCustom() $= "troll") {
	      %this.currentCustomTrollIndex = %index;
	   } else if (%this.filterCustom() $= "generalyeah") {
	      %this.currentCustomGeneralYeahIndex = %index;
	   }
	}
   //else if (%this.mode $= GameMissionInfo.GigaMode)
   //{
      //if (%this.filterDifficulty() $= "beginner")
         //%this.currentGigaBeginnerIndex = %index;
      //else if (%this.filterDifficulty() $= "intermediate")
        //%this.currentGigaIntermediateIndex = %index;
      //else if (%this.filterDifficulty() $= "advanced")
         //%this.currentGigaAdvancedIndex = %index;
      //else   
	      //%this.currentGigaBonusIndex = %index;
   //}
   else if (%this.mode $= GameMissionInfo.GoldMode)
   {
      if (%this.filterDifficulty() $= "beginner")
         %this.currentGoldBeginnerIndex = %index;
      else if (%this.filterDifficulty() $= "intermediate")
        %this.currentGoldIntermediateIndex = %index;
      else if (%this.filterDifficulty() $= "advanced")
         %this.currentGoldAdvancedIndex = %index;
      else   
	      %this.currentGoldBonusIndex = %index;
   }
   else if (%this.mode $= GameMissionInfo.MobileMode)
   {
      %this.currentMobileIndex = %index;
   }
   else if (%this.mode $= GameMissionInfo.EvolvedMode)
   {
      if (%this.filterDifficulty() $= "beginner")
         %this.currentEvolvedBeginnerIndex = %index;
      else if (%this.filterDifficulty() $= "intermediate")
        %this.currentEvolvedIntermediateIndex = %index;
      else if (%this.filterDifficulty() $= "advanced")
         %this.currentEvolvedAdvancedIndex = %index;
      else   
	      %this.currentEvolvedBonusIndex = %index;
   }
   else
	   %this.currentSPIndex = %index;
}

function GameMissionInfo::getCurrentIndex(%this)
{
	if (%this.mode $= GameMissionInfo.MPMode)
	return %this.currentMPIndex;
	else if (%this.mode $= GameMissionInfo.MPCMode)
	return %this.currentMPCIndex;
	else if (%this.mode $= GameMissionInfo.SpecialMode)
	return %this.currentSpecialIndex;
	else if (%this.mode $= GameMissionInfo.CMode)
   {
      if (%this.filterCustom() $= "matt")
	   {
	      if (%this.filterCustomDifficulty() $= "beginner") {
	         return %this.currentCustomMattBeginnerIndex;
	      } else if (%this.filterCustomDifficulty() $= "intermediate") {
	         return %this.currentCustomMattIntermediateIndex;
	      } else if (%this.filterCustomDifficulty() $= "advanced") {
	         return %this.currentCustomMattAdvancedIndex;
	      } else {
	         return %this.currentCustomMattBonusIndex;
	      }
	// enable Electro
	   //} else if (%this.filterCustom() $= "electro") {
	      //return %this.currentCustomElectroIndex;
	   } else if (%this.filterCustom() $= "nature") {
	      return %this.currentCustomNatureIndex;
	   } else if (%this.filterCustom() $= "remix") {
	      return %this.currentCustomRemixIndex;
	   } else if (%this.filterCustom() $= "troll") {
	      return %this.currentCustomTrollIndex;
	   } else if (%this.filterCustom() $= "generalyeah") {
	      return %this.currentCustomGeneralYeahIndex;
	   }
   }
   //else if (%this.mode $= GameMissionInfo.GigaMode)
   //{  
      //if (%this.filterDifficulty() $= "beginner")
         //return %this.currentGigaBeginnerIndex;
      //else if (%this.filterDifficulty() $= "intermediate")
        //return %this.currentGigaIntermediateIndex;
      //else if (%this.filterDifficulty() $= "advanced")
         //return %this.currentGigaAdvancedIndex;
      //else  
	      //return %this.currentGigaBonusIndex;
   //}
   else if (%this.mode $= GameMissionInfo.GoldMode)
   {  
      if (%this.filterDifficulty() $= "beginner")
         return %this.currentGoldBeginnerIndex;
      else if (%this.filterDifficulty() $= "intermediate")
        return %this.currentGoldIntermediateIndex;
      else if (%this.filterDifficulty() $= "advanced")
         return %this.currentGoldAdvancedIndex;
      else  
	      return %this.currentGoldBonusIndex;
   }
   else if (%this.mode $= GameMissionInfo.MobileMode)
   {
      return %this.currentMobileIndex;
   }
   else if (%this.mode $= GameMissionInfo.EvolvedMode)
   {  
      if (%this.filterDifficulty() $= "beginner")
         return %this.currentEvolvedBeginnerIndex;
      else if (%this.filterDifficulty() $= "intermediate")
        return %this.currentEvolvedIntermediateIndex;
      else if (%this.filterDifficulty() $= "advanced")
         return %this.currentEvolvedAdvancedIndex;
      else  
	      return %this.currentEvolvedBonusIndex;
   }
   else
		return %this.currentSPIndex;
}

function GameMissionInfo::getGameModeDisplayName(%this, %modeId)
{
	switch (%modeId)
	{
	case 0:
		return "Gem Hunt"; //$Text::GameModeScrum;
   case 1:
      return "Sumo";
   case 2:
      return "Race";
   case 3:
      return "Gems Only";
	}
	error("GameMissionInfo::getGameModeDisplayName: unknown game mode:" SPC %modeId);
	return "";
}

function GameMissionInfo::getGameModeDisplayList(%this)
{
	return %this.getGameModeDisplayName(0);
}

function GameMissionInfo::getGameModeIdFromMultiplayerMission(%this, %missionId)
{
	// we suck so we have to do a linear search for the missionId
	%group = GameMissionInfo.getMode();//MultiPlayMissionGroup;

	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mission = %group.getObject(%i);
		//if (%mission.level == %missionId)
		if (%i + 1 == %missionId)
		return %this.getGameModeIdFromString(%mission.gameMode);
	}
	
	return -1;
}

function GameMissionInfo::getGameModeIdFromString(%this, %modeString)
{
	if (stricmp(%modeString, "scrum") == 0)
	return 0;    
	if (stricmp(%modeString, "sumo") == 0)
	return 1;
	if (stricmp(%modeString, "race") == 0)
	return 2;
	if (stricmp(%modeString, "gems") == 0)
	return 3;        

	return -1;
}

function GameMissionInfo::getValidIndexForDifficulty(%this, %index)
{
	if( %this.filterDifficulty() !$= "" )
	{
		%group = %this.getCurrentMissionGroup();
		%mission = %group.getObject( %index );

      %tries = 0;
		//%display = 1;
		%broken = false;

		while ( %mission.type !$= %this.filterDifficulty() || %mission.game !$= %this.filterGame() )
		{
         // Hacky Fix
		   if (%tries > 5000)
		   {
		      %broken = true;
		      break;
		   }		   
		   
			if (%goNeg)
			%index--;
			else
			%index++;
			
			if( %index < 0 )
			%index = %this.getCurrentMissionGroup().numAddedMissions - 1;
			else if ( %index > %this.getCurrentMissionGroup().numAddedMissions - 1 )
			%index = 0;
			
			%mission = %group.getObject( %index );
			
         //if (%mission.type $= %this.filterDifficulty() && %mission.game $= %this.filterGame())
			//{
			//   %mission.displayIndex = %display;
			//   %display++;
			//}
			
			%tries++;
		}
	}
	if (%broken)
	{
	   //$previewImage = "./urban";
        RootGui.setContent(DifficultySelectGui);
        XMessagePopupDlg.show(0, "No Levels!", $Text::OK, "","", "");
	   return -1;
   } else
	   return %index;
}

function GameMissionInfo::getBitmap(%this)
{
	return $previewImage;
}

function GameMissionInfo::setBitmap(%this, %mission)
{
	$previewImage = strreplace(%mission.file, ".mis", "");
	%i = strstr($previewImage, "marble/data/missions/");
	%len = strlen($previewImage);
	if (%i >= 0 && %len >= 0)
		$previewImage = getSubStr($previewImage, %i, %len);
}

function GameMissionInfo::selectMission(%this, %index)
{

	// Need to determine which way to go for demo/filtered levels
	%goNeg = false;
	if (%index < %this.getCurrentIndex())
	%goNeg = true;

	if( %index < 0 )
	%index = %this.getCurrentMissionGroup().numAddedMissions - 1;
	else if ( %index > %this.getCurrentMissionGroup().numAddedMissions - 1 )
	%index = 0;

	if( %this.filterDifficulty() !$= "")
	{
		%group = %this.getCurrentMissionGroup();
		%mission = %group.getObject( %index );
		
		%tries = 0;
		//%display = 1;
		%broken = false;

		while ( %mission.type !$= %this.filterDifficulty() || %mission.game !$= %this.filterGame() )
		{
		   // Hacky Fix
		   if (%tries > 5000)
		   {
		      %broken = true;
		      break;
		   }
			if (%goNeg)
			%index--;
			else
			%index++;
			
			if( %index < 0 )
			%index = %this.getCurrentMissionGroup().numAddedMissions - 1;
			else if ( %index > %this.getCurrentMissionGroup().numAddedMissions - 1 )
			%index = 0;
			
			%mission = %group.getObject( %index );
			
			//if (%mission.type $= %this.filterDifficulty() && %mission.game $= %this.filterGame())
			//{
			//   %mission.displayIndex = %display;
			//   %display++;
			//}
			
			%tries++;
		}
	}

   if (!%broken)
   {

      %this.setCurrentIndex(%index);

      %this.setBitmap(%mission);
	
   } else {
         //$previewImage = "./urban";
        RootGui.setContent(DifficultySelectGui);
        XMessagePopupDlg.show(0, "No Levels!", $Text::OK, "","", "");
   }
}

function GameMissionInfo::setCamera(%this)
{
	$previewCamera.setTransform(%this.getCurrentMission().cameraPos);
}

function GameMissionInfo::selectPreviousMission(%this)
{
	%this.selectMission(%this.getCurrentIndex() - 1);
}

function GameMissionInfo::selectNextMission(%this)
{
	%this.selectMission(%this.getCurrentIndex() + 1);
}

function GameMissionInfo::getDefaultMission(%this)
{
	%mission = %this.findMissionByPath($Server::MissionFile);
	if (!isObject(%mission))
	{
		// invalid mission for current mode.  
		// if an index is set for the current mode, load that mission
		%index = %this.getCurrentIndex();
		if (%index == -1)
		// use first mission
		%index = 0;
		
		%group = %this.getCurrentMissionGroup();
		%mission = %group.getObject(%index);
	}

	return %mission.file;
}

function GameMissionInfo::setDefaultMission(%this,%missionId) // note: %missionId parameter broken by FMS
{
	// don't do this in test level mode
	if ($testLevel)
	return;
	
	// if they specified a mission Id, see if we can find it
	%mission = 0;
	if (%missionId !$= "")
	%mission = %this.findMissionById(%missionId);
	
	// if we have a mission object, make sure the server is running that mission
	if (isObject(%mission))
	{
		//if (%mission.file !$= "" && $Server::MissionFile !$= %mission.file)
		//loadMission(%mission.file);
	}
	else
	{
		// no mission object found.  if the server is not running a valid mission for this mode,
		// we'll need to switch it
		
		%mission = %this.findMissionByPath($Server::MissionFile);
		if (!isObject(%mission))
		{
			// invalid mission for current mode.  
			// if an index is set for the current mode, load that mission
			%index = %this.getCurrentIndex();
			if (%index == -1)
			%index = %this.getValidIndexForDifficulty(0);
			else // make sure difficulty is correct
			%index = %this.getValidIndexForDifficulty(%this.getCurrentIndex());
			
			%group = %this.getCurrentMissionGroup();
			%mission = %group.getObject(%index);
			//loadMission(%mission.file);
		}
		else
		{
			// valid mission, but if we have a difficulty filter it may be inappropriate.  if so move 
			// forward until we find a mission with the right difficulty
			%index = %this.getValidIndexForDifficulty(%this.getCurrentIndex());
			if (%index != %this.getCurrentIndex())
			{
				echo("Mission does not match difficulty, switching");
				%group = %this.getCurrentMissionGroup();
				%mission = %group.getObject(%index);
				//loadMission(%mission.file);
			}
		}
	}

	// should always have a mission object at this point
	if (!isObject(%mission))
	error("setDefaultMission: I don't have a mission object and I should");

	// update current mission
	%this.setCurrentMission(%mission.file);

	return %mission;
}

// the ordering of the list returned by this function must match getMissionDisplayNameList below
function GameMissionInfo::getMissionIdList(%this)
{
	%group = %this.getCurrentMissionGroup();
	%list = "";
	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mission = %group.getObject(%i);
		
		if( (%this.filterDifficulty() $= "" || %this.filterDifficulty() $= %mission.type) && %this.filterGame() $= %mission.game )
		{
			%list = %list @ %mission.level;
			if (%i < %group.getCount() - 1)
			%list = %list @ "\t";
		}
	}
	return %list;
}

// the ordering of the list returned by this function must match getMissionDisplayNameList above
function GameMissionInfo::getMissionDisplayNameList(%this)
{
	%group = %this.getCurrentMissionGroup();
	%list = "";
	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mission = %group.getObject(%i);
		
		if( (%this.filterDifficulty() $= "" || %this.filterDifficulty() $= %mission.type) && %this.filterGame() $= %mission.game )
		{
			%list = %list @ %mission.name;
			if (%i < %group.getCount() - 1)
			%list = %list @ "\t";
		}
	}
	return %list;
}

function GameMissionInfo::getMissionDisplayName(%this, %missionId)
{
	%mission = %this.findMissionById(%missionId);
	if (!isObject(%mission))
	return "";
	else
	return %mission.name;
}

// returns true if there was a tie, false otherwise
function setRelativeRanks(%gameModeId, %textListCtrl)
{
	if (!isObject(%textListCtrl))
	{
		error("Parameter is not a text list control:" SPC %textListCtrl);
		return 0;
	}

	// sort by score first
	echo("setRelativeRanks: Sorting scores descending");
	%textListCtrl.sortNumerical(0, false);

	%rank = %textListCtrl.rowCount() - 1;
	%rankDecr = 1;
	for ( %i = 0; %i < %textListCtrl.rowCount() - 1; %i++ )
	{
		%rowid = %textListCtrl.getRowId(%i);
		%val = %textListCtrl.getRowText(%i);

		// Set the rank
		%textListCtrl.setRowById(%rowid, %rank SPC %val);

		// Get the next row and decide if we need to increment rank
		%nextval = %textListCtrl.getRowText(%i + 1);

		if (%val != %nextval)
		{
			// score is different, so decrement the rank by the current decrement value
			// then reset the rank decrement to 1
			%rank -= %rankDecr;
			%rankDecr = 1;
		}
		else
		{
			// increase the rank decrement for each tied player
			%rankDecr++;
		}
	}

	// Set the last
	%rowid = %textListCtrl.getRowId(%textListCtrl.rowCount() - 1);
	%val = %textListCtrl.getRowText(%textListCtrl.rowCount() - 1);
	%textListCtrl.setRowById(%rowid, %rank SPC %val);

	%tied = false;

	// Detect a tie for first
	if (%textListCtrl.rowCount() > 1)
	{
		if (getWord(%textListCtrl.getRowText(0), 0) == getWord(%textListCtrl.getRowText(1), 0))      
		%tied = true;
	}

	// Dump %textListCtrl for debugging
	//   error("Dumping Ranks:");
	//   for ( %i = 0; %i < %textListCtrl.rowCount(); %i++ )
	//   {
	//      %clid = %textListCtrl.getRowId(%i);
	//      %val = %textListCtrl.getRowText(%i);
	//
	//      error(%clid SPC %val);
	//   }

	return %tied;
}


function buildLeaderboardList()
{
	// start sp list with overall leaderboard
	%spList = $Leaderboard::SPOverall;
	
	%group = GameMissionInfo.getCurrentMissionGroup();

	// append all of the sp missions
	for (%i = 0; %i < %group.getCount(); %i++)
	{
		%mis = %group.getObject(%i);
		%spList = %spList TAB %i + 1; //%mis.level;
	}

	// store the list as a property of the mission group
	%group.lbList = %spList;
	%group.lbListCount = getFieldCount(%spList);

	// start mp list with overall leaderboards
	%mpList = 
	$Leaderboard::MPScrumOverall TAB $Leaderboard::MPScrumSkill;
	
	// JMQ: disable MP level leaderboards
	// append all of the mp missions
	//   for (%i = 0; %i < MultiPlayMissionGroup.getCount(); %i++)
	//   {
	//      %mis = MultiPlayMissionGroup.getObject(%i);
	//      %mpList = %mpList TAB %mis.level;
	//   }

	// store the list as a property of the mission group
	MultiPlayMissionGroup.lbList = %mpList;
	MultiPlayMissionGroup.lbListCount = getFieldCount(%mpList);
}

function buildMissionList()
{
	if( !isObject( SinglePlayMissionGroup ) || !isObject( MultiPlayMissionGroup ) || !isObject( SpecialMissionGroup ) )
	{
		if( !isObject( SinglePlayMissionGroup ) )
		{
			new SimGroup( SinglePlayMissionGroup );
			RootGroup.add( SinglePlayMissionGroup );
		}
		
		if( !isObject( MultiPlayMissionGroup ) )
		{
			new SimGroup( MultiPlayMissionGroup );
			RootGroup.add( MultiPlayMissionGroup );
		}

      if( !isObject( MultiPlayCustomMissionGroup ) )
		{
			new SimGroup( MultiPlayCustomMissionGroup );
			RootGroup.add( MultiPlayCustomMissionGroup );
		}		
		
		if( !isObject( SpecialMissionGroup ) )
		{
			new SimGroup( SpecialMissionGroup );
			RootGroup.add( SpecialMissionGroup );
		}
		
		//if( !isObject( GigaBeginnerMissionGroup ) )
		//{
			//new SimGroup( GigaBeginnerMissionGroup );
			//RootGroup.add( GigaBeginnerMissionGroup );
		//}
		//
		//if( !isObject( GigaIntermediateMissionGroup ) )
		//{
			//new SimGroup( GigaIntermediateMissionGroup );
			//RootGroup.add( GigaIntermediateMissionGroup );
		//}
		//
		//if( !isObject( GigaAdvancedMissionGroup ) )
		//{
			//new SimGroup( GigaAdvancedMissionGroup );
			//RootGroup.add( GigaAdvancedMissionGroup );
		//}
		//
		//if( !isObject( GigaBonusMissionGroup ) )
		//{
			//new SimGroup( GigaBonusMissionGroup );
			//RootGroup.add( GigaBonusMissionGroup );
		//}
		
		if( !isObject( GoldBeginnerMissionGroup ) )
		{
			new SimGroup( GoldBeginnerMissionGroup );
			RootGroup.add( GoldBeginnerMissionGroup );
		}
		
		if( !isObject( GoldIntermediateMissionGroup ) )
		{
			new SimGroup( GoldIntermediateMissionGroup );
			RootGroup.add( GoldIntermediateMissionGroup );
		}
		
		if( !isObject( GoldAdvancedMissionGroup ) )
		{
			new SimGroup( GoldAdvancedMissionGroup );
			RootGroup.add( GoldAdvancedMissionGroup );
		}
		
		if( !isObject( GoldBonusMissionGroup ) )
		{
			new SimGroup( GoldBonusMissionGroup );
			RootGroup.add( GoldBonusMissionGroup );
		}
		
		if( !isObject( MobileMissionGroup ) )
		{
			new SimGroup( MobileMissionGroup );
			RootGroup.add( MobileMissionGroup );
		}
		
		if( !isObject( TemplateMissionGroup ) )
		{
			new SimGroup( TemplateMissionGroup );
			RootGroup.add( TemplateMissionGroup );
		}
		
		if( !isObject( CustomGeneralYeahMissionGroup ) )
		{
			new SimGroup( CustomGeneralYeahMissionGroup );
			RootGroup.add( CustomGeneralYeahMissionGroup );
		}
		
		if( !isObject( CustomMattMissionGroup ) )
		{
			new SimGroup( CustomMattMissionGroup );
			RootGroup.add( CustomMattMissionGroup );
		}
		
	// enable Electro
		//if( !isObject( CustomElectroMissionGroup ) )
		//{
			//new SimGroup( CustomElectroMissionGroup );
			//RootGroup.add( CustomElectroMissionGroup );
		//}
		
		if( !isObject( CustomNatureMissionGroup ) )
		{
			new SimGroup( CustomNatureMissionGroup );
			RootGroup.add( CustomNatureMissionGroup );
		}
		
		if( !isObject( CustomRemixMissionGroup ) )
		{
			new SimGroup( CustomRemixMissionGroup );
			RootGroup.add( CustomRemixMissionGroup );
		}
		
		if( !isObject( CustomTrollMissionGroup ) )
		{
			new SimGroup( CustomTrollMissionGroup );
			RootGroup.add( CustomTrollMissionGroup );
		}
		
		if( !isObject( CustomMattBeginnerMissionGroup ) )
		{
			new SimGroup( CustomMattBeginnerMissionGroup );
			RootGroup.add( CustomMattBeginnerMissionGroup );
		}
		
		if( !isObject( CustomMattIntermediateMissionGroup ) )
		{
			new SimGroup( CustomMattIntermediateMissionGroup );
			RootGroup.add( CustomMattIntermediateMissionGroup );
		}
		
		if( !isObject( CustomMattAdvancedMissionGroup ) )
		{
			new SimGroup( CustomMattAdvancedMissionGroup );
			RootGroup.add( CustomMattAdvancedMissionGroup );
		}
		
		if( !isObject( CustomMattBonusMissionGroup ) )
		{
			new SimGroup( CustomMattBonusMissionGroup );
			RootGroup.add( CustomMattBonusMissionGroup );
		}
		
		if( !isObject( EvolvedBeginnerMissionGroup ) )
		{
			new SimGroup( EvolvedBeginnerMissionGroup );
			RootGroup.add( EvolvedBeginnerMissionGroup );
		}
		
		if( !isObject( EvolvedIntermediateMissionGroup ) )
		{
			new SimGroup( EvolvedIntermediateMissionGroup );
			RootGroup.add( EvolvedIntermediateMissionGroup );
		}
		
		if( !isObject( EvolvedAdvancedMissionGroup ) )
		{
			new SimGroup( EvolvedAdvancedMissionGroup );
			RootGroup.add( EvolvedAdvancedMissionGroup );
		}
		
		if( !isObject( EvolvedBonusMissionGroup ) )
		{
			new SimGroup( EvolvedBonusMissionGroup );
			RootGroup.add( EvolvedBonusMissionGroup );
		}

		for(%file = findFirstFile ( GameMissionInfo.MissionFileSpec );
		%file !$= ""; %file = findNextFile( GameMissionInfo.MissionFileSpec ) )
		{ 
			if (strStr(%file, "CVS/") == -1 && strStr(%file, "common/") == -1 && strStr(%file, "testMission.mis") == -1 &&
					strStr(%file, "megaEmpty.mis") == -1 && strStr(%file, "megaMission.mis") == -1)
			{
				getMissionObject(%file);
			}
		}

		sortByLevel( SinglePlayMissionGroup );
		sortByLevel( MultiPlayMissionGroup );
		sortByLevel( MultiPlayCustomMissionGroup );
		sortByLevel( SpecialMissionGroup );
		//sortByLevel( GigaBeginnerMissionGroup );
		//sortByLevel( GigaIntermediateMissionGroup );
		//sortByLevel( GigaAdvancedMissionGroup );
		//sortByLevel( GigaBonusMissionGroup );
		sortByLevel( GoldBeginnerMissionGroup );
		sortByLevel( GoldIntermediateMissionGroup );
		sortByLevel( GoldAdvancedMissionGroup );
		sortByLevel( GoldBonusMissionGroup );
		sortByLevel( MobileMissionGroup );		
		sortByLevel( CustomGeneralYeahMissionGroup );
		sortByLevel( CustomMattMissionGroup );
	// enable Electro
		//sortByLevel( CustomElectroMissionGroup );
		sortByLevel( CustomNatureMissionGroup );
		sortByLevel( CustomRemixMissionGroup );
		sortByLevel( CustomTrollMissionGroup );
		sortByLevel( CustomMattBeginnerMissionGroup );
		sortByLevel( CustomMattIntermediateMissionGroup );
		sortByLevel( CustomMattAdvancedMissionGroup );
		sortByLevel( CustomMattBonusMissionGroup );
		sortByLevel( EvolvedBeginnerMissionGroup );
		sortByLevel( EvolvedIntermediateMissionGroup );
		sortByLevel( EvolvedAdvancedMissionGroup );
		sortByLevel( EvolvedBonusMissionGroup );
		// hack, do this twice to get things into the proper order, don't have time to figure out why a 
		// single sort doesn't work
		sortByLevel( SinglePlayMissionGroup );
		sortByLevel( MultiPlayMissionGroup );
		sortByLevel( MultiPlayCustomMissionGroup );
		sortByLevel( SpecialMissionGroup );
		//sortByLevel( GigaBeginnerMissionGroup );
		//sortByLevel( GigaIntermediateMissionGroup );
		//sortByLevel( GigaAdvancedMissionGroup );
		//sortByLevel( GigaBonusMissionGroup );
		sortByLevel( GoldBeginnerMissionGroup );
		sortByLevel( GoldIntermediateMissionGroup );
		sortByLevel( GoldAdvancedMissionGroup );
		sortByLevel( GoldBonusMissionGroup );
		sortByLevel( MobileMissionGroup );		
		sortByLevel( CustomGeneralYeahMissionGroup );
		sortByLevel( CustomMattMissionGroup );
	// enable Electro
		//sortByLevel( CustomElectroMissionGroup );
		sortByLevel( CustomNatureMissionGroup );
		sortByLevel( CustomRemixMissionGroup );
		sortByLevel( CustomTrollMissionGroup );
		sortByLevel( CustomMattBeginnerMissionGroup );
		sortByLevel( CustomMattIntermediateMissionGroup );
		sortByLevel( CustomMattAdvancedMissionGroup );
		sortByLevel( CustomMattBonusMissionGroup );
		sortByLevel( EvolvedBeginnerMissionGroup );
		sortByLevel( EvolvedIntermediateMissionGroup );
		sortByLevel( EvolvedAdvancedMissionGroup );
		sortByLevel( EvolvedBonusMissionGroup );
		
		// verify that level Ids are unique
		//GameMissionInfo.dupErrors = "";
		//for (%i = 0; %i < SinglePlayMissionGroup.getCount(); %i++)
		//{
			//%mis = SinglePlayMissionGroup.getObject(%i);
			//if (%levelIds[%mis.level] !$= "")
			//{
				//GameMissionInfo.dupErrors = GameMissionInfo.dupErrors @ "duplicate mission Id for level:" SPC %mis.file @ "\n";
				//GameMissionInfo.dupLevelIds[%mis.level] = true;
			//}
			//%levelIds[%mis.level] = true;
		//}
		//for (%i = 0; %i < MultiPlayMissionGroup.getCount(); %i++)
		//{
			//%mis = MultiPlayMissionGroup.getObject(%i);
			//if (%levelIds[%mis.level] !$= "")
			//{
				//GameMissionInfo.dupErrors = GameMissionInfo.dupErrors @ "duplicate mission Id for level:" SPC %mis.file @ "\n";
				//GameMissionInfo.dupLevelIds[%mis.level] = true;
			//}
			//%levelIds[%mis.level] = true;
		//}
		//
		//for (%i = 0; %i < SpecialMissionGroup.getCount(); %i++)
		//{
			//%mis = SpecialMissionGroup.getObject(%i);
			//if (%levelIds[%mis.level] !$= "")
			//{
				//GameMissionInfo.dupErrors = GameMissionInfo.dupErrors @ "duplicate mission Id for level:" SPC %mis.file @ "\n";
				//GameMissionInfo.dupLevelIds[%mis.level] = true;
			//}
			//%levelIds[%mis.level] = true;
		//}
		//
		//if (GameMissionInfo.dupErrors !$= "")
		//error(GameMissionInfo.dupErrors);
	}
}

function isDemoMission( %level ) 
{
	error( "ISDEMOMISSION DEPRICATED -- Use 'isInDemoMode = \"1\";' in mission info instead!" );
	// if( %level == 1 || %level == 3 || %level == 27 || %level == 38 || %level == 49 || %level == 75 )
	//    return 1;
	//else if( %level == 70 || %level == 52 || %level == 87 )
	//return 2;

	//return 0;
}

function sortByLevel(%grp)
{
	%grp.numAddedMissions = 0;
	%newLevelNum = 0;
	%ngrp = new SimGroup();
	// take all the objects out of grp and put them in ngrp
	while((%obj = %grp.getObject(0)) != -1)
	   %ngrp.add(%obj);
	while(%ngrp.getCount() != 0)
	{
		//%lowestDif = %ngrp.getObject(0).difficulty;
		%lvl = %ngrp.getObject(0).level;
		//if (%ngrp.getObject(0).displayLevel !$= "")
		//   %lvl = %ngrp.getObject(0).displayLevel;
      //if (%ngrp.getObject(0).type $= "custom")
      //   %lvl = %lvl + 999;
		%lowestLevel = %lvl;
		
		%lowestIndex = 0;
		for(%i = 1; %i < %ngrp.getCount(); %i++)
		{
			//%dif = %ngrp.getObject(%i).difficulty;
			%lvl2 = %ngrp.getObject(%i).level;
		   //if (%ngrp.getObject(%i).displayLevel !$= "")
		   //   %lvl2 = %ngrp.getObject(%i).displayLevel;
         //if (%ngrp.getObject(%i).type $= "custom")
         //%lvl2 = %lvl2 + 999;
			%level = %lvl2;
			
			//if( %dif < %lowestDif )
			//{
			//   %lowestDif = %dif;
			//   %lowestIndex = %i;
			//}
			//else if( %dif == %lowestDif )
			//{
			if( %lowestLevel > %level )
			{
				%lowestIndex = %i;
				%lowestLevel = %level;
			}
			//}
		}

		%newLevelNum++;
		
		//if( isDemoLaunch() && isDemoMission( %newLevelNum ) == 0 )
		//{
		//   %obj = %ngrp.getObject(%lowestIndex);
		//   %ngrp.remove(%obj);
		//}
		//else
		//{
		%grp.numAddedMissions++;
		%obj = %ngrp.getObject(%lowestIndex);
		//%obj.level = %newLevelNum;
		%grp.add(%obj);
		//}
	}
	%ngrp.delete();
}

function getMissionObject( %missionFile ) 
{
	%file = new FileObject();

	%missionInfoObject = "";

	if ( %file.openForRead( %missionFile ) ) {
		%inInfoBlock = false;
		
		while ( !%file.isEOF() ) {
			%line = %file.readLine();
			%line = trim( %line );
			
			if( %line $= "new ScriptObject(MissionInfo) {" ) {
				%line = "new ScriptObject() {";
				%inInfoBlock = true;
			}
			else if( %inInfoBlock && %line $= "};" ) {
				%inInfoBlock = false;
				%missionInfoObject = %missionInfoObject @ %line; 
				break;
			}
			
			if( %inInfoBlock )
			{
				%missionInfoObject = %missionInfoObject @ %line @ " "; 	
				
				//error("checking line:" SPC %line);
				//if (strpos(%missionfile, "marblepicker") == -1 && 
				//      (strpos(%line, "name =") != -1 || strpos(%line, "startHelpText =") != -1) &&
				//      strpos(%line, "$Text::") == -1)
				//{
				//   GameMissionInfo.dupErrors = GameMissionInfo.dupErrors @ "Bad loc info in" SPC %missionFile @ "\n";
				//}
			}
		}
		
		%file.close();
		%file.delete();
	}
	//echo("checking mission" SPC %missionFile);
	%missionInfoObject = "%missionInfoObject = " @ %missionInfoObject;
	eval( %missionInfoObject );
	
	if (!%missionInfoObject.include)
	{
		echo("skipping mission because include field is not true:" SPC %missionInfoObject.name);
		%missionInfoObject.delete();
		return;
	}

	// All missions available to view, but not to play, in demo mode -pw
	//if( isDemoLaunch() && !%missionInfoObject.isInDemoMode )
	//{
	//   echo( "skipping mission because isInDemoMode field is not true:" SPC %missionInfoObject.name );
	//   %missionInfoObject.delete();
	//   return;
	//}
	
	// find the directory this file belongs in:
	%path = filePath(%missionFile);
	%misPath = filePath(%path);
	
	%index = strpos(%missionFile, "marble/data/missions/");
	
   %gameFile = getSubStr(%missionFile, %index, "8092");	
	
	//%id = strreplace(%gameFile, "/", "");
	//%id = strreplace(%id, " ", "");
	
	%gameFile = stripChars(%gameFile, "\\/ .-");
	
	%missionInfoObject.id = %gameFile;

   if (%missionInfoObject.customFilter $= "")
   {
      %missionInfoObject.customFilter = "generalyeah";
   }
   
   if (%missionInfoObject.customType $= "")
   {
      %missionInfoObject.customType = "bonus";
   }

	// So we can sort by difficulty
	if( %misPath $= "marble/data/missions/beginner" )
	%missionInfoObject.type = "beginner";
	else if( %misPath $= "marble/data/missions/intermediate" )
	%missionInfoObject.type = "intermediate";
	else if( %misPath $= "marble/data/missions/advanced" )
	%missionInfoObject.type = "advanced";
	else if( %misPath $= "marble/data/missions/bonus" )
	%missionInfoObject.type = "bonus";
	else if( %misPath $= "marble/data/missions/custom" )
	%missionInfoObject.type = "custom";
	else if( %misPath $= "marble/data/missions/special" )
	%missionInfoObject.type = "special";
	
	if (%missionInfoObject.game $= "")
	   %missionInfoObject.game = "Ultra";

	if (%missionInfoObject.gameType $= "")
	%missionInfoObject.gameType = GameMissionInfo.SPMode;
	
	if (%missionInfoObject.type $= "template")
	{
	   TemplateMissionGroup.add(%missionInfoObject);
	}
   
	//else if (%missionInfoObject.game $= "giga" && %missionInfoObject.gameType !$= GameMissionInfo.MPMode)
	//{
	   //if (%missionInfoObject.type $= "beginner")
	      //GigaBeginnerMissionGroup.add(%missionInfoObject);
      //else if (%missionInfoObject.type $= "intermediate")
	      //GigaIntermediateMissionGroup.add(%missionInfoObject);
      //else if (%missionInfoObject.type $= "advanced")
	      //GigaAdvancedMissionGroup.add(%missionInfoObject);
      //else
	      //GigaBonusMissionGroup.add(%missionInfoObject);
	//}
	else if (%missionInfoObject.game $= "gold" && %missionInfoObject.gameType !$= GameMissionInfo.MPMode && %missionInfoObject.gameType !$= GameMissionInfo.MPCMode)
	{
	   if (%missionInfoObject.type $= "beginner")
	      GoldBeginnerMissionGroup.add(%missionInfoObject);
      else if (%missionInfoObject.type $= "intermediate")
	      GoldIntermediateMissionGroup.add(%missionInfoObject);
      else if (%missionInfoObject.type $= "advanced")
	      GoldAdvancedMissionGroup.add(%missionInfoObject);
      else
	      GoldBonusMissionGroup.add(%missionInfoObject);
	}
	else if (%missionInfoObject.game $= "mobile" && %missionInfoObject.gameType !$= GameMissionInfo.MPMode && %missionInfoObject.gameType !$= GameMissionInfo.MPCMode)
	{
      MobileMissionGroup.add(%missionInfoObject);
	}
	else if (%missionInfoObject.game $= "evolved" && %missionInfoObject.gameType !$= GameMissionInfo.MPMode && %missionInfoObject.gameType !$= GameMissionInfo.MPCMode)
	{
	   if (%missionInfoObject.type $= "beginner")
	      EvolvedBeginnerMissionGroup.add(%missionInfoObject);
      else if (%missionInfoObject.type $= "intermediate")
	      EvolvedIntermediateMissionGroup.add(%missionInfoObject);
      else if (%missionInfoObject.type $= "advanced")
	      EvolvedAdvancedMissionGroup.add(%missionInfoObject);
      else
	      EvolvedBonusMissionGroup.add(%missionInfoObject);
	}
	else if (%missionInfoObject.type $= "custom" && %missionInfoObject.gameType !$= GameMissionInfo.MPMode && %missionInfoObject.gameType !$= GameMissionInfo.MPCMode)
   {
      if (%missionInfoObject.customfilter $= "matt")
      {
         if (%missionInfoObject.customtype $= "beginner")
         {
            CustomMattBeginnerMissionGroup.add(%missionInfoObject);
         } else if (%missionInfoObject.customtype $= "intermediate")
         {
            CustomMattIntermediateMissionGroup.add(%missionInfoObject);
         } else if (%missionInfoObject.customtype $= "advanced")
         {
            CustomMattAdvancedMissionGroup.add(%missionInfoObject);
         } else
         {
            CustomMattBonusMissionGroup.add(%missionInfoObject);
         }
      }
	// enable Electro
      //else if (%missionInfoObject.customfilter $= "electro")
      //{
         //CustomElectroMissionGroup.add(%missionInfoObject);
      //}
      else if (%missionInfoObject.customfilter $= "nature")
      {
         CustomNatureMissionGroup.add(%missionInfoObject);
      }
      else if (%missionInfoObject.customfilter $= "remix")
      {
         CustomRemixMissionGroup.add(%missionInfoObject);
      }
      else if (%missionInfoObject.customfilter $= "troll")
      {
         CustomTrollMissionGroup.add(%missionInfoObject);
      }
      else if (%missionInfoObject.customfilter $= "generalyeah") {
         CustomGeneralYeahMissionGroup.add(%missionInfoObject);
      }
   }
	else if (%missionInfoObject.gameType $= GameMissionInfo.SPMode)
	SinglePlayMissionGroup.add(%missionInfoObject);
	else if (%missionInfoObject.gameType $= GameMissionInfo.MPMode && %missionInfoObject.official == 1)
	MultiPlayMissionGroup.add(%missionInfoObject);
	else if (%missionInfoObject.gameType $= GameMissionInfo.MPMode)
	MultiPlayCustomMissionGroup.add(%missionInfoObject);
	else if (%missionInfoObject.gameType $= GameMissionInfo.SpecialMode)
	SpecialMissionGroup.add(%missionInfoObject);

	//%missionInfoObject.type = %groupTab;
	%missionInfoObject.setName("");
	%missionInfoObject.file = %missionFile;
	%missionInfoObject.missionGroup = fileBase(%missionFile) @ "Group";

	if (%missionInfoObject.type $= "beginner")
	%missionInfoObject.sky = $sky_beginner;
	if (%missionInfoObject.type $= "custom")
	%missionInfoObject.sky = $sky_beginner;
	if (%missionInfoObject.type $= "intermediate")
	%missionInfoObject.sky = $sky_intermediate;
	if (%missionInfoObject.type $= "advanced")
	%missionInfoObject.sky = $sky_advanced;
	if (%missionInfoObject.type $= "bonus")
	%missionInfoObject.sky = $sky_beginner;
}
