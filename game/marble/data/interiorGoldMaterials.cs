//new Material(RubberFloorMaterial) {
//   friction = 1;
//   restitution = 1;
//   force = 0;
//};
//
//new Material(IceMaterial) {
//   friction = 0.05;
//   restitution = 0.5;
//   force = 0;
//};
//
//new Material(BumperMaterial) {
//   friction = 0.5;
//   restitution = 0;
//   force = 15;
//};
//
//new Material(ButtonMaterial) {
//   friction = 1;
//   restitution = 1;
//   force = 0;
//};

// ---------------------------------------------------------------------------
// Grid materials
// ---------------------------------------------------------------------------

/*%mat = new Material(Material_GridWarm1 : DefaultMaterial)
{
   mapTo = "grid_warm1";

   baseTex[0] = "./textures/gold/grid_warm1";
   bumpTex[0] = "./textures/gold/grid_4square.bump";
};

%mat = new Material(Material_GridWarm2 : DefaultMaterial)
{
   mapTo = "grid_warm2";
   baseTex[0] = "./textures/gold/grid_warm2";
   bumpTex[0] = "./textures/gold/grid_square.bump";
};

%mat = new Material(Material_GridWarm3 : DefaultMaterial)
{
   mapTo = "grid_warm3";
   baseTex[0] = "./textures/gold/grid_warm3";
   bumpTex[0] = "./textures/gold/grid_square.bump";
};

%mat = new Material(Material_grid_neutral3 : DefaultMaterial)
{
   mapTo = "grid_neutral3";
   baseTex[0] = "./textures/gold/grid_neutral3";
   bumpTex[0] = "./textures/gold/grid_square.bump";
};*/

// ---------------------------------------------------------------------------
// Edge, Wall, Stripe materials
// ---------------------------------------------------------------------------
/*%mat = new Material(Material_edge_white : DefaultMaterial)
{
   mapTo = "edge_white";
   baseTex[0] = "./textures/gold/edge_white";
   bumpTex[0] = "./textures/gold/edge_white.bump";
};

%mat = new Material(Material_edge_white2 : DefaultMaterial)
{
   mapTo = "edge_white2";
   baseTex[0] = "./textures/gold/edge_white2";
   bumpTex[0] = "./textures/gold/edge_white2.bump";
};

%mat = new Material(Material_wall_white : DefaultMaterial)
{
   mapTo = "wall_white";
   baseTex[0] = "./textures/gold/wall_white";
};

%mat = new Material(Material_wall_warm2 : DefaultMaterial)
{
   mapTo = "wall_warm2";
   baseTex[0] = "./textures/gold/wall_warm2";
   bumpTex[0] = "./textures/gold/wall.bump";
};*/

%mat = new Material(Material_stripe_caution : DefaultMaterial)
{
   mapTo = "stripe_caution";
   baseTex[0] = "./textures/standard/stripe_caution";
};

/*%mat = new Material(Material_stripe_warm2 : DefaultMaterial)
{
   mapTo = "stripe_warm2";
   baseTex[0] = "./textures/gold/stripe_warm2";
};*/


// ---------------------------------------------------------------------------
// Pattern materials
// ---------------------------------------------------------------------------
/*%mat = new Material(Material_pattern_cool2 : DefaultMaterial)
{
   mapTo = "pattern_cool2";
   baseTex[0] = "./textures/gold/pattern_cool2";
   bumpTex[0] = "./textures/gold/pattern_cool2.bump";
};

%mat = new Material(Material_pattern_warm3 : DefaultMaterial)
{
   mapTo = "pattern_warm3";
   baseTex[0] = "./textures/gold/pattern_warm3";
};

%mat = new Material(Material_pattern_warm4 : DefaultMaterial)
{
   mapTo = "pattern_warm4";
   baseTex[0] = "./textures/gold/pattern_warm4";
};

%mat = new Material(Material_chevron_warm : DefaultMaterial)
{
   mapTo = "chevron_warm";
   baseTex[0] = "./textures/gold/chevron_warm";
   bumpTex[0] = "./textures/gold/chevron_warm.bump";
};

%mat = new Material(Material_trim_warm2 : DefaultMaterial)
{
   mapTo = "trim_warm2";
   baseTex[0] = "./textures/gold/trim_warm2";
   bumpTex[0] = "./textures/gold/trim_warm2.bump";
};

%mat = new Material(Material_wall_neutral2 : DefaultMaterial)
{
   mapTo = "wall_neutral2";
   baseTex[0] = "./textures/gold/wall_neutral2";
   bumpTex[0] = "./textures/gold/wall_neutral2.bump";
};

%mat = new Material(grid_neutral : DefaultMaterial)
{
   mapTo = "grid_neutral";
   baseTex[0] = "./textures/gold/grid_neutral";
   bumpTex[0] = "./textures/gold/grid_neutral.bump";
};*/

//new ShaderData( ReflectBump )
//{
//   DXVertexShaderFile 	= "shaders/planarReflectBumpV.hlsl";
//   DXPixelShaderFile 	= "shaders/planarReflectBumpP.hlsl";
//   pixVersion = 2.0;
//};
//
//%mat = new CustomMaterial(Material_Grey)
//{
//   friction = 1;
//   restitution = 1;
//   force = 0;
//
//   texture[1] = "$backbuff";
//   texture[0] = "marble/data/interiors/grey";
//   texture[2] = "marble/data/interiors/noise.bump";
//   shader = ReflectBump;
//   version = 2.0;
//   planarReflection = true;
//};
//

/*%mat = new Material(Material_wall_neutral3 : DefaultMaterial)
{
   mapTo = "wall_neutral3";
   baseTex[0] = "./textures/gold/wall_neutral3";
   bumpTex[0] = "./textures/gold/wall.bump";
};

%mat = new Material(Material_grid_neutral1 : DefaultMaterial)
{
   mapTo = "grid_neutral1";
   baseTex[0] = "./textures/gold/grid_neutral1";
   bumpTex[0] = "./textures/gold/grid_square.bump";
};

%mat = new Material(Pattern_Cool1 : DefaultMaterial)
{
   mapTo = "pattern_cool1";
   baseTex[0] = "./textures/gold/pattern_cool1";
};

%mat = new Material(Pattern_Warm1 : DefaultMaterial)
{
   mapTo = "pattern_warm1";
   baseTex[0] = "./textures/gold/pattern_warm1";
};

%mat = new Material(Grid_Cool2 : DefaultMaterial)
{
   mapTo = "grid_cool2";
   baseTex[0] = "./textures/gold/grid_cool2";
};

%mat = new Material(Trim_Cool1 : DefaultMaterial)
{
   mapTo = "trim_cool1";
   baseTex[0] = "./textures/gold/trim_cool1";
};

%mat = new Material(Solid_Neutral2 : DefaultMaterial)
{
   mapTo = "solid_neutral2";
   baseTex[0] = "./textures/gold/solid_neutral2";
};

%mat = new Material(Wall_Cool1 : DefaultMaterial)
{
   mapTo = "Wall_Cool1";
   baseTex[0] = "./textures/gold/Wall_Cool1";
};

%mat = new Material(Solid_Warm2 : DefaultMaterial)
{
   mapTo = "solid_warm2";
   baseTex[0] = "./textures/gold/solid_warm2";
};

%mat = new Material(Grid_Warm : DefaultMaterial)
{
   mapTo = "grid_warm";
   baseTex[0] = "./textures/gold/grid_warm";
};

%mat = new Material(Grid_Neutral4 : DefaultMaterial)
{
   mapTo = "grid_neutral4";
   baseTex[0] = "./textures/gold/grid_neutral4";
};

%mat = new Material(Trim_Cool3 : DefaultMaterial)
{
   mapTo = "trim_cool3";
   baseTex[0] = "./textures/gold/trim_cool3";
};*/

%mat = new Material (Material_mbg_white)
{
	mapTo = mbg_edge_white;
	baseTex = "./textures/gold/mbg_edge_white";
	bumpTex = "./textures/gold/mbg_edge_white.bump";
};

%mat = new Material (Material_mbg_white2)
{
	mapTo = mbg_edge_white2;
	baseTex = "./textures/gold/mbg_edge_white2";
	bumpTex = "./textures/gold/mbg_edge_white2.bump";
};

%mat = new Material (Material_grid_cool2)
{
	mapTo = grid_cool2;
	baseTex = "./textures/gold/grid_cool2";
	bumpTex = "./textures/gold/grid_circle.bump";
};

%mat = new Material (Material_grid_neutral4)
{
	mapTo = grid_neutral4;
	baseTex = "./textures/gold/grid_neutral4";
	bumpTex = "./textures/gold/grid_triangle.bump";
};

%mat = new Material (Material_trim_warm2)
{
	mapTo = trim_warm2;
	baseTex = "./textures/gold/trim_warm2";
	bumpTex = "./textures/gold/trim_warm2.bump";
};

%mat = new Material (Material_solid_warm2)
{
	mapTo = solid_warm2;
	baseTex = "./textures/gold/solid_warm2";
};

%mat = new Material (Material_grid_warm)
{
	mapTo = grid_warm;
	baseTex = "./textures/gold/grid_warm";
	bumpTex = "./textures/gold/grid_circle.bump";
};

%mat = new Material (Material_pattern_cool2)
{
	mapTo = pattern_cool2;
	baseTex = "./textures/gold/pattern_cool2";
	bumpTex = "./textures/gold/pattern_cool2.bump";
};

%mat = new Material (Material_wall_warm3)
{
	mapTo = wall_warm3;
	baseTex = "./textures/gold/wall_warm3";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_cool2)
{
	mapTo = wall_cool2;
	baseTex = "./textures/gold/wall_cool2";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_stripe_cool)
{
	mapTo = stripe_cool;
	baseTex = "./textures/gold/stripe_cool";
};

%mat = new Material (Material_grid_cool)
{
	mapTo = grid_cool;
	baseTex = "./textures/gold/grid_cool";
	bumpTex = "./textures/gold/grid_circle.bump";
};

%mat = new Material (Material_pattern_neutral3)
{
	mapTo = pattern_neutral3;
	baseTex = "./textures/gold/pattern_neutral3";
};

%mat = new Material (Material_pattern_neutral2)
{
	mapTo = pattern_neutral2;
	baseTex = "./textures/gold/pattern_neutral2";
};

%mat = new Material (Material_pattern_cool1)
{
	mapTo = pattern_cool1;
	baseTex = "./textures/gold/pattern_cool1";
	bumpTex = "./textures/gold/pattern_cool1.bump";
};

%mat = new Material (Material_edge_cool1)
{
	mapTo = edge_cool1;
	baseTex = "./textures/gold/edge_cool1";
	bumpTex = "./textures/gold/edge_cool1.bump";
};

%mat = new Material (Material_edge_neutral1)
{
	mapTo = edge_neutral1;
	baseTex = "./textures/gold/edge_neutral1";
	bumpTex = "./textures/gold/edge_neutral1.bump";
};

%mat = new Material (Material_trim_cool2)
{
	mapTo = trim_cool2;
	baseTex = "./textures/gold/trim_cool2";
};

%mat = new Material (Material_arrow_warm1)
{
	mapTo = arrow_warm1;
	baseTex = "./textures/gold/arrow_warm1";
};

%mat = new Material (Material_chevron_cool)
{
	mapTo = chevron_cool;
	baseTex = "./textures/gold/chevron_cool";
	bumpTex = "./textures/gold/chevron.bump";
};

%mat = new Material (Material_chevron_neutral)
{
	mapTo = chevron_neutral;
	baseTex = "./textures/gold/chevron_neutral";
	bumpTex = "./textures/gold/chevron.bump";
};

%mat = new Material (Material_chevron_warm)
{
	mapTo = chevron_warm;
	baseTex = "./textures/gold/chevron_warm";
	bumpTex = "./textures/gold/chevron.bump";
};

%mat = new Material (Material_chevron_warm2)
{
	mapTo = chevron_warm2;
	baseTex = "./textures/gold/chevron_warm2";
	bumpTex = "./textures/gold/chevron.bump";
};

%mat = new Material (Material_custom_crate)
{
	mapTo = custom_crate;
	baseTex = "./textures/gold/custom_crate";
};

%mat = new Material (Material_custom_dirt)
{
	mapTo = custom_dirt;
	baseTex = "./textures/gold/custom_dirt";
};

%mat = new Material (Material_custom_logend)
{
	mapTo = custom_logend;
	baseTex = "./textures/gold/custom_logend";
};

%mat = new Material (Material_custom_logside)
{
	mapTo = custom_logside;
	baseTex = "./textures/gold/custom_logside";
};

%mat = new Material (Material_custom_mid_track)
{
	mapTo = custom_mid_track;
	baseTex = "./textures/gold/custom_mid_track";
};

%mat = new Material (Material_custom_parking)
{
	mapTo = custom_parking;
	baseTex = "./textures/gold/custom_parking";
};

%mat = new Material (Material_custom_insd_track)
{
	mapTo = custom_lft_track;//custom_insd_track;
	baseTex = "./textures/gold/custom_insd_track";
};

// Kevin Ryan's accidental texture mistake
//%mat = new Material (Material_custom_lft_track)
//{
//	mapTo = custom_lft_track;
//	baseTex = "./textures/gold/custom_lft_track";
//};

%mat = new Material (Material_custom_red_brick)
{
	mapTo = custom_red_brick;
	baseTex = "./textures/gold/custom_red_brick";
};

%mat = new Material (Material_custom_road)
{
	mapTo = custom_road;
	baseTex = "./textures/gold/custom_road";
};

%mat = new Material (Material_custom_sand)
{
	mapTo = custom_sand;
	baseTex = "./textures/gold/custom_sand";
};

%mat = new Material (Material_custom_skyscrape2)
{
	mapTo = custom_skyscrape2;
	baseTex = "./textures/gold/custom_skyscrape2";
};

%mat = new Material (Material_custom_woodblockside)
{
	mapTo = custom_woodblockside;
	baseTex = "./textures/gold/custom_woodblockside";
};

%mat = new Material (Material_custom_woodblocktop)
{
	mapTo = custom_woodblocktop;
	baseTex = "./textures/gold/custom_woodblocktop";
};

%mat = new Material (Material_custom_woodbox)
{
	mapTo = custom_woodbox;
	baseTex = "./textures/gold/custom_woodbox";
};

%mat = new Material (Material_edge_cool2)
{
	mapTo = edge_cool2;
	baseTex = "./textures/gold/edge_cool2";
	bumpTex = "./textures/gold/edge_cool2.bump";
};

%mat = new Material (Material_edge_neutral2)
{
	mapTo = edge_neutral2;
	baseTex = "./textures/gold/edge_neutral2";
	bumpTex = "./textures/gold/edge_neutral2.bump";
};

%mat = new Material (Material_edge_warm1)
{
	mapTo = edge_warm1;
	baseTex = "./textures/gold/edge_warm1";
	bumpTex = "./textures/gold/edge_warm1.bump";
};

%mat = new Material (Material_edge_warm2)
{
	mapTo = edge_warm2;
	baseTex = "./textures/gold/edge_warm2";
	bumpTex = "./textures/gold/edge_warm2.bump";
};

%mat = new Material (Material_mbg_friction_high)
{
	mapTo = mbg_friction_high;
	baseTex = "./textures/gold/mbg_friction_high";
	friction = 1.50;
    restitution = 0.5;
};

%mat = new Material (Material_mbg_friction_low)
{
	mapTo = mbg_friction_low;
	baseTex = "./textures/gold/mbg_friction_low";
	bumpTex = "./textures/gold/mbg_friction_low.bump";
	friction = 0.20;
    restitution = 0.5;
};

%mat = new Material (Material_mbg_friction_low2)
{
	mapTo = mbg_friction_low2;
	baseTex = "./textures/gold/mbg_friction_low2";
	bumpTex = "./textures/gold/mbg_friction_low2.bump";
	friction = 0.20;
    restitution = 0.5;
};

/*
%mat = new Material (Material_mbg_friction_low2_old)
{
	mapTo = friction_mbg_low2;
	baseTex = "./textures/gold/friction_mbg_low2";
	bumpTex = "./textures/gold/friction_mbg_low2.bump";
	friction = 0.20;
    restitution = 0.5;
};
*/

%mat = new Material (Material_mbg_friction_none)
{
	mapTo = mbg_friction_none;
	baseTex = "./textures/gold/mbg_friction_none";
	friction = 0.01;
    restitution = 0.5;
};

%mat = new Material (Material_mbg_friction_none2)
{
	mapTo = mbg_friction_none2;
	baseTex = "./textures/gold/mbg_friction_none2";
	friction = 0.01;
    restitution = 0.5;
};

%mat = new Material (Material_friction_ramp_yellow)
{
	mapTo = friction_ramp_yellow;
	baseTex = "./textures/gold/friction_ramp_yellow";
	bumpTex = "./textures/gold/friction_ramp_yellow.bump";
	friction = 2;
    restitution = 1;
};

%mat = new Material (Material_grid_cool3)
{
	mapTo = grid_cool3;
	baseTex = "./textures/gold/grid_cool3";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_cool4)
{
	mapTo = grid_cool4;
	baseTex = "./textures/gold/grid_cool4";
	bumpTex = "./textures/gold/grid_circle.bump";
};

%mat = new Material (Material_grid_neutral)
{
	mapTo = grid_neutral;
	baseTex = "./textures/gold/grid_neutral";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_neutral1)
{
	mapTo = grid_neutral1;
	baseTex = "./textures/gold/grid_neutral1";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_neutral2)
{
	mapTo = grid_neutral2;
	baseTex = "./textures/gold/grid_neutral2";
	bumpTex = "./textures/gold/grid_triangle.bump";
};

%mat = new Material (Material_grid_neutral3)
{
	mapTo = grid_neutral3;
	baseTex = "./textures/gold/grid_neutral3";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_warm1)
{
	mapTo = grid_warm1;
	baseTex = "./textures/gold/grid_warm1";
	bumpTex = "./textures/gold/grid_4square.bump";
};

%mat = new Material (Material_grid_warm2)
{
	mapTo = grid_warm2;
	baseTex = "./textures/gold/grid_warm2";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_warm3)
{
	mapTo = grid_warm3;
	baseTex = "./textures/gold/grid_warm3";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_grid_warm4)
{
	mapTo = grid_warm4;
	baseTex = "./textures/gold/grid_warm4";
	bumpTex = "./textures/gold/grid_square.bump";
};

%mat = new Material (Material_pattern_neutral1)
{
	mapTo = pattern_neutral1;
	baseTex = "./textures/gold/pattern_neutral1";
};

%mat = new Material (Material_pattern_warm1)
{
	mapTo = pattern_warm1;
	baseTex = "./textures/gold/pattern_warm1";
};

%mat = new Material (Material_pattern_warm2)
{
	mapTo = pattern_warm2;
	baseTex = "./textures/gold/pattern_warm2";
};

%mat = new Material (Material_pattern_warm3)
{
	mapTo = pattern_warm3;
	baseTex = "./textures/gold/pattern_warm3";
};

%mat = new Material (Material_pattern_warm4)
{
	mapTo = pattern_warm4;
	baseTex = "./textures/gold/pattern_warm3";
	bumpTex = "./textures/gold/pattern_warm24.bump";
};

%mat = new Material (Material_solid_cool1)
{
	mapTo = solid_cool1;
	baseTex = "./textures/gold/solid_cool1";
};

%mat = new Material (Material_solid_cool2)
{
	mapTo = solid_cool2;
	baseTex = "./textures/gold/solid_cool2";
};

%mat = new Material (Material_solid_neutral1)
{
	mapTo = solid_neutral1;
	baseTex = "./textures/gold/solid_neutral1";
};

%mat = new Material (Material_solid_neutral2)
{
	mapTo = solid_neutral2;
	baseTex = "./textures/gold/solid_neutral2";
};

%mat = new Material (Material_solid_warm1)
{
	mapTo = solid_warm1;
	baseTex = "./textures/gold/solid_warm1";
};

%mat = new Material (Material_solid_white)
{
	mapTo = solid_white;
	baseTex = "./textures/gold/solid_white";
};

%mat = new Material (Material_stripe_cool)
{
	mapTo = stripe_cool;
	baseTex = "./textures/gold/stripe_cool";
};

%mat = new Material (Material_stripe_cool2)
{
	mapTo = stripe_cool2;
	baseTex = "./textures/gold/stripe_cool2";
};

%mat = new Material (Material_stripe_neutral)
{
	mapTo = stripe_neutral;
	baseTex = "./textures/gold/stripe_neutral";
};

%mat = new Material (Material_stripe_neutral2)
{
	mapTo = stripe_neutral2;
	baseTex = "./textures/gold/stripe_neutral2";
};

%mat = new Material (Material_stripe_warm)
{
	mapTo = stripe_warm;
	baseTex = "./textures/gold/stripe_warm";
};

%mat = new Material (Material_stripe_warm2)
{
	mapTo = stripe_warm2;
	baseTex = "./textures/gold/stripe_warm2";
};

%mat = new Material (Material_trim_cool1)
{
	mapTo = trim_cool1;
	baseTex = "./textures/gold/trim_cool1";
};

%mat = new Material (Material_trim_cool3)
{
	mapTo = trim_cool3;
	baseTex = "./textures/gold/trim_cool3";
};

%mat = new Material (Material_trim_neutral1)
{
	mapTo = trim_neutral1;
	baseTex = "./textures/gold/trim_neutral1";
};

%mat = new Material (Material_trim_neutral2)
{
	mapTo = trim_neutral2;
	baseTex = "./textures/gold/trim_neutral2";
};

%mat = new Material (Material_trim_warm1)
{
	mapTo = trim_warm1;
	baseTex = "./textures/gold/trim_warm1";
};

%mat = new Material (Material_trim_white2)
{
	mapTo = trim_white2;
	baseTex = "./textures/gold/trim_white2";
};

%mat = new Material (Material_tube_neutral)
{
	mapTo = tube_neutral;
	baseTex = "./textures/gold/tube_neutral";
};

%mat = new Material (Material_tube_warm)
{
	mapTo = tube_warm;
	baseTex = "./textures/gold/tube_warm";
};

%mat = new Material (Material_wall_cool1)
{
	mapTo = wall_cool1;
	baseTex = "./textures/gold/wall_cool1";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_neutral2)
{
	mapTo = wall_neutral2;
	baseTex = "./textures/gold/wall_neutral2";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_neutral3)
{
	mapTo = wall_neutral3;
	baseTex = "./textures/gold/wall_neutral3";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_warm1)
{
	mapTo = wall_warm1;
	baseTex = "./textures/gold/wall_warm1";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_warm2)
{
	mapTo = wall_warm2;
	baseTex = "./textures/gold/wall_warm2";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_wall_white)
{
	mapTo = wall_white;
	baseTex = "./textures/gold/wall_white";
	bumpTex = "./textures/gold/wall.bump";
};

%mat = new Material (Material_white)
{
	mapTo = white;
	baseTex = "./textures/white";
};

%mat = new Material (Material_blue_grid)
{
	mapTo = blue_grid;
	baseTex = "./textures/gold/blue_grid";
};

%mat = new Material (Material_blue_grid2)
{
	mapTo = blue_grid2;
	baseTex = "./textures/gold/blue_grid2";
};
/*
%mat = new Material (Material_friction_low2_old)
{
	mapTo = friction_low2;
	baseTex = "./textures/friction_low2";
	bumpTex = "./textures/friction_low2.bump";
	friction = 0.20;
    restitution = 0.5;
};
*/