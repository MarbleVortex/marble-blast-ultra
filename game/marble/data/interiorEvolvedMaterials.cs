%mat = new CustomMaterial( Material_Tile_Map  )
{
   mapTo = map;
   baseTex[0] = "./textures/evolved/map";
   texture[0] = "./textures/evolved/map";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   shader = NoiseTile;
   version = 2.0;
};