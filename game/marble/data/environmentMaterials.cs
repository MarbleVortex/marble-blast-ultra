%mat = new CustomMaterial( Material_Grass )
{
   mapTo = grass;
   baseTex[0] = "./textures/environment/grass";
   texture[0] = "./textures/environment/grass";
   //texture[1] = "./textures/tile_indermediate.normal";
   //texture[2] = "./textures/noise";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
   
   //shader = WaterShader;
   //version = 2.0;
};

%mat = new CustomMaterial( Material_Dirt )
{
   mapTo = dirt;
   baseTex[0] = "./textures/environment/dirt";
   texture[0] = "./textures/environment/dirt";
   //texture[1] = "./textures/tile_indermediate.normal";
   //texture[2] = "./textures/noise";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
   
   //shader = WaterShader;
   //version = 2.0;
};