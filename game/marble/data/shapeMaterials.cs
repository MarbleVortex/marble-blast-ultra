new CubemapData(sky_environment)
{
	cubeFace[0] = "marble/data/skies/env_SO";
	cubeFace[1] = "marble/data/skies/env_NO";
	cubeFace[2] = "marble/data/skies/env_EA";
	cubeFace[3] = "marble/data/skies/env_WE";
	cubeFace[4] = "marble/data/skies/env_UP";
	cubeFace[5] = "marble/data/skies/env_DN";
};

new CubemapData(gemCubemap)
{
	cubeFace[0] = "marble/data/skies/gemCubemapUp";
	cubeFace[1] = "marble/data/skies/gemCubemapUp";
	cubeFace[2] = "marble/data/skies/gemCubemapUp";
	cubeFace[3] = "marble/data/skies/gemCubemapUp";
	cubeFace[4] = "marble/data/skies/gemCubemapUp";
	cubeFace[5] = "marble/data/skies/gemCubemapUp";
};

new CubemapData(gemCubemap2)
{
	cubeFace[0] = "marble/data/skies/gemCubemapUp2";
	cubeFace[1] = "marble/data/skies/gemCubemapUp2";
	cubeFace[2] = "marble/data/skies/gemCubemapUp2";
	cubeFace[3] = "marble/data/skies/gemCubemapUp2";
	cubeFace[4] = "marble/data/skies/gemCubemapUp2";
	cubeFace[5] = "marble/data/skies/gemCubemapUp2";
};

new CubemapData(gemCubemap3)
{
	cubeFace[0] = "marble/data/skies/gemCubemapUp3";
	cubeFace[1] = "marble/data/skies/gemCubemapUp3";
	cubeFace[2] = "marble/data/skies/gemCubemapUp3";
	cubeFace[3] = "marble/data/skies/gemCubemapUp3";
	cubeFace[4] = "marble/data/skies/gemCubemapUp3";
	cubeFace[5] = "marble/data/skies/gemCubemapUp3";
};

new CubemapData( gemCubemap4 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp4";
   cubeFace[1] = "marble/data/skies/gemCubemapUp4";
   cubeFace[2] = "marble/data/skies/gemCubemapUp4";
   cubeFace[3] = "marble/data/skies/gemCubemapUp4";
   cubeFace[4] = "marble/data/skies/gemCubemapUp4";
   cubeFace[5] = "marble/data/skies/gemCubemapUp4";
};

new CubemapData( gemCubemap5 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp5";
   cubeFace[1] = "marble/data/skies/gemCubemapUp5";
   cubeFace[2] = "marble/data/skies/gemCubemapUp5";
   cubeFace[3] = "marble/data/skies/gemCubemapUp5";
   cubeFace[4] = "marble/data/skies/gemCubemapUp5";
   cubeFace[5] = "marble/data/skies/gemCubemapUp5";
};

new CubemapData( gemCubemap6 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp6";
   cubeFace[1] = "marble/data/skies/gemCubemapUp6";
   cubeFace[2] = "marble/data/skies/gemCubemapUp6";
   cubeFace[3] = "marble/data/skies/gemCubemapUp6";
   cubeFace[4] = "marble/data/skies/gemCubemapUp6";
   cubeFace[5] = "marble/data/skies/gemCubemapUp6";
};

new CubemapData( gemCubemap7 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp7";
   cubeFace[1] = "marble/data/skies/gemCubemapUp7";
   cubeFace[2] = "marble/data/skies/gemCubemapUp7";
   cubeFace[3] = "marble/data/skies/gemCubemapUp7";
   cubeFace[4] = "marble/data/skies/gemCubemapUp7";
   cubeFace[5] = "marble/data/skies/gemCubemapUp7";
};

new CubemapData( gemCubemap8 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp8";
   cubeFace[1] = "marble/data/skies/gemCubemapUp8";
   cubeFace[2] = "marble/data/skies/gemCubemapUp8";
   cubeFace[3] = "marble/data/skies/gemCubemapUp8";
   cubeFace[4] = "marble/data/skies/gemCubemapUp8";
   cubeFace[5] = "marble/data/skies/gemCubemapUp8";
};

new CubemapData( gemCubemap9 )
{
   cubeFace[0] = "marble/data/skies/gemCubemapUp9";
   cubeFace[1] = "marble/data/skies/gemCubemapUp9";
   cubeFace[2] = "marble/data/skies/gemCubemapUp9";
   cubeFace[3] = "marble/data/skies/gemCubemapUp9";
   cubeFace[4] = "marble/data/skies/gemCubemapUp9";
   cubeFace[5] = "marble/data/skies/gemCubemapUp9";
};


//-----------------------------------------------------------------------------
// ShaderData
//-----------------------------------------------------------------------------
new ShaderData(RefractPix)
{
	DXPixelShaderFile = "shaders/refractP.hlsl";
	DXVertexShaderFile = "shaders/refractV.hlsl";
	OGLPixelShaderFile = "shaders/gl/refractP.arb";
	OGLVertexShaderFile = "shaders/gl/refractV.arb";
	pixVersion = 2.0;
};

new ShaderData(StdTex)
{
	DXPixelShaderFile = "shaders/standardTexP.hlsl";
	DXVertexShaderFile = "shaders/standardTexV.hlsl";
	OGLPixelShaderFile = "shaders/gl/standardTexP.arb";
	OGLVertexShaderFile = "shaders/gl/standardTexV.arb";
	pixVersion = 2.0;
};

new CustomMaterial(Material_Marble_BB)
{
	mapTo = "marble.BB.skin";
	
	baseTex[0] = "marble/data/shapes/balls/marble.BB.bump";
	texture[0] = "marble/data/shapes/balls/marble.BB.bump";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/balls/marble.BB.skin";
	
	specular[0] = "1 1 1 1.0";
	specularPower[0] = 12.0;
	
	
	version = 2.0;
	refract = true;
	shader = RefractPix;
	
	texCompression[0] = DXT5;
	
};

%mat = new Material(Material_cap)
{
	mapTo = "cap";
	
	baseTex[0] = "marble/data/shapes/balls/cap";
	bumpTex[0] = "marble/data/shapes/balls/cap_normal";
	
	cubemap = Lobby;
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.8 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};


%mat = new Material(Material_Bumper)
{
	mapTo = "bumper";
	
	baseTex[0] = "marble/data/shapes/bumpers/bumper";
	
	friction = 0.5;
	restitution = 0;
	force = 15;
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.8 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_HazardFan)
{
	mapTo = "fan";
	
	baseTex[0] = "marble/data/shapes/hazards/fan";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_Trapdoor)
{
	mapTo = "trapdoor";
	
	baseTex[0] = "marble/data/shapes/hazards/trapdoor";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_Helicopter)
{
	mapTo = "copter_skin";
	
	baseTex[0] = "marble/data/shapes/images/copter_skin";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_blastOrbit)
{
	mapTo = "blast_orbit_skin";
	
	baseTex[0] = "marble/data/shapes/images/blast_orbit_skin";
	bumpTex[0] = "marble/data/shapes/images/blast_orbit_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_item_glow)
{
	mapTo = "item_glow";
	
	baseTex[0] = "marble/data/shapes/items/item_glow";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_blast_glow)
{
	mapTo = "blast_glow";
	
	baseTex[0] = "marble/data/shapes/images/blast_glow";
	
	glow[0] = true;
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_grow)
{
	mapTo = "grow";
	
	baseTex[0] = "marble/data/shapes/images/grow";
	bumpTex[0] = "marble/data/shapes/images/grow_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 32.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_Grow_Glow)
{
	mapTo = "grow_glow";
	
	baseTex[0] = "marble/data/shapes/images/grow_glow";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_AntiGravSkin)
{
	mapTo = "antigrav_skin";
	
	baseTex[0] = "marble/data/shapes/items/antigrav_skin";
	bumpTex[0] = "marble/data/shapes/items/antigrav_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_AntiGravGlow)
{
	mapTo = "antigrav_glow";
	
	baseTex[0] = "marble/data/shapes/items/antigrav_glow";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_EasterEgg)
{
	mapTo = "egg_skin";
	
	baseTex[0] = "marble/data/shapes/items/egg_skin";
	
	cubemap = "gemCubemap";
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_BaseGem)
{
	mapTo = "base.gem";
	
	baseTex[0] = "marble/data/shapes/items/red.gem";
	
	cubemap = "gemCubemap";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_RedGem)
{
	mapTo = "red.gem";
	
	baseTex[0] = "marble/data/shapes/items/red.gem";
	
	cubemap = "gemCubemap";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_BlueGem)
{
	mapTo = "blue.gem";
	
	baseTex[0] = "marble/data/shapes/items/blue.gem";
	
	cubemap = "gemCubemap3";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_YellowGem)
{
	mapTo = "yellow.gem";
	
	baseTex[0] = "marble/data/shapes/items/yellow.gem";
	
	cubemap = "gemCubemap2";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_GreenGem)
{
   baseTex[0] = "~/data/shapes/items/green.gem";
   cubemap = gemCubemap4;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_PurpleGem)
{
   baseTex[0] = "~/data/shapes/items/purple.gem";
   cubemap = gemCubemap5;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_OrangeGem)
{
   baseTex[0] = "~/data/shapes/items/orange.gem";
   cubemap = gemCubemap6;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_TurquoiseGem)
{
   baseTex[0] = "~/data/shapes/items/turquoise.gem";
   cubemap = gemCubemap7;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_BlackGem)
{
   baseTex[0] = "~/data/shapes/items/black.gem";
   cubemap = gemCubemap8;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_WhiteGem)
{
   baseTex[0] = "~/data/shapes/items/white.gem";
   cubemap = gemCubemap9;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_GemShine)
{
	mapTo = "gemshine";
	
	baseTex[0] = "marble/data/shapes/items/gemshine";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_SuperJump)
{
	mapTo = "superJump_skin";
	
	baseTex[0] = "marble/data/shapes/items/superJump_skin";
	bumpTex[0] = "marble/data/shapes/items/superJump_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_ItemArrow)
{
	mapTo = "itemArrow";
	
	baseTex[0] = "marble/data/shapes/items/itemArrow";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_SuperSpeed)
{
	mapTo = "superSpeed_skin";
	
	baseTex[0] = "marble/data/shapes/items/superSpeed_skin";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_SuperSpeedStar)
{
	mapTo = "superSpeed_star";
	
	baseTex[0] = "marble/data/shapes/items/superSpeed_star";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_TimeTravelSkin)
{
	mapTo = "timeTravel_skin";
	
	baseTex[0] = "marble/data/shapes/items/timeTravel_skin";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_endpad_glow)
{
	mapTo = "endpad_glow";
	
	baseTex[0] = "marble/data/shapes/pads/endpad_glow";
	emissive[0] = true;
	glow[0] = true;
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentanimFlags[0] = $scroll;
	scrollDir[0] = "1.0 0.0";
	scrollSpeed[0] = 0.3;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ringglass)
{
	mapTo = "ringglass";
	
	baseTex[0] = "marble/data/shapes/pads/ringglass";
	bumpTex[0] = "marble/data/shapes/pads/ringnormal";
	
	cubemap = "sky_environment";
	emissive[0] = true;
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.8 1";
	specularPower[0] = 12.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ringtex)
{
	mapTo = "ringtex";
	
	baseTex[0] = "marble/data/shapes/pads/ringtex";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 0.8 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_abyss)
{
	mapTo = "abyss";
	
	baseTex[0] = "marble/data/shapes/pads/abyss";
	
	animFlags[0] = "2";
	
	emissive[0] = true;
	rotPivotOffset[0] = "-0.5 -0.5";
	rotSpeed[0] = "1";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_abyss2)
{
	mapTo = "abyss2";
	
	baseTex[0] = "marble/data/shapes/pads/abyss2";
	
	animFlags[0] = "2";
	
	glow[0] = true;
	rotPivotOffset[0] = "-0.5 -0.5";
	rotSpeed[0] = "1";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_misty_ff)
{
	mapTo = "mistyglow";
	
	baseTex[0] = "marble/data/shapes/pads/mistyglow";
	
	emissive[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

// fall back if this doesn't work
%mat = new Material(Material_misty)
{
	mapTo = "misty";
	
	baseTex[0] = "marble/data/shapes/pads/misty";
	
	fallback = "Material_misty_ff";
	alphaTest = "0";
	animFlags[0] = "1";
	
	scrollDir[0] = "0.1 1";
	scrollSpeed[0] = "0.2";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_mistyglow_ff)
{
	mapTo = "mistyglow";
	
	baseTex[0] = "marble/data/shapes/pads/mistyglow";
	
	translucent = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_mistyglow)
{
	mapTo = "mistyglow";
	
	baseTex[0] = "marble/data/shapes/pads/mistygem";
	
	fallback = "Material_mistyglow_ff";
	alphaTest = "0";
	animFlags[0] = "1";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_corona)
{
	mapTo = "corona";
	
	baseTex[0] = "marble/data/shapes/images/corona";
	
	emissive[0] = true;
	glow[0] = true;
	animFlags[0] = "2";
	rotPivotOffset[0] = "-0.5 -0.5";
	rotSpeed[0] = "3";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_refract)
{
	mapTo = "refract";
	
	baseTex[0] = "marble/data/shapes/images/blast_glow";
	
	glow[0] = true;
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_BaseCautionSign)
{
	mapTo = "base.cautionsign";
	
	baseTex[0] = "marble/data/shapes/signs/base.cautionsign";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_CautionCautionSign)
{
	mapTo = "caution.cautionsign";
	
	baseTex[0] = "marble/data/shapes/signs/caution.cautionsign";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_DangerCautionSign)
{
	mapTo = "danger.cautionsign";
	
	baseTex[0] = "marble/data/shapes/signs/danger.cautionsign";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_CautionSignWood)
{
	mapTo = "cautionsignwood";
	
	baseTex[0] = "marble/data/shapes/signs/cautionsignwood";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_CautionSignPole)
{
	mapTo = "cautionsign_pole";
	
	baseTex[0] = "marble/data/shapes/signs/cautionsign_pole";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_astrolabe)
{
	mapTo = "astrolabe_glow";
	
	baseTex[0] = "marble/data/shapes/astrolabe/astrolabe_glow";
	
	// ENABLE TO LAUGH BUTTOX OFF
    //glow[0] = true;
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_astrolabe_solid)
{
	mapTo = "astrolabe_solid";
	
	baseTex[0] = "marble/data/shapes/astrolabe/astrolabe_solid_glow";
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_clouds_beginner)
{
	mapTo = "clouds_beginner";
	
	baseTex[0] = "marble/data/shapes/astrolabe/clouds_beginner";
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_clouds_intermediate)
{
	mapTo = "clouds_intermediate";
	
	baseTex[0] = "marble/data/shapes/astrolabe/clouds_intermediate";
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_clouds_advanced)
{
	mapTo = "clouds_advanced";
	
	baseTex[0] = "marble/data/shapes/astrolabe/clouds_advanced";
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Mat_Glass_NoRefract)
{
	texture[0] = "marble/data/shapes/structures/glass2";
	baseTex[0] = "marble/data/shapes/structures/glass";
	
	translucent = true;
	blendOp = LerpAlpha;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_glass_fallback)
{
	mapTo = "glass2";
	
	baseTex[0] = "marble/data/shapes/structures/glass2";
	
	emissive[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Material_glass)
{
	mapTo = "glass";
	
	texture[0] = "marble/data/shapes/structures/glass.normal";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/structures/glass";
	
	fallback = "Material_glass_fallback";
	pass[0] = "Mat_Glass_NoRefract";
	renderBin = "TranslucentPreGlow";
	specular[0] = "1 1 1 1";
	specularPower[0] = 12.0;
	
	version = 2.0;
	refract = true;
	shader = RefractPix;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_mbm_glass_fallback)
{
	mapTo = "mbm_glass";
	
	baseTex[0] = "marble/data/shapes/mobile/mbm_glass";
	
	emissive[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Material_mbm_glass)
{
	mapTo = "mbm_glass";
	
	texture[0] = "marble/data/shapes/structures/glass.normal";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/mobile/mbm_glass";
	
	fallback = "Material_mbm_glass_fallback";
	renderBin = "TranslucentPreGlow";
	specular[0] = "1 1 1 1";
	specularPower[0] = 12.0;
	
	version = 2.0;
	refract = true;
	shader = RefractPix;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_GemBeam)
{
	mapTo = "gembeam";
	
	baseTex[0] = "marble/data/shapes/items/gembeam";
	
	emissive[0] = true;
	pixelSpecular[0] = true;
	specular[0] = "0.5 0.6 0.5 0.6";
	specularPower[0] = 12.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ArrowSignArrow)
{
	mapTo = "arrowsign_arrow";
	
	baseTex[0] = "marble/data/shapes/signs/arrowsign_arrow";
	bumpTex[0] = "marble/data/shapes/items/arrow_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ArrowSignArrowGlow)
{
	mapTo = "arrowsign_arrow_glow";
	
	baseTex[0] = "marble/data/shapes/signs/arrowsign_arrow";
	
	glow[0] = true;
	pixelSpecular[0] = true;
	specular[0] = "0.3 0.3 0.3 0.3";
	specularPower[0] = 32.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ArrowSignPost)
{
	mapTo = "arrowsign_post";
	
	baseTex[0] = "marble/data/shapes/signs/arrowsign_post";
	bumpTex[0] = "marble/data/shapes/signs/arrowsign_post_bump";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 12.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ArrowPostUVW)
{
	mapTo = "ArrowPostUVW";
	
	baseTex[0] = "marble/data/shapes/signs/ArrowPostUVW";
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.6 1";
	specularPower[0] = 12.0;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ArrowSignChain)
{
	mapTo = "arrowsign_chain";
	
	baseTex[0] = "marble/data/shapes/signs/arrowsign_chain";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material(TimeTravelGlass_Fallback)
{
	mapTo = "timeTravel_glass_fallback";
	
	baseTex[0] = "marble/data/shapes/structures/glass";
	
	emissive[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Material_TimeTravelGlass)
{
	mapTo = "timeTravel_glass";
	
	texture[0] = "marble/data/shapes/structures/time.normal";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/structures/glass";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = "10";
	fallback = "TimeTravelGlass_Fallback";
	
	refract = true;
	shader = RefractPix;
	version = 2.0;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Material_distort_d)
{
	mapTo = "distort_d";
	
	texture[0] = "marble/data/shapes/Particles/distort_n";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/Particles/distort_d";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	
	refract = true;
	shader = RefractPix;
	version = 2.0;
	
	texCompression[0] = DXT5;
};

new CustomMaterial(Material_cube_glass)
{
	mapTo = "cube_glass";
	
	texture[0] = "marble/data/shapes/structures/cube_glass.normal";
	texture[1] = "$backbuff";
	texture[2] = "marble/data/shapes/structures/cube_glass";
	
	baseTex[0] = "marble/data/shapes/structures/cube_glass";
	
	friction = "0.8";
	restitution = "0.1";
	
	specular[0] = "1 1 1 1";
	specularPower[0] = "10";
	
	refract = true;
	shader = RefractPix;
	version = 2.0;
	pass[0] = "Mat_Glass_NoRefract";
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_blastwave)
{
	mapTo = "blastwave";
	
	baseTex[0] = "marble/data/shapes/images/blastwave";
	
	emissive[0] = true;
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_sigil)
{
	mapTo = sigil;
	
	// stage 0
	baseTex[0] = "marble/data/shapes/pads/sigil";
	
	glow[0] = true;
	emissive[0] = true;
	translucent = true;
   translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_sigil_glow)
{
	mapTo = sigil_glow;
	
	// stage 0
	baseTex[0] = "marble/data/shapes/pads/sigil_glow";
	
	glow[0] = true;
	emissive[0] = true;
	translucent = true;
	translucentanimFlags[0] = $scroll;
	scrollDir[0] = "1.0 0.0";
	scrollSpeed[0] = 0.3;
   translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
	
};

%mat = new Material(Material_sigiloff)
{
	mapTo = "sigiloff";
	
	baseTex[0] = "marble/data/shapes/pads/sigiloff";
	
	translucent = true;
	
	texCompression[0] = DXT5;
};

%mat = new Material (Material_powerup_bounce_new)
{
	mapTo = "powerup-bounce-new";
	baseTex[0] = "./shapes/items/powerup-bounce-new";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;

	texCompression[0] = DXT3;
};

%mat = new Material (Material_powerup_bounce_new_alpha)
{
	mapTo = "powerup-bounce-new-alpha";
	baseTex[0] = "./shapes/items/powerup-bounce-new";
	
   translucent = true;
   translucentBlendOp = LerpAlpha;
   alphaTest = true;  // default value
   alphaRef = 128;   // alpha less than 128 in brightness (255 is max) will not be rendered	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_powerup_bounce)
{
	mapTo = "powerup-bounce";
	baseTex[0] = "./shapes/items/powerup-bounce";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};


%mat = new Material (Material_bounce_center)
{
	mapTo = "bounce_center";
	baseTex[0] = "./shapes/items/bounce_center";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_bounce_posts)
{
	mapTo = "bounce_posts";
	baseTex[0] = "./shapes/items/bounce_posts";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_bounce_rim)
{
	mapTo = "bounce_rim";
	baseTex[0] = "./shapes/items/bounce_rim";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_glow_bounce)
{
	mapTo = glow_bounce;
	baseTex[0] = "./shapes/images/glow_bounce";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	translucent = true;
	
	translucentBlendOp = AddAlpha;
	
	texCompression[0] = DXT5;
};

%mat = new Material (Material_trampoline128_t0)
{
	mapTo = trampoline128_t0;
	baseTex[0] = "./shapes/items/trampoline128_t0";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_shockAbsorber)
{
	mapTo = shockAbsorber;
	baseTex[0] = "./shapes/items/shockAbsorber";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_shockAbsorber_wire)
{
	mapTo = shockAbsorber_wire;
	baseTex[0] = "./shapes/items/sji_shinysteel";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_shockAbsorber_metal)
{
	mapTo = shockAbsorber_metal;
	baseTex[0] = "./shapes/items/sji_shinysteel";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_oilslick)
{
	mapTo = "base.slick";
	baseTex[0] = "./shapes/hazards/base.slick";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	friction = 0.05;
   restitution = 0.5;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material (Material_iceslick)
{
	mapTo = "ice.slick";
	baseTex[0] = "./shapes/hazards/ice.slick";
	
	pixelSpecular[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 32.0;
	
	friction = 0.05;
   restitution = 0.5;
	
	
	texCompression[0] = DXT3;
};

%mat = new Material(Material_flag)
{
	mapTo = "base.flag";
	
	baseTex[0] = "marble/data/shapes/flags/base.flag";
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_flag_edge)
{
	mapTo = "edge";
	
	baseTex[0] = "marble/data/shapes/flags/edge";
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_ShockAbsorberNew)
{
   mapTo = "Shockabsorbertexture";
   
   baseTex[0] = "marble/data/shapes/items/Shockabsorbertexture";
   
   texCompression[0] = DXT5;
};

%mat = new Material(Material_MagnetHandle)
{
   mapTo = "magnet_handle";
   
   baseTex[0] = "marble/data/shapes/hazards/magnet_handle";
   
   texCompression[0] = DXT5;
};

%mat = new Material(Material_Magnet)
{
   mapTo = "magnet";
   
   baseTex[0] = "marble/data/shapes/hazards/magnet";
   
	glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
   
   texCompression[0] = DXT5;
};

%mat = new Material(Material_Lightbulb)
{
   mapTo = "lightbulb";
   
   baseTex[0] = "marble/data/shapes/decorations/lightbulb";
   
   emissive[0] = true;
	//glow[0] = true;
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
   
   texCompression[0] = DXT5;
};

%mat = new Material(Material_Lightbulb_screw)
{
   mapTo = "lightbulb_screw";
   
   baseTex[0] = "marble/data/shapes/decorations/lightbulb_screw";
   
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
   
   texCompression[0] = DXT5;
};

%mat = new Material(Material_TriangleBumper_Side)
{
	mapTo = "triang-side";
	
	baseTex[0] = "marble/data/shapes/bumpers/triang-side";
	
	friction = 0.5;
	restitution = 0;
	force = 15;
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.8 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material(Material_TriangleBumper_Top)
{
	mapTo = "triang-top";
	
	baseTex[0] = "marble/data/shapes/bumpers/triang-top";
	
	friction = 0.5;
	restitution = 0;
	force = 15;
	
	pixelSpecular[0] = true;
	specular[0] = "0.8 0.8 0.8 1";
	specularPower[0] = 12.0;
	
	
	texCompression[0] = DXT5;
};

%mat = new Material (Material_tornado_tex3)
{
	mapTo = tornado_tex3;
	baseTex = "./shapes/hazards/tornado_tex3";
	translucent = true;
};

// Electro
/*
//---------------------------------------------------------------------------------
// metal color plata 
//---------------------------------------------------------------------------------
%mat = new CustomMaterial( Material_plata )
{
   mapTo = plata;
   texture[0] = "./textures/standard/plate.randomize.alpha";
   texture[1] = "./textures/standard/plate.normal";
   texture[2] = "./texturesstandard//plate.randomize.alpha";

   friction = 1;
   restitution = 1;
   force = 0;   

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;

   shader = NoiseTile;
   version = 2.0;
};
// ---------------------------------------------------------------------------
// textura suelo negro
// ---------------------------------------------------------------------------
%mat = new CustomMaterial( Material_Tile_Advanced_Negro  )
{
   mapTo = tile_advanced_negro;
   texture[0] = "./textures/electro/tile_advanced_negro";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura autopista
// ---------------------------------------------------------------------------
%mat = new CustomMaterial( Material_autopista  )
{
   mapTo = autopista;
   texture[0] = "./textures/electro/autopista";
   texture[1] = "./textures/beam.normal";
   texture[2] = "./textures/noise_blanco";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura pasto
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_pasto  )
{
   mapTo = pasto;
   texture[0] = "./textures/electro/pasto";
   texture[1] = "./textures/beam.normal";

   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura suelo verde advance
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Advanced_verde  )
{
   mapTo = tile_advanced_verde;
   texture[0] = "./textures/electro/tile_advanced_verde";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};
// ---------------------------------------------------------------------------
// textura suelo rojo
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Advanced_rojo  )
{
   mapTo = tile_advanced_rojo;
   texture[0] = "./textures/electro/tile_advanced_rojo";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura suelo violeta
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Advanced_Violet  )
{
   mapTo = tile_advanced_violet;
   texture[0] = "./textures/electro/tile_advanced_violet";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura suelo violeta tenue
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Advanced_Violet_ligth  )
{
   mapTo = tile_advanced_violet_ligth;
   texture[0] = "./textures/electro/tile_advanced_violet_ligth";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// ---------------------------------------------------------------------------
// textura de trampolin
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Bounce3  )   // wow esta textura te hace saltar cada vez mass
{
   mapTo = tile_bouncy3;
   texture[0] = "./textures/electro/tile_bouncy3";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 30.0;

   shader = NoiseTile;
   version = 2.0;
   
   friction = 0.5;
   restitution = 2.15;
   force = 0;
};

// ---------------------------------------------------------------------------
// textura de reja sin salto
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_reja  )   // wow esta textura te hace saltar cada vez mass
{
   mapTo = reja;
   texture[0] = "./textures/electro/reja";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 30.0;

   shader = NoiseTile;
   version = 2.0;
   
   friction = 0.2;
   restitution = 0;
   force = 0;
};

// ---------------------------------------------------------------------------
// textura de empujon
// ---------------------------------------------------------------------------
 
%mat = new CustomMaterial( Material_explosion )   // wow esta textura te hace saltar cada vez mass
{
   mapTo = explosion;
   texture[0] = "./textures/electro/explosion";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "10.8 10.8 10.8 11.0";
   specularPower[0] = 300.0;

   shader = NoiseTile;
   version = 2.0;
   
   friction = 0.5;
   restitution = 1.15;
   force = 100;
};

// ---------------------------------------------------------------------------
// textura suelo
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_cement  )
{
   mapTo = cement;
   texture[0] = "./textures/electro/cement";
   texture[1] = "./textures/beam.normal";

   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTile;
   version = 2.0;
};
*/

//plainsign.dts
%mat = new Material(Material_PlainSignWood)
{
   baseTex[0] = "~/data/shapes/signs/plainsignwood";
};

%mat = new Material(Material_BasePlainSign)
{
   baseTex[0] = "~/data/shapes/signs/base.plainSign";
};

%mat = new Material(Material_DownPlainSign)
{
   baseTex[0] = "~/data/shapes/signs/down.plainSign";
};

%mat = new Material(Material_LeftPlainSign)
{
   baseTex[0] = "~/data/shapes/signs/left.plainSign";
};

%mat = new Material(Material_RightPlainSign)
{
   baseTex[0] = "~/data/shapes/signs/right.plainSign";
};

%mat = new Material(Material_UpPlainSign)
{
   baseTex[0] = "~/data/shapes/signs/up.plainSign";
};

%mat = new Material(Material_PlainSignWood2)
{
   baseTex[0] = "~/data/shapes/signs/signwood2";
};

%mat = new Material(Material_SignWood)
{
   baseTex[0] = "~/data/shapes/signs/signwood";
};

new ShaderData( WaterShader )
{
   //DXVertexShaderFile   = "shaders/water/waterReflectRefractV.hlsl";
   //DXPixelShaderFile    = "shaders/water/waterReflectRefractP.hlsl";
   DXVertexShaderFile   = "shaders/noiseTileV.hlsl";
   DXPixelShaderFile    = "shaders/noiseTileP.hlsl";
   pixVersion = 2.0;
};

%mat = new CustomMaterial( Material_Water_Tex )
{
   mapTo = water_tex;
   baseTex[0] = "./shapes/water/water_tex";
   texture[0] = "./shapes/water/water_tex";
   //texture[1] = "./textures/tile_indermediate.normal";
   //texture[2] = "./textures/noise";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
   
	translucent = true;
	blendOp = LerpAlpha;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
   
   //shader = WaterShader;
   //version = 2.0;
};

%mat = new CustomMaterial( Material_Tree_Leaves )
{
   mapTo = tree_leaves;
   baseTex[0] = "./shapes/environment/tree_leaves";
   texture[0] = "./shapes/environment/tree_leaves";
   //texture[1] = "./textures/tile_indermediate.normal";
   //texture[2] = "./textures/noise";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
   
   //shader = WaterShader;
   //version = 2.0;
};

%mat = new CustomMaterial( Material_Tree_Trunk )
{
   mapTo = tree_trunk;
   baseTex[0] = "./shapes/environment/tree_trunk";
   texture[0] = "./shapes/environment/tree_trunk";
   //texture[1] = "./textures/tile_indermediate.normal";
   //texture[2] = "./textures/noise";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
	version = 2.0;
	shader = StdTex;
	
	texCompression[0] = DXT5;
   
   //shader = WaterShader;
   //version = 2.0;
};

%mat = new CustomMaterial( Material_Plant_Stem )
{
   mapTo = plant_stem;
   baseTex[0] = "./shapes/environment/plant_stem";
   texture[0] = "./shapes/environment/plant_stem";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Base_Flower )
{
   mapTo = "base.flower";
   baseTex[0] = "./shapes/environment/base.flower";
   texture[0] = "./shapes/environment/base.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Green_Flower )
{
   mapTo = "green.flower";
   baseTex[0] = "./shapes/environment/green.flower";
   texture[0] = "./shapes/environment/green.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Yellow_Flower )
{
   mapTo = "yellow.flower";
   baseTex[0] = "./shapes/environment/yellow.flower";
   texture[0] = "./shapes/environment/yellow.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Blue_Flower )
{
   mapTo = "blue.flower";
   baseTex[0] = "./shapes/environment/blue.flower";
   texture[0] = "./shapes/environment/blue.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Red_Flower )
{
   mapTo = "red.flower";
   baseTex[0] = "./shapes/environment/red.flower";
   texture[0] = "./shapes/environment/red.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Purple_Flower )
{
   mapTo = "purple.flower";
   baseTex[0] = "./shapes/environment/purple.flower";
   texture[0] = "./shapes/environment/purple.flower";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Grass_Tall )
{
   mapTo = "grass_tall";
   baseTex[0] = "./shapes/environment/grass_tall";
   texture[0] = "./shapes/environment/grass_tall";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Power_Center )
{
   mapTo = "power_center";
   baseTex[0] = "./shapes/items/power_center";
   texture[0] = "./shapes/items/power_center";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
   
   //emissive[0] = true;
   glow[0] = true;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Power_Outer )
{
   mapTo = "power_outer";
   baseTex[0] = "./shapes/items/power_outer";
   texture[0] = "./shapes/items/power_outer";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
   
   //emissive[0] = true;
   //glow[0] = true;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_Anvil )
{
   mapTo = "anvil";
   baseTex[0] = "./shapes/items/anvil";
   texture[0] = "./shapes/items/anvil";
   texture[1] = "$dynamicCubemap";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.9 0.8 0.8";
   specularPower[0] = 12;

   dynamicCubemap = true;
   
   //emissive[0] = true;
   //glow[0] = true;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};

%mat = new CustomMaterial( Material_TopHat )
{
   mapTo = "tophat";
   baseTex[0] = "./shapes/hats/tophat";
   texture[0] = "./shapes/hats/tophat";

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 40.0;
   
   //emissive[0] = true;
   //glow[0] = true;
	
   version = 2.0;
   shader = StdTex;
	
   texCompression[0] = DXT5;
};
