%mat = new Material (Material_cloud)
{
	mapTo = cloud;
	baseTex = "./shapes/hazards/cloud";
	translucent = true;
};

%mat = new Material (Material_landmine_grs)
{
	mapTo = landmine_grs;
	baseTex = "./shapes/hazards/landmine_grs";
};

%mat = new Material (Material_landmine_spikes)
{
	mapTo = landmine_spikes;
	baseTex = "./shapes/hazards/landmine_spikes";
};
