new CubemapData( iceCubemap )
{
   cubeFace[0] = "~/data/textures/acubexpos2";
   cubeFace[1] = "~/data/textures/acubexneg2";
   cubeFace[2] = "~/data/textures/acubezneg2";
   cubeFace[3] = "~/data/textures/acubezpos2";
   cubeFace[4] = "~/data/textures/acubeypos2";
   cubeFace[5] = "~/data/textures/acubeyneg2";
};

new Material(DefaultMaterial) {
   translucent = false;
   friction = 1;

   restitution = 1;
   force = 0;

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 12.0;
};

// Terrain Test

%mat = new CustomMaterial( Default )
{
   mapTo = Default;
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

//-----------------------------------------------------------------------------
// Set Dressing Textures
//-----------------------------------------------------------------------------

new ShaderData( NoiseTile )
{
   DXVertexShaderFile   = "shaders/noiseTileV.hlsl";
   DXPixelShaderFile    = "shaders/noiseTileP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( NoiseTileSmall )
{
   DXVertexShaderFile   = "shaders/noiseTileSmallV.hlsl";
   DXPixelShaderFile    = "shaders/noiseTileSmallP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( HalfTile )
{
   DXVertexShaderFile   = "shaders/halfTileV.hlsl";
   DXPixelShaderFile    = "shaders/halfTileP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( HalfTileSmall )
{
   DXVertexShaderFile   = "shaders/halfTileSmallV.hlsl";
   DXPixelShaderFile    = "shaders/halfTileSmallP.hlsl";
   pixVersion = 2.0;
};

new ShaderData( StandardSmall )
{
   DXVertexShaderFile   = "shaders/standardSmallV.hlsl";
   DXPixelShaderFile    = "shaders/standardSmallP.hlsl";
   pixVersion = 2.0;
};


// Metal Plate random tile texture

%mat = new CustomMaterial( Material_Plate )
{
   mapTo = plate_1;
	baseTex[0] = "./textures/standard/plate.randomize";
   texture[0] = "./textures/standard/plate.randomize";
   texture[1] = "./textures/standard/plate.normal";

   friction = 1;
   restitution = 1;
   force = 0;   

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;

   shader = HalfTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Plate_Small )
{
   mapTo = plate_1_small;
	baseTex[0] = "./textures/standard/plate.randomize";
   texture[0] = "./textures/standard/plate.randomize";
   texture[1] = "./textures/standard/plate.normal";

   friction = 1;
   restitution = 1;
   force = 0;   

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner )
{
   mapTo = tile_beginner;
	baseTex[0] = "./textures/standard/tile_beginner";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Grid  )
{
   mapTo = tile_beginner_grid;//mbu_neutral11_random;
	baseTex[0] = "./textures/standard/tile_beginner";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_shadow )
{
   mapTo = tile_beginner_shadow;
	baseTex[0] = "./textures/standard/tile_beginner";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Red  )
{
   mapTo = tile_beginner_red;
	baseTex[0] = "./textures/standard/tile_beginner_light";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Red_shadow  )
{
   mapTo = tile_beginner_red_shadow;
	baseTex[0] = "./textures/standard/tile_beginner_light";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Blue  )
{
   mapTo = tile_beginner_blue;
	baseTex[0] = "./textures/standard/tile_beginner_dark";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Blue_shadow  )
{
   mapTo = tile_beginner_blue_shadow;
	baseTex[0] = "./textures/standard/tile_beginner_dark";
   texture[0] = "./textures/standard/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate  )
{
   mapTo = tile_intermediate;
	baseTex[0] = "./textures/standard/tile_intermediate";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_shadow  )
{
   mapTo = tile_intermediate_shadow;
	baseTex[0] = "./textures/standard/tile_intermediate";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_green  )
{
   mapTo = tile_intermediate_green;
	baseTex[0] = "./textures/standard/tile_intermediate_light";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_green_shadow  )
{
   mapTo = tile_intermediate_green_shadow;
	baseTex[0] = "./textures/standard/tile_intermediate_light";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_red  )
{
   mapTo = tile_intermediate_red;
	baseTex[0] = "./textures/standard/tile_intermediate_dark";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_red_shadow  )
{
   mapTo = tile_intermediate_red_shadow;
	baseTex[0] = "./textures/standard/tile_intermediate_dark";
   texture[0] = "./textures/standard/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Grid  )
{
   mapTo = tile_advanced_grid;//mbu_neutral11_random;
	baseTex[0] = "./textures/standard/tile_advanced";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};

// exact duplicate of other grid??
/*
%mat = new CustomMaterial( Material_Tile_Advanced_Grid_2  )
{
   mapTo = tile_advanced_grid_2;//mbu_neutral11_random_2;
	baseTex[0] = "./textures/tile_advanced";
   texture[0] = "./textures/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};
*/

%mat = new CustomMaterial( Material_Tile_Advanced_Grid_Blue  )
{
   mapTo = tile_advanced_grid_blue;//mbu_neutral10_random;
	baseTex[0] = "./textures/standard/tile_advanced_dark";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};

/*
%mat = new CustomMaterial( Material_Tile_Advanced_Grid_Blue_2  )
{
   mapTo = tile_advanced_grid_blue_2;//mbu_neutral10_random_2;
	baseTex[0] = "./textures/tile_advanced_dark";
   texture[0] = "./textures/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};
*/

%mat = new CustomMaterial( Material_Tile_Advanced_Grid_Green  )
{
   mapTo = tile_advanced_grid_green;//mbu_neutral8_random;
	baseTex[0] = "./textures/standard/tile_advanced_light";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};

/*
%mat = new CustomMaterial( Material_Tile_Advanced_Grid_Green_2  )
{
   mapTo = tile_advanced_grid_green_2;//mbu_neutral8_random_2;
	baseTex[0] = "./textures/tile_advanced_light";
   texture[0] = "./textures/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTileSmall;
   version = 2.0;
};
*/

%mat = new CustomMaterial( Material_Tile_Advanced  )
{
   mapTo = tile_advanced;
	baseTex[0] = "./textures/standard/tile_advanced";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_shadow  )
{
   mapTo = tile_advanced_shadow;
	baseTex[0] = "./textures/standard/tile_advanced";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Blue  )
{
   mapTo = tile_advanced_blue;
	baseTex[0] = "./textures/standard/tile_advanced_dark";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Blue_shadow  )
{
   mapTo = tile_advanced_blue_shadow;
	baseTex[0] = "./textures/standard/tile_advanced_dark";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Green  )
{
   mapTo = tile_advanced_green;
	baseTex[0] = "./textures/standard/tile_advanced_light";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Green_shadow  )
{
   mapTo = tile_advanced_green_shadow;
	baseTex[0] = "./textures/standard/tile_advanced_light";
   texture[0] = "./textures/standard/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

// Expert Tiles {
%mat = new CustomMaterial( Material_Tile_Expert  )
{
   mapTo = tile_expert;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Expert_shadow  )
{
   mapTo = tile_expert_shadow;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Expert_Blue  )
{
   mapTo = tile_expert_blue;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Expert_Blue_shadow  )
{
   mapTo = tile_expert_blue_shadow;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Expert_Green  )
{
   mapTo = tile_expert_green;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Expert_Green_shadow  )
{
   mapTo = tile_expert_green_shadow;
   baseTex[0] = "./textures/extras/tile_expert";
   texture[0] = "./textures/extras/tile_expert";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_green_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};
// } Expert Tiles

// Start Bonus
%mat = new CustomMaterial( Material_Tile_Bonus  )
{
   mapTo = tile_bonus;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Bonus_shadow  )
{
   mapTo = tile_bonus_shadow;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Bonus_Blue  )
{
   mapTo = tile_bonus_blue;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Bonus_Blue_shadow  )
{
   mapTo = tile_bonus_blue_shadow;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_blue_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Bonus_Red  )
{
   mapTo = tile_bonus_red;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Bonus_Red_shadow  )
{
   mapTo = tile_bonus_red_shadow;
   baseTex[0] = "./textures/extras/tile_bonus";
   texture[0] = "./textures/extras/tile_bonus";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_red_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};
// End Bonus

// Start Extra
%mat = new CustomMaterial( Material_Tile_Extra )
{
   mapTo = tile_extra;
   baseTex[0] = "./textures/extras/tile_extra";
   texture[0] = "./textures/extras/tile_extra";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Extra_shadow  )
{
   mapTo = tile_extra_shadow;
   baseTex[0] = "./textures/extras/tile_extra";
   texture[0] = "./textures/extras/tile_extra";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Extra_Light  )
{
   mapTo = tile_extra_light;
   baseTex[0] = "./textures/extras/tile_extra_light";
   texture[0] = "./textures/extras/tile_extra_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Extra_Light_shadow  )
{
   mapTo = tile_extra_light_shadow;
   baseTex[0] = "./textures/extras/tile_extra_light";
   texture[0] = "./textures/extras/tile_extra_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Extra_Dark  )
{
   mapTo = tile_extra_dark;
   baseTex[0] = "./textures/extras/tile_extra_dark";
   texture[0] = "./textures/extras/tile_extra_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Extra_Dark_shadow  )
{
   mapTo = tile_extra_dark_shadow;
   baseTex[0] = "./textures/extras/tile_extra_dark";
   texture[0] = "./textures/extras/tile_extra_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};
// End Extra

%mat = new CustomMaterial( Material_Tile_Underside  )
{
   mapTo = tile_underside;
	baseTex[0] = "./textures/standard/tile_underside";
   texture[0] = "./textures/standard/tile_underside";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Underside_Grid  )
{
   mapTo = tile_underside_grid;
	baseTex[0] = "./textures/standard/tile_underside";
   texture[0] = "./textures/standard/tile_underside";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   shader = NoiseTileSmall;
   version = 2.0;
};

%mat = new Material(Material_Wall_Beginner : DefaultMaterial) {
   mapTo="wall_beginner";

   baseTex[0] = "./textures/standard/wall_beginner";
   //bumpTex[0] = "./textures/plate.normal";
};

%mat = new CustomMaterial(Material_Edge_White)// : DefaultMaterial)
{
   mapTo = "edge_white";
   baseTex[0] = "./textures/standard/edge_white";
   texture[0] = "./textures/standard/edge_white";
   texture[1] = "./textures/standard/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
   
   shader = Shader_Edge;
   version = 2.0;
   refract = true;
};

%mat = new CustomMaterial(Material_Edge_White_shadow)// : DefaultMaterial)
{
   mapTo = "edge_white_shadow";
   baseTex[0] = "./textures/standard/edge_white";
   texture[0] = "./textures/standard/edge_white_shadow";
   texture[1] = "./textures/standard/edge.normal";

   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 50.0;
   
   shader = Shader_Edge;
   version = 2.0;
   refract = true;
};

%mat = new CustomMaterial(Material_Edge_White2)// : DefaultMaterial)
{
   mapTo = "edge_white2";
   baseTex[0] = "./textures/gold/edge_white2";
   texture[0] = "./textures/gold/edge_white2";
   texture[1] = "./textures/gold/edge_white2.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
   
   shader = Shader_Edge;
   version = 2.0;
   refract = true;
};

// what's the point of this
/*
%mat = new CustomMaterial(Material_Edge_White_small)// : DefaultMaterial)
{
   mapTo = "mbu_edge_white2";
   baseTex[0] = "./textures/standard/edge_white";
   texture[0] = "./textures/standard/edge_white";
   texture[1] = "./textures/standard/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
   
   shader = Shader_Edge;
   version = 2.0;
   refract = true;
};
*/

%mat = new Material(Material_beam : DefaultMaterial) {
   mapTo = "beam";
   baseTex[0] = "./textures/standard/beam";
   bumpTex[0] = "./textures/standard/beam.normal";
};

%mat = new Material(Material_BeamSide : DefaultMaterial) {
   mapTo = "beam_side";
   baseTex[0] = "./textures/standard/beam_side";
   bumpTex[0] = "./textures/standard/beam_side.normal";
};

%mat = new Material(Tube_beginner : DefaultMaterial)
{
   mapto = "tube_beginner";
   baseTex[0] = "./textures/gold/tube_beginner";
};

%mat = new Material(Tube_intermediate : DefaultMaterial)
{
   mapto="tube_cool";
   baseTex[0] = "./textures/gold/tube_intermediate";
};

%mat = new Material(Tube_Advanced : DefaultMaterial)
{
   mapto="tube_advanced";
   baseTex[0] = "./textures/gold/tube_advanced";
};

// ---------------------------------------------------------------------------
// Bounce Materials
// ---------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Bounce  )
{
   mapTo = tile_bouncy;
   baseTex[0] = "./textures/extras/tile_bouncy";
   texture[0] = "./textures/extras/tile_bouncy";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 30.0;

   shader = NoiseTile;
   version = 2.0;
   
   friction = 0.01;
   restitution = 3.5;
   force = 0;
};

%mat = new CustomMaterial( Material_Tile_Bounce_Shadow  )
{
   mapTo = tile_bouncy_shadow;
   baseTex[0] = "./textures/extras/tile_bouncy";
   texture[0] = "./textures/extras/tile_bouncy";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 30.0;
   
   shader = NoiseTile;
   version = 2.0;
   
   friction = 0.01;
   restitution = 3.5;
   force = 0;
};

%mat = new CustomMaterial( Material_Tile_Bounce2  )
{
   mapTo = tile_bouncy2;
   baseTex[0] = "./textures/extras/tile_bouncy2";
   texture[0] = "./textures/extras/tile_bouncy2";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 30.0;

   shader = NoiseTile;
   version = 2.0;
   
   friction = 1;
   restitution = 3.5;
   force = 0;
}; 

%mat = new CustomMaterial( Material_Tile_Bounce2_Shadow  )
{
   mapTo = tile_bouncy2_shadow;
   baseTex[0] = "./textures/extras/tile_bouncy2";
   texture[0] = "./textures/extras/tile_bouncy2";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 30.0;
   
   shader = NoiseTile;
   version = 2.0;
   
   friction = 1;
   restitution = 3.5;
   force = 0;
};


// ---------------------------------------------------------------------------
// Friction Materials
// ---------------------------------------------------------------------------

%mat = new Material(Material_LowFriction) {
   baseTex[0] = "./textures/standard/friction_low";
   bumpTex[0] = "./textures/standard/friction_low.normal";
   friction = 0.20;
   restitution = 1.0;
   force = 0;
   
   mapTo = friction_low;
   
   cubemap[0] = iceCubemap;
   
   //!@#$%^&* ICE
   //baseTex[1] = "./textures/friction_low";
   //bumpTex[1] = "./textures/friction_low.normal";
   //translucent[1] = true;
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 0.8";
   specularPower[0] = 128.0;
};

%mat = new Material(Material_LowFriction_shadow) {
   baseTex[0] = "./textures/standard/friction_low_shadow";
   bumpTex[0] = "./textures/standard/friction_low.normal";
   friction = 0.20;
   restitution = 1.0;
   force = 0;
   
   mapTo = friction_low_shadow;
   
   cubemap[0] = iceCubemap;
   
   //!@#$%^&* ICE
   //baseTex[1] = "./textures/friction_low";
   //bumpTex[1] = "./textures/friction_low.normal";
   //translucent[1] = true;
   pixelSpecular[0] = true;
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 128.0;
};

// not used??
/*
%mat = new Material(Material_HighFriction_small : DefaultMaterial) {
   friction = 4.5;
   restitution = 0.5;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_mp_high";
   baseTex[0] = "./textures/friction_high";
   bumpTex[0] = "./textures/friction_high.normal";
};
*/

%mat = new Material(Material_UltraHighFriction : DefaultMaterial) {
   friction = 6;
   restitution = 0.1;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_ultrahigh";
   baseTex[0] = "./textures/extras/friction_ultrahigh";
   //bumpTex[0] = "./textures/extras/friction_ultrahigh.normal";
};

%mat = new Material(Material_UltraHighFriction_shadow : DefaultMaterial) {
   friction = 6;
   restitution = 0.1;
   force = 0;
  
   specular[0] = "0.15 0.15 0.16 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_ultrahigh_shadow";
   baseTex[0] = "./textures/extras/friction_ultrahigh_shadow";
   //bumpTex[0] = "./textures/extras/friction_ultrahigh.normal";
};

%mat = new CustomMaterial(Material_HighFriction_Small) {
   friction = 4.5;
   restitution = 0.5;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_high_small";
   baseTex[0] = "./textures/standard/friction_high";
   texture[0] = "./textures/standard/friction_high";
   texture[1] = "./textures/standard/friction_high.normal";
   
   shader = "StandardSmall";
   version = 2.0;
};

//updated
%mat = new Material(Material_HighFriction : DefaultMaterial) {
   friction = 4.5;
   restitution = 0.5;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_high";
   baseTex[0] = "./textures/standard/friction_high";
   bumpTex[0] = "./textures/standard/friction_high.normal";
};

%mat = new Material(Material_HighFriction_shadow : DefaultMaterial) {
   friction = 4.5;
   restitution = 0.5;
   force = 0;
  
   specular[0] = "0.15 0.15 0.16 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_high_shadow";
   baseTex[0] = "./textures/standard/friction_high_shadow";
   bumpTex[0] = "./textures/standard/friction_high.normal";
};

%mat = new Material(Material_NoneFriction : DefaultMaterial) {
   friction = 0;
   restitution = 1;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_none";
   baseTex[0] = "./textures/extras/friction_none";
   //bumpTex[0] = "./textures/extras/friction_none.normal";
};

%mat = new Material(Material_BounceFriction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce";
   baseTex[0] = "./textures/extras/friction_bounce";
   //bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Beginner_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_beginner";
   baseTex[0] = "./textures/extras/friction_bounce_beginner";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Beginner_Shadow_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.15 0.15 0.16 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_beginner_shadow";
   baseTex[0] = "./textures/extras/friction_bounce_beginner_shadow";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Intermediate_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_intermediate";
   baseTex[0] = "./textures/extras/friction_bounce_intermediate";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Intermediate_Shadow_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.15 0.15 0.16 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_intermediate_shadow";
   baseTex[0] = "./textures/extras/friction_bounce_intermediate_shadow";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Advanced_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_advanced";
   baseTex[0] = "./textures/extras/friction_bounce_advanced";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

%mat = new Material(Material_Bounce_Advanced_Shadow_Friction : DefaultMaterial) {
   friction = 1;
   restitution = 1;
   force = 15;
  
   specular[0] = "0.15 0.15 0.16 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_bounce_advanced_shadow";
   baseTex[0] = "./textures/extras/friction_bounce_advanced_shadow";
   bumpTex[0] = "./textures/extras/friction_bounce.normal";
};

/*%mat = new Material(Material_VeryHighFriction : DefaultMaterial) {
   friction = 2;
   restitution = 1;
   force = 0;
   
   mapTo = "friction_ramp_yellow";
   
   baseTex[0] = "./textures/gold/friction_ramp_yellow";
   bumpTex[0] = "./textures/gold/friction_ramp_yellow.bump";
};*/

/*
%mat = new Material (Material_friction_none_old)
{
	mapTo = friction_none;
	baseTex = "./textures/friction_none";
	friction = 0.01;
    restitution = 0.5;
};
*/

%mat = new Material (Material_invisible)
{
	mapTo = "tools_invisible";
	baseTex = "./textures/extras/invisible";
	
	translucent = true;
};