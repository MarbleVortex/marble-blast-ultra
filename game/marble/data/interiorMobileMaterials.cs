//----------------------------------------------------------------------------------------
//-------------------------------- Marble Blast Ultra ------------------------------------
//---------------------------- interiorMobileMaterials.cs --------------------------------
//----------------------------------------------------------------------------------------
//------------------------------------- MBU Team -----------------------------------------
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// Brightness Materials
//----------------------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Beginner_MBM  )
{
   mapTo = tile_beginner_mbm;
   baseTex[0] = "./textures/mobile/tile_beginner";
   texture[0] = "./textures/mobile/tile_beginner";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_red";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Light_MBM  )
{
   mapTo = tile_beginner_light_mbm;
   baseTex[0] = "./textures/mobile/tile_beginner_light";
   texture[0] = "./textures/mobile/tile_beginner_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_red";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Beginner_Dark_MBM  )
{
   mapTo = tile_beginner_dark_mbm;
   baseTex[0] = "./textures/mobile/tile_beginner_dark";
   texture[0] = "./textures/mobile/tile_beginner_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_blue";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_MBM  )
{
   mapTo = tile_intermediate_mbm;
   baseTex[0] = "./textures/mobile/tile_intermediate";
   texture[0] = "./textures/mobile/tile_intermediate";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_green";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_Light_MBM  )
{
   mapTo = tile_intermediate_light_mbm;
   baseTex[0] = "./textures/mobile/tile_intermediate_light";
   texture[0] = "./textures/mobile/tile_intermediate_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_green";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Intermediate_Dark_MBM  )
{
   mapTo = tile_intermediate_dark_mbm;
   baseTex[0] = "./textures/mobile/tile_intermediate_dark";
   texture[0] = "./textures/mobile/tile_intermediate_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_red";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_MBM  )
{
// This is no ordinary darkness... it's advanced darkness!
   mapTo = tile_advanced_mbm;
   baseTex[0] = "./textures/mobile/tile_advanced";
   texture[0] = "./textures/mobile/tile_advanced";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_blue";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Dark_MBM  )
{
// This is no ordinary darkness... it's advanced darkness!
   mapTo = tile_advanced_dark_mbm;
   baseTex[0] = "./textures/mobile/tile_advanced_dark";
   texture[0] = "./textures/mobile/tile_advanced_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise_blue";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;
   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Advanced_Light_MBM  )
{
   mapTo = tile_advanced_light_mbm;
   baseTex[0] = "./textures/mobile/tile_advanced_light";
   texture[0] = "./textures/mobile/tile_advanced_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
  // texture[2] = "./textures/noise_green";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Underside_MBM  )
{
   mapTo = tile_underside_mbm;
	baseTex[0] = "./textures/mobile/tile_underside";
   texture[0] = "./textures/mobile/tile_underside";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   //texture[2] = "./textures/noise";
	//noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   shader = HalfTileSmall;
   version = 2.0;
};

//----------------------------------------------------------------------------------------
// Extra Materials
//----------------------------------------------------------------------------------------

%mat = new CustomMaterial(Material_Wall_Brown : DefaultMaterial) {
   mapTo="wall_brown_mbm";

   baseTex[0] = "./textures/mobile/wall_brown";
   texture[0] = "./textures/mobile/wall_brown";
   texture[1] = "./textures/standard/plate.normal";
   //bumpTex[0] = "./textures/standard/plate.normal";
   
   friction = 1;
   restitution = 1;
   force = 0;
   
   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;
   
   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new CustomMaterial(Material_Warn_Wall_Brown : DefaultMaterial) {
   mapTo="warn_wall_brown_mbm";

   baseTex[0] = "./textures/mobile/warn_wall_brown";
   texture[0] = "./textures/mobile/warn_wall_brown";
   texture[1] = "./textures/standard/plate.normal";
   //bumpTex[0] = "./textures/standard/plate.normal";
   
   friction = 1;
   restitution = 1;
   force = 0;
   
   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;
   
   shader = HalfTileSmall;
   version = 2.0;
};

%mat = new Material(Material_Tile_Cracked : DefaultMaterial) {
   mapTo="tile_cracked_mbm";

   baseTex[0] = "./textures/mobile/tile_cracked";
   //bumpTex[0] = "./textures/plate.normal";
};

%mat = new Material(Material_beam_MBM : DefaultMaterial) {
   mapTo = "beam_mbm";
   baseTex[0] = "./textures/mobile/beam";
   bumpTex[0] = "./textures/standard/beam.normal";
};

%mat = new Material (Material_edge_white_MBM)
{
	mapTo = edge_white_mbm;
	baseTex = "./textures/mobile/edge_white";
	bumpTex = "./textures/standard/edge.normal";
};

%mat = new Material(Material_stripe_caution_mbm : DefaultMaterial)
{
   mapTo = "stripe_caution_mbm";
   baseTex[0] = "./textures/mobile/stripe_caution";
};

//----------------------------------------------------------------------------------------
// Friction Materials
//----------------------------------------------------------------------------------------

%mat = new Material(Material_LowFriction_MBM) {
   baseTex[0] = "./textures/mobile/friction_low";
   bumpTex[0] = "./textures/standard/friction_low.normal";
   friction = 0.20;
   restitution = 1.0;
   force = 0;
   
   mapTo = friction_low_mbm;
   
   cubemap[0] = iceCubemap;
   
   //!@#$%^&* ICE
   //baseTex[1] = "./textures/friction_low";
   //bumpTex[1] = "./textures/friction_low.normal";
   //translucent[1] = true;
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 0.8";
   specularPower[0] = 128.0;
};

%mat = new Material(Material_HighFriction_MBM : DefaultMaterial) {
   friction = 4.5;
   restitution = 0.5;
   force = 0;
  
   specular[0] = "0.3 0.3 0.35 1.0";
   specularPower[0] = 10.0;
   
   MAPTO = "friction_high_mbm";
   baseTex[0] = "./textures/mobile/friction_high";
   //bumpTex[0] = "./textures/standard/friction_high.normal";
};
