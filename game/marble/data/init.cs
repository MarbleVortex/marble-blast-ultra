// maps a texture name to the specified material object.  You should not need to call this function,
// unless you want to override a mapping for a texture (normally the mapping is set up by the baseTex
// property of the Material or the mapTo field of the material).
function mapMaterial(%name,%material)
{
   addMaterialMapping(%name, "material: " @ %material);
}

function loadMaterials()
{
   // load base materials
   exec("./baseMaterials.cs");
   
   for( %file = findFirstFile( "*/materials.cs" ); %file !$= ""; %file = findNextFile( "*/materials.cs" ))
   {
      exec( %file );
   }

   // load custom material files
   exec("./interiorMaterials.cs");
   exec("./interiorColorMaterials.cs");
   exec("./interiorGoldMaterials.cs");
   exec("./interiorMobileMaterials.cs");
   //exec("./interiorElectroMaterials.cs");
   exec("./interiorEvolvedMaterials.cs");
   //exec("./shapeMaterials_.cs");
   exec("./shapeMaterials.cs");
   exec("./sizeMaterials.cs"); //Blue Sizing Materials
   exec("./marbleMaterials.cs");
   exec("./extraMaterials.cs");
   exec("./environmentMaterials.cs");
   
   // A temporary hack until we can find a better way to initialize
   // material properties.
   //exec( "./terrains/highplains/propertyMap.cs" );

}

function reloadMaterials()
{
   reloadTextures();
   loadMaterials();
   reInitMaterials();
}

initRenderInstManager();
// load all material.cs files
loadMaterials();   
