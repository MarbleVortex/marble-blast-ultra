//----------------------------------------------------------------------------------------
//-------------------------------- Marble Blast Ultra ------------------------------------
//----------------------------- interiorColorMaterials.cs --------------------------------
//----------------------------------------------------------------------------------------
//------------------------------------- MBU Team -----------------------------------------
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// Tile Materials
//----------------------------------------------------------------------------------------


%mat = new CustomMaterial( Material_Tile_Blue )
{
   mapTo = tile_blue;
	baseTex[0] = "./textures/colors/tile_blue";
   texture[0] = "./textures/colors/tile_blue";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Blue_shadow )
{
   mapTo = tile_blue_shadow;
   baseTex[0] = "./textures/colors/tile_blue";
   texture[0] = "./textures/colors/tile_blue";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cyan )
{
   mapTo = tile_cyan;
   baseTex[0] = "./textures/colors/tile_cyan";
   texture[0] = "./textures/colors/tile_cyan";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cyan_shadow )
{
   mapTo = tile_cyan_shadow;
   baseTex[0] = "./textures/colors/tile_cyan";
   texture[0] = "./textures/colors/tile_cyan";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Green )
{
   mapTo = tile_green;
   baseTex[0] = "./textures/colors/tile_green";
   texture[0] = "./textures/colors/tile_green";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Green_shadow )
{
   mapTo = tile_green_shadow;
   baseTex[0] = "./textures/colors/tile_green";
   texture[0] = "./textures/colors/tile_green";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};


%mat = new CustomMaterial( Material_Tile_Pink )
{
   mapTo = tile_pink;
   baseTex[0] = "./textures/colors/tile_pink";
   texture[0] = "./textures/colors/tile_pink";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Pink_shadow )
{
   mapTo = tile_pink_shadow;
   baseTex[0] = "./textures/colors/tile_pink";
   texture[0] = "./textures/colors/tile_pink";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Purple )
{
   mapTo = tile_purple;
   baseTex[0] = "./textures/colors/tile_purple";
   texture[0] = "./textures/colors/tile_purple";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Purple_shadow )
{
   mapTo = tile_purple_shadow;
   baseTex[0] = "./textures/colors/tile_purple";
   texture[0] = "./textures/colors/tile_purple";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Yellow )
{
   mapTo = tile_yellow;
   baseTex[0] = "./textures/colors/tile_yellow";
   texture[0] = "./textures/colors/tile_yellow";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;
   
   // LOL
   //emissive[0] = true;
   //glow[0] = true;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Yellow_shadow )
{
   mapTo = tile_yellow_shadow;
   baseTex[0] = "./textures/colors/tile_yellow";
   texture[0] = "./textures/colors/tile_yellow";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise_shadow";
   
   specular[0] = "0.2 0.2 0.2 0.2";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

//----------------------------------------------------------------------------------------
// Edge Materials
//----------------------------------------------------------------------------------------

%mat = new Material(Material_Edge_Aqua : DefaultMaterial)
{
   mapTo = "edge_aqua";
   baseTex[0] = "./textures/colors/edge_aqua";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Blue : DefaultMaterial)
{
   mapTo = "edge_blue";
   baseTex[0] = "./textures/colors/edge_blue";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Brown : DefaultMaterial)
{
   mapTo = "edge_brown";
   baseTex[0] = "./textures/colors/edge_brown";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Green : DefaultMaterial)
{
   mapTo = "edge_green";
   baseTex[0] = "./textures/colors/edge_green";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Orange : DefaultMaterial)
{
   mapTo = "edge_orange";
   baseTex[0] = "./textures/colors/edge_orange";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Pink : DefaultMaterial)
{
   mapTo = "edge_pink";
   baseTex[0] = "./textures/colors/edge_pink";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Purple : DefaultMaterial)
{
   mapTo = "edge_purple";
   baseTex[0] = "./textures/colors/edge_purple";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Red : DefaultMaterial)
{
   mapTo = "edge_red";
   baseTex[0] = "./textures/colors/edge_red";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

%mat = new Material(Material_Edge_Yellow : DefaultMaterial)
{
   mapTo = "edge_yellow";
   baseTex[0] = "./textures/colors/edge_yellow";
   bumpTex[0] = "./textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 50.0;
};

//----------------------------------------------------------------------------------------
// Circle Materials
//----------------------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Circle_Blue )
{
   mapTo = tile_circle_blue;
   baseTex[0] = "./textures/colors/tile_circle_blue";
   texture[0] = "./textures/colors/tile_circle_blue";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Blue_Dark )
{
   mapTo = tile_circle_blue_dark;
   baseTex[0] = "./textures/colors/tile_circle_blue_dark";
   texture[0] = "./textures/colors/tile_circle_blue_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Blue_Light )
{
   mapTo = tile_circle_blue_light;
   baseTex[0] = "./textures/colors/tile_circle_blue_light";
   texture[0] = "./textures/colors/tile_circle_blue_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Brown )
{
   mapTo = tile_circle_brown;
   baseTex[0] = "./textures/colors/tile_circle_brown";
   texture[0] = "./textures/colors/tile_circle_brown";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Orange )
{
   mapTo = tile_circle_orange;
   baseTex[0] = "./textures/colors/tile_circle_orange";
   texture[0] = "./textures/colors/tile_circle_orange";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Purple )
{
   mapTo = tile_circle_purple;
   baseTex[0] = "./textures/colors/tile_circle_purple";
   texture[0] = "./textures/colors/tile_circle_purple";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Red )
{
   mapTo = tile_circle_red;
   baseTex[0] = "./textures/colors/tile_circle_red";
   texture[0] = "./textures/colors/tile_circle_red";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_White )
{
   mapTo = tile_circle_white;
   baseTex[0] = "./textures/colors/tile_circle_white";
   texture[0] = "./textures/colors/tile_circle_white";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Circle_Yellow )
{
   mapTo = tile_circle_yellow;
   baseTex[0] = "./textures/colors/tile_circle_yellow";
   texture[0] = "./textures/colors/tile_circle_yellow";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

//----------------------------------------------------------------------------------------
// Cross Materials
//----------------------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Cross_Blue )
{
   mapTo = tile_cross_blue;
   baseTex[0] = "./textures/colors/tile_cross_blue";
   texture[0] = "./textures/colors/tile_cross_blue";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Brown )
{
   mapTo = tile_cross_brown;
   baseTex[0] = "./textures/colors/tile_cross_brown";
   texture[0] = "./textures/colors/tile_cross_brown";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Green )
{
   mapTo = tile_cross_green;
   baseTex[0] = "./textures/colors/tile_cross_green";
   texture[0] = "./textures/colors/tile_cross_green";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Green_Dark )
{
   mapTo = tile_cross_green_dark;
   baseTex[0] = "./textures/colors/tile_cross_green_dark";
   texture[0] = "./textures/colors/tile_cross_green_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Green_Light )
{
   mapTo = tile_cross_green_light;
   baseTex[0] = "./textures/colors/tile_cross_green_light";
   texture[0] = "./textures/colors/tile_cross_green_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Orange )
{
   mapTo = tile_cross_orange;
   baseTex[0] = "./textures/colors/tile_cross_orange";
   texture[0] = "./textures/colors/tile_cross_orange";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Purple )
{
   mapTo = tile_cross_purple;
   baseTex[0] = "./textures/colors/tile_cross_purple";
   texture[0] = "./textures/colors/tile_cross_purple";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_Red )
{
   mapTo = tile_cross_red;
   baseTex[0] = "./textures/colors/tile_cross_red";
   texture[0] = "./textures/colors/tile_cross_red";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Cross_White )
{
   mapTo = tile_cross_white;
   baseTex[0] = "./textures/colors/tile_cross_white";
   texture[0] = "./textures/colors/tile_cross_white";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

//----------------------------------------------------------------------------------------
// Grid Materials
//----------------------------------------------------------------------------------------

%mat = new CustomMaterial( Material_Tile_Grid_Brown )
{
   mapTo = tile_grid_brown;
   baseTex[0] = "./textures/colors/tile_grid_brown";
   texture[0] = "./textures/colors/tile_grid_brown";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Green )
{
   mapTo = tile_grid_green;
   baseTex[0] = "./textures/colors/tile_grid_green";
   texture[0] = "./textures/colors/tile_grid_green";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Green_Dark )
{
   mapTo = tile_grid_green_dark;
   baseTex[0] = "./textures/colors/tile_grid_green_dark";
   texture[0] = "./textures/colors/tile_grid_green_dark";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Green_Light )
{
   mapTo = tile_grid_green_light;
   baseTex[0] = "./textures/colors/tile_grid_green_light";
   texture[0] = "./textures/colors/tile_grid_green_light";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Green_Pale )
{
   mapTo = tile_grid_green_pale;
   baseTex[0] = "./textures/colors/tile_grid_green_pale";
   texture[0] = "./textures/colors/tile_grid_green_pale";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Orange )
{
   mapTo = tile_grid_orange;
   baseTex[0] = "./textures/colors/tile_grid_orange";
   texture[0] = "./textures/colors/tile_grid_orange";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Purple )
{
   mapTo = tile_grid_purple;
   baseTex[0] = "./textures/colors/tile_grid_purple";
   texture[0] = "./textures/colors/tile_grid_purple";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Red )
{
   mapTo = tile_grid_red;
   baseTex[0] = "./textures/colors/tile_grid_red";
   texture[0] = "./textures/colors/tile_grid_red";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_White )
{
   mapTo = tile_grid_white;
   baseTex[0] = "./textures/colors/tile_grid_white";
   texture[0] = "./textures/colors/tile_grid_white";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};

%mat = new CustomMaterial( Material_Tile_Grid_Yellow )
{
   mapTo = tile_grid_yellow;
   baseTex[0] = "./textures/colors/tile_grid_yellow";
   texture[0] = "./textures/colors/tile_grid_yellow";
   texture[1] = "./textures/standard/tile_intermediate.normal";
   texture[2] = "./textures/noise";
	noiseTexFileName = "./textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2.0;
};
