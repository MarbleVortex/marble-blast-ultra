// Electro
//--------------------------------------------------------------
// materia advance by electro
//--------------------------------------------------------------

// ---------------------------------------------------------------------------
// Water texture by electro
// ---------------------------------------------------------------------------
%mat = new Material(Material_water)
{
   mapTo = "water";
   baseTex[0] = "./textures/electro/water";
   friction = 0.01;
   restitution = 1.0;   // esta funcion hace que por cada salto multipliques el salto de 3.5 para notarlo
   force = 0;
   
};
%mat = new Material(Material_ladrillo)
{
   mapTo = "ladrillo";
   baseTex[0] = "./textures/electro/ladrillo";
   friction = 1;
   restitution = 1.0;   // esta funcion hace que por cada salto multipliques el salto de 3.5 para notarlo
   force = 0;
   
};

// Electro
// ---------------------------------------------------------------------------
// textura CRISTAL no mover de esta posicion
// ---------------------------------------------------------------------------

new CustomMaterial(Material_vidrio0)
{
   mapTo = "vidrio0";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio0";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

  
};


new CustomMaterial(Material_vidrio1)
{
   mapTo = "vidrio1";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio1";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};

new CustomMaterial(Material_vidrio2)
{
   mapTo = "vidrio2";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio2";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_amarillo)
{
   mapTo = "vidrio_amarillo";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_amarillo";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_azul)
{
   mapTo = "vidrio_azul";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_azul";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_blanco)
{
   mapTo = "vidrio_blanco";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_blanco";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_blanco2)
{
   mapTo = "vidrio_blanco2";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_blanco2";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_rojo)
{
   mapTo = "vidrio_rojo";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_rojo";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_verde)
{
   mapTo = "vidrio_verde";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_verde";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
   translucentBlendOp = Add;
};
new CustomMaterial(Material_vidrio_oscuro)
{
   mapTo = "vidrio_oscuro";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_oscuro";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_morado)
{
   mapTo = "vidrio_morado";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_morado";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_colores)
{
   mapTo = "vidrio_colores";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_colores";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vitral)
{
   mapTo = "vitral";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vitral";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_metalico)
{
   mapTo = "vidrio_metalico";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_metalico";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_verde2)
{
   mapTo = "vidrio_verde2";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_verde2";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_rojo2)
{
   mapTo = "vidrio_rojo2";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_rojo2";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;

};
new CustomMaterial(Material_vidrio_amarillo2)
{
   mapTo = "vidrio_amarillo2";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_amarillo2";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
sound = BumperDing;
};
new CustomMaterial(Material_vidrio_glass)
{
   mapTo = "vidrio_glass";

   texture[0] = "./textures/electro/cristales/vidrio.normal"; 
   texture[1] = "$backbuff";
   texture[2] = "./textures/electro/cristales/vidrio_glass";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
   blendOp = LerpAlpha;
   translucent[0] = true;
   translucentZWrite = true;
sound = BumperDing;
};