//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Gem base class
//-----------------------------------------------------------------------------

datablock SFXProfile(GotGemSfx)
{
   filename    = "~/data/sound/gem_collect";
   description = Audio2D;
   volume = 0.6;
   preload = true;
};

datablock SFXProfile(OpponentGotGemSfx)
{
   filename    = "~/data/sound/opponent_gem_collect";
   description = AudioClose3d;
   volume = 0.6;
   preload = true;
};

datablock SFXProfile(GotAllGemsSfx)
{
   filename    = "~/data/sound/gem_all";
   description = Audio2D;
   volume = 0.6;
   preload = true;
};


//-----------------------------------------------------------------------------

/*$GemSkinColors[0] = "base";

function Gem::onAdd(%this,%obj)
{
   if (%this.skin !$= "")
      %obj.setSkinName(%this.skin);
   else {
      // Random skin if none assigned
      %obj.setSkinName($GemSkinColors[0]);
   }
}*/

$GemSkinColors[0] = "GemItem";
$GemSkinColors[1] = "GemItem_5pts";
$GemSkinColors[2] = "GemItem_2pts";
$GemSkinColors[3] = "GemItemGreen";
$GemSkinColors[4] = "GemItemPurple";
$GemSkinColors[5] = "GemItemTurquoise";
$GemSkinColors[6] = "GemItemBlack";
$GemSkinColors[7] = "GemItemOrange";
$GemSkinColors[8] = "GemItemWhite";

function Gem::onAdd(%this,%obj)
{
   if (%this.skin !$= "")
      %obj.setSkinName(%this.skin);
   else if (MissionInfo.randomGems) {
      // Random skin if none assigned
      %i = getRandom(8);
      %obj.setDatablock($GemSkinColors[%i]);
      %obj.setSkinName($GemSkinColors[%i].skin);
   } else if (MissionInfo.gameMode $= "Color")
   {
      %i = getRandom(ClientGroup.getCount() - 1);
      %obj.setDatablock($GemSkinColors[%i]);
      %obj.setSkinName($GemSkinColors[%i].skin);
   }
}

function Gem::onPickup(%this,%obj,%user,%amount)
{
   Parent::onPickup(%this,%obj,%user,%amount);
   %user.client.onFoundGem(%amount,%obj,%this.points);
   return true;
}

function Gem::saveState(%this,%obj,%state)
{
   %state.object[%obj.getId()] = %obj.isHidden();
}

function Gem::restoreState(%this,%obj,%state)
{
   %obj.setHidden(%state.object[%obj.getId()]);
}

function Gem::getSkinName(%this)
{
   if (%this.skin $= "")
      return "red";
   else
      return %this.skin;
}

//-----------------------------------------------------------------------------

datablock ItemData(GemItem)
{
   // Mission editor category
   category = "Gems";
   class = "Gem";

   // Basic Item properties
   shapeFile = "~/data/shapes/items/gem.dts";
   mass = 1;
   friction = 1;
   elasticity = 0;
   gravityMod = 0;

   // Dynamic properties defined by the scripts
   pickupName = "a gem!";
   maxInventory = 1;
   noRespawn = true;
   gemType = 1;
   noPickupMessage = false;

   scaleFactor = 1.5;

   renderGemAura = true;
   gemAuraTextureName = "~/data/textures/gemAura";
   noRenderTranslucents = true;
   referenceColor = "0.9 0 0";
   points = 1;
   
   addToHUDRadar = true;

   buddyShapeName = "marble/data/shapes/items/gembeam.dts";
   buddySequence = "ambient";
   
   colorId = 0;
};

datablock ItemData(GemItem_2pts: GemItem) 
{
   skin = "yellow";
   points = 2;
   referenceColor = "0.9 1 0";
   
   colorId = 2;
};

datablock ItemData(GemItem_5pts: GemItem) 
{
   skin = "blue";
   points = 5;
   referenceColor = "0 0 0.9";
   
   colorId = 1;
};

datablock ItemData(GemItem_7pts: GemItem)
{
   skin = "green";
   referenceColor = "0 0.9 0";
   points = 7;
   
   colorId = 3;
};

datablock ItemData(GemItemRespawn : GemItem)
{
   noRespawn = false;
   respawnTime = 5000;
   referenceColor = "0.9 0 0";
};

datablock ItemData(GemItemPurple: GemItem)
{
   skin = "purple";
   referenceColor = "0.9 0 0.9";
   points = 4;
   
   colorId = 4;
};
// 
datablock ItemData(GemItemGreen: GemItem)
{
   skin = "green";
   referenceColor = "0 0.9 0";
   points = 7;
   
   colorId = 3;
};
// 
datablock ItemData(GemItemTurquoise: GemItem)
{
   skin = "turquoise";
   referenceColor = "0 0.7 0.9";
   points = 3;
   
   colorId = 5;
};
// 
datablock ItemData(GemItemOrange: GemItem)
{
   skin = "orange";
   referenceColor = "1 0.6 0";
   points = 6;
   
   colorId = 7;
};
// 
datablock ItemData(GemItemBlack: GemItem)
{
   skin = "black";
   referenceColor = "0 0 0";
   points = 8;
   
   colorId = 6;
};
// 
datablock ItemData(GemItemWhite: GemItem)
{
   skin = "white";
   referenceColor = "0.9 0.9 0.9";
   points = 9;
   
   colorId = 8;
};

// datablock ItemData(GemItemBlue: GemItem) 
// {
   // skin = "blue";
   // points = 3;
// };
// 
// datablock ItemData(GemItemRed: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemYellow: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemPurple: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemGreen: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemTurquoise: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemOrange: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemBlack: GemItem)
// {
   // skin = "base";
// };
// 
// datablock ItemData(GemItemWhite: GemItem)
// {
   // skin = "base";
// };

