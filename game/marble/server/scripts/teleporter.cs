datablock TriggerData(TeleportTrigger)
{
   className = "TeleportTrigger";
   tickPeriodMS = 100;
};

function TeleportTrigger::addParticles(%this, %trigger)
{
	if (%trigger.noParticles)
		return;
   if (%trigger.target $= "")
      return;
   %x = getWord(%trigger.position, 0) + %trigger.scale / 2;
   %y = getWord(%trigger.position, 1) - %trigger.scale / 2;
   %z = getWord(%trigger.position, 2);
   
   %pos = %x SPC %y SPC %z;
      
   %particles = new ParticleEmitterNode()
   {
      position = %pos;
      rotation = %trigger.rotation;
      scale = %trigger.scale;
      dataBlock = "TeleportNode";
      emitter = "TeleportEmitter";
   };
   MissionCleanup.add(%particles);
}

function TeleportTrigger::onLeaveTrigger(%this,%trigger,%obj)
{
   if (isEventPending(%obj.client.tpSched))
   {
      cancel(%obj.client.tpSched);
      //RG_Teleport.setVisible(false);
      commandToClient(%obj.client, 'SetTeleporting', false);
      //addChatLine("Teleportation aborted...");
   }
}

function TeleportTrigger::onEnterTrigger(%this,%trigger,%obj)
{
   %time = %trigger.time;
   if (%time $= "")
      %time = 2500;
   %target = %trigger.target;
   if (%target $= "")
   //{
      return;
      //messageClient(%this, 'MsgItemPickup', "No Target!" );
   //} else
   //{
      commandToClient(%obj.client, 'SetTeleporting', true);
      //RG_Teleport.setVisible(true);
      //addChatLine("Teleportation in progress...");
      %obj.client.tpSched = %obj.client.schedule(%time, "teleport", %target);//, %particles);
   //}
}

function GameConnection::Teleport(%this, %target)//, %particles)
{
   cancel(%this.tpSched);
   
   //%particles.delete();   
   
   %this.player.setPosition(%target.position, 0.45);
   %this.player.setVelocity("0 0 0");
}