//-----------------------------------------------------------------------------// Torque Game Engine// // Copyright (c) 2001 GarageGames.Com//-----------------------------------------------------------------------------//-----------------------------------------------------------------------------datablock StaticShapeData(StartPad){   className = "Pad";   category = "Pads";   shapeFile = "~/data/shapes/pads/startArea.dts";   scopeAlways = true;   emap = false;   cubeMapReflected = true;
   
   dynamicType = BIT(30);};function StartPad::onAdd(%this, %obj){   $Game::StartPad = %obj;   %obj.setName("StartPoint");   %obj.playThread(0,"ambient");}//-----------------------------------------------------------------------------datablock StaticShapeData(EndPad){   className = "Pad";   category = "Pads";   shapeFile = "~/data/shapes/pads/endArea.dts";   scopeAlways = true;   emap = false;   cubeMapReflected = true;
   
   dynamicType = BIT(30);};function EndPad::onAdd(%this, %obj){
   $Game::RealEndPad = %obj;   %obj.setName("EndPoint");   %obj.playThread(0,"ambient");}

datablock StaticShapeData(FakeEndPad)
{
   className = "Pad";
   category = "Pads";
   shapeFile = "~/data/shapes/pads/endArea.dts";
   scopeAlways = true;
   emap = false;
   cubeMapReflected = true;
   dynamicType = BIT(30);
};

function FakeEndPad::onAdd(%this, %obj)
{
   $Game::FakeEndPad = %obj;
   %obj.setName("FakeEndPoint");
   %obj.playThread(0,"ambient");
}