//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

// Change THIS to make your own marbles!!!
$enableMarblePickerTemplates = false;

// Leave this stuff to the pros.
$Const::OfficialMarbleCount = 37;
$Const::MarbleCount = 90;
if ($enableMarblePickerTemplates) {
$Const::TemplateMarbleCount = 5;
} else {
$Const::TemplateMarbleCount = 0;
}

datablock ParticleData(BounceParticle)
{
   textureName          = "~/data/particles/burst";
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = -0.1;
   windCoefficient      = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = -2;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 50;
   useInvAlpha =  false;
   spinSpeed     = 90;
   spinRandomMin = -90.0;
   spinRandomMax =  90.0;

   colors[0]     = "0.5 0.5 0.5 0.3";
   colors[1]     = "0.3 0.3 0.2 0.1";
   colors[2]     = "0.2 0.2 0.1 0.0";

   sizes[0]      = 0.8;
   sizes[1]      = 0.4;
   sizes[2]      = 0.2;

   times[0]      = 0;
   times[1]      = 0.75;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MarbleBounceEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 6.0;
   velocityVariance = 0.25;
   thetaMin         = 80.0;
   thetaMax         = 90.0;
   lifetimeMS       = 250;
   particles = "BounceParticle";
};

//-----------------------------------------------------------------------------

datablock ParticleData(TrailParticle)
{
   textureName          = "~/data/particles/burst";
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0;
   windCoefficient      = 0;
   inheritedVelFactor   = 1;
   constantAcceleration = 0;
   lifetimeMS           = 100;
   lifetimeVarianceMS   = 10;
   useInvAlpha =  true;
   spinSpeed     = 0;

   colors[0]     = "1 1 0 0.0";
   colors[1]     = "1 1 0 1";
   colors[2]     = "1 1 1 0.0";

   sizes[0]      = 0.7;
   sizes[1]      = 0.4;
   sizes[2]      = 0.1;

   times[0]      = 0;
   times[1]      = 0.15;
   times[2]      = 1.0;
};

//datablock ParticleData(TrailParticle2)
//{
//   textureName          = "~/data/particles/star";
//   dragCoeffiecient     = 1.0;
//   gravityCoefficient   = 0;
//   windCoefficient      = 0;
//   inheritedVelFactor   = 0;
//   constantAcceleration = -2;
//   lifetimeMS           = 400;
//   lifetimeVarianceMS   = 100;
//   useInvAlpha =  true;
//   spinSpeed     = 90;
//   spinRandomMin = -90.0;
//   spinRandomMax =  90.0;
//
//   colors[0]     = "0.9 0.0 0.0 0.9";
//   colors[1]     = "0.9 0.9 0.0 0.9";
//   colors[2]     = "0.9 0.9 0.9 0.0";
//
//   sizes[0]      = 0.165;
//   sizes[1]      = 0.165;
//   sizes[2]      = 0.165;
//
//   times[0]      = 0;
//   times[1]      = 0.55;
//   times[2]      = 1.0;
//};

datablock ParticleData(TrailParticle2)
{
   textureName          = "~/data/particles/burst";
   dragCoefficient     = 0.0;
   gravityCoefficient   = 0;   
   inheritedVelFactor   = 0.00;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 0;
   useInvAlpha = false;
   spinRandomMin = -90.0;
   spinRandomMax = 90.0;

   colors[0]     = "0.5 0.3 0.2 1.0";
   colors[1]     = "0.5 0.3 0.2 1.0";
   colors[2]     = "0.2 0.0 0.0 0.0";

   sizes[0]      = 0.6;
   sizes[1]      = 0.5;
   sizes[2]      = 0.1;

   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MarbleTrailEmitter)
{
   ejectionPeriodMS = 9;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 0.25;
   thetaMin         = 60.0;
   thetaMax         = 90.0;
   lifetimeMS       = 1000000;
   particles = TrailParticle2;
};

//-----------------------------------------------------------------------------
// ActivePowerUp
// 0 - no active powerup
// 1 - Super Jump
// 2 - Super Speed
// 3 - Super Bounce
// 4 - Indestructible

datablock SFXProfile(Bounce1Sfx)
{
   filename    = "~/data/sound/bouncehard1";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce2Sfx)
{
   filename    = "~/data/sound/bouncehard2";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce3Sfx)
{
   filename    = "~/data/sound/bouncehard3";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce4Sfx)
{
   filename    = "~/data/sound/bouncehard4";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(MegaBounce1Sfx)
{
   filename    = "~/data/sound/mega_bouncehard1";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce2Sfx)
{
   filename    = "~/data/sound/mega_bouncehard2";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce3Sfx)
{
   filename    = "~/data/sound/mega_bouncehard3";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce4Sfx)
{
   filename    = "~/data/sound/mega_bouncehard4";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(JumpSfx)
{
   filename    = "~/data/sound/Jump";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(RollingHardSfx)
{
   filename    = "~/data/sound/Rolling_Hard";
   description = AudioClosestLooping3d;
   volume = 0.91;
   preload = true;
};

datablock SFXProfile(RollingMegaSfx)
{
   filename    = "~/data/sound/mega_roll";
   description = AudioCloseLooping3d;
   volume = 0.91 * 0.85;
   preload = true;
};

datablock SFXProfile(RollingIceSfx)
{
   filename    = "~/data/sound/ice_roll";
   description = AudioClosestLooping3d;
   volume = 0.91 * 0.85;
   preload = true;
};

datablock SFXProfile(SlippingSfx)
{
   filename    = "~/data/sound/Sliding";
   description = AudioClosestLooping3d;
   volume = 0.60;
   preload = true;
};

datablock MarbleData(DefaultMarble)
{
   shadowEnable = true;
   shadowCanMove = true;
   shadowSize = 256;
   // both of the following need to be true to see shadows on the start pad.  but it gets really slow and 
   // their are render artifacts
   shadowSelfShadow = false; 
   shadowDTSShadow = false;
   
   shapeFile = "~/data/shapes/balls/official/marble1.dts";
   emap = true;
   renderFirstPerson = true;
   maxRollVelocity = 15;
   angularAcceleration = 75; //75
   brakingAcceleration = 30;
   gravity = 20;
   mbuSize = 1.5;
   mbuMegaSize = 1.5 * 2.25;
   mbgSize = 1.0;
   mbgMegaSize = 2.25;
   //size = 1.5;
   //megaSize = 1.5 * 2.25;// *
   mass = 1;
   staticFriction = 1.1; //1.1
   kineticFriction = 0.7;//0.7
   bounceKineticFriction = 0.2;
   maxDotSlide = 0.5;
   bounceRestitution = 0.5;
   jumpImpulse = 7.5;
   maxForceRadius = 50;
   
   blastRechargeTime = 36000;
   maxNaturalBlastRecharge = 30000;

   bounce1 = Bounce1Sfx;
   bounce2 = Bounce2Sfx;
   bounce3 = Bounce3Sfx;
   bounce4 = Bounce4Sfx;
   megabounce1 = MegaBounce1Sfx;
   megabounce2 = MegaBounce2Sfx;
   megabounce3 = MegaBounce3Sfx;
   megabounce4 = MegaBounce4Sfx;

   rollHardSound = RollingHardSfx;
   rollMegaSound = RollingMegaSfx;
   rollIceSound = RollingIceSfx;
   slipSound = SlippingSfx;
   jumpSound = JumpSfx;
   
   // Emitters
   minTrailSpeed = 15;            // Trail threshold
   trailEmitter = MarbleTrailEmitter;
   
   minBounceSpeed = 3;           // Bounce threshold
   bounceEmitter = MarbleBounceEmitter;

   powerUps = PowerUpDefs;

   // Allowable Inventory Items
   maxInv[SuperJumpItem] = 20;
   maxInv[SuperSpeedItem] = 20;
   maxInv[SuperBounceItem] = 20;
   maxInv[IndestructibleItem] = 20;
   maxInv[TimeTravelItem] = 20;
//   maxInv[GoodiesItem] = 10;

   dynamicReflection = true;
};

// Eval may be evil, but could it possibly be more evil than what what was going on here before? I doubt it, and I see no other solution, other than to write out around 100 different datablocks for every marble seperately. Or just use a supermarble like MBG, but we can't do that.
// Official
for ($i=1;$i<=$Const::OfficialMarbleCount;$i++) {
eval("datablock MarbleData(Marble" @ $i @ " : DefaultMarble)\n{\nshapeFile = \"marble/data/shapes/balls/official/marble" @ $i @ ".dts\";\n};");
}
// Custom
for ($i=$Const::OfficialMarbleCount+1;$i<=$Const::MarbleCount;$i++) {
eval("datablock MarbleData(Marble" @ $i @ " : DefaultMarble)\n{\nshapeFile = \"marble/data/shapes/balls/custom/marble" @ $i @ ".dts\";\n};");
}
// Templates
for ($i=1;$i<=$Const::TemplateMarbleCount;$i++) {
eval("datablock MarbleData(Marble" @ ($Const::MarbleCount + $i) @ " : DefaultMarble)\n{\nshapeFile = \"marble/data/shapes/balls/templates/marbletemplate" @ $i @ ".dts\";\n};");
}

//-----------------------------------------------------------------------------

function MarbleData::onAdd(%this, %obj)
{
   echo("New Marble: " @ %obj);
}

function MarbleData::onTrigger(%this, %obj, %triggerNum, %val)
{
}


//-----------------------------------------------------------------------------

function MarbleData::onCollision(%this,%obj,%col)
{
   // JMQ: workaround: skip hidden objects  
   if (%col.isHidden())
      return;
  
   // Try and pickup all items
   if (%col.getClassName() $= "Item")
   {
      %data = %col.getDatablock();
      if (%data.class $= "Gem" && MissionInfo.gameMode $= "Collection")
      {
         if (%obj.client.color !$= %data.getSkinName())
         {
            return false;
         }
      }
      %obj.pickup(%col,1);
   }
}


//-----------------------------------------------------------------------------
// The following event callbacks are punted over to the connection
// for processing

function MarbleData::onEnterPad(%this,%object)
{
   %object.client.onEnterPad();
}

function MarbleData::onLeavePad(%this, %object)
{
   %object.client.onLeavePad();
}

function MarbleData::onStartPenalty(%this, %object)
{
   %object.client.onStartPenalty();
}

function MarbleData::onOutOfBounds(%this, %object)
{
   %object.client.onOutOfBounds();
}

function MarbleData::setCheckpoint(%this, %object, %check)
{
   %object.client.setCheckpoint(%check);
}

function MarbleData::onFinishPoint(%this, %object)
{
   %object.client.onFinishPoint();
}


//-----------------------------------------------------------------------------
// Marble object
//-----------------------------------------------------------------------------

function Marble::setPowerUp(%this,%item,%reset)
{
   if (%item.powerUpId != 6)
   {
      // Hack: don't set powerup gui if blast item
      commandToClient(%this.client,'setPowerup',%item.bmpFile);
      %this.powerUpData = %item;
   }
   %this.setPowerUpId(%item.powerUpId,%reset);
}

function Marble::getPowerUp(%this)
{
   return %this.powerUpData;
}

function Marble::onPowerUpUsed(%obj)
{
   commandToClient(%obj.client,'setPowerup',"");
   %obj.playAudio(0, %obj.powerUpData.activeAudio);
   %obj.powerUpData = "";
}

function Marble::onBlastUsed(%obj)
{
   %obj.playAudio(0, doBlastSfx);
}

function Marble::onTimeFreeze(%this,%timeFreeze)
{
   %this.client.incBonusTime(%timeFreeze);
}

function Marble::onPowerUpExpired( %obj, %powerUpId )
{
   if( %powerUpId == 7 )
      %obj.playAudio( 0, ShrinkMegaMarbleSfx );
}

function setMarbleSize(%client, %datablock)
{
   if (MissionInfo.marbleSize $= "small")
   {
      %datablock.size = %datablock.mbgSize;
      %datablock.megaSize = %datablock.mbgMegaSize;
   } else {
      %datablock.size = %datablock.mbuSize;
      %datablock.megaSize = %datablock.mbuMegaSize;
   }
   commandToClient(%client, 'SetMarbleSize', %datablock.getName(), %datablock.size, %datablock.megaSize);
}

//-----------------------------------------------------------------------------

function marbleVel()
{
   return $MarbleVelocity;
}

function metricsMarble()
{
   Canvas.pushDialog(FrameOverlayGui, 1000);
   TextOverlayControl.setValue("$MarbleVelocity");
}

package MarblePackage {
	function onFrameAdvance(%delta)
	{
	   Parent::onFrameAdvance(%delta);
		if (!isObject(findRealMarble()))
		   return;

      /*%position = findRealMarble().getPosition();
      if ($previousPosition !$= %position)
      {
         $marbleVelocity = VectorScale(VectorSub(%position, $previousPosition), (1000 / $delta));
         $delta = %delta;
      } else {
         $delta += %delta;
      }
		$previousPosition = findRealMarble().getPosition();
		*/
		$marbleVelocity = findRealMarble().getVelocity();
		
		
		%velx = mAbs(getWord($marbleVelocity, 0));
		%vely = mAbs(getWord($marbleVelocity, 1));
		%velz = mAbs(getWord($marbleVelocity, 2));
		$marbleDisplayVelocity = round((%velx + %vely + %velz) / 2);
		
		// if($Server::Hosting)
		// {
			// for(%i = 0; %i < ClientGroup.getCount(); %i++){
				// %vel = ClientGroup.getObject(%i).player.getVelocity();
				// %velx = mAbs(getWord(%vel, 0));
				// %vely = mAbs(getWord(%vel, 1));
				// %velz = mAbs(getWord(%vel, 2));
				// $marbleDisplayVelocity = round((%velx + %vely + %velz) / 2);
			// }
		// }
		
		
		// if (MissionInfo.gameMode $= "race" && $Server::Hosting) {
			//$Race::TimePassed += %delta;
			//if ($Race::TimePassed >= 1.0) {
				//error("Calculating Position");
				// for(%i = 0; %i < ClientGroup.getCount(); %i++){
			
					// %cl = ClientGroup.getObject(%i);
			
					// %marble = %cl.player;
					// %pos = %marble.getPosition();
				
					// for(%j = 0; %j < ClientGroup.getCount(); %j++){
					
						// %cl2 = ClientGroup.getObject(%j);
			
						// if(%cl $= %cl2)
							// continue;
			
						// %marble2 = %cl2.player;
						// %pos2 = %marble2.getPosition();
					
						// %dist = VectorDist(%pos, %pos2);
						
						// messageClient(%cl2, 'MsgClientScoreChangedRace', "", %cl2, false, %dist , 0);
					
					// }
				
				// }
			//}
			
		//}
		
	}
};

activatePackage(MarblePackage);
