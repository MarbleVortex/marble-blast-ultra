function enableCheats()
{

	$useLB = 0;

	if (!isObject(CheatCannon))
		new SimGroup(CheatCannon);
	CheatCannon.allowCheats = true;
		
	echo ("**!!YOU HAVE POWERED THE MOTHERLOAD!!**");
}

function disableCheats()
{


	if (isObject(CheatCannon))
	{
		CheatCannon.allowCheats = false;
		CheatCannon.delete();
	}
		
	echo ("**!!YOU HAVE UNPOWERED THE MOTHERLOAD!!**");
}

function CheatCannon::cancelCheats(%this)
{
	cancel(%this.ultra);
	%this.cancelAll();
}

// -----------------------------------------------------------------------------------------
// ----------------------------------------CheatCannon--------------------------------------
// -----------------------------------------------------------------------------------------
// Do not use - It will murder your Marble...

function CheatCannon::Speed(%this){
	cancel(%this.ultra);
	if (%this.allowCheats)
	{
		clientgroup.getobject(0).player.setpowerup(SuperSpeedItem);
		%this.ultra = %this.schedule(100, "Speed");
	}
}

function CheatCannon::Jump(%this){
	cancel(%this.ultra);
	if (%this.allowCheats)
	{
		clientgroup.getobject(0).player.setpowerup(SuperJumpItem);
		%this.ultra = %this.schedule(100, "Jump");
	}
}

function CheatCannon::Fly(%this){
	cancel(%this.ultra);
	if (%this.allowCheats)
	{
		clientgroup.getobject(0).player.setpowerup(HelicopterItem);
		%this.ultra = %this.schedule(100, "Fly");
	}
}