//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// 3D Sounds
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Single shot sounds



datablock SFXDescription(AudioDefault3d)
{
   volume   = 1.0;
   isLooping= false;

   is3D     = true;
   ReferenceDistance= 20.0;
   MaxDistance= 100.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(AudioClose3d)
{
   volume   = 1.0;
   isLooping= false;

   is3D     = true;
   ReferenceDistance= 10.0;
   MaxDistance= 60.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(AudioClose3dQuieter)
{
   volume   = 0.3;
   isLooping= false;

   is3D     = true;
   ReferenceDistance= 10.0;
   MaxDistance= 60.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(AudioClosest3d)
{
   volume   = 1.0;
   isLooping= false;

   is3D     = true;
   ReferenceDistance= 5.0;
   MaxDistance= 30.0;
   type     = $EffectAudioType;
};


//-----------------------------------------------------------------------------
// Looping sounds

datablock SFXDescription(AudioDefaultLooping3d)
{
   volume   = 1.0;
   isLooping= true;

   is3D     = true;
   ReferenceDistance= 20.0;
   MaxDistance= 100.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(AudioCloseLooping3d)
{
   volume   = 1.0;
   isLooping= true;

   is3D     = true;
   ReferenceDistance= 10.0;
   MaxDistance= 50.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(AudioClosestLooping3d)
{
   volume   = 1.0;
   isLooping= true;

   is3D     = true;
   ReferenceDistance= 5.0;
   MaxDistance= 30.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(QuieterAudioClosestLooping3d)
{
   volume   = 0.5;
   isLooping= true;

   is3D     = true;
   ReferenceDistance= 5.0;
   MaxDistance= 30.0;
   type     = $EffectAudioType;
};

datablock SFXDescription(Quieter3D)
{
   volume   = 0.60;
   isLooping= false;

   is3D     = true;
   ReferenceDistance= 20.0;
   MaxDistance= 100.0;
   type     = $EffectAudioType;
};

//-----------------------------------------------------------------------------
// 2d sounds
//-----------------------------------------------------------------------------

// Used for non-looping environmental sounds (like power on, power off)
datablock SFXDescription(Audio2D)
{
   volume = 1.0;
   isLooping = false;
   is3D = false; 
   type = $EffectAudioType;
};

datablock SFXDescription(Audio2DQuieter)
{
   volume = 0.5;
   isLooping = false;
   is3D = false; 
   type = $EffectAudioType;
};

// Used for Looping Environmental Sounds
datablock SFXDescription(AudioLooping2D)
{
   volume = 1.0;
   isLooping = true;
   is3D = false;
   type = $EffectAudioType;
};


//-----------------------------------------------------------------------------
// Ready - Set - Get Rolling

datablock SFXProfile(pauseSfx)
{
   filename    = "~/data/sound/level_text";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(easterNewSfx)
{
   filename    = "~/data/sound/easter_egg";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(easterNotNewSfx)
{
   filename    = "~/data/sound/pu_easter";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(spawnSfx)
{
   filename    = "~/data/sound/spawn_alternate";
   description = AudioClose3dQuieter;
   preload = true;
};

datablock SFXProfile(pickupSfx)
{
   filename    = "~/data/sound/pickup";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(HelpDingSfx)
{
   filename    = "~/data/sound/InfoTutorial";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(ReadyVoiceSfx)
{
   filename    = "~/data/sound/ready";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(SetVoiceSfx)
{
   filename    = "~/data/sound/set";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(CheckpointSfx)
{
   filename = "~/data/sound/checkpoint";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(GetRollingVoiceSfx)
{
   filename    = "~/data/sound/go";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(NewHighScoreSfx)
{
   filename    = "~/data/sound/new_high_score";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(AchievementSfx)
{
   filename    = "~/data/sound/new_achievement";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(AllAchievementSfx)
{
   filename    = "~/data/sound/all_achievement";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(XboxAchievementSfx)
{
   filename    = "~/data/sound/achievement_xbox";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(WonRaceSfx)
{
   filename    = "~/data/sound/finish";
   description = Audio2DQuieter;
   preload = true;
};

datablock SFXProfile(MissingGemsSfx)
{
   filename    = "~/data/sound/missingGems";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(jumpSfx)
{
   filename    = "~/data/sound/bounce";
   description = AudioDefault3d;
   preload = true;
};

datablock SFXProfile(bounceSfx)
{
   filename    = "~/data/sound/bounce";
   description = AudioDefault3d;
   preload = true;
};

//-----------------------------------------------------------------------------
// Multiplayer

datablock SFXProfile(PlayerJoinSfx)
{
   filename    = "~/data/sound/player_join";
   description = Audio2DQuieter;
   preload = true;
};

datablock SFXProfile(PlayerDropSfx)
{
   filename    = "~/data/sound/player_kicked";
   description = Audio2DQuieter;
   preload = true;
};

//-----------------------------------------------------------------------------
// Misc

datablock SFXProfile(PenaltyVoiceSfx)
{
   filename    = "~/data/sound/penalty";
   description = AudioDefault3d;
   preload = true;
};

// IGC HACK -pw
 datablock SFXProfile(OutOfBoundsVoiceSfx)
 {
    filename    = "~/data/sound/out_of_bounds";
    description = Audio2D;
    preload = true;
 };

datablock SFXProfile(DestroyedVoiceSfx)
{
   filename    = "~/data/sound/destroyedVoice";
   description = AudioDefault3d;
   preload = true;
};

datablock SFXProfile(angrySfx)
{
   filename    = "~/data/sound/taunts/deanscream";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(happySfx)
{
   filename    = "~/data/sound/taunts/woohoo";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(taunt2Sfx)
{
   filename    = "~/data/sound/taunts/taunt2";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(taunt3Sfx)
{
   filename    = "~/data/sound/taunts/taunt3";
   description = Audio2D;
   preload = true;
};

