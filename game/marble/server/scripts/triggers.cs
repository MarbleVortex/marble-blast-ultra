//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------

// Normally the trigger class would be sub-classed to support different
// functionality, but we're going to use the name of the trigger instead
// as an experiment.

//-----------------------------------------------------------------------------

datablock TriggerData(InBoundsTrigger)
{
   tickPeriodMS = 100;
   marbleGravityCheck = true;
};

function InBoundsTrigger::onLeaveTrigger(%this,%trigger,%obj)
{
   // Leaving an in bounds area.
   %obj.getDatablock().onOutOfBounds(%obj);
}


//-----------------------------------------------------------------------------

datablock TriggerData(OutOfBoundsTrigger)
{
   tickPeriodMS = 100;
};

function OutOfBoundsTrigger::onEnterTrigger(%this,%trigger,%obj)
{
   // Entering an out of bounds area
   %obj.getDatablock().onOutOfBounds(%obj);
}

//-----------------------------------------------------------------------------

datablock TriggerData(HelpTrigger)
{
   tickPeriodMS = 100;
};

function HelpTrigger::onEnterTrigger(%this,%trigger,%obj)
{
   // Leaving an in bounds area.
   commandToClient(%obj.client,'setHelpLine',%trigger.text,1);
}

function HelpTrigger::onLeaveTrigger(%this,%trigger,%obj)
{
   commandToClient(%obj.client,'removeHelpLine');
}


datablock TriggerData(CheckPointTrigger)
{
   tickPeriodMS = 100;
};

function CheckPointTrigger::onEnterTrigger(%this, %trigger, %obj)
{
   // Entering a check point
   %obj.getDatablock().setCheckPoint(%obj, %trigger);
}

datablock TriggerData(FinishTrigger)
{
   className = "FinishTrigger";
   tickPeriodMS = 100;
};

function FinishTrigger::addFlag(%this,%obj)
{
   if (%obj.getDataBlock().getName() !$= "FinishTrigger")
      return;
   %position = %obj.getPosition(); 
   %scale = %obj.getScale();
   
   %scale = getWord(%scale, 0) / 2 SPC getWord(%scale, 1) / 2 SPC getWord(%scale, 2) / 2;
   
   %position = getWord(%position, 0) + getWord(%scale, 0) SPC getWord(%position, 1) - getWord(%scale, 1) SPC getWord(%position, 2) + getWord(%scale, 2);  
   
   %flag = new Item()
   {
      datablock = "FinishFlag";
      position = %position;
      static = "1";
      rotate = "1";
   };
   
   MissionCleanup.add(%flag);
}

function FinishTrigger::onEnterTrigger(%this, %trigger, %obj)
{
   // Entering a finish point
   %obj.getDatablock().onFinishPoint(%obj);
}

datablock TriggerData(MissionTrigger)
{
   tickPeriodMS = 100;
};

function MissionTrigger::onEnterTrigger(%this, %trigger, %obj)
{
   $Server::TargetMission = %trigger.mission;
   //$Server::TargetSpawn = %trigger.target;
   
   cycleGame();
}

datablock TriggerData(HitTrigger)
{
   tickPeriodMS = 100;
};

function HitTrigger::onEnterTrigger(%this, %trigger, %obj)
{
   %points = %trigger.points;
   if (%points $= "")
      %points = 1;
   %obj.client.onHitTrigger(%obj, %points);
   echo("Hit Trigger!");
}

//-----------------------------------------------------------------------------

datablock TriggerData(LocationTrigger)
{
   tickPeriodMS = 100;
};

$InLocationTrigger = 0;

function LocationTrigger::onEnterTrigger(%this,%trigger,%obj)
{
   $InLocationTrigger++;
   if (%trigger.location $= "-1")
      %obj.client.setDisplayLocation(false);
   else
      %obj.client.setDisplayLocation(true);
   %obj.client.setLocation(%trigger.location);
}

function LocationTrigger::onLeaveTrigger(%this,%trigger,%obj)
{
   $InLocationTrigger--;
   
   if ($InLocationTrigger == 0)
   {
      if (MissionInfo.location $= "-1")
         %obj.client.setDisplayLocation(false);
      else
         %obj.client.setDisplayLocation(true);
      %obj.client.setLocation(MissionInfo.location);
   }
}

//-----------------------------------------------------------------------------

datablock TriggerData(LevelLocationTrigger)
{
   tickPeriodMS = 100;
};

function LevelLocationTrigger::onEnterTrigger(%this,%trigger,%obj)
{
   $InLocationTrigger++;
   
   if (%trigger.isCustom)
   {
      %obj.client.setLocation(%trigger.game SPC " (Custom)\n" @ %trigger.level);
   } else {
      %obj.client.setLocation(%trigger.game @ "\n" @ %trigger.level);
   }
   %obj.client.setDisplayLocation(true);
}

function LevelLocationTrigger::onLeaveTrigger(%this,%trigger,%obj)
{
   $InLocationTrigger--;
   
   if ($InLocationTrigger == 0)
   {
      if (MissionInfo.location $= "-1")
         %obj.client.setDisplayLocation(false);
      else
         %obj.client.setDisplayLocation(true);
      %obj.client.setLocation(MissionInfo.location);
   }
}
