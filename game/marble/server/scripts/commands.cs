//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Misc. server commands avialable to clients
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

function commandToAll(%func, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9)
{
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      
      commandToClient(%cl, %func, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9);
   }
}

function commandToAllExcept(%client, %func, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9)
{
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      
      if (%cl $= %client)
         continue;
      
      commandToClient(%cl, %func, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9);
   }
}

function serverCmdToggleCamera(%client)
{
   //($testCheats || $Server::ServerType $= "SinglePlayer") &&
   if (!%client.spectatorForce)
   {
      %control = %client.getControlObject();
      if (%control == %client.player)
      {
         %control = %client.camera;
         %control.mode = toggleCameraFly;
         RG_Water.setVisible(false);
      }
      else
      {
         %control = %client.player;
         %control.mode = observerFly;
         if ($isUnderwater)
            RG_Water.setVisible(true);
      }
      %client.setControlObject(%control);
   }
}

function serverCmdDropPlayerAtCamera(%client)
{
   if ($testCheats)
   {
      %client.player.setTransform(%client.camera.getTransform());
      %client.player.setVelocity("0 0 0");
      %client.setControlObject(%client.player);
   }
}

function serverCmdDropCameraAtPlayer(%client)
{
   if ($testCheats)
   {
      %client.camera.setTransform(%client.player.getEyeTransform());
      %client.camera.setVelocity("0 0 0");
      %client.setControlObject(%client.camera);
   }
}

function serverCmdSetMarble( %client, %marbleIdx )
{
   %client.marbleIndex = %marbleIdx;
}


//-----------------------------------------------------------------------------

function serverCmdSuicide(%client)
{
   %client.player.kill("Suicide");
}   

function serverCmdPlayCel(%client,%anim)
{
   if (isObject(%client.player))
      %client.player.playCelAnimation(%anim);
}

function serverCmdPlayDeath(%client)
{
   if (isObject(%client.player))
      %client.player.playDeathAnimation();
}

function serverCmdSelectObject(%client, %mouseVec, %cameraPoint)
{
   //Determine how far should the picking ray extend into the world?
   %selectRange = 200;
   // scale mouseVec to the range the player is able to select with mouse
   %mouseScaled = VectorScale(%mouseVec, %selectRange);
   // cameraPoint = the world position of the camera
   // rangeEnd = camera point + length of selectable range
   %rangeEnd = VectorAdd(%cameraPoint, %mouseScaled);

   // Search for anything that is selectable � below are some examples
   %searchMasks = $TypeMasks::PlayerObjectType | $TypeMasks::CorpseObjectType |
      				$TypeMasks::ItemObjectType | $TypeMasks::TriggerObjectType;

   // Search for objects within the range that fit the masks above
   // If we are in first person mode, we make sure player is not selectable by setting fourth parameter (exempt
   // from collisions) when calling ContainerRayCast
   %player = %client.player;
   if ($firstPerson)
   {
	  %scanTarg = ContainerRayCast (%cameraPoint, %rangeEnd, %searchMasks, %player);
   }
   else //3rd person - player is selectable in this case
   {
	  %scanTarg = ContainerRayCast (%cameraPoint, %rangeEnd, %searchMasks);
   }

   // a target in range was found so select it
   if (%scanTarg)
   {
      %targetObject = firstWord(%scanTarg);

      %client.setSelectedObj(%targetObject);
   }
}

function serverCmdNoMissionFound(%client)
{
	//endPreviewMission();
	/*if ($LobbyGui::LastDirection == 0)
		LobbyGui.onLeft();
	else
		LobbyGui.onRight();*/
}

function serverCmdHasMission(%client, %has)
{
   %client.hasMission = %has;
   %client.setMission = true;
}

function serverCmdIAmAngry(%client)
{
   // Don't Disable Here, this disables it for the entire server and only if your hosting
   // It must be disabled on the client side
   
      //if ($pref::Option::UseSecretSound)
	  //{
	//serverPlay2d(AngrySfx);
	  //}
	  commandToAll('PlayTauntSound', angrySfx);
}

function serverCmdIAmHappy(%client)
{
   // Don't Disable Here, this disables it for the entire server and only if your hosting
   // It must be disabled on the client side
   
      //if ($pref::Option::UseSecretSound)
	  //{
	//serverPlay2d(HappySfx);
	  //}
	  commandToAll('PlayTauntSound', happySfx);
}

function serverCmdTaunt2(%client)
{
   // Don't Disable Here, this disables it for the entire server and only if your hosting
   // It must be disabled on the client side
   
      //if ($pref::Option::UseSecretSound)
	  //{
	//serverPlay2d(Taunt2Sfx);
	  //}
	  commandToAll('PlayTauntSound', Taunt2Sfx);
}

function serverCmdTaunt3(%client)
{
   // Don't Disable Here, this disables it for the entire server and only if your hosting
   // It must be disabled on the client side
   
      //if ($pref::Option::UseSecretSound)
	  //{
	//serverPlay2d(Taunt3Sfx);
	  //}
	  commandToAll('PlayTauntSound', Taunt3Sfx);
}

function serverCmdDoOOBClick(%client, %val)
{
   if ($Game::ISOOB && %val)
   {
      %client.player.onOOBClick();
      return;
   }
   
   if (%val)
      $mvOOBClick = 1;
   else
      $mvOOBClick = 0;
   
   if ($mvOOBClick)
      %client.OOBUpdate = %client.schedule(100, "updateOOB");
   else
      cancel(%client.OOBUpdate);
}

function GameConnection::updateOOB(%this)
{
   cancel(%this.OOBUpdate);
   
   if ($Game::ISOOB && $mvOOBClick)
      %this.player.onOOBClick();
   
   %this.OOBUpdate = %this.schedule(100, updateOOB);
}

function serverCmdQuickRespawn(%client)
{
	if (MissionInfo.gameMode !$= "" && !MissionInfo.quickRespawn)
		return;
		
	if ($Game::State $= "play" || $Game::State $= "go")
		%client.respawnPlayer();
}

function GameConnection::setDisplayLocation(%this, %flag)
{
   commandToClient(%this, 'SetDisplayLocation', %flag);
}

function GameConnection::setLocation(%this, %text)
{
   commandToClient(%this, 'SetLocation', %text);
}

function serverCmdSetPlayerControl(%client, %playerGhostId)
{
   %player = %client.resolveObjectFromGhostIndex(%playerGhostId);
   
   echo("Setting Player Control: " @ %player);
   %client.setControlObject(%player);
}