//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Penalty and bonus times.
$Game::TimeTravelBonus = 5000;
$Game::CurrentMode = "Scrum";
// Item respawn values, only powerups currently respawn
$Item::PopTime = 10 * 1000;
$Item::RespawnTime = 7 * 1000;

// Game duration in secs, no limit if the duration is set to 0
$Game::Duration = 0;

// Pause while looking over the end game screen (in secs)
$Game::EndGamePause = 5;

// Simulated net parameters
$Game::packetLoss = 0;
$Game::packetLag = 0;

//-----------------------------------------------------------------------------
// Variables extracted from the mission
$Game::GemCount = 0;
$Game::StartPad = 0;
$Game::FakeEndPad = 0;
$Game::RealEndPad = 0;
$Game::EndPad = 0;

// Variables for tracking end game condition
$Game::GemsFound = 0;
$Game::ClientsFinished = 0;

// Variables for Controls
$Game::NoJump = 0;
$Game::OutOfBounds = 0;

//-----------------------------------------------------------------------------
//  Functions that implement game-play
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
function execServerScripts()
{
   if ($Server::ScriptsLoaded)
   {
      error("Server scripts already loaded, exiting");
      return;
   }
      
   $Server::ScriptsLoaded = true;
   
   // game commands (lobby, ready status, and stuff)
   exec("./gameCommands.cs");
   
   // Load up all datablocks, objects etc.  
   exec("./audioProfiles.cs");
   exec("./camera.cs");
   exec("./markers.cs");
   exec("./triggers.cs");
   exec("./inventory.cs");
   exec("./shapeBase.cs");
   exec("./staticShape.cs");
   exec("./item.cs");
   exec("./worldObjects.cs");
	exec("./decorations.cs");

   // Basic items
   exec("./powerUps.cs");
   exec("./marble.cs");
   exec("./gems.cs");
   //exec("./buttons.cs");
   exec("./hazards.cs");
   exec("./pads.cs");
   exec("./bumpers.cs");
   exec("./signs.cs");
   exec("./fireworks.cs");
   exec("./teleporter.cs");

   //exec("./glowBuffer.cs");
   // Platforms and interior doors
   exec("./pathedInteriors.cs");
   
   // Water
   exec("./water.cs");

   // Easter Eggs
   exec("./easter.cs");
	
	//exec("./cheats.cs");
	
   exec("./flags.cs");
	
	//exec("./aimarble.cs");

  // Tim Particles & Environment
  exec("./particle_effects.cs");
  exec("./environment.cs");
}

function onServerCreated()
{
   // Server::GameType is sent to the master server.
   // This variable should uniquely identify your game and/or mod.
   $Server::GameType = "Marble Game";

   // Server::MissionType sent to the master server.  Clients can
   // filter servers based on mission type.
   $Server::MissionType = "Deathmatch";

   // GameStartTime is the sim time the game started. Used to calculated
   // game elapsed time.
   $Game::StartTime = 0;

   // Keep track of when the game started
   $Game::StartTime = $Sim::Time;
}

function onServerDestroyed()
{
   // Perform any game cleanup without actually ending the game
   destroyGame();
}

//-----------------------------------------------------------------------------

function onMissionLoaded()
{
   if (MissionInfo.lowGravity)
   {
	   exec("./lowGravity.cs");
   } else {
	   exec("./normalGravity.cs");
   }
   if (MissionInfo.yellowText)
   {
	   $yellowText = 1;
   } else {
	   $yellowText = 0;
   }
   
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      commandToClient(%cl, 'SetYellowText', $yellowText);
   }

   fixAllowJumping();
   
   $cameraStyle = "Free";
   if (MissionInfo.cameraStyle $= "Side") {
      $cameraStyle = "Side";
   }
   if (MissionInfo.cameraStyle $= "Inside") {
      $cameraStyle = "Inside";
   }
   
   // Called by loadMission() once the mission is finished loading.
   updateHostedMatchInfo();
   updateServerParams();

   // Start the game in a wait state
   setGameState("wait");

   if (MissionInfo.gameMode $= "Sumo")
      $Game::GemCount = MissionInfo.maxGems;
   else
      $Game::GemCount = countGems(MissionGroup);

   // JMQ: don't start mission yet, wait for command from Lobby 
   // (Also note that serverIsInLobby check may not be valid at this point; its possible that the 
   // mission is loading while the game is being created)

   // Start the game here if multiplayer and we're not in lobby
   if ($Server::ServerType $= "MultiPlayer" && !serverIsInLobby())
      startGame();
}

function onMissionEnded()
{
   // Called by endMission(), right before the mission is destroyed
   // This part of a normal mission cycling or end.
   endGame();
}

function onMissionReset()
{
   //serverEndFireWorks();
   if (isObject(ServerGroup))
      ServerGroup.onMissionReset();

   // Reset globals for next round
   $tied = false;
   $Game::GemsFound = 0;
   $Game::ClientsJoined = 0;
   $Game::ClientsFinished = 0;
   $timeKeeper = 0;
   
   $InLocationTrigger = 0;

   // Reset the players
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      %cl = ClientGroup.getObject( %clientIndex );
      commandToClient(%cl, 'GameStart');
      %cl.resetStats();
   }

   if (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Sumo" || MissionInfo.gameMode $= "Color")
   {
      SetupGems(MissionInfo.numGems);
   }

   $Game::Running = true;
}

function SimGroup::onMissionReset(%this, %checkpointConfirmNum )
{
   for(%i = 0; %i < %this.getCount(); %i++)
      %this.getObject(%i).onMissionReset( %checkpointConfirmNum );
}

function SimObject::onMissionReset(%this, %checkpointConfirmNum)
{
}

function GameBase::onMissionReset(%this, %checkpointConfirmNum)
{
   %this.getDataBlock().onMissionReset(%this, %checkpointConfirmNum);
}

//-----------------------------------------------------------------------------

function startGame()
{
//allowConnect(true);
   if ($Game::Running)
   {
      error("startGame: End the game first!");
      return;
   }
   echo("Starting game");

   $Game::Running = true;
   $Game::Qualified = false;
	$Game::CurrentMode = MissionInfo.gameMode;
   if (MissionInfo.gameMode $= "Hunt" ||  MissionInfo.gameMode $= "Sumo" ||  MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Arcade" || MissionInfo.gameMode $= "Color" || MissionInfo.gameMode $= "Collection")
      $Game::Duration = MissionInfo.time;
      
   if ($Game::Duration >= 6000000)
   {
      $Game::Duration = 5999990;
   }
      
   
   if (MissionInfo.gameMode $= "Sumo")
   {
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %client = ClientGroup.getObject(%i);
         
         %client.points = 10;
         //commandToClient(%client,'setGemCount',%client.points,10);
         //commandToClient(%client, 'SetPoints', %client.getId(), 10);
      }
   }
	//allowConnect(false);
   onMissionReset();

   resetAvgPlayerCount();
}

function endGame()
{
   if (!$Game::Running) {
      error("endGame: No game running!");
      return;
   }
   destroyGame();
   
   // close the chat dialog to make it possible for Anson to vote
   canvas.popDialog(newMessageHud);

   // Inform the clients the game is over
   for (%index = 0; %index < ClientGroup.getCount(); %index++)
   {
      %client = ClientGroup.getObject(%index);
      commandToClient(%client, 'GameEnd');
      commandToClient(%client, 'setTimer',"stop");
      %client.scoreTime = %client.player.getMarbleTime();
      echo("Gameconnection time: " @ %client.scoreTime);
   }

   // If single player, stop timer immediately and update game stats using playgui
   if (isObject(PlayGui))
   {
      PlayGUI.stopTimer();
      $Game::ScoreTime = PlayGUI.elapsedTime;
      $Game::ElapsedTime = %client.player.getFullMarbleTime();
      $Game::PenaltyTime = PlayGUI.penaltyTime;
      $Game::BonusTime = $Game::ElapsedTime-$Game::ScoreTime;
      echo("PlayGui time: " @ PlayGui.elapsedTime);
   }

   // Not all missions have time qualifiers
   $Game::Qualified = MissionInfo.time? $Game::ScoreTime < MissionInfo.time: true;

   // Bump up the max level
   if (!$playingDemo && $Game::Qualified && (MissionInfo.level + 1) > $Pref::QualifiedLevel[MissionInfo.type])
         $Pref::QualifiedLevel[MissionInfo.type] = MissionInfo.level + 1;
         
   if ($Server::ServerType $= "MultiPlayer")
   {
      // decrement rounds remaining
      $Server::NumRounds--;
      echo($Server::NumRounds SPC "rounds left");
      if ($Server::NumRounds <= 0)
      {
         // all done, return to lobby
         enterLobbyMode();
         return;
      }
      
      // start the next round
      for (%index = 0; %index < ClientGroup.getCount(); %index++)
      {
         %client = ClientGroup.getObject(%index);
   
         %client.onClientJoinGame();
         %client.respawnPlayer();
      }
   }
}

$pauseTimescale = 0.00001;

function pauseGame()
{
   // we always store a variable indicating when they have requested a game pause (the UI keys off this)
   $gamePauseWanted = true;
   stopDemoTimer(); 
   sfxPauseAll();
   //if(xbHasPlaybackControl())
   //   pauseMusic();
   sfxPlay(PauseSfx);
   if ($Server::ServerType $= "SinglePlayer" && $Game::Running)
   {
      // we only actually halt the game events in single player games
      $gamePaused = true;
      if ($timeScale > $pauseTimescale)
         $saveTimescale = $timescale;

      $timescale = $pauseTimescale;
   }
}

function resumeGame()
{
   $gamePaused = false;
   $gamePauseWanted = false;
   sfxUnpauseAll();
   //if(xbHasPlaybackControl())
   //   unpauseMusic();
   // restart demo timer if necessary
   if (ServerConnection.gameState $= "play" && $Client::connectedMultiplayer)
      startDemoTimer();
      
   if ($saveTimescale $= "")
      $saveTimescale = 1.0;
   $timescale = $saveTimescale;
}

function destroyGame()
{
   // Set the game back to a wait state
   setGameState("wait");

   // Cancel any client timers
   for (%index = 0; %index < ClientGroup.getCount(); %index++)
   {
      %client = ClientGroup.getObject(%index);
      if (isEventPending(%client.respawnSchedule))
         cancel(%client.respawnSchedule);
      if (isEventPending(%client.stateSchedule))
         cancel(%client.stateSchedule);
      %client.respawnSchedule = 0;
      %client.stateSchedule = 0;
   }

   // commenting out the below code causes AI Marbles to work, though I'm not entirely sure why. - Anthony
   //if (isObject(ActiveGemGroups))
   //   ActiveGemGroups.delete();
      
   if (isObject(FreeGemGroups))
      FreeGemGroups.delete();

   // Perform cleanup to reset the game.
   if (isEventPending($Game::cycleSchedule))
      cancel($Game::CycleSchedule);
      
   if (isEventPending($finishPointSchedule))
      cancel($finishPointSchedule);
      
   if (isEventPending($Game::BlockJIPSchedule))
      cancel($Game::BlockJIPSchedule);
      
   $Game::cycleSchedule = 0;
   $Game::Duration = 0;

   $Game::Running = false;

   $timeKeeper = 0;
}

function faceGems( %player )
{
   // In multi-player, set the position, and face gems
   if( ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt) &&
       isObject( $LastFilledGemSpawnGroup ) &&
       $LastFilledGemSpawnGroup.getCount() )
   {
      %avgGemPos = 0;
      // Ok, for right now, just use the first gem in the group. In final implementation
      // do an average of all the positions of the gems, and look at that.
      for( %i = 0; %i < $LastFilledGemSpawnGroup.getCount(); %i++ )
      {
         %gem = $LastFilledGemSpawnGroup.getObject( %i );
         
         %avgGemPos = VectorAdd( %avgGemPos, %gem.getPosition() );
      }
      %avgGemPos = VectorScale( %avgGemPos, 1 / $LastFilledGemSpawnGroup.getCount() );
      
      %player.cameraLookAtPt( %avgGemPos );
   }
}
//-----------------------------------------------------------------------------

function onGameDurationEnd()
{
   cancel($Game::CycleSchedule);
   // This "redirect" is here so that we can abort the game cycle if
   // the $Game::Duration variable has been cleared, without having
   // to have a function to cancel the schedule.
   if ($Game::Duration && !$infiniteTime)//isObject(EditorGui))
   {
      //cycleGame();
      // Cosmetic: it looks odd when the marble time doesn't match the duration time
      // Find a new $timeKeeper
      for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
      {
         %cl = ClientGroup.getObject( %clientIndex );
         commandToClient(%cl, 'setTimer', "set", $Game::Duration);
      }

      buildRanks();

      doRankNotifications();

      setGameState("end");
   }
}

function cycleGame()
{
   // This is setup as a schedule so that this function can be called
   // directly from object callbacks.  Object callbacks have to be
   // carefull about invoking server functions that could cause
   // their object to be deleted.
   if (!$Game::Cycling) {
      $Game::Cycling = true;
      $Game::CycleSchedule = schedule(0, 0, "onCycleExec");
   }
}

function onCycleExec()
{
   cancel($Game::CycleSchedule);
   // End the current game and start another one, we'll pause for a little
   // so the end game victory screen can be examined by the clients.
   endGame();
   $Game::CycleSchedule = schedule($Game::EndGamePause * 1000, 0, "onCyclePauseEnd");
}

function onCyclePauseEnd()
{
   cancel($Game::CycleSchedule);
   $Game::Cycling = false;
   //loadNextMission();
   loadMission($Server::TargetMission);
}

function loadNextMission()
{
   %nextMission = "";

   // Cycle to the next level, or back to the start if there aren't
   // any more levels.
   for (%file = findFirstFile($Server::MissionFileSpec);
         %file !$= ""; %file = findNextFile($Server::MissionFileSpec))
      if (strStr(%file, "CVS/") == -1 && strStr(%file, "common/") == -1)
      {
         %mission = getMissionObject(%file);
         if (%mission.type $= MissionInfo.type) {
            if (%mission.level == 1)
               %nextMission = %file;
            if ((%mission.level + 0) == MissionInfo.level + 1) {
               echo("Found one!");
               %nextMission = %file;
               break;
            }
         }
      }
   $doShowEditor = false;
   loadMission(%nextMission);
}

function getGem(%gemDB)
{
   if (!isObject(%gemDB))
   {
      // ugh
      error("error, invalid gem db passed to getGem()");
      return 0;
   }
   
   %gemDB = %gemDB.getId();
   
   %freeGroup = nameToId("FreeGems" @ %gemDB);
   if (isObject(%freeGroup) && %freeGroup.getCount() > 0)
   {
      //echo("allocating gem from pool");
      %gem = %freeGroup.getObject(0);
      %freeGroup.remove(%gem);
      %gem.setHidden(false);
      return %gem;
   } 
   
   //echo("allocating new gem");
   // spawn new Gem   
   %gem = new Item() {
      dataBlock = %gemDB;
      collideable = "0";
      static = "1";
      rotate = "1";
      permanent = "0";
      deleted = "0";
   };
   return %gem;
}

function freeGem(%gem)
{
   if (!isObject(%gem))
      return;
      
   if (!isObject(FreeGemGroups))
      new SimGroup(FreeGemGroups);
      
   %gemDB = %gem.getDatablock();
   %gemDB = %gemDB.getId();
   
   %freeGroupName = "FreeGems" @ %gemDB;
   %freeGroup = nameToId(%freeGroupName);
   if (!isObject(%freeGroup))
   {
      %freeGroup = new SimGroup(%freeGroupName);
      FreeGemGroups.add(%freeGroup);
   }
   
   //echo("freeing gem to pool");
   //%freeGroup.add(%gem);
   //%gem.setHidden(true);
   
   %gem.delete();
}

function removeGem(%gem)
{
   freeGem(%gem);
   
   // refill gem groups if necessary
   if ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt)
      refillGemGroups(/*%gem.getDatablock().colorId*/);
}

// due to gem groups, this function isn't used anymore.  use spawnGemAt() instead
//function spawnGem()
//{
//   if (!isObject(TargetGems))
//   {
//      %targetGroup = new SimGroup(TargetGems);
//
//      MissionGroup.add(%targetGroup);
//   }
//
//   if (!isObject(TargetGemsLights))
//   {
//      %targetGroup = new SimGroup(TargetGemsLights);
//
//      MissionGroup.add(%targetGroup);
//   }
//
//   %group = nameToID("MissionGroup/GemSpawns");
//
//   if (%group != -1)
//   {
//      %count = %group.getCount();
//
//      if (%count > 0)
//      {
//         %spawnpos = "0 0 0";
//
//         for (%i = 0; %i < 6; %i++)
//         {
//            %index = getRandom(%count - 1);
//            %spawn = %group.getObject(%index);
//            %spawnpos = %spawn.getTransform();
//
//            %badSpawn = false;
//            for (%j = 0; %j < TargetGems.getCount(); %j++)
//            {
//               %obj = TargetGems.getObject(%j);
//
//               if (%obj.deleted)
//                  continue;
//
//               %objpos = %obj.getPosition();
//               if (VectorDist(%objpos, %spawnpos) < 3.0)
//               {
//                  %badSpawn = true;
//                  break;
//               }
//            }
//
//            if (!%badSpawn)
//               break;
//         }
//
//         %gem = new Item() {
//            dataBlock = "GemItem";
//            collideable = "0";
//            static = "1";
//            rotate = "1";
//            permanent = "0";
//            deleted = "0";
//         };
//
//         %gem.setTransform(%spawnpos);
//         
//         TargetGems.add(%gem);
//
//         %light = new Item() {
//            dataBlock = "GemLightShaft";
//         };
//
//         %light.setTransform(%spawnpos);
//         
//         TargetGemsLights.add(%light);
//
//         %gem.gemLight = %light;
//         
//         return %gem;
//      }
//      else
//         error("No spawn points found in GemSpawns SimGroup");
//	}
//	else
//		error("Missing GemSpawns SimGroup");
//}

function spawnGemAt(%spawnPoint, %includeLight)
{
   if (!isObject(%spawnPoint))
   {
      error("Unable to spawn gem at specified point: spawn point does not exist" SPC %spawnPoint);
      return 0;
   }
   
   // if it has a gem on it, that is not good
   if (isObject(%spawnPoint.gem) && !%spawnPoint.gem.isHidden())
   {
      error("Gem spawn point already has an active gem on it");
      return %spawnPoint.gem;
   }
     
   // see if the spawn point has a custom gem datablock
   %gemDB = %spawnPoint.getGemDataBlock();
   if (!%gemDB)
      %gemDB = "GemItem";
      
   %gem = getGem(%gemDB);
   %gem.setBuddy(%includeLight);
   
   %gem.setTransform(%spawnPoint.getTransform());
   
   // point the gem on the spawn point
   %spawnPoint.gem = %gem;
      
   return %gem;
}

function getRandomObject(%groupName)
{
   %group = nameToID(%groupName);

   %object = 0;
   
   if (%group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         %index = getRandom(%count - 1);
         %object = %group.getObject(%index);
      }
   }
   return %object;
}

function getRandomActiveGemSpawnObject(%groupName)
{
   %group = nameToID(%groupName);

   %object = 0;
   
   if (%group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         %index = getRandom(%count - 1);
         %object = %group.getObject(%index);
         %tries = 0;
         while (!isObject(%object.gem) || %object.gem.isHidden() && %tries < 10000)
         {
            %index = getRandom(%count - 1);
            %object = %group.getObject(%index);
            %tries++;
         }
      }
   }
   return %object;
}

function findFinishPoint()
{
   for (%i = 0; %i < MissionGroup.getCount(); %i++)
   {
      %obj = MissionGroup.getObject(%i);
      if (%obj.getClassName() $= "Trigger" && %obj.getDatablock().getName() $= "FinishTrigger")
         return %obj;
      if (%obj.getClassName() $= "StaticShape" && %obj.getDatablock().getName() $= "FakeEndPad")
         return %obj;
      if (%obj.getClassName() $= "StaticShape" && %obj.getDatablock().getName() $= "EndPad")
         return %obj;
   }
   warn("Could not find finish point!");
   return 0;
}

// returns a random gem spawn point
function findActiveGemSpawn()
{
   if (isObject(GemSpawns))
      return getRandomActiveGemSpawnObject("MissionGroup/GemSpawns");
   return -1;
}

// returns a random gem spawn point
function findGemSpawn()
{
   if (isObject(GemSpawns))
      return getRandomObject("MissionGroup/GemSpawns");
   return -1;
}

// returns a random gem spawn group
function findGemSpawnGroup()
{
   return getRandomObject("GemSpawnGroups");
}

// test function
function gemSpawnReport()
{
   %group = nameToId("MissionGroup/GemSpawns");
   for (%i = 0; %i < %group.getCount(); %i++)
   {
      %spawn = %group.getObject(%i);
      if (isObject(%spawn.gem))
         echo(%spawn.gem.getDatablock().getName() SPC "object found on gem spawn point");
   }
}

// test function
function deleteSomeGemSpawnGroups()
{
   for (%i = GemSpawnGroups.getCount() - 1; %i > 0; %i--)
   {
      %x = GemSpawnGroups.getObject(%i);
      GemSpawnGroups.remove(%x);
      %x.delete();
   }
}

// returns a random gem spawn group that is sufficiently far from other active gem groups that still
// have gems in them.
function pickGemSpawnGroup()
{
   if ($Client::showPreviews)
      return findGemSpawnGroup();
   if (!isObject(ActiveGemGroups))
   {
      error("ActiveGemGroups is not an object");
      return findGemSpawnGroup();
   }
   
   %gemGroupRadius = $Server::GemGroupRadius;   
   // allow mission to override radius
   if (MissionInfo.gemGroupRadius > 0)
      %gemGroupRadius = MissionInfo.gemGroupRadius;
   echo("PickGemSpawnGroup: using radius" SPC %gemGroupRadius);
   
   // double the radius to make it that much more unlikely that we'll pick a group that is too close
   // to another active group
   %gemGroupRadius *= 2;
   
   // we'll make 6 attempts to find a group that is sufficiently far from other groups
   // after that we give up and use a random group
   %group = 0;
   for (%attempt = 0; %attempt < 6; %attempt++)
   {
      %group = findGemSpawnGroup();
      if (!isObject(%group))
      {
         error("findGemSpawnGroup returned non-object");
         return 0;
      }
      if (%group.getCount() == 0)
      {
         error("gem spawn group contains no spawn points");
         continue;
      }

      %groupSpawn = %group.getObject(0);
            
      // see if the spawn group is far enough from the primary spawn point of point of the active
      // gem groups
      %ok = true;
      for (%i = 0; %i < ActiveGemGroups.getCount(); %i++)
      {
         %active = ActiveGemGroups.getObject(%i);
         if (%active.getCount() == 0)
            continue;
         %gem = %active.getObject(0);
         if (VectorDist(%gem.getTransform(), %groupSpawn.getTransform()) < %gemGroupRadius)
         {
            %ok = false;
            break;
         }
      }

      if (%ok) 
         return %group;
   }
   
   // if we get here we did not find an appropriate group, just use a random one
   error("unable to find a spawn group that works with active gem groups, using random");
   return findGemSpawnGroup();
}

function emptyGemGroups()
{
   for (%i = 0; %i < ActiveGemGroups.getCount(); %i++)
   {
      %gemGroup = ActiveGemGroups.getObject(%i);
      
      for (%j = 0; %j < %gemGroup.getCount(); %j++)
      {
         removeGem(%gemGroup.getObject(%j));
      }
   }
}

// re-fill any gem groups that are empty
function refillGemGroups(%color)
{  
   if (!isObject(ActiveGemGroups))
   {
      error("ActiveGemGroups does not exist, can't refill gem groups");
      return;
   }
     
   %noMore = true;     
     
   for (%i = 0; %i < ActiveGemGroups.getCount(); %i++)
   {
      %gemGroup = ActiveGemGroups.getObject(%i);
      
      if (MissionInfo.gameMode $= "Color")
      {
         for (%j = 0; %j < %gemGroup.getCount(); %j++)
         {
            %obj = %gemGroup.getObject(%j);
            
            if (%obj.getDatablock().colorId == %color && !%obj.isHidden())
            {
               %noMore = false;
            }
         }
      }
      
      if (%gemGroup.getCount() == 0 || (%noMore && MissionInfo.gameMode $= "Color"))
      {
         /*for (%j = 0; %j < %gemGroup.getCount(); %j++)
         {
            %obj = %gemGroup.getObject(%j);
            %obj.deleted = true;
            %obj.delete();
         }*/
         
         for (%k = 0; %k < ActiveGemGroups.getCount(); %k++)
         {
            %gemGroup = ActiveGemGroups.getObject(%k);
            
            for (%j = 0; %j < %gemGroup.getCount(); %j++)
            {
               %obj = %gemGroup.getObject(%j);
               
               %obj.deleted = true;
               //%obj.setHidden(true);
               freeGem(%obj);
            }
         }
         
         // pick a new spawnGroup for the group
         %spawnGroup = pickGemSpawnGroup();
         if (!isObject(%spawnGroup))
         {
            error("Unable to locate gem spawn group, aborting");
            break;
         }
         %gemGroup.spawnGroup = %spawnGroup;
         fillGemGroup(%gemGroup);
      }
   }
}

// fill the specified gem group with gems using spawn points from its spawnGroup
function fillGemGroup(%gemGroup)
{
   %start = getRealTime();
   
   if (!isObject(%gemGroup))
   {
      error("Can't fill gem group, supplied group is not an object:" SPC %gemGroup);
      return;
   }

   %spawnGroup = %gemGroup.spawnGroup;
   if (!isObject(%spawnGroup))
   {
      error("Can't fill gem group, it does not contain a valid spawn group:" SPC %gemGroup);
      return;
   }
   
   // it should be empty already but clear it out anyway
   %gemGroup.clear();
   
   // refill it using spawn points from its spawn group
   for (%i = 0; %i < %spawnGroup.getCount(); %i++)
   {
      %spawn = %spawnGroup.getObject(%i);
      
      // don't spawn duplicate gems
      if (isObject(%spawn.gem) && !%spawn.gem.isHidden())
         continue;
      
      // spawn a gem and light at the spawn point
      %gem = spawnGemAt(%spawn,true);
            
      // add gem to gemGroup
      %gemGroup.add(%gem);
   }
   
   echo("Took" SPC (getRealTime() - %start) @ "ms to fill gem groups");
   
   $LastFilledGemSpawnGroup = %gemGroup;
}

// setup the gem groups
function SetupGems(%numGemGroups)
{
   %start = getRealTime();
   
   // delete any active gem groups and their associated gems
   if (isObject(ActiveGemGroups))
      ActiveGemGroups.delete();
      
   // get gem group configuration params
   %gemGroupRadius = $Server::GemGroupRadius;
   %maxGemsPerGroup = $Server::MaxGemsPerGroup;
      
   // allow mission to override
   if (MissionInfo.gemGroupRadius > 0)
      %gemGroupRadius = MissionInfo.gemGroupRadius;
   if (MissionInfo.maxGemsPerGroup > 0)
      %maxGemsPerGroup = MissionInfo.maxGemsPerGroup;
   echo("SetupGems: using radius" SPC %gemGroupRadius SPC "and max gems per group" SPC %maxGemsPerGroup);
   
   // clear the "gem" field on each spawn point.  these may contain bogus but valid object ids that were
   // persisted by the mission editor   
   %group = nameToId("MissionGroup/GemSpawns");
   if (!isObject(%group))
      return;
   for (%i = 0; %i < %group.getCount(); %i++)
      %group.getObject(%i).gem = 0;
   
   // set up the gem spawn groups (this is done in engine code to improve performance).
   // it will create a GemSpawnGroups object that contains groups of clustered spawn points
   SetupGemSpawnGroups(%gemGroupRadius, %maxGemsPerGroup);
   
   //GemSpawnGroups.dump();
   
   for (%i = 0; %i < GemSpawnGroups.getCount(); %i++)
   {
      %obj = GemSpawnGroups.getObject(%i);
	  
	  for (%j = 0; %j < %obj.getCount(); %j++)
	  {
		  %obj2 = %obj.getObject(%j);
		  if (%obj2.dataBlock $= "SpawnSphereMarker")
		  {
			%obj.remove(%obj2);
			echo("Found Player Spawn in Gem Spawns!");
		  }
	  }
   }
   
   // ActiveGemGroups contains groups of spawned gems.  the groups are populated using the spawn point
   // information from the ActiveGemGroups object.  when a group is empty, a new gem spawn group is 
   // selected for it, and then it is refilled with gems
   new SimGroup(ActiveGemGroups);
   MissionCleanup.add(ActiveGemGroups);
   
   // create the active gem groups
   for (%i = 0; %i < %numGemGroups; %i++)
   {
      %gemGroup = new SimGroup();
      ActiveGemGroups.add(%gemGroup);
   }
   
   // fill them up
   refillGemGroups();
   
   echo("Took" SPC (getRealTime() - %start) @ "ms to set up gems for mission");
}

function setupCollectionGems()
{
   %colors = $Game::ClientsJoined;
   %group = nameToID("MissionGroup/GemSpawns");
   for (%i = 0; %i < %group.getCount(); %i++)
   {
      %obj = %group.getObject(%i);
      
      if (%obj.getClassName() $= "SpawnSphere" && %obj.getDataBlock() $= "GemSpawnSphereMarker")
      {
         %rand = getRandom(0, %colors);
         echo("Rand: " @ %rand);
         switch$ (%rand)
         {
            case "1":
               %datablock = "GemItem_2pts";
               break;
            case "2":
               %datablock = "GemItemTurquoise";
               break;
            case "3":
               %datablock = "GemItemPurple";
               break;
            case "4":
               %datablock = "GemItem_5pts";
               break;
            case "5":
               %datablock = "GemItemOrange";
               break;
            case "6":
               %datablock = "GemItem_7pts";
               break;
            case "7":
               %datablock = "GemItemBlack";
               break;
            case "8":
               %datablock = "GemItemWhite";
               break;
            default:
               %datablock = "GemItem";
         }
         %obj.gemDataBlock(%datablock);
      }
   }
}

//-----------------------------------------------------------------------------
// GameConnection Methods
// These methods are extensions to the GameConnection class. Extending
// GameConnection make is easier to deal with some of this functionality,
// but these could also be implemented as stand-alone functions.
//-----------------------------------------------------------------------------

function GameConnection::incPenaltyTime(%this,%dt)
{
   %this.penaltyTime += %dt;
}

function GameConnection::incBonusTime(%this,%dt)
{
   %this.player.setMarbleBonusTime(%this.player.getMarbleBonusTime() + %dt);
}

//-----------------------------------------------------------------------------

function GameConnection::onClientEnterGame(%this)
{
   $Game::ClientsJoined++;
   if ($Game::ClientsJoined == 1 && $Server::Dedicated)
   {
      %this.isTempHost = true;
      commandToClient(%this, 'setTempHost', true);
   }
   commandToClient(%this, 'SetYellowText', $yellowText);
   fixAllowJumping();
   commandToClient(%this, 'NotifyGameMode', MissionInfo.gameMode);
   if (MissionInfo.gameMode $= "Color")
      %this.color = $Game::ClientsJoined - 1;
   else
      %this.color = -1;
      
   if (MissionInfo.gameMode $= "Collection")
   {
      %color = $Game::ClientsJoined - 1;
      switch$ (%color)
      {
         case "0":
            %this.color = "red";
         case "1":
            %this.color = "yellow";
         case "2":
            %this.color = "turquoise";
         case "3":
            %this.color = "purple";
         case "4":
            %this.color = "blue";
         case "5":
            %this.color = "orange";
         case "6":
            %this.color = "green";
         case "7":
            %this.color = "black";
         case "8":
            %this.color = "white";
      }
      setupCollectionGems();
   }
   
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'SetColorMode', %this.getId(), %this.color);
      setMarbleSize(%this, %client.getMarbleChoice());
      setMarbleSize(%client, %this.getMarbleChoice());
   }
   SetupGems(MissionInfo.numGems);
      // create a camera for the client
      if (!isObject(%this.camera))
      {
         // Create a new camera object.
         %this.camera = new Camera() {
            dataBlock = Observer;
         };
         MissionCleanup.add( %this.camera );
         %this.camera.scopeToClient(%this);
         
         if (isObject(%this.player))
            %this.camera.setTransform(%this.player.getEyeTransform());   
      }
      
   //if ($Server::Dedicated && $Game::ClientsJoined == 1)
   //   setGameState("start");
   
   if ($Game::State $= "wait")
   {
      // all clients enter wait state when joining
      %this.enterWaitState();
   }
   else
   {
      // this next line was commented out before. I don't know why it was commented out before. I just know it caused MP matches to infinitely load for people joining. - Anthony
      %this.onClientJoinGame();
      if ($Game::Running)
         commandToClient(%this, 'GameStart');
   }

   // Setup game parameters and create the player
   %this.resetStats();
   
   if (MissionInfo.gameMode $= "Sumo")
   {
	   if (MissionInfo.sumoLives)
	   {
		   %this.points = MissionInfo.sumoLives;
	   } else {
		   %this.points = 10;
	   }
         //commandToClient(%this,'setGemCount',%this.points,10);
         //commandToClient(%this, 'SetPoints', %client.getId(), 10);
		 allowConnect(false);
   }

   // tell the client whether we are in the lobby   
   messageClient(%this, 'MsgClientUpdateLobbyStatus', "", serverIsInLobby() );
   
   // tell him his score 
   messageClient(%this, 'MsgClientScoreChanged', "", %this, true, %this.points, %this.points);
   // tell him about everyone else's score
   for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
   {
      %cl = ClientGroup.getObject( %clientIndex );
      
      if (%cl == %this)
         continue;
         
      messageClient(%this, 'MsgClientScoreChanged', "", %cl, false, %cl.points, %cl.points);
      messageClient(%cl, 'MsgClientScoreChanged', "", %this, false, %this.points, %this.points);
   }

   if ($Game::packetLoss || $Game::packetLag)
      %this.setSimulatedNetParams($Game::packetLoss,$Game::packetLag/2);
}

function GameConnection::onClientJoinGame(%this)
{
	MissionLoadingProgress.setVisible(false);
	MissionLoadingText.setVisible(false);
	
   commandToClient(%this, 'ForcePlayGui');
   // If we don't have a player object then create one
   if (!isObject(%this.player))
      %this.spawnPlayer();

   // If this is the first client to join then start the game
   // Otherwise catchthis client up to the current game state
   if ($Game::State $= "wait" || MissionInfo.gameMode $= "")
   {
      // Start the game now
      startGame();

      // Start the game state machine
      setGameState("start");

      // Flag this client as the time keeper
      $timeKeeper = %this;
   }
   else
      %this.updateGameState();

   updateAvgPlayerCount();
   
   faceGems( %this.player );

   // keep Lobby status updated   
   messageClient(%this, 'MsgClientUpdateLobbyStatus', "", serverIsInLobby() );
}

function GameConnection::enterWaitState(%this)
{
   %spawnPoint = pickSpawnPoint();
   %spawnPos = getSpawnPosition(%spawnPoint);
   
   if (isSinglePlayerMode() || $Client::showPreviews)
   {
      /*if (!isObject($previewCamera))
      {
         $previewCamera = new Camera() {
            datablock = Observer;
         };
         
         // Make sure our $previewCamera doesn't end up in MissionCleanup
         if (isObject(MegaServerGroup))
            MegaServerGroup.add( $previewCamera );
         else
            ServerGroup.add( $previewCamera );
      }
      
      %this.camera = $previewCamera;*/
		
		commandToClient(%this, 'SetCamera');
   }

   if (!isObject(%this.camera))
   {
      // Create a new camera object.
      %this.camera = new Camera() {
         dataBlock = Observer;
      };
      MissionCleanup.add( %this.camera );
      %this.camera.scopeToClient(%this);
   }
   
      %this.camera.setTransform(%spawnPos);
   %this.setControlObject(%this.camera);
   
   // smoke the player
   if (isObject(%this.player))
      %this.player.delete();
   %this.player = 0;
}

function GameConnection::onClientLeaveGame(%this)
{
   $Game::ClientsJoined--;
   if (%this.isTempHost && $Game::ClientsJoined > 0)
   {
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         if (%this.getId() !$= %cl.getId())
         {
            %cl.isTempHost = true;
            break;
         }
      }
   }
   // Check to see if we need to set a new $timeKeeper
   if (%this == $timeKeeper && ClientGroup.getCount() > 1)
   {
      // Find a new $timeKeeper
      for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
      {
         %cl = ClientGroup.getObject( %clientIndex );
         if (%cl != %this)
         {
            $timeKeeper = %cl;
            break;
         }
      }
   }

   if (isObject(%this.camera) && %this.camera != $previewCamera)
      %this.camera.delete();
   if (isObject(%this.player))
      %this.player.delete();
   %this.camera = 0;
   %this.player = 0;

   // update average player count, but need to temporarily dec player
   // count (because count doesn't get decremented till later)
   $Server::PlayerCount--;
   updateAvgPlayerCount();
   $Server::PlayerCount++;
   
   SetupGems(MissionInfo.numGems);
}

function GameConnection::resetStats(%this)
{
   // Reset game stats
   %this.gemCount = 0;
   %this.points = 0;
   %this.rank = 0;
   %this.finishTime = 0;
   if (isObject(%this.player) && $Server::ServerType $= "SinglePlayer")
      %this.player.setMarbleTime(0);

   // Reset the checkpoint, except in single player modes
   if ($Server::ServerType !$= "SinglePlayer" && isObject(%this.checkPoint))
   {
      %this.checkPoint = 0;
      %this.checkPointPowerUp = 0;
      %this.checkPointShape = 0;
      %this.checkPointGemCount = 0;
      %this.checkPointBlastEnergy = 0.0;
      %this.checkPointGemConfirm = 0;
   }
}


//-----------------------------------------------------------------------------

function GameConnection::onEnterPad(%this)
{
//%mission = GameMissionInfo.getCurrentMission();
   // Handle level ending
   if (MissionInfo.gameType $= "SinglePlayer")
   {
      if (%this.player.getPad() == $Game::RealEndPad) {

         if ($Game::GemCount && ((%this.gemCount < $Game::GemCount && MissionInfo.gameMode !$= "Quota") || (%this.gemCount < MissionInfo.quotaCount && MissionInfo.gameMode $= "Quota"))) {
            %this.play2d(MissingGemsSfx);
            messageClient(%this, 'MsgItemPickup', $Text::Tagged::NotAllGems );
         }
         else
         {
            %this.player.setMode(Victory);
            messageClient(%this, 'MsgItemPickup', $Text::Tagged::Congratulations );
            serverplay2d(WonRaceSfx);
            setGameState("end");
			//$UserLevelsCompleted[%mission.level] = 1;
			//doSPAchievementWrite();
         }
      } else if (%this.player.getPad() == $Game::FakeEndPad) {
		 %this.play2d(angrySfx);
         messageClient(%this, 'MsgItemPickup', "You have unsuccessfully beaten this level!" );
         %this.player.setPad($Game::RealEndPad);
	  }
   }
}

function GameConnection::onLeavePad(%this)
{
   // Don't care if the leave
}

function GameConnection::onFinishPoint(%this)
{
   //error("onFinishPoint called, but shouldn't be?  (Race mode only");
   
   serverplay2d(WonRaceSfx);
   
   // Freeze the player
   %this.player.setMode(Victory);

   // Grab the marble time (which should stop when the mode is set to Victory)
   %this.finishTime = %this.player.getMarbleTime();

   // A new person finished
   $Game::ClientsFinished++;

   // If this is the first person to finish set a timer to end the whole race
   if (!isEventPending($finishPointSchedule))
      $finishPointSchedule = schedule(10000, 0, "endFinishPoint");
      
      if (MissionInfo.gameMode $= "race")
      {
		%this.wonAlready = true;
	  messageClient(%this, 'MsgClientScoreChangedRace', "", %this.getId(), true, $Game::ClientsFinished , 0);
      // send message to everybody except client telling them that he scored
      messageAllExcept(%this, -1, 'MsgClientScoreChangedRace', "", %this.getId(), false, $Game::ClientsFinished , 0);
	  
	 //playerlistgui.update(%this,returnRacePlayer(%this),0,0,0,$Game::ClientsFinished,0,0);
	  //PlayerListGui::updateScore(%this, $Game::ClientsFinished, $Game::ClientsFinished);
	  
   // Notify them of what palce they came in
   if ($Game::ClientsFinished == 1)
      %msg = "1st Place!";//$Text::Tagged::WonMP;
   else if ($Game::ClientsFinished == 2)
      %msg = "2nd Place!";//$Text::Tagged::SecondMP;
   else if ($Game::ClientsFinished == 3)
      %msg = "3rd Place!";//$Text::Tagged::ThirdMP;
   else if ($Game::ClientsFinished == 4)
      %msg = "4th Place!";//$Text::Tagged::ThirdMP;
   else if ($Game::ClientsFinished == 5)
      %msg = "5th Place!";//$Text::Tagged::ThirdMP;
   else if ($Game::ClientsFinished == 6)
      %msg = "6th Place!";//$Text::Tagged::ThirdMP;
   else if ($Game::ClientsFinished == 7)
      %msg = "7th Place!";//$Text::Tagged::ThirdMP;
   else if ($Game::ClientsFinished == 8)
      %msg = "8th Place!";//$Text::Tagged::ThirdMP;
   else
      %msg = "You Lose!";//'You got ' @ $Game::ClientsFinished @ ' place...';//$Text::Tagged::NthMP;

      } else {
         %msg = "Congratulations! You've finished!";
      }

   // JMQ: we aren't using race mode anymore so just send down "game over"
   //%msg = $Text::Tagged::GameOver;
   if (MissionInfo.gameMode $= "race")
   {
      messageClient(%this, 'MsgFinishRace', %msg, $Game::ClientsFinished);
   } else {
      messageClient(%this, 'MsgItemPickup', %msg, $Game::ClientsFinished);
   }
   //messageClient(%this, 'SetHelpLine', %msg);
  
   // If everyone has finished end the game
   if ($Game::ClientsFinished >= ClientGroup.getCount())
      endFinishPoint();
}

function GameConnection::onHitTrigger(%this, %obj, %points)
{
   if (MissionInfo.gameMode $= "Arcade")
   {
      %oldPoints = %this.points;
      %this.points = %oldPoints + %points;
      
      messageClient(%this, 'MsgClientScoreChanged', "", %this.getId(), true, %this.points, %oldPoints);
      // send message to everybody except client telling them that he scored
      messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this.getId(), false, %this.points, %oldPoints);
   }
}

function GameConnection::FinishGemMode(%this)
{
   %this.player.setMode(Victory);
   %this.finishTime = %this.player.getMarbleTime();
   messageClient(%this, 'MsgItemPickup', $Text::Tagged::Congratulations );
   serverplay2d(WonRaceSfx);
   setGameState("end");
}

function endFinishPoint()
{
   cancel($finishPointSchedule);
   
   // Rank the players
   buildRanks();

   setGameState("end");
}

function doRankNotifications()
{
   if (!isObject(PlayersRanks))
   {
      error("PlayersRanks does not exist, can't do rank notifications");
      return;
   }
   
   if (PlayersRanks.rowCount() == 0)
   {
      error("PlayersRanks has no rows, can't do rank notifications");
      return;
   }
   
   // assume the ranks are sorted in descending order
   // get the name of the leader
   %leader = PlayersRanks.getRowId(0);
   // %leader is a client Id (game connection object)
   %leaderName = %leader.name;
   %leaderPoints = %leader.points;
   
   for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
   {
      %cl = ClientGroup.getObject( %clientIndex );
      %rank = PlayersRanks.getRowTextById(%cl);
      
      if (%rank == PlayersRanks.rowCount() - 1)
      {
         %cl.player.setMode(Victory);
         //if ($tied)
         //   messageClient(%cl, 'MsgRaceOver', $Text::Tagged::TiedMP );
         //else
         //   messageClient(%cl, 'MsgRaceOver', $Text::Tagged::WonMP );
         %cl.play2D(WonRaceSfx);
      }
      else
      {
         %cl.player.setMode(Lost);
         //messageClient(%cl, 'MsgRaceOver', $Text::Tagged::YouLost );
         %cl.play2D(LostRaceSfx);
      }
      messageClient(%cl, 'MsgMPGameOver', "", $tied, %leaderName, %leaderPoints );
   }
}


//-----------------------------------------------------------------------------

function GameConnection::onOverload(%this)
{
   if ($Game::State $= "End" || $Game::State $= "wait")
      return;
      
   %this.setOutOfBoundsNew(true);

   if(!isEventPending(%this.respawnSchedule))
   {
      // Lose a gem if you fall off in Sumo
      if (MissionInfo.gameMode $= "Sumo")
      {
         %oldPoints = %this.points;
         %this.points--;
         
         if (%this.points <= 0)
         {
            messageClient(%this, 'MsgSumoPlayerLost', "", %this, true, %this.nameBase);
            
            messageAllExcept(%this, -1, 'MsgSumoPlayerLost', "", %this, false, %this.nameBase);
            
         } else {

            // send message to client telling him he scored (true parameter means msg recipient is scoring client)
            messageClient(%this, 'MsgClientScoreChanged', "", %this, true, %this.points, %oldPoints);
            // send message to everybody except client telling them that he scored
            messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this, false, %this.points, %oldPoints);
            
            //commandToClient(%this, 'setPoints', %this, %this.points);
         
         }

      }
   }
   if ((MissionInfo.gameMode $= "Sumo" && %this.points > 0) || MissionInfo.gameMode !$= "Sumo")
   {
      // Reset the player back to the last checkpoint
      commandToClient(%this,'setMessage',"overload",2000);
   } else if (MissionInfo.gameMode $= "Sumo" && %this.points <= 0) {
      commandToClient(%this,'setMessage',"gameOver",2000);
   }
   %this.player.setOOB(true);
   if(!isEventPending(%this.respawnSchedule))
   {
      if ((MissionInfo.gameMode $= "Sumo" && %this.points > 0) || MissionInfo.gameMode !$= "Sumo")
      {
         //if ($pref::Option::UseOOBSound)
         //   %this.play2d(OutOfBoundsVoiceSfx);
         %this.respawnSchedule = %this.schedule(2500, respawnPlayer);
         //if (MissionInfo.gameMode $= "Sumo")
         //{
            //commandToClient(%this,'setGemCount',%this.points,10);
         //}
      } else if (%this.points <= 0) {
         //echo("THINGY: " @ %this.name);
      } else {
         %this.respawnSchedule = %this.schedule(2500, removePlayer);
      }
   }
}

function GameConnection::onOutOfBounds(%this)
{
   if ($Game::State $= "End" || $Game::State $= "wait")
      return;
      
   %this.setOutOfBoundsNew(true);

   if(!isEventPending(%this.respawnSchedule))
   {
      // Lose a gem if you fall off in Sumo
      if (MissionInfo.gameMode $= "Sumo")
      {
         %oldPoints = %this.points;
         %this.points--;
         
         if (%this.points <= 0)
         {
            messageClient(%this, 'MsgSumoPlayerLost', "", %this, true, %this.nameBase);
            
            messageAllExcept(%this, -1, 'MsgSumoPlayerLost', "", %this, false, %this.nameBase);
            
         } else {

            // send message to client telling him he scored (true parameter means msg recipient is scoring client)
            messageClient(%this, 'MsgClientScoreChanged', "", %this, true, %this.points, %oldPoints);
            // send message to everybody except client telling them that he scored
            messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this, false, %this.points, %oldPoints);
            
            //commandToClient(%this, 'setPoints', %this, %this.points);
         
         }

      }
   }
   if ((MissionInfo.gameMode $= "Sumo" && %this.points > 0) || MissionInfo.gameMode !$= "Sumo")
   {
      // Reset the player back to the last checkpoint
      commandToClient(%this,'setMessage',"outOfBounds",2000);
   } else if (MissionInfo.gameMode $= "Sumo" && %this.points <= 0) {
      commandToClient(%this,'setMessage',"gameOver",2000);
   }
   %this.player.setOOB(true);
   if(!isEventPending(%this.respawnSchedule))
   {
      if ((MissionInfo.gameMode $= "Sumo" && %this.points > 0) || MissionInfo.gameMode !$= "Sumo")
      {
         if ($pref::Option::UseOOBSound)
            %this.play2d(OutOfBoundsVoiceSfx);
         %this.respawnSchedule = %this.schedule(2500, respawnPlayer);
         //if (MissionInfo.gameMode $= "Sumo")
         //{
            //commandToClient(%this,'setGemCount',%this.points,10);
         //}
      } else if (%this.points <= 0) {
         //echo("THINGY: " @ %this.name);
      } else {
         %this.respawnSchedule = %this.schedule(2500, removePlayer);
      }
   }
}

function Marble::onOOBClick(%this)
{
   cancel(%this.client.respawnSchedule);
   if($Game::State $= "play" || $Game::State $= "go")
   {
      %this.setOutOfBoundsNew(false);
      if (MissionInfo.gameMode $= "Sumo" && %this.client.points <= 0)
      {
         %this.client.removePlayer();
      } else {
         %this.client.respawnPlayer();
      }
   }
}

function GameConnection::onDestroyed(%this)
{
   if ($Game::State $= "End")
      return;

   // Reset the player back to the last checkpoint
   //PlayGui.setMessage("destroyed",2000); destroyed?
   %client.play2d(DestroyedVoiceSfx);
   %this.player.setOOB(true);
   if(!isEventPending(%this.respawnSchedule))
      %this.respawnSchedule = %this.schedule(2500, respawnPlayer);
}

function buildRanks()
{
   // MDFHACK: Please mommy don't hit me!
   if (!isObject(PlayersRanks))
      new GuiTextListCtrl(PlayersRanks);

   PlayersRanks.clear();

   // Shove the clients and their gemCounts into the textlist
   for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
   {
      %cl = ClientGroup.getObject( %clientIndex );

      if (MissionInfo.gameMode $= "Sumo" || MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Color")
         %val = %cl.points;
      else if (MissionInfo.gameMode $= "Race")
      {
         %val = %cl.finishTime;
         // JMQ: not all clients may have finished.  use marble time in this case
         if (%val == 0)
            %val = %cl.player.getMarbleTime();
      }
      else
         %val = %cl.gemCount;

      PlayersRanks.addRow(%cl, %val);
   }
   
   // set the relative ranks
   $tied = setRelativeRanks($Server::GameModeId, PlayersRanks);
}

function GameConnection::onFoundGem(%this,%amount,%gem,%points)
{
   if (MissionInfo.gameMode $= "Collection")
   {
      //if (%this.color $= %gem.getDataBlock().getSkinName())
      //{
         %remaining = $Game::gemCount - %this.gemCount;
         %oldPoints = %this.points;
         %this.points += 1;
         
         if(%remaining == 1)
            %msg = $Text::Tagged::OneGemLeft;
         else
            %msg = $Text::Tagged::NGemsLeft;
          
         messageClient(%this, 'MsgItemPickup', %msg, %remaining);
         %this.play2d(GotGemSfx);
         
         for(%cl = 0; %cl < ClientGroup.getCount(); %cl++)
         {
            %recipient = ClientGroup.getObject(%cl);
            if(%recipient != %this)
               %recipient.play3D(OpponentGotGemSfx, %this.getControlObject().getTransform());
         }
         if (MissionInfo.gameMode $= "Quota")
         {
            commandToClient(%this,'setGemCount',%this.points,MissionInfo.quotaCount, $Game::GemCount);
         } else {
            commandToClient(%this,'setGemCount',%this.points,$Game::GemCount);
         }
         
         messageClient(%this, 'MsgClientScoreChanged', "", %this.getId(), true, %this.points, %oldPoints);
         // send message to everybody except client telling them that he scored
         messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this.getId(), false, %this.points, %oldPoints);
      
      
         //if (%gem.isStatic())
         //{
            //if (%gem.getDataBlock().noRespawn)
               //%gem.setHidden(true);
            //else
            //{
               //if (%gem.respawnTime > 0)
                  //%gem.respawn(%gem.respawnTime);
               //else if (%gem.getDataBlock().respawnTime > 0)
                  //%gem.respawn(%gem.getDataBlock().respawnTime);
               //else
                  //%gem.respawn($Item::RespawnTime);
            //}
         //}
         //else
         //{
            //%gem.delete();
         //}
      //}
      return;
      
   } else if (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Sumo" || MissionInfo.gameMode $= "Color")
   {
      %gem.deleted = true;

      schedule(0, 0, "removeGem", %gem);

      %oldPoints = %this.points;
      
      if (MissionInfo.gameMode $= "Color")
      {
         if (%this.color $= %gem.getDataBlock().colorId)
            %this.points++;
         else
            %this.points--;
      } else {
         %this.points += %points;
      }

      // send message to client telling him he scored (true parameter means msg recipient is scoring client)
      %this.play2D(GotGemSfx);
      %count = ClientGroup.getCount();
      for(%cl= 0; %cl < %count; %cl++)
      {
         %recipient = ClientGroup.getObject(%cl);
         if(%recipient != %this)
            %recipient.play3D(OpponentGotGemSfx, %this.getControlObject().getTransform());
      }
      
      messageClient(%this, 'MsgClientScoreChanged', "", %this.getId(), true, %this.points, %oldPoints);
      // send message to everybody except client telling them that he scored
      messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this.getId(), false, %this.points, %oldPoints);
      
      //commandToClient(%this, 'setPoints', %this, %this.points);
      //if (%points == 1)
      //   messageClient(%this, 'MsgItemPickup', $Text::Tagged::GemPickupOnePoint);
      //else
      //   messageClient(%this, 'MsgItemPickup', $Text::Tagged::GemPickupNPoints, %points);

      return;
   } else if (MissionInfo.gameMode $= "Quota")
   {
      $Game::GemsFound += %points;
      %this.gemCount += %points;
      
      %remaining = MissionInfo.quotaCount - %this.gemCount;
      
      if (%remaining <= 0) {
         if (%this.gemCount >= MissionInfo.quotaCount)
         {
            if (%this.gemCount == MissionInfo.quotaCount)
            {
               messageClient(%this, 'MsgItemPickup', 'Quota Reached! Head for the finish!' );
               commandToClient(%this, 'SetMessage', "quota", 2000 );
            } else {
               //messageClient(%this, 'MsgItemPickup', 'You\'ve Gone Overboard!' );
               //%this.incBonusTime("-3");
               %this.onOverload();
            }
         }
         if (%this.gemCount $= MissionInfo.quotaCount || %this.gemCount $= $Game::GemCount)
            %this.play2d(GotAllGemsSfx);
         else
            %this.play2d(GotGemSfx);
      } else {
         if(%remaining == 1)
         %msg = $Text::Tagged::OneGemLeft;
         else
            %msg = $Text::Tagged::NGemsLeft;
          
         messageClient(%this, 'MsgItemPickup', %msg, %remaining);
         //messageClient(%this, 'MsgLocalizedItemPickup', %msg, %remaining);
         %this.play2d(GotGemSfx);
      }
      
      // If we are in single player mode, keep track of the gems we have found
      // with regard to checkpoint status so you can't cheat with checkpoints
      if( MissionInfo.gameType $= "Singleplayer" && isObject( %this.checkPoint ) )
      {
         %gem.checkPointConfirmationNumber = %this.checkPointGemConfirm + 1;
      }
      
      messageClient(%this, 'MsgQuotaGem', "", %points);
      
      commandToClient(%this,'setGemCount',%this.gemCount,MissionInfo.quotaCount, $Game::GemCount);
   
      return;
   }

   $Game::GemsFound += %amount;
   %this.gemCount += %amount;

   if (MissionInfo.gameMode $= "Hunt")
      %remaining = $Game::gemCount - $Game::GemsFound;
   else if (MissionInfo.gameMode $= "Quota")
      %remaining = MissionInfo.quotaCount - %this.gemCount;
   else
      %remaining = $Game::gemCount - %this.gemCount;

   if (%remaining <= 0) {
      if ((MissionInfo.gameType $= "Multiplayer" || $Game::SPGemHunt))
      {
         // Rank the players
         buildRanks();
         doRankNotifications();
		 
         setGameState("end");
         
      }
      else
      {
         if (MissionInfo.gameMode $= "Quota" && %this.gemCount >= MissionInfo.quotaCount)
         {
            if (%this.gemCount == MissionInfo.quotaCount)
            {
               messageClient(%this, 'MsgItemPickup', 'Quota Reached! Head for the finish!' );
               commandToClient(%this, 'SetMessage', "quota", 2000 );
            } else {
               //messageClient(%this, 'MsgItemPickup', 'You\'ve Gone Overboard!' );
               //%this.incBonusTime("-3");
               %this.onOverload();
            }
         }
         else
            messageClient(%this, 'MsgItemPickup', $Text::Tagged::HaveAllGems );
         
         if (MissionInfo.gameMode $= "Quota" && (%this.gemCount $= MissionInfo.quotaCount || %this.gemCount $= $Game::GemCount))
            %this.play2d(GotAllGemsSfx);
         else if (MissionInfo.gameMode $= "Quota")
            %this.play2d(GotGemSfx);
         else
            %this.play2d(GotAllGemsSfx);
         if (MissionInfo.gameMode !$= "Quota")
            //%this.gemCount = MissionInfo.quotaCount;
         //else
            %this.gemCount = $Game::GemCount;
         if (MissionInfo.gameMode $= "gems")
         {
            %this.FinishGemMode();
         }
      }
   }
   else
   {
      if(%remaining == 1)
         %msg = $Text::Tagged::OneGemLeft;
      else
         %msg = $Text::Tagged::NGemsLeft;
		 
      messageClient(%this, 'MsgItemPickup', %msg, %remaining);
	  //messageClient(%this, 'MsgLocalizedItemPickup', %msg, %remaining);
      %this.play2d(GotGemSfx);
   }
   
   // If we are in single player mode, keep track of the gems we have found
   // with regard to checkpoint status so you can't cheat with checkpoints
   if( MissionInfo.gameType $= "Singleplayer" && isObject( %this.checkPoint ) )
   {
      %gem.checkPointConfirmationNumber = %this.checkPointGemConfirm + 1;
   }
   
   if (MissionInfo.gameMode $= "Quota")
   {
      commandToClient(%this,'setGemCount',%this.gemCount,MissionInfo.quotaCount, $Game::GemCount);
   } else {
      commandToClient(%this,'setGemCount',%this.gemCount,$Game::GemCount);
   }
}

//-----------------------------------------------------------------------------

function GameConnection::spawnPlayer(%this)
{
   // Combination create player and drop him somewhere
   %this.checkPoint = 0; // clear any checkpoints
   %this.checkPointPowerUp = 0;
   %this.checkPointShape = 0;
   %this.checkPointGemCount = 0;
   %this.checkPointBlastEnergy = 0.0;
   %this.checkPointGemConfirm = 0;
   
   %spawnPoint = pickSpawnPoint();
   %this.createPlayer(%spawnPoint);
   serverPlay3d(spawnSfx, %this.player.getTransform());
}

function restartLevel()
{
   if( isObject( LocalClientConnection.checkPointShape ) )
      LocalClientConnection.checkPointShape.stopThread( 0 );
      
   if (isEventPending($Game::cycleSchedule))
      cancel($Game::CycleSchedule);
      
   LocalClientConnection.checkPoint = 0;
   LocalClientConnection.checkPointPowerUp = 0;
   LocalClientConnection.checkPointShape = 0;
   LocalClientConnection.CheckPointGemCount = 0;
   LocalClientConnection.checkPointBlastEnergy = 0.0;
   LocalClientConnection.checkPointGemConfirm = 0;
   LocalClientConnection.respawnPlayer();
   
   if (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Sumo" || MissionInfo.gameMode $= "Color")
   {
      if (isObject(ActiveGemGroups))
         ActiveGemGroups.delete();
      
      if (isObject(FreeGemGroups))
         FreeGemGroups.delete();
      //LocalClientConnection.points = 0;
      onMissionReset();
      setGameState("start");
      LocalClientConnection.player.setMode(Start);
      LocalClientConnection.player.setMarbleTime(0);
      LocalClientConnection.player.setBlastEnergy( 0.0 );
      LocalClientConnection.player.setPowerUp(0);
   }   
   
   if (MissionInfo.gameMode $= "Sumo")
   {
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %client = ClientGroup.getObject(%i);
         
         %client.points = 10;
         //commandToClient(%client,'setGemCount',%client.points,10);
      }
	//  allowConnect(false);
   
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %client = ClientGroup.getObject(%i);
      %client.spectatorForce = false;
   }
   
}
}
function GameConnection::setSpectatorMode(%this, %force)
{
   if (%force)
   {
      %this.spectatorForce = %force;  
   }
   %control = %this.camera;
   %control.mode = toggleCameraFly;
   
   %this.setControlObject(%control);
}

function GameConnection::removePlayer(%this)
{
   // Reset the player back to the last checkpoint
   cancel(%this.respawnSchedule);
   
   %this.setOutOfBoundsNew(false);
   
   // clear any messages being displayed to client
   commandToClient(%this, 'setMessage',"");
   
   // Put the player into spectate mode
   %this.setSpectatorMode(true);
   
   // Delete the player's marble
   %this.player.delete();
   
   %numDone = 0;
   
   //echo("PICKLES");
   
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %client = ClientGroup.getObject(%i);
      
      if (!isObject(%client.player))//(%client.spectatorForce)
      {
         //echo("Spectator: " @ %client.name);
         %numDone++;
      }
   }
   
   %players = ClientGroup.getCount();
   
   echo("Done: " @ %numDone);
   echo("Players: " @ %players);
   
   if (%numDone >= %players - 1)
   {
      %winner = 0;
      //echo("I IS POTATO");
      for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
      {
         %cl = ClientGroup.getObject( %clientIndex );
         commandToClient(%cl, 'setTimer', "set", $Game::Duration);
         
         if (isObject(%cl.player))
         {
            %winner = %cl;
         }
      }
      
      if (%players == 1)
      {
         echo("Only 1 Player -> Overriding Winner");
         %winner = ClientGroup.getObject(0);
      }
      
      //%winner.dump();

      buildRanks();
      
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         
         %winString = detag(%winner.nameBase) @ " is the winner!";
         
         messageClient(%cl, 'MsgItemPickup', %winString);
         
      }

      //doRankNotifications();
      
      setGameState("end");
   
   }
}

function GameConnection::respawnPlayer(%this)
{
   // Reset the player back to the last checkpoint
   cancel(%this.respawnSchedule);
   
   %this.setOutOfBoundsNew(false);
   
   // clear any messages being displayed to client
   commandToClient(%this, 'setMessage',"");

   if ($Server::ServerType $= "SinglePlayer" && !$Game::SPGemHunt && MissionInfo.gameMode !$= "Race" && MissionInfo.gameMode !$= "Arcade")
   {
      // if the player has no checkpoint, assume a full mission restart is occuring
      if (!isObject(%this.checkPoint))
      {
         onMissionReset();
         setGameState("start");
         %this.player.setMode(Start);
         %this.player.setMarbleTime(0);
         %this.gemCount = 0;
         if (MissionInfo.gameMode $= "Quota")
         {
            commandToClient(%this, 'setGemCount', %this.gemCount, MissionInfo.quotaCount, $Game::GemCount);
         } else {
            commandToClient(%this, 'setGemCount', %this.gemCount, $Game::GemCount);
         }
      }
      else
      {
         // Reset the moving platforms ONLY
         if( isObject( ServerGroup ) )
            ServerGroup.onMissionReset( %this.checkPointGemConfirm );
         
         // This animation is fucked up -pw
         //%this.checkPointShape.stopThread( 1 );
         //%this.checkPointShape.schedule( 200, "playThread", 1, "respawn" );
         %this.gemCount = %this.checkPointGemCount;
         if (MissionInfo.gameMode $= "Quota")
         {
            commandToClient(%this, 'setGemCount', %this.gemCount, MissionInfo.quotaCount, $Game::GemCount);
         } else {
            commandToClient(%this, 'setGemCount', %this.gemCount, $Game::GemCount);
         }
         
         %this.player.setBlastEnergy( %this.checkPointBlastEnergy );
      }
   }
   
   %this.player.setMarbleBonusTime(0);
   
   %powerUp = %this.player.getPowerUp();
   %this.player.setPowerUp(0, true);
   if ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt || MissionInfo.gameMode $= "Arcade")
      %this.player.setPowerUp(%powerUp);
   else if( $Server::ServerType $= "SinglePlayer" && isObject( %this.checkPoint ) && isObject( %this.checkPointPowerUp ) )
      %this.player.setPowerUp( %this.checkPointPowerUp );

   %this.player.setOOB(false);
   %this.player.setVelocity("0 0 0");
   %this.player.setVelocityRot("0 0 0");
   
   
   
   if (($Server::ServerType $= "SinglePlayer" || MissionInfo.gameMode $= "Race") && isObject(%this.checkPoint))
   {
      // Check and if there is a checkpoint shape, use that instead so the marble gets centered
      // on it properly. Mantis bug: 0000819
      if( isObject( %this.checkPointShape ) )
         %spawnPoint = %this.checkPointShape;
      else
         %spawnPoint = %this.checkPoint;
   }
   else
   {
      // in multiplayer mode, try to use the spawn point closest to the 
      // marble's last contact position.  if this position is invalid 
      // the findClosest function will defer to pickSpawnPoint()
      if ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt || MissionInfo.gameMode $= "Arcade")
         %spawnPoint = findClosestSpawnPoint(%this.player.getLastContactPosition());
      else
      {
         // If we restarted the level, reset the blast energy
         %this.player.setBlastEnergy( 0.0 );
         
         %spawnPoint = pickSpawnPoint();
      }
   }

   %spawnPos = getSpawnPosition(%spawnPoint);
   
   %this.player.setPosition(%spawnPos, 0.45);
   setGravity(%this.player, %spawnPoint);
   
   faceGems( %this.player );
   
   //%this.player.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);
   
   serverPlay3d(spawnSfx, %this.player.getTransform());
}

//-----------------------------------------------------------------------------

function GameConnection::getMarbleChoice( %this )
{
// assume that we might have 100 marbles someday plus templates
   switch(//$pref::marbleIndex)
   %this.marbleIndex )
   {
      case 1:
         return Marble2;
      case 2:
         return Marble3;
      case 3:
         return Marble4;
      case 4:
         return Marble5;
      case 5:
         return Marble6;
      case 6:
         return Marble7;
      case 7:
         return Marble8;
      case 8:
         return Marble9;
      case 9:
         return Marble10;
      case 10:
         return Marble11;
      case 11:
         return Marble12;
      case 12:
         return Marble13;
      case 13:
         return Marble14;
      case 14:
         return Marble15;
      case 15:
         return Marble16;
      case 16:
         return Marble17;
      case 17:
         return Marble18;
      case 18:
         return Marble19;
      case 19:
         return Marble20;
      case 20:
         return Marble21;
      case 21:
         return Marble22;
      case 22:
         return Marble23;
      case 23:
         return Marble24;
      case 24:
         return Marble25;
      case 25:
         return Marble26;
      case 26:
         return Marble27;
      case 27:
         return Marble28;
      case 28:
         return Marble29;
      case 29:
         return Marble30;
      case 30:
         return Marble31;
      case 31:
         return Marble32;
	  case 32:
         return Marble33;         
      case 33:
         return Marble34;
      case 34:
         return Marble35;
      case 35:
         return Marble36;
      case 36:
         return Marble37;
      case 37:
         return Marble38;
      case 38:
         return Marble39;
      case 39:
         return Marble40;
      case 40:
         return Marble41;
      case 41:
         return Marble42;
      case 42:
         return Marble43;
      case 43:
         return Marble44;
      case 44:
         return Marble45;
      case 45:
         return Marble46;
      case 46:
         return Marble47;
      case 47:
         return Marble48;
	  case 48:
		 return Marble49;
      case 49:
         return Marble50;
      case 50:
         return Marble51;
      case 51:
         return Marble52;
      case 52:
         return Marble53;
      case 53:
         return Marble54;
      case 54:
         return Marble55;
      case 55:
         return Marble56;
      case 56:
         return Marble57;
      case 57:
         return Marble58;
      case 58:
         return Marble59;
      case 59:
         return Marble60;
      case 60:
         return Marble61;
      case 61:
         return Marble62;
      case 62:
         return Marble63;
      case 63:
         return Marble64;
      case 64:
         return Marble65;
      case 65:
         return Marble66;
      case 66:
         return Marble67;
      case 67:
         return Marble68;
      case 68:
         return Marble69;
      case 69:
         return Marble70;
      case 70:
         return Marble71;
      case 71:
         return Marble72;
      case 72:
         return Marble73;
      case 73:
         return Marble74;
      case 74:
         return Marble75;
      case 75:
         return Marble76;
      case 76:
         return Marble77;
      case 77:
         return Marble78;
      case 78:
         return Marble79;
      case 79:
         return Marble80;
      case 80:
         return Marble81;
      case 81:
         return Marble82;
      case 82:
         return Marble83;
      case 83:
         return Marble84;
      case 84:
         return Marble85;
      case 85:
         return Marble86;
      case 86:
         return Marble87;
      case 87:
         return Marble88;
      case 88:
         return Marble89;
      case 89:
         return Marble90;
      case 90:
         return Marble91;
      case 91:
         return Marble92;
      case 92:
         return Marble93;
      case 93:
         return Marble94;
      case 94:
         return Marble95;
      case 95:
         return Marble96;
      case 96:
         return Marble97;
      case 97:
         return Marble98;
      case 98:
         return Marble99;
      case 99:
         return Marble100;
      case 100:
         return Marble101;
      case 101:
         return Marble102;
      case 102:
         return Marble103;
      case 103:
         return Marble104;
      case 104:
         return Marble105;
   }
   
   return Marble1;
}

function GameConnection::createPlayer(%this, %spawnPoint)
{
   if (%this.player > 0)  {
      // The client should not have a player currently
      // assigned.  Assigning a new one could result in
      // a player ghost.
      error( "Attempting to create an angus ghost!" );
   }
   
   setMarbleSize(%this, %this.getMarbleChoice());  
   
   %player = new Marble() {
      dataBlock = %this.getMarbleChoice();
      client = %this;
   };
   
   MissionCleanup.add(%player);
   
   %this.playerDataBlock = %player.dataBlock;

   // Player setup...
   %spawnPos = getSpawnPosition(%spawnPoint);
   %player.setPosition(%spawnPos, 0.45);
   %player.setEnergyLevel(60);
   %player.setShapeName(%this.name);
   %player.client.status = 1;
   if ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt)
      %player.setUseFullMarbleTime(true);
   setGravity(%player, %spawnPoint);
   //%player.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);

   // Update the camera to start with the player
   if (isObject(%this.camera))
      %this.camera.setTransform(%player.getEyeTransform());       
   
   // In multi-player, set the position, and face gems
   faceGems( %player );
   echo( "Created Player." );

   // Give the client control of the player
   %this.player = %player;
   //%this.setControlObject(%player);
   //commandToClient(%this, 'playerAdded');
   
   %this.player.hidden = 0;
   %this.playerDataBlock.cameraDistance = 2.5;
   %this.playerDataBlock.jumpImpulse = 7.5;
   
   if ($CameraStyle $= "Side") {
      %this.playerDataBlock.cameraDistance = 10;
      %this.playerDataBlock.jumpImpulse = 10;
   }
   if ($CameraStyle $= "Inside") {
      %this.player.hidden = 1;
      %this.playerDataBlock.cameraDistance = 0.01;
   }
   
   if (!isObject(%this.camera))
   {
      // Create a new camera object.
      %this.camera = new Camera() {
         dataBlock = Observer;
      };
      MissionCleanup.add( %this.camera );
      %this.camera.scopeToClient(%this);
      
      if (isObject(%this.player))
         %this.camera.setTransform(%this.player.getEyeTransform());   
   }
   
   if ($Server::ServerType $= "SinglePlayer")
   {
      %this.setControlObject(%this.camera);
      %this.addPlayer();
   } else {
      %this.setControlObject(%this.player);
   }
}

function GameConnection::addPlayer(%this, %tries)
{
   if (%tries $= "")
   {
      echo("Ghosting Player...");
      %tries = 0;
   }
   %playerGhostID = %this.getGhostID(%this.player);
   if (%playerGhostID == -1)
   {
      %tries++;
      warn("Failed to Ghost Player...Retrying... (Tried " @ %tries @ " times)");
      %this.schedule(10, "addPlayer", %tries);
      return;
   }
   commandToClient(%this, 'playerGhosted', %playerGhostID);
}


//-----------------------------------------------------------------------------

function GameConnection::setCheckpoint(%this, %checkPoint)
{
   // The CheckPoint triggers should have a sequence field that can
   // help us keep them in order
   if (%checkPoint.sequence >= %this.checkPoint.sequence && gravityIsEqual( %this.player, %checkpoint ) )
   {
      
      %sameOne = %checkPoint == %this.checkPoint;
      

      if (!%sameOne)
      {
         %this.checkPointGemConfirm++;
         //echo("checkpoint: " @ %this.checkPointGemConfirm);

         %this.checkPoint = %checkPoint;
         %this.checkPointPowerUp = %this.player.getPowerUp();
         %this.checkPointGemCount = %this.gemCount;
         %this.checkPointBlastEnergy = %this.player.getBlastEnergy();

         %chkGrp = %checkPoint.getGroup();
         
         for(%i = 0; ( %chkShape = %chkGrp.getObject( %i ) ) != -1; %i++ )
         {
            // This looks wonkey, however "checkpointShape" is the name of the datablock which
            // the shapes use. -pw
            if( %chkShape.getClassName() $= "StaticShape" && %chkShape.getDatablock().getId() == checkpointShape.getId() )
            {
               if( isObject( %this.checkPointShape ) )
                  %this.checkPointShape.stopThread( 0 );
               
               %this.checkPointShape = %chkShape;
               break;
            }
         }
   
         serverplay2d(CheckpointSfx);
         %this.checkPointShape.stopThread( 0 );
         %this.checkPointShape.playThread( 0, "activate" );
         commandToClient(%this, 'CheckpointHit', %checkPoint.sequence );
      }
   }
}

function setGravity(%player, %spawnobj)
{
   %rotation = getWords(%spawnobj.getTransform(), 3);
   %ortho = vectorOrthoBasis(%rotation);
   %orthoinv = getWords(%ortho, 0, 5) SPC VectorScale(getWords(%ortho, 6), -1);
   %player.setGravityDir(%orthoinv,true);
}

function gravityIsEqual( %shapebaseA, %shapebaseB )
{
   // Hack for now
   return true;
   
   %rotationA = getWords( %shapebaseA.getTransform(), 3 );
   %orthoA = vectorOrthoBasis( %rotationA );
   %orthoinvA = getWords( %orthoA, 0, 5 ) SPC VectorScale( getWords( %orthoA, 6 ), -1 );
   
   %rotationB = getWords( %shapebaseB.getTransform(), 3 );
   %orthoB = vectorOrthoBasis( %rotationB );
   %orthoinvB = getWords( %orthoB, 0, 5 ) SPC VectorScale( getWords( %orthoB, 6 ), -1 );
   
   echo( "ShapeBaseA (" @ %shapebaseA.getName() @ "): " @ %orthoinvA );
   echo( "ShapeBaseB (" @ %shapebaseB.getName() @ "): " @ %orthoinvB );
   
   return %orthoinvA $= %orthoinvB;
}

function getSpawnPosition(%spawnobj)
{
   if (!isObject(%spawnobj))
      return "0 0 0";

   %pos = %spawnobj.getWorldBoxCenter();
   %rotation = getWords(%spawnobj.getTransform(), 3);

   if (isSinglePlayerMode() || $Client::showPreviews)
      %offset = MatrixMulVector(%spawnobj.getTransform(), "0 0 1.5");
   else
      %offset = MatrixMulVector(%spawnobj.getTransform(), "0 0 0.5");

   %pos = VectorAdd(%pos, %offset);

   return %pos SPC %rotation;
}

$Server::PlayerSpawnMinDist = 3.0;

function findClosestSpawnPoint(%position)
{
   %spawn = 0;

   %group = nameToID("MissionGroup/SpawnPoints");
   if (%position !$= "" && %group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         %closest = 0;
         %closestDist = 0;
         for (%i = 0; %i < %count; %i++)
         {
            %spawn = %group.getObject(%i);
            %spawnpos = %spawn.getPosition();
            
            %dist = VectorDist(%spawnpos, %position);
            
            if (%closest == 0 || %dist < %closestDist)
            {
               %closest = %spawn;
               %closestDist = %dist;
            }
         }
         %spawn = %closest;
      }
   }
   
   if (!isObject(%spawn))
   {
      // defer to pickSpawnPoint()
      error("Unable to find closest spawn point to position, defering to pickSpawnPoint().  Input position:" SPC %position);
      %spawn = pickSpawnPoint();
   }
   else
   {
      // Use the container system to iterate through all the objects
      // within the spawn radius.
      InitContainerRadiusSearch(%spawn.getPosition(), $Server::PlayerSpawnMinDist, $TypeMasks::PlayerObjectType);

      if (containerSearchNext())
      {
         echo("Player spawn point occupied, picking random spawn");
         // spawn point occupied, get a random one
         %spawn = pickSpawnPoint();
      }
   }
   
   return %spawn;
}

// test function
function echoClosestMarble()
{
   if (!isObject(LocalClientConnection.player))
   {
      echo("can't find player");
      return;
   }
   InitContainerRadiusSearch(LocalClientConnection.player.getPosition(), 1000, $TypeMasks::PlayerObjectType);
   %marble = containerSearchNext();
   if (%marble.getId() == LocalClientConnection.player.getId())
      %marble = containerSearchNext();
   if (isObject(%marble))
   {
      %dist = VectorDist(%marble.getPosition(), LocalClientConnection.player.getPosition());
      echo("closest marble is" SPC %dist SPC "units");
   }
   else
   {
      echo("no other marbles found in radius");
   }
}

function pickSpawnPoint()
{
   if ($Client::showPreviews)
      return nameToID("MissionGroup/CameraObj");
   %group = nameToID("MissionGroup/SpawnPoints");

   if (%group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         for (%i = 0; %i < %count; %i++)
         {
            %index = getRandom(%count-1);
            %spawn = %group.getObject(%index);
            %spawnpos = %spawn.getPosition();

            // Use the container system to iterate through all the objects
            // within the spawn radius.
            InitContainerRadiusSearch(%spawnpos, $Server::PlayerSpawnMinDist, $TypeMasks::PlayerObjectType);

            if (!containerSearchNext())
               return %spawn;
         }

         // Unable to find an empty pad so spawn at a random one
         %index = getRandom(%count-1);
         %spawn = %group.getObject(%index);

         return %spawn;
      }
      else
         error("No spawn points found in SpawnPoints SimGroup");
	}
	else
   {
		error("Missing SpawnPoints SimGroup");

      %missionGroup = GameMissionInfo.getCurrentMission().missionGroup;

      if (isObject(%missionGroup))
         return GameMissionInfo.getCurrentMission().cameraPoint;
   }

	// Could be no spawn points, in which case we'll stick the
	// player at the center of the world.
   return 0;
}

//-----------------------------------------------------------------------------
// Manage game state
//-----------------------------------------------------------------------------

// This is used to inform all of the clients that the
// server is in a "wait" state
function setWaitState(%client)
{
   if (MissionInfo.gameMode $= "Quota")
   {
      commandToClient(%client, 'setGemCount', 0, MissionInfo.quotaCount, $Game::GemCount);
   } else {
      commandToClient(%client, 'setGemCount', 0, $Game::GemCount);
   }
   commandToClient(%client, 'setTimer', "reset");
   commandToClient(%client, 'setMessage', "");
   commandToClient(%client, 'setGameDuration', 0, 0);
}

// This is used to inform all of the clients that the
// server is in a "start" state
function setStartState(%client)
{
   if (MissionInfo.gameMode $= "Quota")
   {
      commandToClient(%client, 'setGemCount', 0, MissionInfo.quotaCount, $Game::GemCount);
   } else {
      commandToClient(%client, 'setGemCount', 0, $Game::GemCount);
   }
   %help = MissionInfo.startHelpText;
   // For some reason the startHelpText cannot be set to nothing.
   // To prevent it from flashing on the screen for a quick second, set it to something invisible. - Anthony
   if (%help $= "")
   {
      %help = " ";
   //   commandToClient(%client,'removeHelpLine', true);
   }
   //else
   //{
      commandToClient(%client, 'setHelpLine', %help);
      commandToClient(%client,'removeHelpLine');
   //}
   commandToClient(%client, 'setMessage',"");

   commandToClient(%client, 'setTimer', "reset");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);
   else
      commandToClient(%client, 'setGameDuration', 0, 0);
   
   if (MissionInfo.location $= "" || MissionInfo.location $= "-1")
   {
      %client.setDisplayLocation(false);
   } else {
      %client.setLocation(MissionInfo.location);
      %client.setDisplayLocation(true);
   }
   
   if (isObject($timeKeeper) && %client == $timeKeeper)
      %client.player.setMarbleTime(0);
   else if (isObject($timeKeeper))
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());

	
	%client.wonAlready = false;	  
   %client.player.setMode(Start);
}

function checkRacePos()
{
	cancel($checkRacePosSched);
	if(!MissionInfo.gameMode $= "race" || !$Game::Running)
		return;
	
	//for(%i = 0; %i < ClientGroup.getCount(); %i++)
	//{
			
		//%cl = ClientGroup.getObject(%i);

		//%marble = %cl.player;
		//%pos = %marble.getPosition();
	
		//%playerDists = 0;
		//%playerIndices = 0;
	
		//%lastInit = false;
		//%lastLowest = 0;
		//%lastIndex = 0;
		//%lastLowest2 = 0;
	
		//for(%k = 0; %k < ClientGroup.getCount(); %k++)
		//{
		
	
			for(%j = 0; %j < ClientGroup.getCount(); %j++)
		{
			
				%cl2 = ClientGroup.getObject(%j);
				%marble2 = %cl2.player;
				%padpos = %marble2.getPad().getPosition();
			
				%dist = VectorDist(%marble2.getPosition(), %padpos);
				
				 %playerDists[%j] = %dist;
				 %playerIndices[%j] = %j;
			
				// //if((%dist < %lastLowest[%k] || !%lastInit[%k]) && %dist > %lastLowest)
				// //{
				
			// //		%lastLowest[%k] = %dist;
				// //	%lastInit[%k] = true;
			// //		%lastIndex[%k] = %j;
		// //			%lastLowest2 = %dist;
			// //	}
			
			// }
		// //}
		
		// //%marbleDistsFromFinish = [6, 8, 3, 4, 5, 7, 10];
		// %newPlayerDists = 0;
		// %temp = 0;
		// %templength = ClientGroup.getCount();
		// %length2 = ClientGroup.getCount();
		// for (%m=0; %m < %templength; %m++) {
			// %temp = 0;
			// for (%j=0; %j < %length2; %j++) {
				// if(%playerDists[%j] == -1 || %playerDists[%temp] == -1)
					// continue;
				// if(%playerIndices[%j] == -1 || %playerIndices[%temp] == -1)
					// continue;
				// if (%playerDists[%temp] > %playerDists[%j]) {
					// //error(%marbleDistsFromFinish[%temp] + " is less than " + %marbleDistsFromFinish[%j]);
					// %temp = %j;
				// }
			// }
			// %newPlayerDists[%m] = %playerDists[%temp];
			// %newPlayerIndices[%m] = %playerIndices[%temp];
			// %playerDists[%temp] = -1;
			// %playerIndices[%temp] = -1;
			//%length2 -= 1;
			//%playerDists.splice(%temp, 1);
		}
		//console.log(marbleDistsFromFinishNew);
		
%playerDistsNew = 0;
%playerIndicesNew = 0;
%playerDistIndex = 0;
%len = ClientGroup.getCount();
// echo("-----------------------Start----------------------------");
		// for(%l = 0; %l < ClientGroup.getCount(); %l++)
		// {
		
			// msgbox(%l @ ": " @ %playerDistsNew[%l] @ " | " @ %playerIndicesNew[%l]);
		
		// }
		// echo("-----------------------End----------------------------");
for (%i = 0;%i < %len; %i++) {
    %playerDistIndex = 0;
    while(%playerDists[%playerDistIndex] == -1) {
        %playerDistIndex++;
    }
    for (%j=0;%j < ClientGroup.getCount(); %j++) {
        if(%playerDists[%j] != -1) {
		//msgbox(%playerDists[%j]);
            if (%playerDists[%j] < %playerDists[%playerDistIndex]) {
         
			//	msgbox(%playerDists[%j] @ " is less than " @ %playerDists[%playerDistIndex]);
				%playerDistIndex = %j;
			}
        }
    }
    %playerDistsNew[%i] = %playerDists[%playerDistIndex];
    %playerIndicesNew[%i] = %playerIndices[%playerDistIndex];
    %playerDists[%playerDistIndex] = -1;
}
%playerDists = %playerDistsNew;
%playerIndices = %playerIndicesNew;
		for(%l = 0; %l < ClientGroup.getCount(); %l++)
		{
			//echo("VAL: " @ %l @ " | " @ %newPlayerIndices[%l]);
			%cl = ClientGroup.getObject(%playerIndicesNew[%l]);
			if(%cl.wonAlready)
				continue;
			%position = %l + 1;
		
			messageClient(%cl, 'MsgClientScoreChangedRace', "", %cl, false, %position , 0);
			messageAllExcept(%cl, -1, 'MsgClientScoreChangedRace', "", %cl, true, %position , 0);
		
		}
		
		//{
		
			//%cl2 = ClientGroup.getObject(%j);

			//if(%cl $= %cl2)
			//	continue;

			//%marble2 = %cl2.player;
			//%pos2 = %marble.getPad().getPosition();
		
			//%dist = VectorDist(%pos, %pos2);
			
			// messageClient(%cl, 'MsgClientScoreChangedRace', "", %cl, false, %dist , 0);
			// messageAllExcept(%cl, 'MsgClientScoreChangedRace', "", %cl, true, %dist , 0);
		
		//}
			
	//}
	$checkRacePosSched = schedule(250, 0, checkRacePos);
}

// This is used to inform all of the clients that the
// server is in a "ready" state
function setReadyState(%client)
{
   commandToClient(%client, 'setMessage', "ready");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);
   if (MissionInfo.gameMode $= "race")
      serverplay2d(ReadyVoiceSfx);
      
   if (MissionInfo.location $= "" || MissionInfo.location $= "-1")
   {
      %client.setDisplayLocation(false);
   } else {
      %client.setLocation(MissionInfo.location);
      %client.setDisplayLocation(true);
   }

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
}

// This is used to inform all of the clients that the
// server is in a "set" state
function setSetState(%client)
{
   commandToClient(%client, 'setMessage', "set");

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
   
   
   if (MissionInfo.location $= "" || MissionInfo.location $= "-1")
   {
      %client.setDisplayLocation(false);
   } else {
      %client.setLocation(MissionInfo.location);
      %client.setDisplayLocation(true);
   }
}

// This is used to inform all of the clients that the
// server is in a "go" state
function setGoState(%client)
{
   commandToClient(%client, 'setMessage', "go");
   if (MissionInfo.gameMode $= "race")
      serverplay2d(GetRollingVoiceSfx);
   commandToClient(%client, 'setTimer', "start");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);


      %client.player.setPad($Game::FinishFlag);

   if (MissionInfo.gameType $= "SinglePlayer")
   {
      if (isObject($Game::FakeEndPad))
         $Game::EndPad = $Game::FakeEndPad;
      else
         $Game::EndPad = $Game::RealEndPad;
      %client.player.setPad($Game::EndPad);
   }
      
   //if (MissionInfo.gameMode $= "Race")
      //%client.player.setPad($Game::FinishFlag);

   %client.player.setMode(Normal);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
      
   if (MissionInfo.location $= "" || MissionInfo.location $= "-1")
   {
      %client.setDisplayLocation(false);
   } else {
      %client.setLocation(MissionInfo.location);
      %client.setDisplayLocation(true);
   }
}

// This is used to inform all of the clients that the
// server is in a "play" state
function setPlayState(%client)
{
   commandToClient(%client, 'setMessage', "");

   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, $Server::PlayStart);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
   
   if (MissionInfo.location $= "" || MissionInfo.location $= "-1")
   {
      %client.setDisplayLocation(false);
   } else {
      %client.setLocation(MissionInfo.location);
      %client.setDisplayLocation(true);
   }   
   
	if(MissionInfo.gameMode $= "race"){
		//$Race::TimePassed = 0;
		$checkRacePosSched = schedule(250, 0, checkRacePos);
	}  	  
}

// This is used to inform all of the clients that the
// server is in a "end" state
function setEndState(%client, %winner)
{
   commandToClient(%client, 'setTimer', "stop");
   commandToClient(%client, 'setEndTime', $Server::PlayEnd);
}

function packRanksForClient()
{
   if (!isObject(PlayersRanks))
   {
      error("packRanksForClient: PlayersRanks does not exist");
      return "";
   }
   
   // First the gameMode
   %data = MissionInfo.gameMode;
   
   // Second is the mission id
   %data = %data SPC MissionInfo.level;
   
   // Third is the "ScoreBase" for this game mode: par time for race, total number of gems collected
   // for hunt, 0 for sumo.  this is used to compute score multipliers
   %scoreBase = 0;
   if (MissionInfo.gameMode $= "race")
   {
      %scoreBase = MissionInfo.goldTime;
   }
   else if (MissionInfo.gameMode $= "hunt")
   {
      // walk the clients and sum up the gems
      for (%i = 0; %i < PlayersRanks.rowCount(); %i++)
      {
         %row = PlayersRanks.getRowText(%i);
         %gems = getWord(%row, 1);
         %scoreBase += %gems;
      }
   }
   %data = %data SPC %scoreBase;

   // Fourth is the number of clients
   %data = %data SPC PlayersRanks.rowCount();
   
   // Now push back the client data
   for ( %i = 0; %i < PlayersRanks.rowCount(); %i++ )
   {
      %clid = PlayersRanks.getRowId(%i);
      %val = PlayersRanks.getRowText(%i);

      %data = %data SPC %clid SPC %val;
   }

   return %data;
}

// The server maintains the state and transitions to other states
function setGameState(%state)
{
   // Skip out of the current transitions
   if (isEventPending($stateSchedule))
      cancel($stateSchedule);

   $stateSchedule = 0;
   
   if (%state $= "play")
      $Server::PlayStart = getSimTime();
   if (%state $= "end")
      $Server::PlayEnd = getSimTime();

   // Update client states
   for (%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
   {
      %cl = ClientGroup.getObject( %clientIndex );

      switch$ (%state)
      {
         case "wait"  :
            setWaitState(%cl);
         case "start" :
            setStartState(%cl);
         case "ready" :
            setReadyState(%cl);
         case "go"    :
            setGoState(%cl);
         case "play"  :
            setPlayState(%cl);
         case "end"   :
            setEndState(%cl);
      }

      commandToClient(%cl, 'setGameState', %state);
   }

   // schedule the transition to the next state
   switch$ (%state)
   {
      case "start" :
         $stateSchedule = schedule(500, 0, "setGameState", "ready");
      case "ready" :
         $stateSchedule = schedule(3000, 0, "setGameState", "go");
//      case "set"   :
//         $stateSchedule = schedule(1500, 0, "setGameState", "go");
      case "go"    :
         $stateSchedule = schedule(2000, 0, "setGameState", "play");
         if ($Game::Duration)
         {
            $Game::CycleSchedule = schedule($Game::Duration + 90, 0, "onGameDurationEnd" );
            if ($Server::ServerType $= "MultiPlayer" || $Game::SPGemHunt)
            {
               // block join in progress in last minute in all games.  it may be already blocked
               // (e.g. in ranked games)
               %blockAt = $Game::Duration - 60 * 1000;
               if (%blockAt > 0)
               {
                  $Game::BlockJIPSchedule = schedule(%blockAt, 0, "serverSetJIPEnabled", false);
               }  
            }
         }
      case "end"   :
         $stateSchedule = schedule( 5000, 0, "endGame");
   }
   
   $Game::State = %state;
}

// This is used to "catch up" newly joining clients
function GameConnection::updateGameState(%this)
{
   switch$ ($Game::State)
   {
      case "wait"  :
         setWaitState(%this);
      case "start" :
         setStartState(%this);
      case "ready" :
         setReadyState(%this);
      case "set"   :
         setSetState(%this);
      case "go"    :
         setGoState(%this);
      case "play"  :
         setPlayState(%this);
      case "end"   :
         setEndState(%this);
   }

   commandToClient(%this, 'setGameState', $Game::State);
}

//-----------------------------------------------------------------------------
// Support functions
//-----------------------------------------------------------------------------

function serverCmdJoinGame(%client)
{
   %client.onClientJoinGame();
}

function serverCmdSetWaitState(%client)
{
   // %JMQ: in mp games, will probably only want to destroy the game when the host enters wait state
   destroyGame();
   %client.enterWaitState();
}

function spawnStupidMarble(%val, %forceNew)
{
   if (%val)
      commandToServer('spawnStupidMarble', %forceNew);
}

function serverCmdSpawnStupidMarble(%client,%forceNew)
{
   %obj = %client.getControlObject();
   if (isObject(%obj))
   {
      if( !%forceNew && isObject( %client.dummyMarble ) )
         %client.dummyMarble.delete();
      
      %pos = %obj.getPosition();
      %dummy = new Marble()
      {
         dataBlock = %client.getMarbleChoice();
      };
      MissionCleanup.add(%dummy);
      %client.dummyMarble = %dummy;

      %dummy.setPosition(VectorAdd(%pos,"0 1.5 1"), 0.45);
      %dummy.setShapeName(%client.getMarbleChoice());
      %dummy.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);
      echo("stupid marble id" SPC %dummy SPC "position" SPC %pos);
   }
}

$pickerVelocity = "0 3 0";
$pickerRandomVelx = 2;
$pickerRandomVely = 1.5;
$pickerRandomVelz = 1;

function serverCmdSpawnMarblePickerMarble(%client)
{
   if( isObject( %client.dummyMarble ) )
      %client.dummyMarble.delete();
   
   %pos = "0 -2 0.5";//$previewCamera.getPosition();
   %dummy = new Marble()
   {
      dataBlock = %client.getMarbleChoice();
   };
   MissionCleanup.add(%dummy);
   %client.dummyMarble = %dummy;

   %velAdd = (1-2*getRandom()) * $pickerRandomVelx;
   %velAdd = %velAdd SPC (1-2*getRandom()) * $pickerRandomVely;
   %velAdd = %velAdd SPC (1-2*getRandom()) * $pickerRandomVelz;
   %vel = VectorAdd($pickerVelocity,%velAdd);

   %dummy.setPosition(VectorAdd(%pos,"0 1 1"), 0.45);
   %dummy.setVelocity(%vel);
   %dummy.setShapeName(%client.getMarbleChoice());
   %dummy.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);
}

function serverCmdDestroyMarblePickerMarble(%client)
{
   if( isObject( %client.dummyMarble ) )
      %client.dummyMarble.delete();
   
   %client.dummyMarble = -1;
}
//-----------------------------------------------------------------------------

function countGems(%group)
{
   // Count up all gems out there are in the world
   %gems = 0;
   for (%i = 0; %i < %group.getCount(); %i++)
   {
      %object = %group.getObject(%i);
      %type = %object.getClassName();
      if (%type $= "SimGroup")
         %gems += countGems(%object);
      else
         if (%type $= "Item" &&
               %object.getDatablock().classname $= "Gem")
            %gems++;
   }
   return %gems;
}

//-----------------------------------------------------------------------------
// Averagae player count code -- track integral of # of players over time
//-----------------------------------------------------------------------------

function updateAvgPlayerCount()
{   
   if (isObject($timeKeeper))
   {
      // update player count averages
      echo("Updating average player count...");
      %time = $timeKeeper.player.getMarbleTime()/1000;
      %delta = %time-$Game::avgPlayerCountTime;
      echo("   Previous count was" SPC $Game::avgPlayerCount SPC "for" SPC $Game::avgPlayerCountTime SPC "seconds.");
      if (%delta<0)
      {
         // safety...
         error("updateAvgPlayerCount: negative time...");
         %delta=0;
      }
      echo("   Adding in" SPC $Game::lastPlayerCount SPC "players over an additional" SPC %delta SPC "seconds.");
      $Game::avgPlayerCount = $Game::avgPlayerCount * $Game::avgPlayerCountTime + $Game::lastPlayerCount * %delta;
      if (%time>0.0001)
         $Game::avgPlayerCount /= %time;
      $Game::avgPlayerCountTime = %time;
      $Game::lastPlayerCount =    $Server::PlayerCount;
      echo("   New average player count is" SPC $Game::avgPlayerCount SPC "and there are currently" SPC $Game::lastPlayerCount SPC "players in the game");
   }
   else
   {
      // Not nessecarily an error anymore.
      echo("No time keeper marble. Oooh, is this one of those exciting new level previews!?");
      resetAvgPlayerCount();
   }
}

function resetAvgPlayerCount()
{
   $Game::avgPlayerCount = $Server::PlayerCount;
   $Game::avgPlayerCountTime = 0;
   $Game::lastPlayerCount = 0;
}

function getAvgPlayerCount()
{
    return $Game::avgPlayerCount;
}

// Sets if Jumping is allowed or not based on MissionInfo
function fixAllowJumping()
{
   if (MissionInfo.noJumping)
   {
      %jumping = false;
   } else {
      %jumping = true;
   }
   
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'SetAllowJumping', %jumping);
   }
}

function GameConnection::setOutOfBoundsNew(%this, %flag)
{
   $Game::ISOOB = %flag;
   commandToClient(%this, 'setOutOfBoundsNew', %flag);
}

//-----------------------------------------------------------------------------

function simLag(%loss,%lag)
{
   $Game::packetLoss = %loss;
   $Game::packetLag = %lag;
   // if in single player mode, don't change parameters on
   // connections now, wait till game loads.
   if (!isSinglePlayerMode())
   {
      for (%i=0; %i < ClientGroup.getCount(); %i++)
      {
         %client = ClientGroup.getObject(%i);
         %client.setSimulatedNetParams(%loss,%lag/2);
      }
      if (!isBroadcastSimNetParams())
         // not broadcasting sim net params, so set local value here
         ServerConnection.setSimulatedNetParams(%loss,%lag/2);
   }
   error("simLag called with loss" SPC %loss SPC "and lag" SPC %lag);
}
