
datablock StaticShapeData(canopy)
{
   className = "canopy";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/canopy.dts";
   scopeAlways = true;
};

datablock StaticShapeData(canopy2)
{
   className = "canopy";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/canopy2.dts";
   scopeAlways = true;
};

datablock StaticShapeData(ramp)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/ramp.dts";
   scopeAlways = true;
};

datablock StaticShapeData(curve2)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/curve2.dts";
   scopeAlways = true;
};

datablock StaticShapeData(piece)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/piece.dts";
   scopeAlways = true;
};

datablock StaticShapeData(platforms)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/platforms.dts";
   scopeAlways = true;
};

datablock StaticShapeData(platforms2)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/platforms2.dts";
   scopeAlways = true;
};

datablock StaticShapeData(holes)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/holes.dts";
   scopeAlways = true;
};

datablock StaticShapeData(main)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/main.dts";
   scopeAlways = true;
};

datablock StaticShapeData(arch)
{
   className = "ramp";
   category = "polysoup";
   shapeFile = "~/data/shapes/polysoup/arch.dts";
   scopeAlways = true;
};

if ($pref::polysoup_addon::useOldMethod)
{
   datablock StaticShapeData(pieceCopy)
   {
      className = "ramp";
      category = "polysoup";
      shapeFile = "~/data/shapes/polysoup/piece - Copy.dts";
      scopeAlways = true;
   };

   datablock StaticShapeData(holesCopy)
   {
      className = "ramp";
      category = "polysoup";
      shapeFile = "~/data/shapes/polysoup/holes - Copy.dts";
      scopeAlways = true;
   };

   datablock StaticShapeData(mainCopy)
   {
      className = "ramp";
      category = "polysoup";
      shapeFile = "~/data/shapes/polysoup/main - Copy.dts";
      scopeAlways = true;
   };
}
