// UPDATED ASTROLABE SHAPES (SEPARATE CLOUD LAYERS)

datablock TSShapeConstructor(astrolabe_Shape)
{
  baseShape = "~/data/shapes/astrolabe/astrolabe.dts";
  sequence0 = "~/data/shapes/astrolabe/astrolabe_root.dsq root";
};

datablock StaticShapeData(astrolabeShape)
{
   className = "astrolabe";
   category = "Misc";
   shapeFile = "~/data/shapes/astrolabe/astrolabe.dts";
   scopeAlways = true;
   sortLast = true;
   astrolabe = true;
   astrolabePrime = false;
};

datablock StaticShapeData(astrolabeCloudsBeginnerShape)
{
   className = "clouds";
   category = "Misc";
   shapeFile = "~/data/shapes/astrolabe/astrolabe_clouds_beginner.dts";
   scopeAlways = true;
   astrolabe = true;
   astrolabePrime = true;
};

datablock StaticShapeData(astrolabeCloudsIntermediateShape)
{
   className = "clouds";
   category = "Misc";
   shapeFile = "~/data/shapes/astrolabe/astrolabe_clouds_intermediate.dts";
   scopeAlways = true;
   astrolabe = true;
   astrolabePrime = true;
};

datablock StaticShapeData(astrolabeCloudsAdvancedShape)
{
   className = "clouds";
   category = "Misc";
   shapeFile = "~/data/shapes/astrolabe/astrolabe_clouds_advanced.dts";
   scopeAlways = true;
   astrolabe = true;
   astrolabePrime = true;
};

function astrolabeShape::onAdd(%this,%obj)
{
   %obj.playThread(0,"root");
}

// OTHER SHAPES

datablock StaticShapeData(checkpointShape)
{
   className = "checkpoint";
   category = "pads";
   shapeFile = "~/data/shapes/pads/checkpad.dts";
   scopeAlways = true;
};

datablock StaticShapeData(glass_3shape)
{
   className = "glass_3";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_3.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_6shape)
{
   className = "glass_6";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_6.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_9shape)
{
   className = "glass_9";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_9.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_12shape)
{
   className = "glass_12";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_12.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_15shape)
{
   className = "glass_15";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_15.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_18shape)
{
   className = "glass_18";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_18.dts";
   scopeAlways = true;
   glass = true;
};


datablock StaticShapeData(glass_flat)
{
   className = "glass_flat";
   category = "structures";
   shapeFile = "~/data/shapes/structures/glass_flat.dts";
   scopeAlways = true;
   glass = true;
};

datablock StaticShapeData(TriTwist_Glass)
{
   className = "tritwist_glass";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/tritwist_glass.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Horizon_0_Glass)
{
   className = "tritwist_glass";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/horizon_0_glass.dts";
   scopeAlways = true;
};

//datablock StaticShapeData(billboard)
//{
   //className = "billboard";
   //category = "structures";
   //shapeFile = "~/data/shapes/structures/billboard.dts";
   //scopeAlways = true;
//};
//
//datablock StaticShapeData(billboard_hanging)
//{
   //className = "billboard";
   //category = "structures";
   //shapeFile = "~/data/shapes/structures/billboard_hanging.dts";
   //scopeAlways = true;
//};

/*datablock StaticShapeData(BlastClub)
{
   className = "blastclub";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/blastclub.dts";
   scopeAlways = true;
};*/

/*datablock StaticShapeData(Bowl)
{
   className = "bowl";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/bowl.dts";
   scopeAlways = true;
};*/

/*datablock StaticShapeData(Core)
{
   className = "core";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/core.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Epicenter)
{
   className = "epicenter";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/epicenter.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Forkinroad)
{
   className = "forkinroad";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/forkinroad.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Gauntlet)
{
   className = "gauntlet";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/gauntlet.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Gemsinroad)
{
   className = "gemsinroad";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/gemsinroad.dts";
   scopeAlways = true;
};*/

// Next to Implements
//------------------------

/*datablock StaticShapeData(HalfPipe0)
{
   className = "half-pipe0";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/half-pipe0.dts";
   scopeAlways = true;
};

datablock StaticShapeData(HalfPipe2)
{
   className = "half-pipe2";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/half-pipe2.dts";
   scopeAlways = true;
};*/

/*datablock StaticShapeData(HopSkipJump)
{
   className = "hopskipjump";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/hopskipjump.dts";
   scopeAlways = true;
};*/

/*datablock StaticShapeData(Horizon)
{
   className = "horizon";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/horizon.dts";
   scopeAlways = true;
};

datablock StaticShapeData(MarblePlay)
{
   className = "marbleplay";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/marbleplay.dts";
   scopeAlways = true;
};*/
/*
datablock StaticShapeData(RampMatrix)
{
   className = "rampmatrix";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/rampmatrix.dts";
   scopeAlways = true;
};

datablock StaticShapeData(RoyaleFull)
{
   className = "royale_full";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/royale_full.dts";
   scopeAlways = true;
};
*/
/*datablock StaticShapeData(SprawlWalled)
{
   className = "sprawl_walled";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/sprawl_walled.dts";
   scopeAlways = true;
};

datablock StaticShapeData(ThrillRide)
{
   className = "thrillride";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/thrillride.dts";
   scopeAlways = true;
};*/
/*
datablock StaticShapeData(TornadoMap)
{
   className = "tornadomap";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/tornado.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Tornado3)
{
   className = "tornado3";
   category = "electro";
   shapeFile = "~/data/shapes/mobile/tornado3.dts";
   scopeAlways = true;
};

datablock StaticShapeData(TriTwist)
{
   className = "tritwist";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/tritwist.dts";
   scopeAlways = true;
};

datablock StaticShapeData(TriTwistMid)
{
   className = "tritwist_mid";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/tritwist_mid.dts";
   scopeAlways = true;
};
*/
/*datablock StaticShapeData(Triumvirate)
{
   className = "triumvirate";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/triumvirate.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Upward)
{
   className = "upward";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/upward.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Zenith)
{
   className = "zenith";
   category = "mobile";
   shapeFile = "~/data/shapes/mobile/zenith.dts";
   scopeAlways = true;
};*/

//exec("./polysoup.cs");
