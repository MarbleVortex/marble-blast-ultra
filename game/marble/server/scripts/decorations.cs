datablock StaticShapeData(LightBulb)
{
   className = "LightBulb";
   category = "decorations";
   shapeFile = "~/data/shapes/decorations/lightbulb.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Tree)
{
   className = "Tree";
   category = "decorations";
   shapeFile = "~/data/shapes/environment/tree.dts";
   scopeAlways = true;
};

datablock StaticShapeData(Flower)
{
   className = "Flower";
   category = "decorations";
   shapeFile = "~/data/shapes/environment/flower.dts";
   scopeAlways = true;
};

function Flower::onAdd(%this, %obj)
{
	if (%obj.skin !$= "")
	{
		%obj.setSkinName(%obj.skin);
	}
}

datablock StaticShapeData(TallGrass)
{
   className = "TallGrass";
   category = "decorations";
   shapeFile = "~/data/shapes/environment/tallgrass.dts";
   scopeAlways = true;
};
