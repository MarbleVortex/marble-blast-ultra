datablock ItemData(FinishFlag)
{
   category = "Flags";
   className = "FinishFlag";   
   
   shapeFile = "~/data/shapes/flags/flag.dts";
   emap = true;
   
   //renderGemAura = true;
   //gemAuraTextureName = "~/data/textures/gemAura";
   //noRenderTranslucents = true;
   referenceColor = "0.9 0.9 0.9";
   
   addToHUDRadar = true;
   
   permanent = true;
};

function FinishFlag::onAdd(%this,%obj)
{
   %obj.scale = "3 3 3";
   //%obj.playThread(0,"ambient");
   
   $Game::FinishFlag = %obj;
}