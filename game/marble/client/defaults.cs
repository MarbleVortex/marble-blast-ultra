//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------
// The master server is declared with the server defaults, which is
// loaded on both clients & dedicated servers.  If the server mod
// is not loaded on a client, then the master must be defined. 
//$pref::Master[0] = "2:v12master.dyndns.org:28002";
//$pref::highScoreName = "Alex";
//$pref::Player::Name = "Alex";
$pref::Player::defaultFov = 90;
$pref::Player::zoomSpeed = 0;
$pref::Net::LagThreshold = 400;

//$pref::Net::PacketRateToClient = "32";
//$pref::Net::PacketRateToServer = "32";
//$pref::Net::PacketSize = "450";

$pref::Net::PacketRateToClient = "1024";
$pref::Net::PacketRateToServer = "1024";
$pref::Net::PacketSize = "450";

$pref::shadows = "2";
$pref::HudMessageLogSize = 40;
$pref::ChatHudLength = 1;
$pref::useStencilShadows = true;
$pref::Input::LinkMouseSensitivity = 1;
// DInput keyboard, mouse, and joystick prefs
$pref::displayMPHelpText = 1;
$pref::Input::KeyboardEnabled = 1;
$pref::Input::MouseEnabled = 1;
$pref::Input::JoystickEnabled = 0;
$pref::Input::KeyboardTurnSpeed = 25.0;
$pref::Input::MouseSensitivity = 0.005;
$pref::Input::InvertYAxis = false;
// how did this get in here, and why is it false when it's always true
$pref::Input::AlwaysFreeLook = false;
$pref::sceneLighting::cacheSize = 20000;
$pref::sceneLighting::purgeMethod = "lastCreated";
$pref::sceneLighting::cacheLighting = 1;
$pref::sceneLighting::terrainGenerateLevel = 1;
$pref::Terrain::DynamicLights = 1;
$pref::Interior::TexturedFog = 0;


// Note: folowing moved to example/main.cs because of moved canvas init
$pref::Video::displayDevice = "D3D";
$pref::Video::allowOpenGL = 1;
$pref::Video::allowD3D = 1;
$pref::Video::preferOpenGL = 1;
$pref::Video::appliedPref = 0;
$pref::Video::disableVerticalSync = 1;
$pref::Video::monitorNum = 0;
$pref::Video::resolution = "800 600 32";
$pref::Video::wideScreen = false;
$pref::Video::windowedRes = "1280 720";
$pref::Video::fullScreen = "0";


$pref::showBuildNumberDisplay = "1";
$pref::OpenGL::force16BitTexture = "0";
$pref::OpenGL::forcePalettedTexture = "0";
$pref::OpenGL::maxHardwareLights = 3;
$pref::VisibleDistanceMod = 1.0;
$pref::SFX::provider = "FMod";
$pref::SFX::useHardware = false;
$pref::SFX::maxSoftwareBuffers = 8;
$pref::Audio::forceMaxDistanceUpdate = 0; // JMQMERGE is this MBU specific?
$pref::Audio::environmentEnabled = 0;
$pref::SFX::OpenALVolumeScale = 0.0; // base volumes are for direct sound, so multiply volumes by this when using OpenAL 
$pref::SFX::FModVolumeScale = 0.0; // base volumes are for direct sound, multiply volumes by this when using Fmod
$pref::SFX::masterVolume   = 10.0;
$pref::SFX::channelVolume1 = 10.0;
//$pref::SFX::channelVolume2 = 0.0;
$pref::SFX::channelVolume3 = 10.0;
$pref::SFX::channelVolume4 = 10.0;
$pref::SFX::channelVolume5 = 10.0;
$pref::SFX::channelVolume6 = 10.0;
$pref::SFX::channelVolume7 = 10.0;
$pref::SFX::channelVolume8 = 10.0;
$pref::SFX::fadeInTime = 500;
$pref::SFX::fadeOutTime = 500;

$pref::Client::AutoStart = true;
$pref::Client::AutoStartMission = "marble/data/missions/intermediate/Urban Jungle/urban.mis";
$pref::Client::AutoStartMissionIndex = 21;

$Client::MatchMode = 0; // standard, 1 = ranked
$Client::UseGGConnectMasterServer = false;
$Option::DefaultMusicVolume = 0.25;
$Option::DefaultFXVolume = 0.75;
$pref::Option::MusicVolume = $Option::DefaultMusicVolume;
$pref::Option::FXVolume = $Option::DefaultFXVolume;
$pref::Option::UseSecretSound = "1";

$pref::Language = "";

$Shell::gameModeFilter = 0; // currently not possible to wildcard game mode
$Shell::maxPlayersFilter = -1;

$Demo::DefaultStartingTime = 7 * 60 * 1000; // 7 minutes
$Demo::TimeRemaining = $Demo::DefaultStartingTime; 
//$Demo::TimeRemaining = 10 * 1000; // 10 seconds for testing

$Client::gracePeriodMS = 45 * 1000; // 45 seconds?
