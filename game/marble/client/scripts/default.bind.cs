//-----------------------------------------------------------------------------
// Torque Shader Engine
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

$xInputStartDelay = 100; ///< milliseconds of delay to wait before calling enableXInput()

if ( isObject( moveMap ) )
moveMap.delete();
new ActionMap(moveMap);

if ( isObject( chatMap ) )
   chatMap.delete();
new ActionMap( chatMap );

$screenShotMode = true;

function screenShotMode()
{
	$screenShotMode = !$screenShotMode;

	RootShapeNameHud.setVisible($screenShotMode);
	PlayGui.setVisible($screenShotMode);
	PlayerListGui.setVisible($screenShotMode);

	if ($screenShotMode)
	{
		//if ($Game::SPGemHunt)
		Canvas.pushDialog(PlayerListGui);
	} else {
		//if ($Game::SPGemHunt)
		Canvas.popDialog(PlayerListGui);
	}

	//   CenterMessageDlg.setVisible($screenShotMode);
	//   HUD_ShowGem.setVisible($screenShotMode);
	//   SUMO_NegSign.setVisible($screenShotMode);
	//   GemsFoundHundred.setVisible($screenShotMode);
	//   GemsFoundTen.setVisible($screenShotMode);
	//   GemsFoundOne.setVisible($screenShotMode);
	//   GemsSlash.setVisible($screenShotMode);
	//   GemsTotalHundred.setVisible($screenShotMode);
	//   GemsTotalTen.setVisible($screenShotMode);
	//   GemsTotalOne.setVisible($screenShotMode);
	//   Min_Ten.setVisible($screenShotMode);
	//   Min_One.setVisible($screenShotMode);
	//   MinSec_Colon.setVisible($screenShotMode);
	//   Sec_Ten.setVisible($screenShotMode);
	//   Sec_One.setVisible($screenShotMode);
	//   MinSec_Point.setVisible($screenShotMode);
	//   Sec_Tenth.setVisible($screenShotMode);
	//   Sec_Hundredth.setVisible($screenShotMode);
	//   PG_NegSign.setVisible($screenShotMode);
	//   PG_BoostBar.setVisible($screenShotMode);
	//   HelpTextForeground.setVisible($screenShotMode);
	//   ChatTextForeground.setVisible($screenShotMode);
	//   HUD_ShowPowerUp.setVisible($screenShotMode);
	//
	//   PlayerListGuiList.setVisible($screenShotMode);
	//   RootShapeNameHud.setVisible($screenShotMode);
}

GlobalActionMap.bindCmd(keyboard, "ctrl l", "", "screenShotMode();");

function saveMegaMission()
{
	levelOneGroup.setHidden(true);
	MegaMissionGroup.setName("MissionGroup");
	MissionGroup.save("marble/data/missions/megaMission.mis");
	MissionGroup.setName("MegaMissionGroup");
	levelOneGroup.setHidden(false);
	error("Saved marble/data/missions/megaMission.mis");
}

function MultiplayerListOnY()
{
	echo("Switching Player List Size");
	togglePlayerListLength();
}

GlobalActionMap.bindCmd(keyboard, "alt y", "", "MultiplayerListOnY();");

//GlobalActionMap.bindCmd(keyboard, "alt m", "", "saveMegaMission();");

function pauseToggle(%defaultItem)
{
	if( $GameEndNoAllowPause )
	return;

	if (ServerConnection.isMultiplayer)
	%defaultItem = 0;

	if( $gamePauseWanted )
	{
		Canvas.popDialog(GamePauseGui);

		// Make sure controler remove isn't up
		%doNotResume = false;

		for( %i = 0; %i < Canvas.getCount(); %i++ )
		{
			if( Canvas.getObject(%i) == ErrorUnplugGui.getId() ||
					Canvas.getObject(%i) == NetworkDisconnectGui.getId() )
			{
				%doNotResume = true;
			}
		}

		if( %doNotResume == false )
		resumeGame();
	}
	else
	{
		Canvas.pushDialog(GamePauseGui);
		PauseMenu.setSelectedIndex(%defaultItem);
		pauseGame();
	}
}

//------------------------------------------------------------------------------
// Non-remapable binds
//------------------------------------------------------------------------------
function stopMissionLoad()
{
	error("Stopping mission load");
	$missionSequence++;
	$missionRunning = false;
}

// its ok for both of these params to be empty
function escapeFromGame(%forcePreviewMode, %userEvent)
{
	if (isUsingBrowserUi() && ($naturalEnd))
	{
		if (! $GameEndHandshakeWaiting)
		{
			if (%forcePreviewMode || %userEvent)
			{
				// this is an error condition, screw things up so we can catch it as
				// soon as possible.
				ggcErrorLog("attempting handshake with unhandled params in escapeFromGame(" @ %forcePreviewMode @ ", " @ %userEvent @ ")");
				return;
			}
			$GameEndHandshakeCaller = "escapeFromGame";
			Game::initiateGameEndHandshake();
			return;
		}

		if (! $GameEndHandshakeAcknowledged)
		{
			// do not allow this function to complete until handshake acknowledged
			return;
		}
	}

	// ignore user's attempts to escape while missionloading is awake.  IGC hack, may want to keep it though
	if (%userEvent && MissionLoadingGui.isAwake())
	{
		error("Ignoring user escapeFromGame since MissionLoading is awake");
		return;
	}

	// GGCTODO: this function may need some rework to interop properly with GGC UI (don't display upsell gui, etc)
	$Client::willfullDisconnect = true;

	%killMission = MissionLoadingGui.isAwake();

	// unpause
	// supress GGCSessionResumed messages
	$DontSendGGCResume = true;
	if ($gamePauseWanted)
	GamePauseGui.doUnpause();
	$DontSendGGCResume = false;

	//fadeOutAudio();
	
	//RG_Water.setVisible(false);
	resetWaterTrigger(LocalClientConnection.playerDataBlock);
	RG_Teleport.setVisible(false);

	// if we are hosting a multiplayer server, we just re-enter preview mode
	// without disconnecting
	if ($Server::Hosting || $Client::TempHost)
	{
		// return to Lobby unless they want to back out to preview mode
		if (!%forcePreviewMode && ($Server::UsingLobby || $Client::TempHost))
		enterLobbyMode(); // GGCTODO: send HostEnterLobby message here?
		else
		{
		enterPreviewMode();
		}

		// this is needed for games that are aborted during mission load
		if (%killMission)
		{
		   if ($Client::TempHost)
		   {
		      commandToServer('tempHostQuit');
		   } else {
            //it is only safe to do this if we are hosting
            endMission();
            // clients (and us) may be loading a mission, and we need to put
            // a stop to that to prevent bad states (rendering a partially loaded
            // mission, etc).  this is one way to do it...
            stopMissionLoad();
		   }
		}
	}
	// multiplayer client's just disconnect
	else if ($Client::connectedMultiplayer) { // this is also true for hosts so we check hosting first
	   disconnect();
		loadPreviewMission();
		connectToServer("");
	}
	// if single player game is running, exit to level preview
	else if ($Game::Running || PlayGui.isAwake() || MissionLoadingGui.isAwake() || GameEndGui.isAwake())
	{

		// need to end the current game
		//commandToServer('SetWaitState');
		if (!isObject($disconnectGui))
		$disconnectGui = levelPreviewGui;
		RootGui.setContent($disconnectGui);
		
		endMission();
		stopMissionLoad();
		
		endPreviewMission();
		loadPreviewMission();
	}
	else if (!isUsingBrowserUI())
	{
		// inside shell but not in game session: go back to previous gui

		
		RootGui.contentGui.onB();
	}
}

//------------------------------------------------------------------------------
// Movement Keys
//------------------------------------------------------------------------------

$movementSpeed = 1; // m/s

function setSpeed(%speed)
{
	if(%speed)
	$movementSpeed = %speed;
}

function moveleft(%val)
{
	$mvLeftAction = %val;
}

function moveright(%val)
{
	$mvRightAction = %val;
}

function moveforward(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	$mvForwardAction = %val;
}

function movebackward(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	$mvBackwardAction = %val;
}

function moveup(%val)
{
	$mvUpAction = %val;
}

function movedown(%val)
{
	$mvDownAction = %val;
}

function turnLeft( %val )
{
   if ( $CameraStyle $= "Side")
      return;
      
	$mvDeviceIsKeyboardMouse = true;

	$mvYawLeftSpeed = %val ? $Pref::Input::KeyboardTurnSpeed : 0;
}

function turnRight( %val )
{
   if ( $CameraStyle $= "Side")
      return;
      
	$mvDeviceIsKeyboardMouse = true;

	$mvYawRightSpeed = %val ? $Pref::Input::KeyboardTurnSpeed : 0;
}

function panUp( %val )
{
	$mvPitchDownSpeed = %val ? $Pref::Input::KeyboardTurnSpeed : 0;
}

function panDown( %val )
{
	$mvPitchUpSpeed = %val ? $Pref::Input::KeyboardTurnSpeed : 0;
}

function getMouseAdjustAmount(%val)
{
	// based on a default camera fov of 90'
	return(%val * ($cameraFov / 90) * $pref::Input::MouseSensitivity);
}

function spewmovevars()
{
	echo("yaw:" SPC $mvYaw SPC "pitch:" SPC $mvPitch);
	$Debug::spewmovesched = schedule(500,0,spewmovevars);
}

function gamepadYaw(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	if( $pref::invertXCamera )
	%val *= -1.0;

	// if we get a non zero val, the user must have moved the stick, so switch
	// out of keyboard/mouse mode
	if (%val != 0.0)
	$mvDeviceIsKeyboardMouse = false;

	// stick events come in even when the user isn't using the gamepad, so make
	// sure we don't slam the move if we don't think the user is using the gamepad
	if (!$mvDeviceIsKeyboardMouse)
	{
		%scale = (ServerConnection.gameState $= "wait") ? -0.1 : 3.14;
		$mvYawRightSpeed = -(%scale * %val);
	}
}

function gamepadPitch(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	if( $pref::invertYCamera )
	%val *= -1.0;

	// if we get a non zero val, the user must have moved the stick, so switch
	// out of keyboard/mouse mode
	if (%val != 0.0)
	$mvDeviceIsKeyboardMouse = false;

	// stick events come in even when the user isn't using the gamepad, so make
	// sure we don't slam the move if we don't think the user is using the gamepad
	if (!$mvDeviceIsKeyboardMouse)
	{
		%scale = (ServerConnection.gameState $= "wait") ? -0.05 : 3.14;
		$mvPitchUpSpeed = %scale * %val;
	}
}

function mouseYaw(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	if( $pref::invertXCamera )
	%val *= -1.0;

	$mvDeviceIsKeyboardMouse = true;

	$mvYaw += getMouseAdjustAmount(%val);
}

function mousePitch(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	if( $pref::invertYCamera )
	%val *= -1.0;

	$mvDeviceIsKeyboardMouse = true;

	$mvPitch += getMouseAdjustAmount(%val);
}

function jumpOrStart(%val)
{
   if ($Game::NoJump)
   {
      commandToServer('DoOOBClick', %val);
   }
   if ($Game::NoJump)// && !$Game::OutOfBounds)
      return;
	if (%val)
	$mvTriggerCount2 = 1;
	else
	$mvTriggerCount2 = 0;
}

function jumpOrPowerup( %val )
{
	// LTrigger
	if( %val > 0.0 )
	$mvTriggerCount2++;
	else
	$mvTriggerCount0++;
}

function moveXAxisL(%val)
{
	if (%val != 0.0)
	$mvDeviceIsKeyboardMouse = false;

	$mvXAxis_L = %val;
}

function moveYAxisL(%val)
{
   if ( $CameraStyle $= "Side")
      return;
      
	if (%val != 0.0)
	$mvDeviceIsKeyboardMouse = false;

	$mvYAxis_L = %val;
}

function centercam(%val)
{
	$mvTriggerCount3++;
}

function cycleDebugPredTiles()
{
	$curTile++;

	if( $curTile > getNumPredTiles() )
	$curTile = 0;

	setDebugPredTile( $curTile );
}

//------------------------------------------------------------------------------
// Mouse Trigger
//------------------------------------------------------------------------------

function mouseFire(%val)
{
	if (%val)
	$mvTriggerCount0 = 1;
	else
	$mvTriggerCount0 = 0;
}

function altTrigger(%val)
{
	if (%val)
	$mvTriggerCount1 = 1;
	else
	$mvTriggerCount1 = 0;
}

function middleMouse(%val)
{
	if (%val)
	{
      if ($pref::Option::UseSecretSound)
	  {
		commandToServer('IAmAngry');
		newMessageHud.open("SAY");
		NMH_Type.setValue("[Aaaahhhhhh]");
		NMH_Type.Type();
		NMH_Type.Send();
      }
	}
}

function tauntHappy(%val)
{
	if (%val)
	{
      if ($pref::Option::UseSecretSound)
	  {
		commandToServer('IAmHappy');
		newMessageHud.open("SAY");
		NMH_Type.setValue("[Woohoo]");
		NMH_Type.Type();
		NMH_Type.Send();
      }
	}
}

function taunt2(%val)
{
	if (%val)
	{
      if ($pref::Option::UseSecretSound)
	  {
		commandToServer('Taunt2');
		newMessageHud.open("SAY");
		NMH_Type.setValue("[Y U NO Gimme Gem!?]");
		NMH_Type.Type();
		NMH_Type.Send();
      }
	}
}

function taunt3(%val)
{
	if (%val)
	{
      if ($pref::Option::UseSecretSound)
	  {
		commandToServer('Taunt3');
		newMessageHud.open("SAY");
		NMH_Type.setValue("[They See Me Rolling]");
		NMH_Type.Type();
		NMH_Type.Send();
      }
	}
}

function musicPlayNext(%val)
{
	if (%val)
	{
      DjAnson.playNext();
	}
}

//------------------------------------------------------------------------------
// Camera & View functions
//------------------------------------------------------------------------------

function toggleFreeLook( %val )
{
	if ( %val )
	$mvFreeLook = true;
	else
	$mvFreeLook = false;
}

function toggleFirstPerson(%val)
{
	if (%val)
	{
		$firstPerson = !$firstPerson;
	}
}

function toggleCamera(%val)
{
	if ($testcheats){
		if (%val)
		commandToServer('ToggleCamera');
	}
}

function toggleVersionDisplay(%val)
{
	if (%val)
	$showBuildNumberDisplay = !$showBuildNumberDisplay;
}

//------------------------------------------------------------------------------
// Demo recording functions
//------------------------------------------------------------------------------

function startRecordingDemo( %val )
{
	//    if ( %val )
	//       beginDemoRecord();
	error( "** This function has temporarily been disabled! **" );
}

function stopRecordingDemo( %val )
{
	//    if ( %val )
	//       stopRecord();
	error( "** This function has temporarily been disabled! **" );
}

//------------------------------------------------------------------------------
// Helper Functions
//------------------------------------------------------------------------------

function dropCameraAtPlayer(%val)
{
	if (%val && $testcheats)
	   commandToServer('dropCameraAtPlayer');
}

function dropPlayerAtCamera(%val)
{
	if (%val && $testcheats)
	   commandToServer('DropPlayerAtCamera');
}

function bringUpOptions(%val)
{
	if(%val)
	Canvas.pushDialog(OptionsDlg);
}

//------------------------------------------------------------------------------
// Chatses
//------------------------------------------------------------------------------

function GlobalChat(%val)
{
	if(%val && $Pref::Chat::LineTime > 0)
	newMessageHud.open("SAY");
}
function TeamChat(%val)
{
	if(%val && $Pref::Chat::LineTime > 0)
	newMessageHud.open("TEAM");
}

//------------------------------------------------------------------------------
// Dubuging Functions
//------------------------------------------------------------------------------

$MFDebugRenderMode = 0;
function cycleDebugRenderMode(%val)
{
   if (!$testcheats)
      return;
	if (!%val)
	return;

	if (getBuildString() $= "Debug")
	{
		if($MFDebugRenderMode == 0)
		{
			// Outline mode, including fonts so no stats
			$MFDebugRenderMode = 1;
			GLEnableOutline(true);
		}
		else if ($MFDebugRenderMode == 1)
		{
			// Interior debug mode
			$MFDebugRenderMode = 2;
			GLEnableOutline(false);
			setInteriorRenderMode(7);
			showInterior();
		}
		else if ($MFDebugRenderMode == 2)
		{
			// Back to normal
			$MFDebugRenderMode = 0;
			setInteriorRenderMode(0);
			GLEnableOutline(false);
			show();
		}
	}
	else
	{
		echo("Debug render modes only available when running a Debug build.");
	}
}

function pauseOrEscape()
{
	if (Canvas.getContent() == EditorGui.getId())
	{
		Editor.close("PlayGui");
		Canvas.setContent(RootGui);
		RootGui.setContent(PlayGui);
	} else 
	if (PlayGui.isAwake())
	{
		if (!GamePauseGui.isAwake())
		   pauseToggle(0);
		// otherwise wait for them to make the selection...
	}
	else
	{
		if (! isUsingBrowserUi())
		{
			//escapeFromGame(false,true);
			RootGui.contentGui.onB();
		}
		else
		{
			%mode = getSpHostClientToken();
			$naturalEnd = true;
			ggcSendRpc("game." @ %mode @ ".end");
		}
	}
}

//------------------------------------------------------------------------------
// Bind input to commands
//------------------------------------------------------------------------------

// Global Action Map:

// keyboard
//if (! isShippingBuild())
GlobalActionMap.bindCmd(keyboard, "alt enter", "", "Canvas.toggleFullscreen();");//"fixFullscreen();");

/*function toggleBind()
{
   if ($Client::MoveMapBind)
   {
      moveMap.unbind();
      $Client::MoveMapBind = false;
   } else {
      moveMap.bind();
      $Client::MoveMapBind = true;
   }
}

GlobalActionMap.bindCmd(keyboard, "alt b", "", "toggleBind();");*/

//if ($testCheats)
//{
//
	GlobalActionMap.bind(keyboard, "F9", cycleDebugRenderMode);
//	GlobalActionMap.bindCmd(keyboard, "ctrl 1", "", "setVideoMode(640,480,32,0);");
//	GlobalActionMap.bindCmd(keyboard, "ctrl 2", "", "setVideoMode(1280,720,32,0);");
//	GlobalActionMap.bindCmd(keyboard, "ctrl 3", "", "setVideoMode(1024,768,32,0);");
//	GlobalActionMap.bindCmd(keyboard, "ctrl 4", "", "setVideoMode(800,600,32,0);");
//	GlobalActionMap.bindCmd(keyboard, "ctrl t", "", "reloadMaterials();");
	moveMap.bind(keyboard, "F8", dropCameraAtPlayer);
	moveMap.bind(keyboard, "F7", dropPlayerAtCamera);
//}

GlobalActionMap.bindCmd(keyboard, "escape", "", "pauseOrEscape();");
//GlobalActionMap.bindCmd(keyboard, "tilde", toggleConsole);

moveMap.bind( keyboard, left, turnLeft );
moveMap.bind( keyboard, right, turnRight );
moveMap.bind( keyboard, a, moveleft );
moveMap.bind( keyboard, d, moveright );
moveMap.bind( keyboard, w, moveforward );
moveMap.bind( keyboard, s, movebackward );
moveMap.bind( keyboard, q, mouseFire );
moveMap.bind( keyboard, e, altTrigger );
moveMap.bind( keyboard, space, jumpOrStart );

function quickRespawn(%val)
{
	if (%val)
	{
		commandToServer('quickRespawn');
	}
}

moveMap.bind( keyboard, r, quickRespawn );
moveMap.bind( keyboard, backspace, quickRespawn );
//if (!isShippingBuild())
//{


	// don't ask me what the n has to do with cameras
//}
moveMap.bind(keyboard, "ctrl f", toggleVersionDisplay);
//moveMap.bind( keyboard, F3, startRecordingDemo );
//moveMap.bind( keyboard, F4, stopRecordingDemo );
moveMap.bindCmd( keyboard, o, "", "pauseToggle(0);" );
moveMap.bindCmd( keyboard, p, "", "pauseToggle(1);" );

// GlobalActionMap for lobby??
ChatMap.bind(keyboard, "t", GlobalChat);
//moveMap.bind(keyboard, "ENTER", GlobalChat);
//moveMap.bind(keyboard, "y", TeamChat);

// mouse
moveMap.bind( mouse, button0, mouseFire );
moveMap.bind( mouse, button1, altTrigger );
moveMap.bind( mouse, button2, middleMouse );
moveMap.bind( mouse, xaxis, mouseYaw );
moveMap.bind( mouse, yaxis, mousePitch );

// gamepad
//if (isPCBuild())
//{
// set up some defaults
moveMap.bind( xinput, btn_x, altTrigger );
moveMap.bind( xinput, btn_a, jumpOrStart );
moveMap.bind( xinput, btn_b, mouseFire );
moveMap.bind( xinput, trigger_r, mouseFire );
moveMap.bind( xinput, trigger_l, jumpOrStart );
moveMap.bind( xinput, zaxis, jumpOrPowerup );
moveMap.bind( xinput, btn_rshoulder, altTrigger );
moveMap.bind( xinput, btn_lshoulder, altTrigger );

moveMap.bindCmd( xinput, btn_back, "pauseToggle(1);", "" );
moveMap.bindCmd( xinput, btn_start, "pauseToggle(0);", "" );
moveMap.bindCmd( xinput, btn_y, "togglePlayerListLength();", "" );

moveMap.bind( xinput, thumb_lx, "DN", "-0.23 0.23", moveXAxisL );
moveMap.bind( xinput, thumb_ly, "DN", "-0.23 0.23", moveYAxisL );
moveMap.bind( xinput, thumb_rx, "D", "-0.23 0.23", gamepadYaw );
moveMap.bind( xinput, thumb_ry, "D", "-0.23 0.23", gamepadPitch );

moveMap.bind( keyboard, "1", tauntHappy );
moveMap.bind( keyboard, "2", taunt2 );
moveMap.bind( keyboard, "3", taunt3 );

//if ( isObject( musicMap ) )
//musicMap.delete();
//new ActionMap(musicMap);
moveMap.bind( keyboard, "tab", musicPlayNext );

//if ($testCheats)
//{
moveMap.bind(keyboard, "alt c", toggleCamera);
moveMap.bind(keyboard, "tilde", toggleConsole);
//GlobalActionMap.bindCmd(keyboard, "tilde", toggleConsole);
//GlobalActionMap.bindCmd(keyboard, "F11", toggleEditor);

//moveMap.bind(keyboard, m, spawnStupidMarble);
//}
//moveMap.bind( gamepad, btn_y, spawnStupidMarble );

//if (strstr(getGamepadName(), "Logitech") != -1)
//{
//   error("Binding for" SPC getGamepadName() SPC "controller");
//   moveMap.bind( gamepad, zaxis, "D", "-0.23 0.23", gamepadYaw );
//   moveMap.bind( gamepad, rzaxis,  "D", "-0.23 0.23", gamepadPitch );
//}
//}
//if (!isPCBuild())
//else // xbox
//{
// gamepad
//GlobalActionMap.bindCmd( gamepad, lshoulder, "cycleDebugPredTiles();", "" );

//moveMap.bind( gamepad, btn_x, altTrigger );
//moveMap.bind( gamepad, btn_a, jumpOrStart );
//moveMap.bind( gamepad, btn_b, mouseFire );
//moveMap.bind( gamepad, rtrigger, mouseFire );
//moveMap.bind( gamepad, ltrigger, jumpOrStart );
//moveMap.bind( gamepad, rshoulder, altTrigger );
//moveMap.bind( gamepad, lshoulder, altTrigger );

//moveMap.bindCmd( gamepad, back, "pauseToggle(1);", "" );
//moveMap.bindCmd( gamepad, start, "pauseToggle(0);", "" );
// moveMap.bindCmd( gamepad, btn_y, "togglePlayerListLength();", "" );

//  moveMap.bind( gamepad, xaxis, "DN", "-0.23 0.23", moveXAxisL );
//  moveMap.bind( gamepad, yaxis, "DN", "-0.23 0.23", moveYAxisL );
// moveMap.bind( gamepad, rxaxis, "D", "-0.23 0.23", gamepadYaw );
//moveMap.bind( gamepad, ryaxis, "D", "-0.23 0.23", gamepadPitch );
//moveMap.bind( gamepad, zaxis, "D", "-0.23 0.23", gamepadYaw );
//moveMap.bind( gamepad, rzaxis,  "D", "-0.23 0.23", gamepadPitch );

//moveMap.bind( gamepad, btn_y, spawnStupidMarble );

//}

function gamepadLSX(%val)
{
// Xbox 360 Controller in MultiPlayer is broken HERE
	if (($Game::Running || $Client::connectedMultiplayer) && !LobbyGui.isAwake() && !$gamePaused)
	{
		moveXAxisL(%val);
	}else {

		if (%val < -0.23 && ($Sim::Time - $delayx1) > 0.200)
		{
			sfxPlay(AudioButtonOver);
			RootGui.contentGui.onLeft();
			$delayx1 = $Sim::Time;
			//schedule(200, 0, resetDelayx1);
		}
		if (%val > 0.23 && ($Sim::Time - $delayx2) > 0.200)
		{
			sfxPlay(AudioButtonOver);
			RootGui.contentGui.onRight();
			$delayx2 = $Sim::Time;
			//schedule(200, 0, resetDelayx2);
		}
	}
}

function gamepadLSY(%val)
{
	if (($Game::Running || $Client::connectedMultiplayer) && !LobbyGui.isAwake() && !$gamePaused)
	{
		moveYAxisL(%val);
	}else {
		if (%val < -0.23 && ($Sim::Time - $delayy1) > 0.200)
		{
			sfxPlay(AudioButtonOver);
			RootGui.contentGui.onUp();
			GamePauseGui.onUp();
			// use Sim Time because schedule doesn't work with the timescale very low and pausing the game causes a low timescale
			$delayy1 = $Sim::Time;
			//schedule(200, 0, resetDelayy1);
		}
		if (%val > 0.23 && ($Sim::Time - $delayy2) > 0.200)
		{
			sfxPlay(AudioButtonOver);
			RootGui.contentGui.onDown();
			GamePauseGui.onDown();
			$delayy2 = $Sim::Time;
			//schedule(200, 0, resetDelayy2);
		}
	}

}
/*
function resetDelayx1()
{
	$delayx1 = 0;
}
function resetDelayx2()
{
	$delayx2 = 0;
}

function resetDelayy1()
{
	$delayy1 = 0;
}
function resetDelayy2()
{
	$delayy2 = 0;
}
*/
GlobalActionMap.bind( xinput, thumb_lx, "DN", "-0.23 0.23", gamepadLSX );
GlobalActionMap.bind( xinput, thumb_ly, "DN", "-0.23 0.23", gamepadLSY );

//-------------------------------------------------
// Script code for doing profiling while holding
// down ctrl-F3 (bind only in pc build).
//-------------------------------------------------

// Enabling Editor Test controls

GlobalActionMap.bind(keyboard, "tilde", toggleConsole);
//
//
// That's all so far. Placeholder above


function doProfile(%val)
{
	if ($testcheats){
		if (%val || isShippingBuild())
		{
			// key down -- start profile
			echo("Starting profile session...");
			profilerDump();
			profilerEnable(true);
		}
		else
		{
			// key up -- finish off profile
			echo("Ending profile session...");
			profilerDump();
			profilerEnable(false);
		}
	}
}

if (isPCBuild() && !isShippingBuild())
{
	GlobalActionMap.bind(keyboard, "ctrl F3", doProfile);
}

function enableGamepad()
{
	schedule($xInputStartDelay, 0, xbxcontroller);
}

function xbxcontroller()
{
	enableXInput();
	//enableJoystick();
}
