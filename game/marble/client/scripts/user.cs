if (!isObject(UserDownloader))
   new TCPObject(UserDownloader);
   
$MBUserServerPHP = "/connection/login.php";

function loadUserName()
{
   UserDownloader.retrieve();
}

function getUserName()
{
   if ($Net::Username $= "")
   {
      if ($pref::GameKey !$= "" && $pref::Cache::Key $= $pref::GameKey && $pref::Cache::Username !$= "")
      {
         $Net::Username = $pref::Cache::Username;
         return $Net::Username;
      } else {
         return "Guest";
      }
   }
   return $Net::Username;
}

function UserDownloader::onLine(%this, %line)
{
   //echo("UserDownloader::onLine: Got Message " @ %line);
   if (getWord(%line, 0) !$= "User:")
      return; 
      
   $Net::Username = getWord(%line, 1);
   $pref::Cache::Username = $Net::Username;
   $pref::Cache::Key = $pref::GameKey;
   savePCUserProfile();
}

function UserDownloader::retrieve(%this)
{
   echo("Retrieving Username");
   %this.get($MBServerURL1, $MBUserServerPHP, "cmd=getName===" @ $pref::GameKey);
}