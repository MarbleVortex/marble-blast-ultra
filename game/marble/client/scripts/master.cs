if (!isObject(MasterConnection))
   new TCPObject(MasterConnection);
// this is in a seperate variable to handle failure
if (!isObject(PortcheckConnection))
   new TCPObject(PortcheckConnection);

$MBGameServerPHP = "/connection/master.php";
$MBPortServerPHP = "/connection/portcheck.php";

$useMaster = !$devMode;

//$PortForwarded = true;

function MasterConnection::startServer(%this)
{
   if (!$checkedPorts && !$Server::Dedicated)
   {
      PortcheckConnection.checkPorts();
      return;
   }
	if ($useMaster && ($PortForwarded || $Server::Dedicated) && !$hiddenServer)
	{
		echo("Registering Server on Master");
		if($Server::Dedicated) {
		%get = "cmd=register===" @ strreplace($NameDedicated, " ", "_") @ "===" @ strreplace($MaxPlayersDedicated, " ", "_") @ "===" @ strreplace($Mission::Mode, " ", "_") @ "===" @ strreplace($Mission::Name, " ", "_") @ "===" @ strreplace($currentVersion, " ", "_");	
		} else {
		%get = "cmd=register===" @ strreplace($pref::Server::Name, " ", "_") @ "===" @ strreplace($pref::Server::MaxPlayers, " ", "_") @ "===" @ strreplace($Mission::Mode, " ", "_") @ "===" @ strreplace($Mission::Name, " ", "_") @ "===" @ strreplace($currentVersion, " ", "_");	
		}
      // comment THIS line if the MultiPlayer is crashing so much... but you won't show up on the server list!
      // EDIT: thanks to TCPObject, this doesn't crash anymore and commenting it makes little to no difference.
		%this.get($MBServerURL1, $MBGameServerPHP, %get);
		
		//$checkedPorts = false;
		startHB();
	}
}

function MasterConnection::stopServer(%this)
{
	if ($useMaster && $PortForwarded && !$hiddenServer)
	{
		echo("Removing Server from Master");
		%get = "cmd=unregister";
		%this.get($MBServerURL1, $MBGameServerPHP, %get);
	}
}

function startHB()
{
	if ($useMaster && $PortForwarded && !$hiddenServer)
	{
		echo("Starting Master Heartbeat");
		cancel($picklebeater);
		$picklebeater = schedule(10000, 0, "heartbeat");
	}
}

function heartbeat()
{
	if ($useMaster && $PortForwarded && !$hiddenServer)
	{
		echo("Sending Heartbeat to Master");
		cancel($picklebeater);
		MasterConnection.get($MBServerURL1, $MBGameServerPHP, "cmd=heartbeat===" @ ClientGroup.getCount());
		$picklebeater = schedule(10000, 0, "heartbeat");
	}
}

function MasterConnection::stopHeartbeat(%this)
{
	if ($useMaster && !$hiddenServer)
	{
		echo("Stopping Master Heartbeat");
		cancel(%this.beater);
	}
}

function MasterConnection::listServers(%this)
{
	if ($useMaster)
	{
		$serverCount = 0;
		%this.get($MBServerURL1, $MBGameServerPHP, "cmd=connect");
	} else {
		findGameGui.onSearchComplete("pc");
	}
}

/*function MasterConnectionChecker::onConnectionRequest(%this, %address, %id)
{
   echo("Received Connection Request");
   new TCPObject( TCPClient, %id);
}

function MasterConnectionChecker::onLine(%this, %line)
{
   echo ("Got line!");
   if (%line $= "info:check")
	{
	   echo("Received Master Server Check Request");
	} else {
	   echo("Received Unknown Request from Master Server");
	}
}*/

// If we get a PORT FAILURE message, set PortForwarded to false and disallow the player.
// If we get back any other message, allow the player.
// If we fail to connect altogether, assume the server is down and whitelist the player.
// If nothing happened for ten secs, the code has failed so whitelist the player.

function PortcheckConnection::checkPorts(%this)
{
   if (isShippingBuild())
	{
	   //if (isObject(PortChecker))
	   //   PortChecker.delete();
	   //new TCPObject(PortChecker);
	   //PortChecker.listen($Pref::Server::Port);
	   
	   // this will fail if there is more than one get request to it at once, so checkPorts should only be called ONCE!
	   %this.get($MBServerURL0, $MBPortServerPHP, "port=" @ $Pref::Server::Port);
	   schedule(10000, 0, "assumePortsAreOpen");
	}
}

// this can't be part of PortcheckConnection because schedules are iffy with it.
function assumePortsAreOpen() {
   if (!$checkedPorts)
   {
      $PortForwarded = true;
      $checkedPorts = true;
      //if (isObject(PortChecker))
      //   PortChecker.delete();
      MasterConnection.startServer();
   }
}

function assumePortsAreClosed() {
   $PortForwarded = false;
   $checkedPorts = true;
      //if (isObject(PortChecker))
         //PortChecker.delete();
   multiPlayerFrame.setBitmap("marble/client/ui/multiplayer/1", true);
	Canvas.pushDialog(multiPlayerDlg);
	$closeMultiPlayer = Canvas.schedule(10000, "popDialog", multiPlayerDlg);
   //XMessagePopupDlg.show(0, "Port 28000 is blocked! It needs to be opened to allow people to join your multiplayer games. Would you like a tutorial?", $Text::OK, "gotoWebPage(\"http://" @ $MBServerURL1 @ "/game/infotutorial/multiplayersetuphowto.php\");", $Text::Cancel, "");
   // if we wanted to be extra careful we could do this, but let's not make more requests than nessecary.
   // most likely, no server will even be running at this point, since we havn't even proven its success.
   //destroyGame();
   //stopMultiplayerServer();
}

function PortcheckConnection::onDNSFailed(%this) {
   assumePortsAreOpen();
}

function PortcheckConnection::onConnectFailed(%this) {
   assumePortsAreOpen();
}

function PortcheckConnection::onLine(%this, %line)
{
   if (%line $= "PORT FAILURE")
   {
      echo("Port Mapping Failure");
      assumePortsAreClosed();
   } else
   {
      // if we never eventually get either message,
      // we'll let the schedule we set in checkPorts deal with it. I know, sounds stupid, but what else can we do?
      // Andrea said she found something important in Bosh's tunnels.
      if (%line $= "PORT SUCCESS")
      {
         echo("Port Mapping Success");
         //XMessagePopupDlg.show(0, "Port Mapping Successful, you can now host multiplayer games!", "OK", "", "", "");
         assumePortsAreOpen();
      }
   }
}
	
function MasterConnection::onLine(%this, %line)
{
   //echo("MasterConnection::onLine: Got Message " @ %line);
	if ($useMaster)
	{
		//echo("Received Line from Master: " @ %line);
		//if (%line $= "master:success")
		//{
		//   %this.startHeartbeat();
		//   return;
		//} else 
		if (%line $= "error:noservers")
		{
			//XMessagePopupDlg.show(0, "No Servers Found", "OK", "", "", "");
			findGameGui.onSearchComplete("pc");
			return;
		}
      if (strlen(%line) > 8)
      {
         if (strcmp(getWord(%line, 0), "SERVER:"))
            return;
      }
		
		//if (!startswith(%line, "SERVER: "))
		//   return;
      //%line = strstr(%line, "SERVER: ");
		%count = getWordCount(%line);
		
		$serverCount = 0;
		
		for (%i = 1; %i < %count; %i++)
		{
			
			%name = getWord(%line, %i);
			echo("Name: " @ %name);
			%i++;
			%ip = getWord(%line, %i);
			echo("IP: " @ %ip);
			%i++;
			%min = getWord(%line, %i);
			echo("Min: " @ %min);
			%i++;
			%max = getWord(%line, %i);
			echo("Max: " @ %max);
			
			echo ("Server Found: " @ %name @ ": " @ %ip);
			//%server[%i] = %ip;
			//connectToServer(%ip);
			$serverList[$serverCount] = %ip;
			$serverListName[$serverCount] = %name;
			$serverListMin[$serverCount] = %min;
			$serverListMax[$serverCount] = %max;
			
			//XMessagePopupDlg.show(0, "Server Found: " @ %ip, "OK", "", "", "");
			//break;
			$serverCount++;
		}
		
		findGameGui.onSearchComplete("pc");
	}
}

$allowConnect = false;

function allowConnect(%bool)
{
	$allowConnect = %bool;
	/*if (isObject(MasterConnectionChecker))
		MasterConnectionChecker.delete();
   new TCPObject(MasterConnectionChecker);
	if ($allowConnect)
	{
		MasterConnectionChecker.listen(28001);
	}*/
	
	if ($allowConnect && ($PortForwarded || $Server::Dedicated) && !$hiddenServer)
	{
	   MasterConnection.startServer();
	}
	
	allowConnections(%bool);
}

function doesAllowConnect()
{
	return $allowConnect && ($PortForwarded || $Server::Dedicated) && doesAllowConnections();
}
