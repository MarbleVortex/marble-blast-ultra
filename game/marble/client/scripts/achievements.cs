exec("marble/client/ui/AchievementDlg.gui");
// Handy function
function BIT(%place)
{
   return 1 << %place;
}

$totalAchievements = 14;

function setupAchievementMasks()
{
   echo("Setting up achievement masks");
   
   $pointThreshold11 = 75;
   $pointThreshold12 = 2000;

   $allBeginnerMask = 0;
   $allIntermediateMask = 0;
   $allAdvancedMask = 0;
   $allBonusMask = 0;
   
   $numLevelsBeginner = 0;
   $numLevelsIntermediate = 0;
   $numLevelsAdvanced   = 0;
   $numLevelsBonus   = 0;

   $numEasterEggs = 20;
   $allEggsMask = 0;
   for (%i = 1; %i <= $numEasterEggs; %i++)
   {
      $allEggsMask |= BIT(%i);
   }
   
   for (%i = 0; %i < SinglePlayMissionGroup.getCount(); %i++)
   {
      %mis = SinglePlayMissionGroup.getObject(%i);
      
      if (%mis.type $= "")
      {
         error("   Mission does not have a difficulty set!" SPC %mis.file);
         continue;
      }
      
      %beginnerStart = 1;
      %beginnerEnd = 20;
      
      %intermediateStart = 21;
      %intermediateEnd = 40;
      
      %advancedStart = 41;
      %advancedEnd   = 60;
	  
	  %bonusStart = 99961;
      %bonusEnd   = 99980;
      
      switch$ (%mis.type)
      {
         case "beginner":
            // make sure level id is in range
            if (%mis.level < %beginnerStart || %mis.level > %beginnerEnd)
            {
               error("   Mission level is out of range for difficulty:" SPC %mis.file);
               continue;
            }
            $allBeginnerMask |= BIT(%mis.level - %beginnerStart + 1);
            $numLevelsBeginner++;
         case "intermediate":
            // make sure level id is in range
            if (%mis.level < %intermediateStart || %mis.level > %intermediateEnd)
            {
               error("   Mission level is out of range for difficulty:" SPC %mis.file);
               continue;
            }
            $allIntermediateMask |= BIT(%mis.level - %intermediateStart + 1);
            $numLevelsIntermediate++;
         case "advanced":
            // make sure level id is in range
            if (%mis.level < %advancedStart || %mis.level > %advancedEnd)
            {
               error("   Mission level is out of range for difficulty:" SPC %mis.file);
               continue;
            }
            $allAdvancedMask |= BIT(%mis.level - %advancedStart + 1);
            $numLevelsAdvanced++;
		case "bonus":
            // make sure level id is in range
            if (%mis.level < %bonusStart || %mis.level > %bonusEnd)
            {
               error("   Mission level is out of range for difficulty:" SPC %mis.file);
               continue;
            }
            $allBonusMask |= BIT(%mis.level - %bonusStart + 1);
            $numLevelsBonus++;
      }
   }
   echo("   Found" SPC $numLevelsBeginner SPC "beginner levels");
   echo("   Found" SPC $numLevelsIntermediate SPC "intermediate levels");
   echo("   Found" SPC $numLevelsAdvanced SPC "advanced levels");
   echo("   Found" SPC $numLevelsBonus SPC "bonus levels");
   
   if ($Test::CheatsEnabled !$= "" && $Test::EasyAchievements)
   {
      error("   ACHIEVEMENT CHEATS ENABLED!");
      
      $allBeginnerMask = 3;
      $allIntermediateMask = 3;
      $allAdvancedMask = 3;
	  $allBonusMask = 3;
   
      $numLevelsBeginner = 2;
      $numLevelsIntermediate = 2;
      $numLevelsAdvanced   = 2;
	  $numLevelsBonus   = 2;
      
      $numEasterEggs = 2;
      $allEggsMask = 3;

      $pointThreshold11 = 10;
      $pointThreshold12 = 20;
   }
}

setupAchievementMasks();

$loadLoops = 0; // Just to make sure we don't end up in an infinite loop somehow

// Check to see if the player has won another achievement
function checkForAchievements()
{
   // Get the Live/controller port
   //%port = XBLiveGetSigninPort();

   // Make sure the player's achievements are loaded
  // if (XBLiveAreAchievementsLoaded(%port))
   //   $loadLoops = 0;
   //else
  // {
  //    echo("checkForAchievements: I am loading achievements, not good");
   //   if ($loadLoops < 3)
  //    {
  //       XBLiveLoadAchievements(%port, "checkForAchievements();");
  //       $loadLoops++;
  //       return;
  //    }
  //    else
   //      errorf("@@@@@ checkForAchievements(): unable to load achievements after 3 attempts");
   //}

   // complete a level under par time
   if (!XBLiveHasAchievement(1))
   {
      //%pars = getBeginnerPars() | getIntermediatePars() | getAdvancedPars();
      if ($partime){//%pars){
	     $partime = false;
         WriteAchievement(1);
		 notifyAwardOfAchievement(1);
	  }
   }

   // Check to see if we have finished all Beginner levels
   if (!XBLiveHasAchievement(2))
   {
      %fblvs = AllBeginnerLevels();
      // Check to see if all of the levels have been completed
      if (%fblvs)
         {
		 WriteAchievement(2);
		 notifyAwardOfAchievement(2);
		 }
   }

   // Check to see if we have finished all Intermediate levels
   if (!XBLiveHasAchievement(3))
   {
      %falvs = AllIntermediateLevels();
      // Check to see if all of the levels have been completed
      if (%falvs){
         WriteAchievement(3);
		 notifyAwardOfAchievement(3);
		 }
   }

   // Check to see if we have finished all Advanced levels
   if (!XBLiveHasAchievement(4))
   {
      %felvs = AllAdvancedLevels();
      // Check to see if all of the levels have been completed
      if (%felvs)
         
		 {
			WriteAchievement(4);
		 notifyAwardOfAchievement(4);
		 }
   }
   
   // Check to see if we have gotten all Beginner Par Times
   if (!XBLiveHasAchievement(5))
   {
      %bPar = AllBeginnerPars();
      // Check to see if all of the levels have been completed with Par times
      if (%bPar)
         {
			WriteAchievement(5);
		 notifyAwardOfAchievement(5);
		 }
   }

   // Check to see if we have gotten all Intermediate Par Times
   if (!XBLiveHasAchievement(6))
   {
      %aPar = AllIntermediatePars();
      // Check to see if all of the levels have been completed with Par times
      if (%aPar)
         {
			WriteAchievement(6);
		 notifyAwardOfAchievement(6);
		 }
   }

   // Check to see if we have gotten all Advanced Par Times
   if (!XBLiveHasAchievement(7))
   {
      %ePar = AllAdvancedPars();
      // Check to see if all of the levels have been completed with Par times
      if (%ePar)
         {
			WriteAchievement(7);
		 notifyAwardOfAchievement(7);
		 }
   }

   // Check to see if we found our first Easter Egg
   if (!XBLiveHasAchievement(8))
   {
      %eggs = getEasterEggs();
      // Check to see if have any easter eggs
      if (%eggs)
         {
			WriteAchievement(8);
		 notifyAwardOfAchievement(8);
		 }
   }

   // Check to see if we found all Easter Eggs
   if (!XBLiveHasAchievement(9))
   {
      %aeggs = 0;
	  for (%i = 0; %i < 21; %i++){
	  
	     if ($UserAchievements::hasEgg[%i])
		 {
		    %aeggs++;
		 }
	  
	  }
      // Check to see if all of the easter eggs have been found
      if (%aeggs == $numEasterEggs)
         {
			WriteAchievement(9);
		 notifyAwardOfAchievement(9);
		 }
   }
   
   // Win first place in multiplayer
   if (!XBLiveHasAchievement(10))
   {
      %gotIt = getFirstPlace();
      if (%gotIt)
         {
			WriteAchievement(10);
		 notifyAwardOfAchievement(10);
		 }
   }

   // Get 75 points in multiplayer
   if (!XBLiveHasAchievement(11))
   {
      if ($UserAchievements::MPHighScore >= $pointThreshold11)
         {
			WriteAchievement(11);
		 notifyAwardOfAchievement(11);
		 }
   }

   // Get 2000 points in multiplayer
   if (!XBLiveHasAchievement(12))
   {
      %mpOverallpoints = getMPOverallPoints();
      if (%mpOverallpoints >= $pointThreshold12)
         {
			WriteAchievement(12);
		 notifyAwardOfAchievement(12);
		 }      
   }
   
   // Check to see if we have finished all Bonus levels
   if (!XBLiveHasAchievement(13))
   {
      %felvs = AllBonusLevels();
      // Check to see if all of the levels have been completed
      if (%felvs)
         
		 {
			WriteAchievement(13);
		 notifyAwardOfAchievement(13);
		 }
   }
   
   // Check to see if we have gotten all Bonus Par Times
   if (!XBLiveHasAchievement(14))
   {
      %bPar = AllBonusPars();
      // Check to see if all of the levels have been completed with Par times
      if (%bPar)
         {
			WriteAchievement(14);
		 notifyAwardOfAchievement(14);
		 }
   }
}

function WriteAchievement(%id)
{
	$UserAchievementsGot::achieved[%id] = 1;
	%inc = GetGamerScoreForAchievement(%id);
	$UserAchievements::GamerScore = $UserAchievements::GamerScore + %inc;
	
	submitPoints(%inc);
}

function GetGamerScoreForAchievement(%id)
{
	switch$ (%id)
	{
		case 1: return 5;
		case 2: return 10;
		case 3: return 15;
		case 4: return 25;
		case 5: return 15;
		case 6: return 25;
		case 7: return 35;
		case 8: return 5;
		case 9: return 25;
		case 10: return 10;
		case 11: return 10;
		case 12: return 20;
		case 13: return 15;
		case 14: return 25;
	}
}

function XBLiveHasAchievement(%id)
{
   return $UserAchievementsGot::achieved[%id] $= 1;
}

function getNumAwardedAchievements()
{
   //if (XBLiveAreAchievementsLoaded(XBLiveGetSigninPort()))
   //   return countbits(XBLiveGetAchievementMask(XBLiveGetSigninPort()));
   //return 0;
   
   %num = 0;
   
   for (%i = 1; %i <= $totalAchievements; %i++)
   {
      if(XBLiveHasAchievement(%i))
      {
         %num++;
      }
   }
   
   return %num;
}

function dumpAchievements()
{
   echo("Achievement data");
   
   for (%i = 1; %i <= $totalAchievements; %i++)
   {
      echo("   " @ %i SPC XBLiveHasAchievement(%i));
   }
}
$tasks = 0;
$ctask = 0;
function addTask(%task)
{
   $taskArray[$tasks] = %task;
   $tasks++;
   if ($ctask == 0)
   {
      nextTask();
   }
   
}

function nextTask()
{
   //if (ctask == tasks)
   //{
   if ($ctask >= $tasks)
   {
		$tasks = 0;
		$ctask = 0;
		return;
   }
   eval($taskArray[$ctask]);
   $ctask++;
   
   //} else {
   //   eval($taskArray[$ctask]);
   //   $ctask++;
   //}
   
}

function notifyAwardOfAchievement(%achievementId)
{
    echo("got achievement" SPC %achievementId);
	//$testTaskMgr.setDefaultTaskDelay(2000);
	addTask( "notifyAchievement(" @ %achievementId @ ");"); 
	
}

function notifyAchievement(%achievementId)
{
   
   /*if (isEventPending($closeAchievement))
   {
      if (isEventPending($newAchievement))
	  {
         %duration = getTimeSinceStart($newAchievement);
         $newAchievementTwo = schedule(3500 - %duration, 0, notifyAwardOfAchievement(%achievementId));
	  } else {
         %duration = getTimeSinceStart($closeAchievement);
         $newAchievement = schedule(3500 - %duration, 0, notifyAwardOfAchievement(%achievementId));
	  }
   }*/
   sfxPlay(AchievementSfx);
   //addChatLine("You got the " @ %achievementId @ " achievement!");
   $AchievementId = %achievementId;
   Canvas.pushDialog(AchievementDlg);
   $closeAchievement = schedule(3000, 0, CloseAchievementDlg);
   if (getNumAwardedAchievements() == $totalAchievements)
      $allAchievement = schedule(3500, 0, AllAchievement);
}

function AllAchievement()
{
   sfxPlay(AllAchievementSfx);
   $AchievementId = "all";
   Canvas.pushDialog(AchievementDlg);
   $closeAchievement = schedule(3000, 0, CloseAchievementDlg);
}

function CloseAchievementDlg()
{
	Canvas.popDialog(AchievementDlg);
	$newAchievement = schedule(500, 0, nextTask);
	
}

// Functions for accessing the various leaderboards to get point/finish values
// Set the id for the Completed Leaderboard here
$completedLB = 95;
// Set the Scrum Aggregate Leaderboard id here
$scrumABoard = 98;

function getBeginnerLevels()
{
   return $UserAchievements::beginnerLevels;
   //return XBLiveGetStatValue($completedLB, "beginnerLevels");
}

function getIntermediateLevels()
{
   return $UserAchievements::IntermediateLevels;
   //return XBLiveGetStatValue($completedLB, "IntermediateLevels");
}

function getAdvancedLevels()
{
   return $UserAchievements::AdvancedLevels;
   //return XBLiveGetStatValue($completedLB, "AdvancedLevels");
}

function getAdvancedLevels()
{
   return $UserAchievements::BonusLevels;
   //return XBLiveGetStatValue($completedLB, "AdvancedLevels");
}

function getEasterEggs()
{
   return $UserAchievements::easterEggs;
   //return XBLiveGetStatValue($completedLB, "easterEggs");
}

function getBeginnerPars()
{
   return $UserAchievements::beginnerPars;
   //return XBLiveGetStatValue($completedLB, "beginnerPars");
}

function getIntermediatePars()
{
   return $UserAchievements::IntermediatePars;
   //return XBLiveGetStatValue($completedLB, "IntermediatePars");
}

function getAdvancedPars()
{
   return $UserAchievements::AdvancedPars;
   //return XBLiveGetStatValue($completedLB, "AdvancedPars");
}

function getBonusPars()
{
   return $UserAchievements::BonusPars;
   //return XBLiveGetStatValue($completedLB, "AdvancedPars");
}

function getMPOverallPoints()
{
   return $UserAchievements::MPTotalScore;
}

function getFirstPlace()
{
   return $UserAchievements::MPFirstPlace;
}

// Used to update the achievement leaderboards
function finishedMission(%levelid)
{
   echo("finished mission" SPC %levelid);
   %column = "";
   %offset = 0;

   // Which block of missions are we updating
   if (%levelid < 21)
   {
      %column = "beginnerLevels";
      %offset = 0;
   }
   else if (%levelid < 41)
   {
      %column = "IntermediateLevels";
      %offset = 20;
   }
   else if (%levelid < 61)
   {
      %column = "AdvancedLevels";
      %offset = 40;
   }
   else if (%levelid > 99960 && %levelid < 99981)
   {
      %column = "BonusLevels";
      %offset = 99960;
   }
   
   // Determine the bit that represents this mission
   %bit = BIT(%levelid - %offset);
   //$UserAchievements::BeginnerLevels 238
   eval("$UserAchievements::" @ %column @ " |= %bit;");
   
   $UserLevelsCompleted[%levelid] = 1;

   //%val = XBLiveGetStatValue($completedLB, %column);
   //%val |= %bit;

   //XBLiveWriteStats($completedLB, %column, %val, "");
}

function finishedFirstPlaceInMP()
{
   $UserAchievements::MPFirstPlace = 1;
}

//function getMission(%missionId)
//{
//   %group = SinglePlayMissionGroup;
//   for (%ai = 0; %ai < %group.getCount(); %ai++)
//   {
//      %mission = %group.getObject(%ai);
//      if (%mission.level == %missionId)
//         return %mission;
//   }
//   return 0;
//}

function AllBeginnerLevels()
{
	%blevels = 0;
	for (%bi = 1; %bi < 21; %bi++)
	{
		if ($UserLevelsCompleted[%bi])
		{
			%blevels++;
		}
	}
	if (%blevels == 20) {
		return true;
	} else {
		return false;
	}
}

function AllIntermediateLevels()
{
	%ilevels = 0;
	for (%ia = 21; %ia < 41; %ia++)
	{
		if ($UserLevelsCompleted[%ia])
		{
			%ilevels++;
		}
	}
	if (%ilevels == 20) {
		return true;
	} else {
		return false;
	}
}

function AllAdvancedLevels()
{
	%alevels = 0;
	for (%ib = 41; %ib < 61; %ib++)
	{
		if ($UserLevelsCompleted[%ib])
		{
			%alevels++;
		}
	}
	if (%alevels == 20) {
		return true;
	} else {
		return false;
	}
}

function AllBonusLevels()
{
	%alevels = 0;
	for (%ib = 99961; %ib < 99981; %ib++)
	{
		if ($UserLevelsCompleted[%ib])
		{
			%alevels++;
		}
	}
	if (%alevels == 20) {
		return true;
	} else {
		return false;
	}
}

function AllBeginnerPars()
{
	%bpars = 0;
	for (%ic = 1; %ic < 21; %ic++)
	{
		//%mission = getMission(%ic);
		if ($UserLevelsCompleted::Par[%ic])
		{
			%bpars++;
		}
	}
	if (%bpars == 20) {
		return true;
	} else {
		return false;
	}
}

function AllIntermediatePars()
{
	%ipars = 0;
	for (%ie = 21; %ie < 41; %ie++)
	{
		//%mission = getMission(%ie);
		if ($UserLevelsCompleted::Par[%ie])
		{
			%ipars++;
		}
	}
	if (%ipars == 20) {
		return true;
	} else {
		return false;
	}
}

function AllAdvancedPars()
{
	%apars = 0;
	for (%ig = 41; %ig < 61; %ig++)
	{
		//%mission = getMission(%ig);
		if ($UserLevelsCompleted::Par[%ig])
		{
			%apars++;
		}
	}
	if (%apars == 20) {
		return true;
	} else {
		return false;
	}
}

function AllBonusPars()
{
	%apars = 0;
	for (%ig = 99961; %ig < 99981; %ig++)
	{
		//%mission = getMission(%ig);
		if ($UserLevelsCompleted::Par[%ig])
		{
			%apars++;
		}
	}
	if (%apars == 20) {
		return true;
	} else {
		return false;
	}
}

function finishedPar(%levelid)
{
   echo("finished Par on" SPC %levelid);
   %column = "";
   %offset = 0;

   // Which block of missions are we updating
   if (%levelid < 21)
   {
      %column = "beginnerPars";
      %offset = 0;
   }
   else if (%levelid < 41)
   {
      %column = "IntermediatePars";
      %offset = 20;
   }
   else if (%levelid < 61)
   {
      %column = "AdvancedPars";
      %offset = 40;
   } else if (%levelid > 99960 && %levelid < 99981)
   {
		%column = "AdvancedPars";
      %offset = 99960;
   }

   // Determine the bit that represents this mission
   %bit = BIT(%levelid - %offset);

   eval("$UserAchievements::" @ %column @ " |= %bit;");
   
   $partime = true;
   
   $UserLevelsCompleted::Par[%levelid] = 1;
   
   //%val = XBLiveGetStatValue($completedLB, %column);
   //%val |= %bit;

   //XBLiveWriteStats($completedLB, %column, %val, "");
}

function gotEasterEgg(%eggid)
{
   %hasFoundIt = !hasFoundEgg(%eggid);
   
   %bit = BIT(%eggid);

   %column = "easterEggs";
   eval("$UserAchievements::" @ %column @ " |= %bit;");
   
   $UserAchievements::hasEgg[%eggid] = true;

   // JMQ: don't need stat sessions to save achievements
   // Write out the easter egg immediatly
//   if( clientAreStatsAllowed() && %hasFoundIt )
//   {
//      %thisStartedStatSession = false;
//      
//      if( !XBLiveIsStatsSessionActive() )
//      {
//         %thisStartedStatSession = true;
//         XBLiveStartStatsSession();
//      }
    
   // do achievement save if we're signed in locally or better and it isn't a demo  
   if (%hasFoundIt)
   {
      checkForAchievements();
      savePCUserProfile();
   }
      
//      if( %thisStartedStatSession )
//         XBLiveEndStatsSession();
//   }
}

//function loadAchData()
//{
//   if (!XBLiveIsSignedIn())
//   {
//      error("User is not logged on, cannot load achievement data");
//      return;
//   }
//   
//   %port = XBLiveGetSignInPort();
//   
//   if (!xbIsProfileContentReady( %port ))
//   {
//      error("Profile content not ready, can't load achievement data");
//      return;
//   }
//   
//   echo("Loading user achievement data");
//   XBLiveLoadAchData();
//   $UserAchievements::Loaded = true;
//}

//function saveAchData()
//{
//   if (!XBLiveIsSignedIn())
//   {
//      error("User is not logged on, cannot save achievement data");
//      return;
//   }
//   
//   echo("Saving user achievement data");
//   checkProfileWrite("XBLiveSaveAchData(\"$UserAchievements::*\");");
//}