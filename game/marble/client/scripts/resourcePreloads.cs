//$preload_skip["game/client/ui/GameSplash.jpg"] = 1;
//$preload_skip["game/client/ui/Missions/bgSpooky.jpg"] = 1;
//$preload_skip["game/client/ui/Missions/bgFrantic.jpg"] = 1;
//$preload_skip["game/client/ui/Missions/bgLush.jpg"] = 1;
//$preload_skip["game/client/ui/Missions/bgDefault.jpg"] = 1;

// immediately preload all textures specified by %spec
function preloadTextures(%spec, %func, %dxt)
{
   echo("Preloading Textures:" SPC %spec);
   if (%func $= "")
      %func = preloadTexture;
      
   %absStart = getRealTime();
   for (%file = findFirstFile(%spec); %file !$= ""; %file = findNextFile(%spec))
   {
      if ($preload_skip[%file])
         continue;
      %start = getRealTime();
      call(%func, %file, %dxt);
      //echo(%file SPC getRealTime() - %start);
   }
   echo("Preload elapsed:" SPC getRealTime() - %absStart);
}

$preload_idx = 0;
$preload_delay = 30;
$preload_list_len = 0;

// add the specified textures to the preload list for incremental preloading
// (gives main thread time to do other things)
function addToPreloadList(%spec, %func, %funcparam1, %funcparam2)
{
   if (%func $= "")
      %func = preloadTexture;

   for (%file = findFirstFile(%spec); %file !$= ""; %file = findNextFile(%spec))
   {
      if ($preload_skip[%file])
         continue;
      $preload_list[$preload_list_len] = %file;
      $preload_list_func[$preload_list_len] = %func;
      $preload_list_func_p1[$preload_list_len] = %funcparam1;
      $preload_list_func_p2[$preload_list_len] = %funcparam2;
      $preload_list_len++;
   }
}

function preloadNext()
{     
   // preload the next file then reschedule
   if ($preload_idx < $preload_list_len)
   {
      %func = $preload_list_func[$preload_idx];
      %funcparam1 = $preload_list_func_p1[$preload_idx];
      %funcparam2 = $preload_list_func_p2[$preload_idx];

      %start = getRealTime();
      if (%funcparam2 !$= "")
         call(%func, $preload_list[$preload_idx], %funcparam1, %funcparam2);
      else if (%funcparam1 !$= "")
         call(%func, $preload_list[$preload_idx], %funcparam1);
      else
         call(%func, $preload_list[$preload_idx]);
         
      //echo("Deferred preload:" SPC $preload_list[$preload_idx] SPC getRealTime() - %start);
   }
   $preload_idx++;
   
   if ($preload_idx < $preload_list_len)
      schedule($preload_delay, 0, "preloadNext");
}

function clientPreloadTextures()
{
   // gui stuff
   for( %i = 0; %i < 10; %i++ )
   {
      preloadTexture( "~/client/ui/game/numbers/" @ %i @ ".png" );
   }
   preloadTexture( "~/client/ui/game/numbers/colon.png" );
   preloadTexture( "~/client/ui/game/numbers/dash.png" );
   preloadTexture( "~/client/ui/game/numbers/point.png" );
   preloadTexture( "~/client/ui/game/numbers/slash.png" );
   preloadTexture( "~/client/ui/game/fade_black.png" );
   
   preloadTextures("~/client/ui/game/*.png");
   
   // DXT enabled for immediate pre-loads only
   // The '1' parameter refers to DXT1, '2' would be DXT2 etc
   //preloadTextures("~/data/skies/*.jng", preloadStaticDiffuseTexture, 3); 
   addToPreloadList("~/data/skies/*.jng", preloadStaticDiffuseTexture, 3); 
   
   preloadTexture("marble/client/ui/xbox/roundedBG.png");
   //addToPreloadList("game/client/ui/xbox/controls*.png");
   
   // start first incremental preload
   preloadNext();
}


// Preload some resources
//preloadResource( "~/data/shapes/balls/ball-superball.dts" );
//preloadTexture( "~/data/particles/star.png" );
//preloadTexture( "~/data/particles/smoke.png" );
//preloadTexture( "~/data/particles/twirl.png" );
//preloadTexture( "~/data/particles/spark.png");
