//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------
// Functions dealing with connecting to a server

//-----------------------------------------------------------------------------
// Server connection error
//-----------------------------------------------------------------------------
addMessageCallback( 'MsgConnectionError', handleConnectionErrorMessage );
function handleConnectionErrorMessage(%msgType, %msgString, %msgError)
{
   // On connect the server transmits a message to display if there
   // are any problems with the connection.  Most connection errors
   // are game version differences, so hopefully the server message
   // will tell us where to get the latest version of the game.

   // JMQ: displaying text from a remote server is not localization-friendly.  Hardcode it to local version.
   //$ServerConnectionErrorMessage = %msgError;
   $ServerConnectionErrorMessage = $Text::ErrorCannotConnect;
}

function clientCmdSetLobby()
{
   RootGui.setContent(LobbyGui);
}

//----------------------------------------------------------------------------
// GameConnection client callbacks
//----------------------------------------------------------------------------
function GameConnection::initialControlSet(%this)
{
   
   echo ("*** Initial Control Object");

   // The first control object has been set by the server
   // and we are now ready to go.

   // first check if the editor is active
   if (!Editor::checkActiveLoadDone())
      $LoadingDone = true;

   //if (ServerConnection.joinImmediately)
   //{
   //   commandToServer('JoinGame');
   //   ServerConnection.joinImmediately = false;
   //}

   // we're loaded now, so render the real server
   if (RootGui.isAwake() && ServerConnection.gameState !$= "wait" && !$Client::showPreviews)
   {
      RootGui.setContent(PlayGui);
   //   if (isUsingBrowserUI())
   //   {
   //      onGCCSessionStarted();
   //   }
      //DjAnson.play();
      //fadeInAudio();
   }
   
   recycleClientMissionCleanup();
}

function GameConnection::controlObjectUpdated(%this)
{
   echo("*** New control object" SPC %this.getControlObject().getClassName());
	if (!$Client::showPreviews)
		clientCheckPlayGui();
}

function GameConnection::setLagIcon(%this, %state)
{
   if (%this.getAddress() $= "local")
      return;
   LagIcon.setVisible(%state $= "true");
}

function GameConnection::onConnectionAccepted(%this)
{
   // Called on the new connection object after connect() succeeds.
   LagIcon.setVisible(false);
   commandToServer('SetMarble', $pref::marbleIndex);

   onGgcConnectionComplete($ggcSuccess);
}

function GameConnection::onConnectionTimedOut(%this)
{
   // Called when an established connection times out
   RootGui.setContent(connErrorGui, $Text::ErrorConnectionLost, "Internal Error: Connection Timed Out");

   onGgcConnectionComplete($GgcConnectionTimeout);
}

function GameConnection::onConnectionDropped(%this, %msg)
{
   %this.dropped = true;
   // Established connection was dropped by the server
   if (%msg $= "CR_YOUAREKICKED")
      RootGui.setContent(connErrorGui, $Text::ErrorHostKickedYou, %msg);
   else if (%msg $= "ARB_FAILED")
      RootGui.setContent(connErrorGui, $Text::ErrorCannotConnect, %msg);
   else if (%msg $= "DEMO_OUTOFTIME")
   {
      $disconnectGui = "";
      disconnectedCleanup();
      UpsellGui.displayMPPostGame(false);
      UpsellGui.setBackToGui(MainMenuGui);
   }
   else {
      if (%msg $= "")
         %msg = "Internal Error: The server disconnected all connected clients.";
      RootGui.setContent(connErrorGui, $Text::ErrorHostKilled, %msg);
   }

   onGgcConnectionComplete($GgcConnectionDropped, %msg);
}

function GameConnection::onConnectionError(%this, %msg)
{
   // General connection error, usually raised by ghosted objects
   // initialization problems, such as missing files.  We'll display
   // the server's connection error message.
   RootGui.setContent(connErrorGui, $ServerConnectionErrorMessage, %msg);

   onGgcConnectionComplete($GgcConnectionError, %msg);
}

//----------------------------------------------------------------------------
// Connection Failed Events
// These events aren't attached to a GameConnection object because they
// occur before one exists.
//----------------------------------------------------------------------------
function GameConnection::onConnectRequestRejected( %this, %msg )
{
   switch$(%msg)
   {
      case "CR_CL_OLD":
         %error = "Client Outdated";
      case "CR_SRV_OLD":
         %error = "Server Outdated";
      case "CR_INVALID_PROTOCOL_VERSION":
         %error = $Text::ErrorCannotConnect;
      case "CR_INVALID_CONNECT_PACKET":
         %error = $Text::ErrorSessionEnded;
      case "CR_YOUAREBANNED":
         %error = $Text::ErrorYouBanned;
      case "CR_YOUAREKICKED":
         %error = $Text::ErrorHostKickedYou;
      case "CR_SERVERFULL":
         %error = $Text::ErrorSessionGone;
      case "CR_DEMOUPGRADE":
         %error = $Text::ErrorDemoUpgrade;
      case "CR_DEMOREJECT":
         %error = $Text::ErrorCannotConnect;
      case "CHR_PASSWORD":
         // no password support on xbox
         %error = $Text::ErrorCannotConnect;
      case "CHR_PROTOCOL":
         %error = $Text::ErrorCannotConnect;
      case "CHR_CLASSCRC":
         %error = $Text::ErrorCannotConnect;
      case "CHR_INVALID_CHALLENGE_PACKET":
         %error = $Text::ErrorSessionEnded;
      default:
         %error = $Text::ErrorCannotConnect;
   }

   RootGui.setContent(connErrorGui, %msg, %error);

   onGgcConnectionComplete($GgcConnectionRejected, %msg);
}

function GameConnection::onConnectRequestTimedOut(%this)
{
   RootGui.setContent(connErrorGui, $Text::ErrorConnectionLost, "Internal Error: Connect Request Timed Out");

   onGgcConnectionComplete($GgcConnectionRequestTimedOut, %msg);
}

//------------------------------------------------------------------------------
// Grace period changes for patch -pw

function clearClientGracePeriod()
{
   if( isEventPending( $Client::graceSchedule ) )
      cancel( $Client::graceSchedule );

   $Client::isInGracePeriod = false;
   $Client::willfullDisconnect = false;

   echo( "Client (" @ $Player::ClientId @ ") grace period reset." );
}

function checkClientGracePeriod()
{
   $Client::isInGracePeriod = false;
   echo( "Client (" @ $Player::ClientId @ ") grace period check, NO LONGER IN GRACE PERIOD!!" );
}

function resetClientGracePeriod()
{
   $Client::isInGracePeriod = true;

   if( isEventPending( $Client::graceSchedule ) )
      cancel( $Client::graceSchedule );

   schedule( $Client::gracePeriodMS, 0, "checkClientGracePeriod" );

   echo( "Client (" @ $Player::ClientId @ ") grace peroid reset, " @ $Client::gracePeriodMS @ " MS until reset." );
}

//-----------------------------------------------------------------------------
// Disconnect
//-----------------------------------------------------------------------------
function disconnect()
{
   // clean up stats sessions
      // store the fact that I am dropping (may need it for ratings calculation)
      echo("disconnect(): I dropped");

      // Changed for patch -pw
      if( $Client::isInGracePeriod || $Game::Ranked )
         clientAddDroppedClient( $Player::ClientId, $Player::XBLiveId );
      else if( !$Client::isInGracePeriod && $Client::willfullDisconnect )
	  // FIX FIX FIX FIX FIX FOR SUMO!!!
         zeroMyClientScoreCuzICheat();

      if ($Client::currentGameCounts)
         clientWriteMultiplayerScores();
      echo("disconnect(): Ending stats session and cleaning up stats");
      //XBLiveEndStatsSession();
      clientCleanupStats();

   // Delete the connection if it's still there.
   if (isObject(ServerConnection))
      ServerConnection.delete();
   disconnectedCleanup();
   // Call destroyServer in case we're hosting
   // JMQ: we don't destroy our local server - just leave it running
   //destroyServer();
}
function disconnectedCleanup()
{
	if (GameMissionInfo.getCurrentMission().file $= "")
	{
		GameMissionInfo.setDefaultMission();
	}
   // clean up stats sessions
      if (!$Server::Hosting)
      {
         echo("disconnectedCleanup(): host dropped me");
         // if the connection was dropped and the game was in play state, the host may have
         // ended the game early, so track that for ratings calculation
         if (isObject(ServerConnection) && ServerConnection.dropped &&
             ServerConnection.gameState $= "play")
            clientAddDroppedClient($Host::ClientId, $Host::XBLiveId);
      }

      if ($Client::currentGameCounts)
         clientWriteMultiplayerScores();
      echo("disconnectedCleanup(): Ending stats session and cleaning up stats");
      //XBLiveEndStatsSession();
   clientCleanupStats();

   if (isObject(StatsUserRatings_Level))
      StatsUserRatings_Level.clear();
   if (isObject(StatsUserRatings_Overall))
      StatsUserRatings_Overall.clear();

   recycleClientMissionCleanup();

   // Clear misc script stuff
   HudMessageVector.clear();

   LagIcon.setVisible(false);
   PlayerListGui.clear();
   LobbyGui.clear();

   // Clear all print messages
   clientCmdclearBottomPrint();
   clientCmdClearCenterPrint();

   if ($Server::Hosting)
   {
      // nothing special for host
   }
   else if ($Client::connectedMultiplayer)
   {
      XBLiveDisconnect();
		if ($disconnectGui !$= "!self")
		{
      if (!isObject($disconnectGui))
         $disconnectGui = MultiPlayerGui;
         RootGui.setContent($disconnectGui);
      //$reconnectSched = schedule(0,0,connectToPreviewServer);
		
		}
      if (State::isClient())
      {
         ggcSendRpc("game.client.shutdown");
      }
   }

   // re-enable Live if it was disabled
   XBLiveSetEnabled(true);

   // Dump anything we're not using
   clearTextureHolds();
   purgeResources();
   //stopDemo();

   unlockControler();

   // Call destroyServer in case we're hosting
   // JMQ: we don't destroy our local server - just leave it running
   //schedule(0,0,destroyServer);

   if (isUsingBrowserUI())
   {
      closeNetPort();
   }
}

function fmsHostCleanup()
{
      if (isObject(MissionCleanup))
         MissionCleanup.delete();
}

function enterPreviewMode(%clientDropCode)
{
   if ($Client::TempHost)
   {
      disconnect();
		loadPreviewMission();
		connectToServer("");
      return;
   }
   // clean up our server if we are hosting
   //if ($Server::Hosting)
   //{
      cancel($Server::ArbSched);
	  
      destroyGame();

      // tell everyone that I'm trashing the game
      // clean up stats sessions
         echo("enterPreviewMode(): Host dropped");

         if( $Client::willfullDisconnect && !$Client::isInGracePeriod && !$Game::Ranked && ClientGroup.getCount() > 1 )
			// again FIX FIX FIX FIX FIX FOR SUMO!!!
            zeroMyClientScoreCuzICheat();
         else
            clientAddDroppedClient($Host::ClientId, $Host::XBLiveId);

         if ($Client::currentGameCounts)
            clientWriteMultiplayerScores();
         echo("enterPreviewMode(): Ending stats session and cleaning up stats");
         //XBLiveEndStatsSession();
         clientCleanupStats();

      // evict remote clients.  move local connection to front so that we don't delete it.
      ClientGroup.bringToFront(LocalClientConnection);
      while (ClientGroup.getCount() > 1)
      {
         %client = ClientGroup.getObject(1);
         %client.delete(%clientDropCode);
      }

      fmsHostCleanup();

      // reset local to wait state
      LocalClientConnection.enterWaitState();

      GGConnectUnregisterHostedMatch();

      // clean up client side state from last game
      disconnectedCleanup();

      stopMultiplayerServer();

      if (isObject($disconnectGui))
         RootGui.setContent($disconnectGui);
   //}
}
