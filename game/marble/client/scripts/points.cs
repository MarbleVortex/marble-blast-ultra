if (!isObject(PointSystem))
   new TCPObject(PointSystem);
   
$MBPointServerPHP = "/connection/points.php";

function retrievePoints()
{
   PointSystem.retrieve();
}

function submitPoints(%points)
{
   PointSystem.submit(%points);
}

function PointSystem::onLine(%this, %line)
{
   //echo("PointSystem::onLine: Got Message " @ %line);
   if (getWord(%line, 0) !$= "FUNC:")
      return; 
      
   if (getWord(%line, 1) $= "MPoints=")
   {
      %points = getWord(%line, 2);
      $UserAchievements::GamerScore = %points;
   }
      
   //$Net::Username = getWord(%line, 1);
}

function PointSystem::retrieve(%this)
{
   echo("Retrieving Points");
   %this.get($MBServerURL1, $MBPointServerPHP, "cmd=retrieve===" @ $pref::GameKey);
}

function PointSystem::submit(%this, %points)
{
   echo("Submitting Points");
   %this.get($MBServerURL1, $MBPointServerPHP, "cmd=submit===" @ $pref::GameKey @ "===" @ %points);
}