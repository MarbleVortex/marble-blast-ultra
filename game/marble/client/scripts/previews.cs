function loadPreviewMission(%override, %noanim)
{
   if ($Client::showPreviews)
      return;
	  
	// Block moveMap in menus
	moveMap.pop();
      
   $Client::NoAnim = %noanim;
	%mis = GameMissionInfo.getCurrentMission().file;
	if (%override !$= "")
		%mis = %override;
   $Client::PreviewFile = %mis;
   RootClouds.setVisible(true);
   //NoPreviewBG.setVisible(true);
   RootTSCtrl.setVisible(false);
   if (!%noanim)
   {
      RootGui.setCenterText($Text::Loading);
      RootGui.displayLoadingAnimation( "center" );
   }
   $Client::showPreviews = true;
   $doShowEditor = false;
   loadMission(%mis, true);
   if (!$Client::connectedMultiplayer)
		connectToServer("");
   if (!$Server::Dedicated)
      allowConnect(false);
   //LocalClientConnection.enterWaitState();
   if (%noanim)
   {
      RootGui.setCenterText();
      RootGui.displayLoadingAnimation();
   }
}

function endPreviewMission(%noanim)
{
   // Re-enable moveMap
   moveMap.push();
   $Client::NoAnim = %noanim;
   $Client::PreviewFile = "";
   if (!%noanim)
   {
      RootGui.setCenterText($Text::Loading);
      RootGui.displayLoadingAnimation( "center" );
   }
   endMission();
   $Client::showPreviews = false;
   RootClouds.setVisible(true);
   //NoPreviewBG.setVisible(true);
   RootTSCtrl.setVisible(false);
   if (%noanim)
   {
      RootGui.setCenterText();
      RootGui.displayLoadingAnimation();
   }
}

function reloadPreviewMission(%override, %noanim)
{
   if ($Client::showPreviews && ($Client::PreviewFile $= GameMissionInfo.getCurrentMission().file || $Client::PreviewFile $= %override))
   {
      return;
   }
   $Client::NoAnim = %noanim;
   endPreviewMission(%noanim);
   loadPreviewMission(%override, %noanim);
}
