function TCPObject::dump(%this) {
   // Jeff: I choose to override this because we do not want the client
	// to be able to view our queries.  This is for data security, because
	// all queries and info are stored into the actual tcp object itself.
	// This is a standard c++ error code, implimented to make this thing look legit.
	warn("<input> (0): Unknown command dump.");
	if (%this.getName() $= "")
   	warn("  Object (" @ %this.getID() @ ") TCPObject -> SimObject");
	else
   	warn("  Object " @ %this.getName() @ "(" @ %this.getID() @ ") " @ %this.getName() @ " -> TCPObject -> SimObject");
}