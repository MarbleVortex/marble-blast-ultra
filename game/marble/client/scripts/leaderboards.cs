//if (!isObject(LeaderboardConnection))
      //LeaderboardConnection.delete();
   //new TCPObject(LeaderboardConnection);
   
$MBLBServerPHP = "/connection/leaderboards.php";

$useLB = !$devMode;

//function TestLB()
//{
//   new TCPObject(TCPObj);
//   TCPObj.connect($MBServerURL1);
//   TCPObj.send("GET /lb/connect.php cmd=connect===marbledatamissionsultraintermediateUrbanJungleurbanmis\r\n");
//}

function getLB(%id)
{
   //LeaderboardConnection.cancel();
   //if (!$useLB)
   //   return;
   if (isObject(LeaderboardConnection))
      LeaderboardConnection.delete();
   new TCPObject(LeaderboardConnection);
   LeaderboardConnection.update(%id);
   //echo("Updating Leaderboards for: " @ %id);
   //%get = "cmd=connect===" @ %id;
   //LeaderboardConnection.connect($MBLBServerURL);
   //LeaderboardConnection.send("GET /lb/connect.php" SPC %get SPC "\r\n");
   //LeaderboardConnection.send("GET" SPC $MBLBServerPHP SPC %get);
}

function setLB(%id, %missionName, %time, %key, %score, %difficulty, %artist, %game)
{
   if (!$useLB || $testcheats)
      return;
   if (isObject(LeaderboardConnection))
      LeaderboardConnection.delete();
   new TCPObject(LeaderboardConnection);
   LeaderboardConnection.submit(%id, %missionName, %time, %key, %score, %difficulty, %artist, %game);
}

function LeaderboardConnection::update(%this, %id)
{
   //if (!$useLB)
   //   return;
   echo("Updating Leaderboards for: " @ %id);
   %get = "cmd=connect===" @ %id;
   %this.get($MBServerURL1, $MBLBServerPHP, %get);
}

function LeaderboardConnection::onDNSFailed(%this)
{
   XMessagePopupDlg.show(0, "Failed to submit score to leaderboards. The server must be down right now.", $Text::OK);
}

function LeaderboardConnection::onConnectFailed(%this)
{
   XMessagePopupDlg.show(0, "Failed to submit score to leaderboards. Please check your internet connection.", $Text::OK);
}

function LeaderboardConnection::onLine(%this, %line)
{
   //if (!$useLB)
   //   return;
   %count = getWordCount(%line);
   
   if (getWord(%line, 0) $= "LB:Success")
   {
      echo("LB Submit Success");
      return;
   }
   
   if (getWord(%line, 0) $= "LB:Failed")
   {
      XMessagePopupDlg.show(0, "Failed to submit score to leaderboards. Ensure that you have your key in, in the options.", $Text::OK);
      return;
   }
   
   if (getWord(%line, 0) !$= "LB:")
      return;
      
   $LB::RowCount = getWord(%line, 1);
      
   %rank = strreplace(getWord(%line, 2), "/n", "\n");
   %score = strreplace(getWord(%line, 3), "/n", "\n");
   %name = strreplace(getWord(%line, 4), "/n", "\n");
   %timeRaw = strreplace(getWord(%line, 5), "/n", "" TAB "");
   
   %time = "";   
   
   for (%i = 0; %i < getFieldCount(%timeRaw); %i++)
   {
      %time = %time @ formatTime(getField(%timeRaw, %i)) @ "\n";
   }
   
   LevelScoresGui.add(%rank, %score, %name, %time);
   
   echo ("Received Scores");
}

function LeaderboardConnection::submit(%this, %id, %missionName, %time, %key, %score, %difficulty, %artist, %game)
{
   if (!$useLB || $testcheats)
      return;
   if (%key $= "")
      return;
   echo("Submitting Leaderboards Score");
   %get = "cmd=add===" @ %id @ "===" @ %missionName @ "===" @ %key @ "===" @ %time @ "===" @ %score @ "===" @ %difficulty @ "===" @ %artist @ "===" @ %game;
   %get = urlencode(%get);
   %this.get($MBServerURL1, $MBLBServerPHP, %get);
}

function urlencode(%url)
{
	%url = strreplace(%url, " ", "%20");
	%url = strreplace(%url, "&", "%26");
	return %url;
}