function refreshAudio()
{
	sfxSetChannelVolume(1, $pref::Option::FXVolume);
	sfxSetChannelVolume(2, $pref::Option::MusicVolume);
	sfxSetChannelVolume(3, $pref::Option::FXVolume);
	sfxSetChannelVolume(4, $pref::Option::FXVolume);
	sfxSetChannelVolume(5, $pref::Option::FXVolume);
	sfxSetChannelVolume(6, $pref::Option::FXVolume);
	sfxSetChannelVolume(7, $pref::Option::FXVolume);
	sfxSetChannelVolume(8, $pref::Option::FXVolume);
}

function fadeOutAudio()
{
	sfxFadeMasterVolume($pref::SFX::fadeOutTime, 0.0);
}

function fadeInAudio()
{
   sfxSetMasterVolume($pref::SFX::masterVolume*0.01);
	sfxFadeMasterVolume($pref::SFX::fadeInTime, $pref::SFX::masterVolume);
}