if (!isObject(VersionChecker))
   new TCPObject(VersionChecker);

$devMode = 0; // should be 0 when website is working or on release

$versionCheck = !$devMode;   

$VersionCode = 35;

$currentVersion = "1.9.1.6";

$MBVersionChannel = 0;

$MBVersionServerPHP = "/game/download/version.php";

function VersionChecker::onLine(%this, %line)
{
   if (%line $= "info:latest" || getField(%line, 1) $= "info:latest")
      return;
      
   if (getField(%line, 0) !$= "VC:")
      return;
      
   XMessagePopupDlg.show(0, "New Version! Would you like to update now?\n\n" @ getField(%line, 1), $Text::OK, "gotoWebPage(\"http://www.marblevortex.com\")", $Text::Cancel, "");//"gotoWebPage(\"http://mbudownload.tk/?channel=" @ $MBVersionChannel @ "\");", $Text::Cancel, "");
}

function VersionChecker::check(%this)
{
   if ($versionCheck)
   {
	  echo("Checking for updates...");
      %this.get($MBServerURL1, $MBVersionServerPHP, "version=" @ $currentVersion @ "===" @ $MBVersionChannel);
   }
}