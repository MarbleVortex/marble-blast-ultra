//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// PlayGui is the main TSControl through which the game is viewed.
// The PlayGui also contains the hud controls.
//-----------------------------------------------------------------------------
function PlayGui::AddSubDialog(%this, %dialog)
{
   if (!isObject(%this.subDialogs))
   {
      %this.subDialogs = new SimSet();
      RootGroup.add(%this.subDialogs); // beware $instantGroup !
   }
   %this.subDialogs.add(%dialog);
}

$Client::CurLocation = "";
$Client::CurLocationVisible = false;

function PlayGui::onWake(%this)
{
   PG_Location.visible = $Client::CurLocationVisible;
   PG_LocationText.setText($Client::CurLocation);   
   
   enableGamepad();

   // Turn off any shell sounds...
   //sfxStop( ... );

   // Load user prefs
   //if( xbIsProfileContentReady( $Input::LockedController ) )
   //   loadUserPrefs();

   %this.timerInc = 50;
   // just update the action map here
   moveMap.push();
   chatMap.push();

   if (isObject(%this.subDialogs))
      for (%i = 0; %i < %this.subDialogs.getCount(); %i++)
         Canvas.pushDialog(%this.subDialogs.getObject(%i));

   if (ServerConnection.isMultiplayer || $Game::SPGemHunt || MissionInfo.gameMode $= "Collection" || MissionInfo.gameMode $= "Arcade")
   {
      Canvas.pushDialog(PlayerListGui);

      // Always expanded by default (Bug 14955) -pw
      // expand player list by default in widescreen version
      if (isWidescreen() && ShortPlayerList.visible)
         togglePlayerListLength();
      // contract player list by default in non widescreen
      //if (!isWidescreen() && !ShortPlayerList.visible)
      //   togglePlayerListLength();
   }
   else if (PlayerListGui.isAwake())
      Canvas.popDialog(PlayerListGui);

   if ($gamePauseWanted)
      Canvas.pushDialog(GamePauseGui);
   else
      setHudVisible( true );

   DemoTimerGui.init(PlayGui);

   PG_BoostBar.setVisible(true);

   RootGui.setTitle("");
   if (! $GamePauseGuiActive)
      RootBackgroundBitmaps.SetVisible(false);
   else
      RootGui.setTitle($Text::Paused);
      
   %this.setDisplay();

   // hack city - these controls are floating around and need to be clamped
   schedule(0, 0, "refreshCenterTextCtrl");
   schedule(0, 0, "refreshBottomTextCtrl");
   //schedule(0, 0, "refreshLocationTextCtrl");

   %this.setPowerUp( "" );
}

function PlayGui::setDisplay()
{
   PG_Details.setVisible($showDetails);
}

function PlayGui::show(%this)
{
   // Make sure the display is up to date
   if (!$GamePauseGuiActive)
   {
      %this.resizeGemSlots();
      %this.setGemCount(%this.gemCount);
      %this.setMaxGems(%this.maxGems);
      %this.scaleGemArrows();

      // recenter center things
      reCenter(TimeBox, 0);
      reCenter(HelpTextBox, 0);
      reCenter(ChatTextBox, 0);
   }
}

function PlayGui::onSleep(%this)
{
   if (PlayerListGui.isAwake())
      Canvas.popDialog(PlayerListGui);
   if (GamePauseGui.isAwake())
      Canvas.popDialog(GamePauseGui);

   if (isObject(%this.subDialogs))
      for (%i = 0; %i < %this.subDialogs.getCount(); %i++)
         Canvas.popDialog(%this.subDialogs.getObject(%i));

   // Terminate all playing sounds
   sfxStopAll( $SimAudioType );

   // pop the keymaps
   chatMap.pop();
   moveMap.pop();
   $mvYawRightSpeed = 0;
   $mvYawLeftSpeed = 0;
   $mvPitchUpSpeed = 0;
   $mvPitchDownSpeed = 0;
   $mvXAxis_L = 0;
   $mvYAxis_L = 0;
}

//-----------------------------------------------------------------------------
function PlayGui::setMessage(%this,%text,%timer)
{
   if (%text $= "ready")
   {
      // Handle controler removed
      if( $ControlerRemovedDuringLoad )
      {
         lockedControlerRemoved();
      }

      // Handle network cable pull
      if( $NetworkPulledDuringLoad )
      {
         $NetworkPulledDuringLoad = false;
         NetworkDisconnectGui.onNetworkDisconnect();
      }
   }

   switch$ (%text)
   {
      case "ready":
         %text = ($Client::connectedMultiplayer || $Game::SPGemHunt || MissionInfo.gameMode $= "race") ? $Text::InGameReady : "";
      case "go":
         %text = ($Client::connectedMultiplayer || $Game::SPGemHunt || MissionInfo.gameMode $= "race") ? $Text::InGameGo : "";
      case "outOfBounds":
         %text = $Text::InGameOutofBounds;
      case "gameOver":
         %text = "Game Over";
      case "quota":
         %text = "Quota Reached!";
      case "overload":
         %text = "Overloaded!";
   }

   // Set the center message text
   if (%text !$= "")  {
      CenterMessageDlgText.setText(%text);
      CenterMessageDlg.setVisible(true);
      cancel(CenterMessageDlg.timer);
      if (%timer)
         CenterMessageDlg.timer = CenterMessageDlg.schedule(%timer,"setVisible",false);
   }
   else
      CenterMessageDlg.setVisible(false);
}

//-----------------------------------------------------------------------------
function PlayGui::setPowerUp(%this,%bmpFile)
{
   // Update the power up hud control
   if (%bmpFile $= "")
   {
      HUD_ShowPowerUp.setBitmap("marble/client/ui/game/powerup.png");
   }
   else
   {
      HUD_ShowPowerUp.setBitmap("marble/client/ui/game/" @ %bmpFile);
   }
   MinSec_Colon.setBitmap("marble/client/ui/game/numbers/colon.png");
   MinSec_Point.setBitmap("marble/client/ui/game/numbers/point.png");
   PG_NegSign.setBitmap("marble/client/ui/game/numbers/dash.png");
   HUD_ShowGem.setBitmap("marble/client/ui/game/gem.png");
   GemsSlash.setBitmap("marble/client/ui/game/numbers/slash.png");
   TimeBox.setBitmap("marble/client/ui/game/timebackdrop0");
   BoostBarFG.setBitmap("marble/client/ui/game/powerbar.png");
}

//-----------------------------------------------------------------------------

function PlayGui::resizeGemSlots(%this)
{
   // These control gemCount/Max number placement
   $gemsVertPos = 4;
   $showGemVertPos = 2;

   // resize the position variables
   %newExt = GemBox.extent;
   %oldExt = GemBox.normextent;
   %horzScale = getWord(%newExt,0)/getWord(%oldExt,0);

   $gemsFoundFirstSlot  = 20*%horzScale;
   $gemsFoundSecondSlot = 38*%horzScale;
   $gemsFoundThirdSlot  = 56*%horzScale;

   $gemsSlashPos = 73*%horzScale;

   $gemsTotalFirstSlot  = 89*%horzScale;
   $gemsTotalSecondSlot = 107*%horzScale;
   $gemsTotalThirdSlot  = 125*%horzScale;

   $showGemFirstSlot = 107*%horzScale;
   $showGemSecondSlot = 125*%horzScale;
   $showGemThirdSlot = 144*%horzScale;
}

function PlayGui::setMaxGems(%this,%count)
{
   %this.maxGems = %count;
   %one = %count % 10;
   %ten = (%count - %one) % 100;
   %hundred = (%count - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsTotalHundred.setNumber(%hundred);
   GemsTotalTen.setNumber(%ten);
   GemsTotalOne.setNumber(%one);
   %visible = %count != 0;

   // Decide which of these should be visible and
   // where they should be positioned
   if (%count < 10)
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(false);
      GemsTotalTen.setVisible(false);
      GemsFoundHundred.setVisible(false);
      GemsTotalHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - 48 SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalFirstSlot - 48 SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos - 48 SPC $gemsVertPos;

      HUD_ShowGem.setVisible(%visible);
      HUD_ShowGem.position = $showGemFirstSlot - 48 SPC $showGemVertPos;
   }
   else if (%count < 100)
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsTotalTen.setVisible(%visible);
      GemsFoundHundred.setVisible(false);
      GemsTotalHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - 24 SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalSecondSlot - 24 SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot - 24 SPC $gemsVertPos;
      GemsTotalTen.position = $gemsTotalFirstSlot - 24 SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos - 24 SPC $gemsVertPos;

      HUD_ShowGem.setVisible(%visible);
      HUD_ShowGem.position = $showGemSecondSlot - 24 SPC $showGemVertPos;
   }
   else
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsTotalTen.setVisible(%visible);
      GemsFoundHundred.setVisible(%visible);
      GemsTotalHundred.setVisible(%visible);

      GemsFoundOne.position = $gemsFoundThirdSlot SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalThirdSlot SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot SPC $gemsVertPos;
      GemsTotalTen.position = $gemsTotalSecondSlot SPC $gemsVertPos;
      GemsFoundHundred.position = $gemsFoundFirstSlot SPC $gemsVertPos;
      GemsTotalHundred.position = $gemsTotalFirstSlot SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos SPC $gemsVertPos;

     %width = getWord(Canvas.getVideoMode(), 0);
     if (%width > 640)
        HUD_ShowGem.setVisible(%visible);
     else
        HUD_ShowGem.setVisible(false);
     HUD_ShowGem.position = $showGemThirdSlot SPC $showGemVertPos;
   }

   GemsSlash.setVisible(%visible);
   SUMO_NegSign.setVisible(false);

   //HUD_ShowGem.setModel("marble/data/shapes/items/gem.dts","");
}

function PlayGui::setGemCount(%this,%count)
{
   %this.gemCount = %count;
   %one = %count % 10;
   %ten = (%count - %one) % 100;
   %hundred = (%count - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsFoundHundred.setNumber(%hundred);
   GemsFoundTen.setNumber(%ten);
   GemsFoundOne.setNumber(%one);
}

function PlayGui::setQuotaGems(%this,%count)
{
   if (%count > 0)
   {
      GemsQuotaHundred.setVisible(true);
      GemsQuotaTen.setVisible(true);
      GemsQuotaOne.setVisible(true);
   } else {
      GemsQuotaHundred.setVisible(false);
      GemsQuotaTen.setVisible(false);
      GemsQuotaOne.setVisible(false);
   }
   %this.quotaGems = %count;
   %one = %count % 10;
   %ten = (%count - %one) % 100;
   %hundred = (%count - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsQuotaHundred.setNumber(%hundred);
   GemsQuotaTen.setNumber(%ten);
   GemsQuotaOne.setNumber(%one);
}

//-----------------------------------------------------------------------------
function PlayGui::setPoints(%this, %clientid, %points)
{
   %pts = %points;

   %drawNeg = false;
   %negOffset = 0;
   if(%pts < 0)
   {
      %pts = - %pts;
      %drawNeg = true;
      %negOffset = 24;
   }

   %one = %pts % 10;
   %ten = (%pts - %one) % 100;
   %hundred = (%pts - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsFoundHundred.setNumber(%hundred);
   GemsFoundTen.setNumber(%ten);
   GemsFoundOne.setNumber(%one);

   %visible = %pts != 0;

   GemsSlash.setVisible(false);
   HUD_ShowGem.setVisible(false);
   GemsTotalOne.setVisible(false);
   GemsTotalTen.setVisible(false);
   GemsTotalHundred.setVisible(false);

   SUMO_NegSign.setVisible(%drawNeg);

   // Decide which of these should be visible and
   // where they should be positioned
   if (%pts < 10)
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(false);
      GemsFoundHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - %negOffset SPC $gemsVertPos;
   }
   else if (%pts < 100)
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsFoundHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot SPC $gemsVertPos;
   }
   else
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsFoundHundred.setVisible(%visible);

      GemsFoundOne.position = $gemsFoundThirdSlot + %negOffset SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot + %negOffset SPC $gemsVertPos;
      GemsFoundHundred.position = $gemsFoundFirstSlot + %negOffset SPC $gemsVertPos;
   }
}

function PlayGui::setDisplayLocation(%this, %flag)
{
   $Client::CurLocationVisible = %flag;
   PG_Location.visible = %flag;
}

function PlayGui::setLocation(%this, %text)
{
   %lineCount = getRecordCount(%text);// - 1;
   
   //%yPos = 18 - %lineCount * 8;  
   
   // 1 = 28
   // 2 = 12
   
   %yPos = 12;
   
   if (%lineCount == 1)
   {
      %yPos = 36;
   }
   
   %xPos = getWord(PG_LocationText.position, 0);
   
   PG_LocationText.position = %xPos SPC %yPos;
   
   $Client::CurLocation = "<font:ColiseumRR Medium:48><just:center><color:FFFFFF>" @ %text;
   
   PG_LocationText.setText($Client::CurLocation);
}

//-----------------------------------------------------------------------------
// Elapsed Timer Display
function PlayGui::setTimer(%this,%dt)
{
   %this.elapsedTime = %dt;
   %this.updateControls();
}

function PlayGui::resetTimer(%this)
{
   %this.elapsedTime = 0;
   %this.stopTimer();
   %this.updateControls();
}

function PlayGui::startTimer(%this)
{
}

function PlayGui::stopTimer(%this)
{
   if($BonusSfx !$= "")
   {
      sfxStop($BonusSfx);
      $BonusSfx = "";
   }
}

function onFrameAdvance(%delta)
{
   // To Be Overridden
   //echo("DELTA: " @ %delta);
   
   if ($xzxzgr542gbcvagduriowrjoicn)
   {
      if (getRandom(0, 5) == 0)
      {
         for (%i = 0; %i < ServerConnection.getCount(); %i++)
         {
            %obj = ServerConnection.getObject(%i);
            if (%obj.getClassName() $= "Marble" || %obj.getClassName() $= "Item")
            {
               %rx = getRandom(1, 4);
               %ry = getRandom(1, 4);
               %rz = getRandom(1, 7);
               %obj.scale = %rx SPC %ry SPC %rz;
            }
         }
      }
   }
   
   if ($xzxzgr542gbcvagduriowrjoicx)
   {
      if (getRandom(0, 5) == 0)
      {
         for (%i = 0; %i < ServerConnection.getCount(); %i++)
         {
            %obj = ServerConnection.getObject(%i);
            if (%obj.getClassName() $= "StaticShape")
            {
               %rx = getRandom(1, 2);
               %ry = getRandom(1, 2);
               %rz = getRandom(1, 2);
               %obj.scale = %rx SPC %ry SPC %rz;
            }
         }
      }
   }
   
   PlayGui.setDisplay();
   if ($showDetails)
   {
      PG_DetailsFPS.setText("<font:Arial Bold:18>FPS: " @ mRound($fps::real));
      PG_DetailsMarbleSmooth.setText("<font:Arial Bold:18>Smooth: (" @ $Marble::smooth1 @ ", " @ $Marble::smooth2 @ ")");
   }
}

function PlayGui::updateTimer(%this, %time, %bonusTime)
{
   %startTime = $Sim::Time;
   %delta = (%startTime - $LastTime) * 1000;
   $LastTime = %startTime;
   
   // This is now done via engine-hacks so that
   // it works even when there is no marble or
   // if the game is paused ~Matt (October 1st 2016)
   //onFrameAdvance(%delta);
   
   if (%bonusTime && $BonusSfx $= "")
   {
      $BonusSfx = sfxPlay(TimeTravelLoopSfx);
   }
   else if (%bonusTime == 0 && $BonusSfx !$= "")
   {
      sfxStop($BonusSfx);
      $BonusSfx = "";
   }

   %this.elapsedTime = %time;

   // Some sanity checking
   if (%this.elapsedTime > 3600000)
      %this.elapsedTime = 3599999;
   %this.updateControls();
}
function PlayGui::updateControls(%this)
{

	if (isWidescreen())
   {   
	   %lineSpacingHack = "<font:Arial Bold:30> <font:Arial Bold:24>";
	   %biggerFont = "<font:Arial Bold:30>";
	}
	else
	{
	   %lineSpacingHack = "<font:Arial Bold:24> <font:Arial Bold:20>";
	   %biggerFont = "<font:Arial Bold:24>";		
	}

	if (isObject(PG_TPS))
		PG_TPS.setText(%lineSpacingHack @ "<font:ColiseumRR Medium:48>" @ strFormat("%03d", $marbleDisplayVelocity));	
   if (PlayGui.gameDuration)
      %et = PlayGui.gameDuration - %this.elapsedTime;
   else
      %et = %this.elapsedTime;
   %drawNeg = false;
   if(%et < 0)
   {
      %et = - %et;
      %drawNeg = true;
   }
   %hundredth = mFloor((%et % 1000) / 10);
   %totalSeconds = mFloor(%et / 1000);
   %seconds = %totalSeconds % 60;
   %minutes = (%totalSeconds - %seconds) / 60;
   %secondsOne      = %seconds % 10;
   %secondsTen      = (%seconds - %secondsOne) / 10;
   %minutesOne      = %minutes % 10;
   %minutesTen      = (%minutes - %minutesOne) / 10;
   %hundredthOne    = %hundredth % 10;
   %hundredthTen    = (%hundredth - %hundredthOne) / 10;
   // Update the controls
   Min_Ten.setNumber(%minutesTen);
   Min_One.setNumber(%minutesOne);
   Sec_Ten.setNumber(%secondsTen);
   Sec_One.setNumber(%secondsOne);
   Sec_Tenth.setNumber(%hundredthTen);
   Sec_Hundredth.setNumber(%hundredthOne);
   PG_NegSign.setVisible(%drawNeg);

   if (%this.lastHundredth != %hundredth)
   {
      TimeBox.animBitmap("timebackdrop");
      %this.lastHundredth = %hundredth;
   }
}

function PlayGui::scaleGemArrows(%this)
{
   // defaults
 	//%ellipseScreenFractionX = "0.790000";
 	//%ellipseScreenFractionY = "0.990000";
   %fullArrowLength = "60";
   %fullArrowWidth = "40";
 	//%maxArrowAlpha = "0.6";
 	//%maxTargetAlpha = "0.4";
    %minArrowFraction = "0.4";

	//%gwidth = getWord(getResolution(),0);
    //%gheight = getWord(getResolution(),1);
	
   // defaults based on 640x480 scale up accordingly
 	//%xscale = (getWord(RootShapeNameHud.getExtent(), 0)/640);
   %yscale = (getWord(RootShapeNameHud.getExtent(), 1)/480);

  	//RootShapeNameHud.ellipseScreenFraction = (%ellipseScreenFractionX*%xscale) SPC (%ellipseScreenFractionY*%yscale);
    RootShapeNameHud.fullArrowLength = %fullArrowLength*%yscale;
    RootShapeNameHud.fullArrowWidth = %fullArrowWidth*%yscale;
   	//RootShapeNameHud.minArrowFraction = %minArrowFraction*%yscale;
}


//-----------------------------------------------------------------------------
function GuiBitmapCtrl::setNumber(%this,%number)
{
   %this.setBitmap($Con::Root @ "~/client/ui/game/numbers/" @ %number @ ".png");
   %this.setBitmap("~/client/ui/game/numbers/" @ %number @ ".png");
}

function GuiBitmapCtrl::animBitmap(%this,%bitmapPrefix)
{
   %this.setBitmap($Con::Root @ "~/client/ui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.setBitmap("~/client/ui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.animCurrent = (%this.animCurrent >= %this.animMax) ? 0 : %this.animCurrent++;
}

//-----------------------------------------------------------------------------
function refreshBottomTextCtrl()
{
   BottomPrintText.position = "0 0";
}
function refreshCenterTextCtrl()
{
   CenterPrintText.position = "0 0";
}

//function refreshLocationTextCtrl()
//{
   //PG_LocationText.position = "0 0";
//}

// hack playgui test
function hackTestPlayguiReload()
{
   // looking for memory leaks and any other badnesses
   for (%i = 0; %i < 100; %i++)
   {
      PlayGui.delete();
      exec("marble/client/ui/PlayGui.gui");
      RootGui.setContent(PlayGui);
   }
}
