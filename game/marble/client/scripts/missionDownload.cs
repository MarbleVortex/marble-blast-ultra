//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// Mission Loading & Mission Info
// The mission loading server handshaking is handled by the
// common/client/missingLoading.cs.  This portion handles the interface
// with the game GUI.
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// Loading Phases:
// Phase 1: Download Datablocks
// Phase 2: Download Ghost Objects
// Phase 3: Scene Lighting
//----------------------------------------------------------------------------
// Phase 1
//----------------------------------------------------------------------------
function onMissionDownloadPhase1(%missionName, %musicTrack)
{
   // comment this out until we know its safe
   // prevent these from being checked to allow greater download speeds - Anthony
   // whether or not this actually has an effect has yet to be fully tested, but a msgbox reveals the variables do update.
   /*
   if ($pref::Net::PacketRateToClient !$= "32" || $pref::Net::PacketRateToServer !$= "32" || $pref::Net::PacketSize !$= "450") {
   $pref::Net::PacketRateToClient = "32";
   $pref::Net::PacketRateToServer = "32";
   $pref::Net::PacketSize = "450";
   }
   */
   // This callback happens when phase 1 loading starts
   $LoadingDone = false;
   
   if ($Client::connectedMultiplayer && !MissionLoadingGui.isAwake())
   {
      RootGui.setContent(MissionLoadingGui);
      RootGui.setA($Text::Ready);
   }
		
	MissionLoadingProgress.setValue(0);
	MissionLoadingText.setText("0%");
}

function onPhase1Progress(%progress)
{
   // This callback happens during phase 1 loading
	
	MissionLoadingProgress.setValue(%progress);
	MissionLoadingText.setText(mFloor(%progress * 100) @ "%");
   Canvas.repaint();
}

function onPhase1Complete()
{
   // Callback on phase1 complete
	MissionLoadingProgress.setValue( 1 );
	MissionLoadingText.setText("100%");
   Canvas.repaint();
}

//----------------------------------------------------------------------------
// Phase 2
//----------------------------------------------------------------------------
function onMissionDownloadPhase2()
{
	MissionLoadingProgress.setValue(0);
	MissionLoadingText.setText("0%");
   Canvas.repaint();
}

function onPhase2Progress(%progress)
{
   MissionLoadingProgress.setValue(%progress);
	MissionLoadingText.setText(mFloor(%progress * 100) @ "%");
   Canvas.repaint();
}

function onPhase2Complete()
{
   MissionLoadingProgress.setValue( 1 );
	MissionLoadingText.setText("100%");
   Canvas.repaint();
}   

function onFileChunkReceived(%file, %ofs, %size)
{
   echo("Packet Rate: " @ $pref::Net::PacketRateToClient);
	MissionLoadingProgress.setVisible(true);
	MissionLoadingText.setVisible(true);
	//$Client::showPreviews = false;
   MissionLoadingProgress.setValue(%ofs / %size);
	MissionLoadingText.setText(mFloor((%ofs / %size) * 100) @ "%");
   //LoadingProgressTxt.setValue("Downloading " @ %fileName @ "...");
   //echo2("Downloading Mission: " @ (%ofs / %size) @ "%");
   //RootGui.setCenterText("Downloading Mission: " @ ((%ofs / %size) * 100) @ "%");
	//commandToServer('NoMissionFound');
}

//----------------------------------------------------------------------------
// Phase 3
//----------------------------------------------------------------------------
function onMissionDownloadPhase3()
{
	MissionLoadingProgress.setValue(0);
	MissionLoadingText.setText("0%");
   Canvas.repaint();
}

function onPhase3Progress(%progress)
{
	MissionLoadingProgress.setValue(%progress);
	MissionLoadingText.setText(mFloor(%progress * 100) @ "%");
   Canvas.repaint();
}

function onPhase3Complete()
{
   //$Client::showPreviews = true;
   MissionLoadingProgress.setValue( 1 );
	MissionLoadingText.setText("100%");
   Canvas.repaint();
   $lightingMission = false;
}
//----------------------------------------------------------------------------
// Mission loading done!
//----------------------------------------------------------------------------
function onMissionDownloadComplete()
{
   // Client will shortly be dropped into the game, so this is
   // good place for any last minute gui cleanup.
   
   if (!$Client::NoAnim)
   {
      RootGui.setCenterText("");
      RootGui.displayLoadingAnimation();
      $Client::NoAnim = false;
   }
	
	// this is now done when someone joins the game to allow switching to other levels to work - Anthony
	//MissionLoadingProgress.setVisible(false);
	//MissionLoadingText.setVisible(false);
	
   RootClouds.setVisible(false);
   //NoPreviewBG.setVisible(false);
   RootTSCtrl.setVisible(true);
   // Do this a bit later
   $LoadingDone = true;
   $inMenu = false;
   // Don't EVER join the game while in the lobby.
   // EVER. - Anthony
   if (!$Client::showPreviews && !LobbyGui.isAwake())
      commandToServer('JoinGame');
      
   if ($doShowEditor)
   {
      // recreate and open the editor
      Editor::create();
      MissionCleanup.add(Editor);
      EditorGui.loadingMission = true;
      Editor.open();
   }
   //resumeGame();
}

//------------------------------------------------------------------------------
// Before downloading a mission, the server transmits the mission
// information through these messages.
//------------------------------------------------------------------------------
addMessageCallback( 'MsgLoadInfo', handleLoadInfoMessage );
addMessageCallback( 'MsgLoadDescripition', handleLoadDescriptionMessage );
addMessageCallback( 'MsgLoadInfoDone', handleLoadInfoDoneMessage );
//------------------------------------------------------------------------------
function handleLoadInfoMessage( %msgType, %msgString, %mapName ) 
{
   // %JMQ: need to make sure this is handled in the RootGui
   warn("server is loading mission");
	// Need to pop up the loading gui to display this stuff.
	//Canvas.setContent("LoadingGui");
}
//------------------------------------------------------------------------------
function handleLoadDescriptionMessage( %msgType, %msgString, %line )
{
}
//------------------------------------------------------------------------------
function handleLoadInfoDoneMessage( %msgType, %msgString )
{
   // This will get called after the last description line is sent.
}

