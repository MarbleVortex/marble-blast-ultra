//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------
// This variable is never mentioned in another script so it will never be true. Besides - it always does use other audio channels anyway!
//if (!$AudioChannelTypesDefined)
   //error("Audio channel types are not defined, all sounds will use same channel");

new SFXDescription(AudioGui)
{
   volume   = 1.0;
   isLooping= false;
   is3D     = false;
   type     = $GuiAudioType;
};
new SFXDescription(AudioMessage)
{
   volume   = 1.0;
   isLooping= false;
   is3D     = false;
   type     = $MessageAudioType;
};
new SFXDescription(ClientAudioLooping2D)
{
   volume = 1.0;
   isLooping = true;
   is3D = false;
   type = $EffectAudioType;
};
new SFXProfile(TimeTravelLoopSfx)
{
   filename    = "~/data/sound/TimeTravelActive";
   description = ClientAudioLooping2d;
   preload = true;
};
new SFXProfile(AudioButtonDown)
{
   filename = "~/data/sound/ButtonPress";
   description = "AudioGui";
   preload = true;
};
new SFXProfile(AudioButtonOver)
{
   filename = "~/data/sound/buttonOver";
   description = "AudioGui";
   preload = true;
};

new SFXProfile(OptionsGuiTestSound)
{
   filename = "~/data/sound/buttonOver";
   description = "AudioGui";
   preload = true;
};

new SFXDescription(AudioMusic)
{
   volume   = 1.0;
   isLooping = true;
   isStreaming = true;
   is3D     = false;
   type     = $MusicAudioType;
};

new SFXDescription(TimTranceDescription : AudioMusic)
{
   volume = 0.75;
};

new SFXProfile(TimTranceSfx)
{
   fileName = "~/data/sound/Tim_Trance.ogg";
   description = "TimTranceDescription";
   preload = 0;
};

new SFXDescription(MarbleFantasyDescription : AudioMusic)
{
   volume = 0.75;
};

new SFXProfile(MarbleFantasySfx)
{
   fileName = "~/data/sound/Marble_Fantasy.ogg";
   description = "MarbleFantasyDescription";
   preload = 0;
};

new SFXDescription(AnthonyTranceDescription : AudioMusic)
{
   volume = 0.75;
};

new SFXProfile(AnthonyTranceSfx)
{
   fileName = "~/data/sound/TOMYSSHADOW_Trance.ogg";
   description = "AnthonyTranceDescription";
   preload = 0;
};

new SFXDescription(SkailMBXDescription : AudioMusic)
{
   volume = 0.75;
};

new SFXProfile(SkailMBXSfx)
{
   fileName = "~/data/sound/skail_mbx.ogg";
   description = "SkailMBXDescription";
   preload = 0;
};

DjAnson.addTrack(TimTranceSfx);
DjAnson.addTrack(MarbleFantasySfx);
DjAnson.addTrack(AnthonyTranceSfx);
DjAnson.addTrack(SkailMBXSfx);
