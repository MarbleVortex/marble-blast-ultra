//-----------------------------------------------------------------------------
// Variables used by client scripts & code.  The ones marked with (c)
// are accessed from code.  Variables preceeded by Pref:: are client
// preferences and stored automatically in the ~/client/prefs.cs file
// in between sessions.
//
//    (c) Client::MissionFile             Mission file name
//    ( ) Client::Password                Password for server join
//    (?) Pref::Player::CurrentFOV
//    (?) Pref::Player::DefaultFov
//    ( ) Pref::Input::KeyboardTurnSpeed
//    (c) pref::Master[n]                 List of master servers
//    (c) pref::Net::RegionMask     
//    (c) pref::Client::ServerFavoriteCount
//    (c) pref::Client::ServerFavorite[FavoriteCount]
//    .. Many more prefs... need to finish this off
// Moves, not finished with this either...
//    (c) firstPerson
//    $mv*Action...
//-----------------------------------------------------------------------------
// These are variables used to control the shell scripts and
// can be overriden by mods:
//-----------------------------------------------------------------------------

function initClient()
{
	echo("\n--------- Initializing FPS: Client ---------");
	//exec("./prefs.cs");

	exec("./scripts/xbLive.cs");
   loadPCUserProfile();
	
	$MBServerURL0 = "old.marblevortex.com:80";
	$MBServerURL1 = "old.marblevortex.com:80";
	
	exec("./scripts/tcp.cs");
	//exec("./scripts/master.cs");
	exec("./scripts/points.cs");
	exec("./scripts/previews.cs");
	//portInit($Pref::Server::Port);
    //allowConnections(true);
    
	//$PortForwarded = true;
	//$checkedPorts = true;
	
    //MasterConnection.checkPorts();
    
    //schedule(5000, 0, "allowConnections", false);

	// Make sure this variable reflects the correct state.
	$Server::Dedicated = false;
	// Game information used to query the master server
	$Client::GameTypeQuery = "Marble Game";
	$Client::MissionTypeQuery = "Any";
	// Default level qualification
	if (!$pref::QualifiedLevel["Beginner"])
	$pref::QualifiedLevel["Beginner"] = 1;
	if (!$pref::QualifiedLevel["Intermediate"])
	$pref::QualifiedLevel["Intermediate"] = 1;
	if (!$pref::QualifiedLevel["Advanced"])
	$pref::QualifiedLevel["Advanced"] = 1;
	// The common module provides basic client functionality
	initBaseClient();
	// InitCanvas starts up the graphics system.
	// The canvas needs to be constructed before the gui scripts are
	// run because many of the controls assume the canvas exists at
	// load time.
	initCanvas("Marble Blast");

	// initCanvasSize determines some virtual sizing and widescreen vs. normal
	// parameters.
	initCanvasSize();
	
	if (isPCBuild())
	Canvas.setCursor("DefaultCursor");
	else
	Canvas.hideCursor();

	// Loadup Misc client functions
	exec("./scripts/client.cs");

	/// Load client-side Audio Profiles/Descriptions
	exec("./scripts/audioProfiles.cs");
	// Load up the Game GUIs
	exec("./ui/defaultGameProfiles.cs");
	exec("./ui/loadingGui.gui");
	exec("./ui/missionLoadingGui.gui");
	exec("./ui/presentsGui.gui");
	// Get something on the screen as fast as possible 
	Canvas.setContent(loadingGui);
	Canvas.repaint();
	$EnableDatablockCanvasRepaint = true;
	
	//initRenderInstManager();

	// fire up networkin'
	setNetPort(0);

	// fire up Mr. Telnet Console
	//if (!isRelease())
	// needed!?
	//telnetSetParameters(23, "", "");

	// Resource preloads
	exec("./scripts/resourcePreloads.cs");
	clientPreloadTextures();

	sfxStartup();

	if (isObject(DjAnson))
	{
		DjAnson.activate();
	}
	
	exec("./scripts/shaders.cs");

	// initialize game data - this is done here because materials and shaders may need the gfx object, 
	// which isn't available until the canvas is created
	exec("~/data/init.cs");
	
	// Load chat profiles here (before RootGui)
	//exec("./ui/chathud/guiProfiles.cs");

	// now we keep loadin' er up
	// Load up the shell GUIs
	exec("./ui/RootGui.gui");
	exec("./ui/MainMenuGui.gui");
	exec("./ui/DifficultySelectGui.gui");
	exec("./ui/PlayGui.gui");
	exec("./ui/XMessagePopupDlg.gui");
	exec("./ui/optimatchGui.gui");
	exec("./ui/GamePauseGui.gui");
	exec("./ui/DevMenuGui.gui");
	exec("./ui/ConnErrorGui.gui");
	exec("./ui/MultiPlayerGui.gui");
	exec("./ui/FindGameGui.gui");
	exec("./ui/CreateGameGui.gui");
	exec("./ui/LobbyGui.gui");
	exec("./ui/LobbyPopupDlg.gui");
	exec("./ui/lbMenuGui.gui");
	exec("./ui/LevelPreviewGui.gui");
	exec("./ui/LevelScoresGui.gui");
	exec("./ui/ControlerUnplugGui.gui");
	exec("./ui/UpsellGui.gui");
	exec("./ui/PlayerListGui.gui");
	exec("./ui/HelpAndOptionsGui.gui");
	exec("./ui/ESRBGui.gui");
	exec("./ui/inputAndSoundOptionsGui.gui");
	exec("./ui/marblePickerGui.gui");
	exec("./ui/aboutMenuOptionsGui.gui");
	exec("./ui/helpGui.gui");
	exec("./ui/demoTimerGui.gui");
	exec("./ui/GameEndGui.gui");
	exec("./ui/StartupErrorGui.gui");
	exec("./ui/controlerDisplayGui.gui");
	exec("./ui/joinipgui.gui");
	exec("./ui/videoOptionsGui.gui");
	exec("./ui/languagePickerGui.gui");
	exec("./ui/changeKeyGui.gui");
	exec("./ui/GameCompleteGui.gui");
	exec("./ui/MusicDlg.gui");
	exec("./ui/multiPlayerDlg.gui");
	exec("./ui/remapDlg.gui");
	exec("./ui/remapListGui.gui");

	// Client scripts
	exec("./scripts/missionDownload.cs");
	exec("./scripts/serverConnection.cs");
	exec("./scripts/chatHud.cs");
	exec("./scripts/playGui.cs");
	exec("./scripts/centerPrint.cs");
	exec("./scripts/game.cs");
	exec("./scripts/fireworks.cs");
	// This is being exec'd twice
	//exec("./scripts/version.cs");
	exec("./scripts/user.cs");
	exec("./scripts/playerList.cs");
	exec("./scripts/recordings.cs");
	
	// MOVED! To after GameMissionInfo where it can count the number of levels itself.
	exec("./scripts/achievements.cs");
	exec("./ui/AchievementListGui.gui");
	exec("./ui/gameSelectGui.gui");
	exec("./ui/customSelectGui.gui");
	exec("./ui/customDifficultySelectGui.gui");
	exec("./ui/multiplayerSelectGui.gui");

	//exec("./scripts/xbLive.cs");

	// Default player key bindings
	exec("./scripts/default.bind.cs");
	// load key bindings with the prefs, if it exists
	if (isFile("./scripts/config.cs") || isFile("./scripts/config.cs.dso"))
    exec( "./scripts/config.cs" );
	exec("./scripts/xbControler.cs");
	//exec("./scripts/master.cs");
	exec("./ui/chathud/init.cs"); // load chat
	exec("./scripts/audio.cs");
	
	exec("./scripts/version.cs");
	
	exec("./scripts/leaderboards.cs");
	   exec("./scripts/precip.cs");

	refreshAudio();

	// Copy saved script prefs into C++ code.
	setShadowDetailLevel( $pref::shadows );
	setDefaultFov( $pref::Player::defaultFov );
	setZoomSpeed( $pref::Player::zoomSpeed );
	
	$enableDirectInput = "1";
	activateDirectInput();
	enableGamepad();

	%missionFile = "";

	if($missionArg !$= "") 
	{
		%missionFile = findNamedFile($missionArg, ".mis");
	}
	else if($interiorArg !$= "") {
		%file = findNamedFile($interiorArg, ".dif");
		if(%file !$= "")
		{
			// hack - need the server scripts now
			execServerScripts();
			
			%missionGroup = createEmptyMission($interiorArg);
			%interior = new InteriorInstance() {
				position = "0 0 0";
				rotation = "1 0 0 0";
				scale = "1 1 1";
				interiorFile = %file;
			};
			%missionGroup.add(%interior);
			%interior.magicButton();

			if (!isObject(SpawnPoints))
			%spawnPointsGroup = new SimGroup(SpawnPoints);
			MissionGroup.add(%spawnPointsGroup);

			if (!isObject(StartPoint))
			{
				// create spawn points group
				new StaticShape(StartPoint) {
					position = "0 -5 25";
					rotation = "1 0 0 0";
					scale = "1 1 1";
					dataBlock = "StartPad";
				};
			}
			%spawnPointsGroup.add(StartPoint);

			if(!isObject(EndPoint))
			{
				%pt = new StaticShape(EndPoint) {
					position = "0 5 100";
					rotation = "1 0 0 0";
					scale = "1 1 1";
					dataBlock = "EndPad";
				};
				MissionGroup.add(%pt);
			}
			%box = %interior.getWorldBox();
			%mx = getWord(%box, 0);
			%my = getWord(%box, 1);
			%mz = getWord(%box, 2);
			%MMx = getWord(%box, 3);
			%MMy = getWord(%box, 4);
			%MMz = getWord(%box, 5);
			%pos = (%mx - 3) SPC (%MMy + 3) SPC (%mz - 3);
			%scale = (%MMx - %mx + 6) SPC (%MMy - %my + 6) SPC (%MMz - %mz + 20);
			echo(%box);
			echo(%pos);
			echo(%scale);
			new Trigger(Bounds) {
				position = %pos;
				scale = %scale;
				rotation = "1 0 0 0";
				dataBlock = "InBoundsTrigger";
				polyhedron = "0.0000000 0.0000000 0.0000000 1.0000000 0.0000000 0.0000000 0.0000000 -1.0000000 0.0000000 0.0000000 0.0000000 1.0000000";
			};
			MissionGroup.add(Bounds);

			%missionFile = "marble/data/missions/testMission.mis";
			%missionGroup.save(%missionFile);
			%missionGroup.delete();
		}
	}

	// Start up the main menu... this is separated out into a 
	// method for easier mod override.
	schedule(0,0,loadMainMenu,%missionFile);
}

function reloadAllGuis()
{
	// fix for multiplayer dropping the points. Has to be done before everything else for some reason, even though one would think it would only need to be done before lobby itself.
	LobbyGuiPlayerList.loopThrough();
	PlayerListGuiList.loopThrough();
	
	exec("./ui/defaultGameProfiles.cs");
	loadingGui.delete();
	exec("./ui/loadingGui.gui");
	MissionLoadingGui.delete();
	exec("./ui/missionLoadingGui.gui");
	presentsGui.delete();
	exec("./ui/presentsGui.gui");
	
	RootGui.delete();
	exec("./ui/RootGui.gui");
	exec("./ui/chathud/init.cs"); // re-load chat
	//RootGui.AddSubDialog(newChatHud);
	mainMenuGui.delete();
	exec("./ui/MainMenuGui.gui");
	DifficultySelectGui.delete();
	exec("./ui/DifficultySelectGui.gui");
	PlayGui.delete();
	exec("./ui/PlayGui.gui");
	//RootGui.AddSubDialog(newChatHud);
	XMessagePopupDlg.delete();
	exec("./ui/XMessagePopupDlg.gui");
	optimatchGui.delete();
	exec("./ui/optimatchGui.gui");
	GamePauseGui.delete();
	exec("./ui/GamePauseGui.gui");
	DevMenuGui.delete();
	exec("./ui/DevMenuGui.gui");
	ConnErrorGui.delete();
	exec("./ui/ConnErrorGui.gui");
	MultiPlayerGui.delete();
	exec("./ui/MultiPlayerGui.gui");
	FindGameGui.delete();
	exec("./ui/FindGameGui.gui");
	CreateGameGui.delete();
	exec("./ui/CreateGameGui.gui");
	
	LobbyGui.delete();
	exec("./ui/LobbyGui.gui");
	LobbyPopupDlg.delete();
	exec("./ui/LobbyPopupDlg.gui");
	lbMenuGui.delete();
	exec("./ui/lbMenuGui.gui");
	levelPreviewGui.delete();
	exec("./ui/LevelPreviewGui.gui");
	LevelScoresGui.delete();
	exec("./ui/LevelScoresGui.gui");
	ControlerUnplugGui.delete();
	exec("./ui/ControlerUnplugGui.gui");
	UpsellGui.delete();
	exec("./ui/UpsellGui.gui");
	PlayerListGui.delete();
	exec("./ui/PlayerListGui.gui");
	HelpAndOptionsGui.delete();
	exec("./ui/HelpAndOptionsGui.gui");
	ESRBGui.delete();
	exec("./ui/ESRBGui.gui");
	inputAndSoundOptionsGui.delete();
	exec("./ui/inputAndSoundOptionsGui.gui");
	marblePickerGui.delete();
	exec("./ui/marblePickerGui.gui");
	aboutMenuOptionsGui.delete();
	exec("./ui/aboutMenuOptionsGui.gui");
	helpGui.delete();
	exec("./ui/helpGui.gui");
	demoTimerGui.delete();
	exec("./ui/demoTimerGui.gui");
	GameEndGui.delete();
	exec("./ui/GameEndGui.gui");
	StartupErrorGui.delete();
	exec("./ui/StartupErrorGui.gui");
	controlerDisplayGui.delete();
	exec("./ui/controlerDisplayGui.gui");
	joinipgui.delete();
	exec("./ui/joinipgui.gui");
	videoOptionsGui.delete();
	exec("./ui/videoOptionsGui.gui");
	languagePickerGui.delete();
	exec("./ui/languagePickerGui.gui");
	changeKeyGui.delete();
	exec("./ui/changeKeyGui.gui");
	GameCompleteGui.delete();
	exec("./ui/GameCompleteGui.gui");
	MusicDlg.delete();
	exec("./ui/MusicDlg.gui");
	multiPlayerDlg.delete();
	exec("./ui/multiPlayerDlg.gui");
	
	AchievementListGui.delete();
	exec("./ui/AchievementListGui.gui");
	gameSelectGui.delete();
	exec("./ui/gameSelectGui.gui");
	customSelectGui.delete();
	exec("./ui/customSelectGui.gui");
	customDifficultySelectGui.delete();
	exec("./ui/customDifficultySelectGui.gui");
	multiplayerSelectGui.delete();
	exec("./ui/multiplayerSelectGui.gui");
	if (MissionInfo.gameMode $= "Scrum") {
	   PlayGui.gameDuration = GameMissionInfo.getCurrentMission().time;
	}
}

function hackUpInteriors()
{
   for( %file = findFirstFile( "*/*.dif" ); %file !$= ""; %file = findNextFile( "*/*.dif" ))
   {
      echo( %file );
      hackUpInterior(%file);
   }
}

function initCanvasSize()
{
	// note: the following assumes resolution already set.  In current
	// calling order, that is fine even though we haven't called initCanvas
	// yet because canvas is now created well before initCanvas.
	%width = getWord(getResolution(), 0);
	%height = getWord(getResolution(), 1);

	// add root center control now and figure out how to size it
	if (%width < 1280 || %height < 720)
	{
		%w = 640;
		%h = 480;
		$Shell::Widescreen = false;
	}
	else
	{
		%w = 1280;
		%h = 720;
		$Shell::Widescreen = true;
	}
	%offsetX = (%width-%w)/2;
	%offsetY = (%height-%h)/2;
	$Shell::Resolution = %w SPC %h;
	$Shell::Offset = %offsetX SPC %offsetY;
	$Shell::VirtualResolution = %w SPC %h;
	$PlayGui::safeVerMargin = 1 + (%height * 0.15)/2;
	$PlayGui::safeHorMargin  = 1 + (%width * 0.15)/2;
}

function makeFonts()
{
	populateFontCacheRange("Arial", 12, 1, 255);
	populateFontCacheRange("Arial", 13, 1, 255);
	populateFontCacheRange("Arial", 14, 1, 255);
	populateFontCacheRange("Arial", 16, 1, 255);
	populateFontCacheRange("Arial", 32, 1, 255);
	populateFontCacheRange("Arial Bold", 18, 1, 255);
	populateFontCacheRange("Arial Bold", 20, 1, 255);
	populateFontCacheRange("Arial Bold", 22, 1, 255);
	populateFontCacheRange("Arial Bold", 24, 1, 255);
	populateFontCacheRange("Arial Bold", 30, 1, 255);
	populateFontCacheRange("Arial Bold", 32, 1, 255);
	populateFontCacheRange("Arial Bold", 36, 1, 255);
	populateFontCacheRange("Lucida Console", 12, 1, 255);
	populateFontCacheRange("ColiseumRR Medium", 48, 1, 255);
	writeFontCache();
}

function findNamedFile(%file, %ext)
{
	if(fileExt(%file) !$= %ext)
	%file = %file @ %ext;
	%found = findFirstFile(%file);
	if(%found $= "")
	%found = findFirstFile("*/" @ %file);
	return %found;
}

//-----------------------------------------------------------------------------
function loadMainMenu(%missionFile)
{
	waitForPreviewLevel(); 
	
	
   Canvas.setCursor("DefaultCursor");
}

function waitForPreviewLevel()
{
	$EnableDatablockCanvasRepaint = false;

	RootGui.show();
	
	if ($JoinGameAddress !$= "")
	{
      glogo.visible = 0;
      RootGui.setContent(MissionLoadingGui);
      connectToServer($JoinGameAddress);
      DjAnson.play();
      refreshAudio();
      fadeInAudio();
	} else {
	   // weird way to make the OK button work - Anthony
      RootGui.setContent(ESRBGui);
      RootGui.setContent(ESRBGui);
      //GameMissionInfo.setFilterDifficulty("intermediate");
      GameMissionInfo.setFilterGame("Ultra");
      GameMissionInfo.selectMission(21);//38);
      loadPreviewMission();
	}

	%errors = "";

	//if ($ScriptError !$= "")
	//{
		//%errors = %errors @ "Script Errors:" NL $ScriptErrorContext NL "";
		////XMessagePopupDlg.show(0, $ScriptErrorContext, $Text::OK);
		//error($ScriptError);
		//error($ScriptErrorContext);
	//}
	if ($LocaleErrors !$= "")
	{
		%errors = %errors @ "Localization File Errors:" NL $LocaleErrors NL "";
		//XMessagePopupDlg.show(0, "Errors in locale file detected", $Text::OK);
		error($LocaleErrors);
	}
	//if (GameMissionInfo.dupErrors !$= "")
	//{
		//%errors = %errors @ "Duplicate Mission Errors:" NL GameMissionInfo.dupErrors NL "";
		////XMessagePopupDlg.show(0, "Duplicate Mission Ids detected", $Text::OK);
		//%group = SinglePlayMissionGroup;
		//for (%i = 0; %i < %group.getCount(); %i++)
		//if (GameMissionInfo.duplevelIds[%group.getObject(%i).level])
		//error("LEVEL ID DUP:" SPC %group.getObject(%i).file SPC %group.getObject(%i).level);
		//else
		//echo(%group.getObject(%i).file SPC %group.getObject(%i).level);
		//
		//%group = MultiPlayMissionGroup;
		//for (%i = 0; %i < %group.getCount(); %i++)
		//if (GameMissionInfo.duplevelIds[%group.getObject(%i).level])
		//error("LEVEL ID DUP:" SPC %group.getObject(%i).file SPC %group.getObject(%i).level);
		//else
		//echo(%group.getObject(%i).file SPC %group.getObject(%i).level);
	//}
	if ($dupLocGlobals !$= "")
	{
		%errors = %errors @ "Duplicate Localization Variables:" NL $dupLocGlobals NL "";
		//XMessagePopupDlg.show(0, "Duplicate Localization variable detected (press ~ for details)", $Text::OK);
		error($dupLocGlobals);
	}

	if (%errors !$= "")
	{
		RootGui.setContent(StartupErrorGui, %errors);
		XMessagePopupDlg.show(0, "Errors detected on startup, press A to view errors", $Text::OK);
	}

	// set global indicating that the game is loaded
	$Client::GameLoaded = true;
}

// adding this function from the MBO Official scripts
// and uncommenting the line in serverConnection.cs that calls it
// (it's near the beginning of the file)
// allows marble trail sparkles to work!
// and as a side effect, landmine explosions too - Anthony
function recycleClientMissionCleanup()
{
   if (isObject(ClientMissionCleanup))
      ClientMissionCleanup.delete();
   new SimGroup(ClientMissionCleanup);
   RootGroup.add(ClientMissionCleanup);
}

// connect to a server.  if address is empty a local connect is assumed
function connectToServer(%address,%invited)
{
	if (isObject(ServerConnection))
	ServerConnection.delete();
	
	// clear the scores gui here so that our the client id from the preview server doesn't
	// show up in the scores list.
	PlayerListGui.clear();
	// reset client Id since we are connecting to a different server
	$Player::ClientId = 0;

	%conn = new GameConnection(ServerConnection);
	RootGroup.add(ServerConnection);

	%xbLiveVoice = 0; // XBLiveGetVoiceStatus();

	if ($UserAchievements::GamerScore $= "")
	$UserAchievements::GamerScore = 0;

	%score = $UserAchievements::GamerScore;

	// we expect $Player:: variables to be properly populated at this point   
	//%isDemoLaunch = isDemoLaunch();
	
   //error("Name is: " @ $Player::Name);
   // the fifth argument is whether or not we're a demo - but we're on the free PC version
	%conn.setConnectArgs(getUserName(), $Player::XBLiveId, %xbLiveVoice, %invited, false, %score, $VersionCode);
	%conn.setJoinPassword($Client::Password); 

	if (%address $= "")
	{
		// local connections are not considered to be multiplayer connections initially.  
		// they become multiplayer connections when multiplayer mode is started
		$Client::connectedMultiplayer = false;
		%conn.connectLocal();
		setNetPort(0);
	}
	else
	{
		// connecting as a client to remote server
		// need to open up the port so that we can get voice data
		if (!isPCBuild())
		{
			setNetPort(0);
			//portInit($Pref::Server::Port);
		}
		$Client::connectedMultiplayer = true;
		%conn.connect(%address);
	}
}

function connectToPreviewServer()
{
	connectToServer("");
}

function setPreviewCamera()
{
	CameraObj.setTransform(LocalClientConnection.camera.getTransform());
	EWorldEditor.isDirty = true;
	
	echo("Preview Camera has been moved!");
}

// connect to a server.  if address is empty a local connect is assumed
function establishConnection(%address, %mp, %invited)
{
	if (isObject(ServerConnection))
	ServerConnection.delete();
	
	// clear the scores gui here so that our the client id from the preview server doesn't
	// show up in the scores list.
	PlayerListGui.clear();
	// reset client Id since we are connecting to a different server
	$Player::ClientId = 0;

	%conn = new GameConnection(ServerConnection);
	RootGroup.add(ServerConnection);

	%xbLiveVoice = 0; // XBLiveGetVoiceStatus();

	if ($UserAchievements::GamerScore $= "")
	$UserAchievements::GamerScore = 0;

	%score = $UserAchievements::GamerScore;

	// we expect $Player:: variables to be properly populated at this point   
	//%isDemoLaunch = isDemoLaunch();
	// we're never a demo, fifth argument never true!
	%conn.setConnectArgs(getUserName(), $Player::XBLiveId, %xbLiveVoice, %invited, false, %score, $VersionCode);
	%conn.setJoinPassword($Client::Password);

	if (%address $= "")
	{
		if (%mp)
		$Client::connectedMultiplayer = true;
		else
		$Client::connectedMultiplayer = false;
		%conn.connectLocal();
		setNetPort(0);
	}
	else
	{
		// connecting as a client to remote server
		// need to open up the port so that we can get voice data
		if (!isPCBuild())
		{
			setNetPort(0);
			//portInit($Pref::Server::Port);
		}
		$Client::connectedMultiplayer = true;
		%conn.connect(%address);
	}
}

function populatePreviewMission()
{
	%start = getRealTime();

	$instantGroup = MegaMissionGroup;

	for (%i = 0; %i < SinglePlayMissionGroup.getCount(); %i++)
	{
		%info = SinglePlayMissionGroup.getObject(%i);      
		%mission = fileName(%info.file);

		// First the InteriorInstance's
		%info.missionGroup = loadObjectsFromMission(%mission);

		// Then any camera markers
		loadObjectsFromMission(%mission, "SpawnSphere", "CameraSpawnSphereMarker");

		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_3shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_6shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_9shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_12shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_15shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_18shape");
	}

	for (%i = 0; %i < MultiPlayMissionGroup.getCount(); %i++)
	{
		%info = MultiPlayMissionGroup.getObject(%i);      
		%mission = fileName(%info.file);

		// First the InteriorInstance's
		%info.missionGroup = loadObjectsFromMission(%mission);
		
		// Then any camera markers
		loadObjectsFromMission(%mission, "SpawnSphere", "CameraSpawnSphereMarker");

		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_3shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_6shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_9shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_12shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_15shape");
		
		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_18shape");
	}

	for (%i = 0; %i < SpecialMissionGroup.getCount(); %i++)
	{
		%info = SpecialMissionGroup.getObject(%i);      
		%mission = fileName(%info.file);
		
		// First the InteriorInstance's
		%info.missionGroup = loadObjectsFromMission(%mission);
		
		// Then any camera markers
		loadObjectsFromMission(%mission, "SpawnSphere", "CameraSpawnSphereMarker");

		// Then the SpawnSphere's
		loadObjectsFromMission(%mission, "SpawnSphere", "SpawnSphereMarker");

		// Then the StartPad's
		loadObjectsFromMission(%mission, "StaticShape", "StartPad");

		// Then the glass
		loadObjectsFromMission(%mission, "StaticShape", "glass_3shape");

		// Then the other glass
		loadObjectsFromMission(%mission, "TSStatic");
	}

	%diff = getRealTime() - %start;
	error("Took " @ %diff / 1000 @ " seconds to populate the preview mission");
}

function getCameraObject(%missionGroup)
{
	%spawnObj = 0;

	// Attempt to find a SpawnSphere
	%obj = 0;
	%found = false;
	for (%i = 0; %i < %missionGroup.getCount(); %i++)
	{
		%obj = %missionGroup.getObject(%i);

		if (%obj.getClassName() $= "SpawnSphere")
		{
			%found = true;
			break;
		}
		else if (%obj.getClassName() $= "StaticShape")
		{
			if (%obj.getDataBlock().getName() $= "StartPad")
			{
				%found = true;
				break;
			}
		}
	}

	if (%found)
	%spawnObj = %obj;
	else
	{
		error("Was unable to find a camera position in " @ %missionGroup);

		// See if there is one in SpawnPoints
		%group = nameToID("MissionGroup/SpawnPoints");

		if (%group != -1)
		{
			if (%group.getCount() > 0)
			%spawnObj = %group.getObject(0);
		}
	}

	return %spawnObj;
}

function setupCameras()
{
	for (%i = 0; %i < SinglePlayMissionGroup.getCount(); %i++)
	{
		%info = SinglePlayMissionGroup.getObject(%i);
		%info.cameraPoint = getCameraObject(%info.missionGroup);
		%info.cameraPos = getSpawnPosition(%info.cameraPoint);
	}

	for (%i = 0; %i < MultiPlayMissionGroup.getCount(); %i++)
	{
		%info = MultiPlayMissionGroup.getObject(%i);
		%info.cameraPoint = getCameraObject(%info.missionGroup);
		%info.cameraPos = getSpawnPosition(%info.cameraPoint);
	}

	for (%i = 0; %i < SpecialMissionGroup.getCount(); %i++)
	{
		%info = SpecialMissionGroup.getObject(%i);
		%info.cameraPoint = getCameraObject(%info.missionGroup);
		%info.cameraPos = getSpawnPosition(%info.cameraPoint);
	}
}

function createPreviewServer(%mission)
{
	%serverType = "SinglePlayer";
	if (%mission $= "")
	%mission = $pref::Client::AutoStartMission;

	//else
	//createServer(%serverType, %mission);

	//connectToPreviewServer();

	GameMissionInfo.setMode(GameMissionInfo.SPMode);
	GameMissionInfo.setCurrentMission(%mission);
}

function createEmptyMission(%interiorArg)
{
	return new SimGroup(MissionGroup) {
		new ScriptObject(MissionInfo) {
			level = "001";
			desc = "A preview mission";
			time = "60000";
			include = "1";
			difficulty = "4";
			name = "Preview Mission";
			type = "custom";
			gameType = "SinglePlayer";
		};
		new MissionArea(MissionArea) {
			area = "-360 -648 720 1296";
			flightCeiling = "300";
			flightCeilingRange = "20";
			locked = "true";
		};
		new Sky(Sky) {
			position = "336 136 0";
			rotation = "1 0 0 0";
			scale = "1 1 1";
			hidden = "0";
			materialList = "~/data/skies/sky_intermediate.dml";
			cloudHeightPer[0] = "0";
			cloudHeightPer[1] = "0";
			cloudHeightPer[2] = "0";
			cloudSpeed1 = "0.0001";
			cloudSpeed2 = "0.0002";
			cloudSpeed3 = "0.0003";
			visibleDistance = "1500";
			fogDistance = "1000";
			fogColor = "0.600000 0.600000 0.600000 1.000000";
			fogStorm1 = "0";
			fogStorm2 = "0";
			fogStorm3 = "0";
			fogVolume1 = "-1 1.84699e+025 3670.77";
			fogVolume2 = "-1 7.22507e+028 5.10655e+028";
			fogVolume3 = "-1 1.01124e+012 1.69273e+022";
			windVelocity = "1 0 0";
			windEffectPrecipitation = "0";
			SkySolidColor = "0.600000 0.600000 0.600000 1.000000";
			useSkyTextures = "1";
			renderBottomTexture = "1";
			noRenderBans = "1";
			renderBanOffsetHeight = "50";
			skyGlow = "0";
			skyGlowColor = "-0.026242 -0.031545 0.999158 0.887938";
			fogVolumeColor3 = "128.000000 128.000000 128.000000 1109264997360148200000000000.000000";
			fogVolumeColor1 = "128.000000 128.000000 128.000000 274952335980479310000000000.000000";
			fogVolumeColor2 = "128.000000 128.000000 128.000000 11918.598633";
		};

		new StaticShape(Cloud_Shape) {      
			position = "0 0 0";      
			rotation = "1 0 0 0";      
			scale = "1 1 1";      
			hidden = "0";      
			dataBlock = "astrolabeCloudsIntermediateShape";  
		};

		new Sun() {
			direction = "0.573201 0.275357 -0.771764";
			color = "1.080000 1.030000 0.900000 1.000000";
			ambient = "0.400000 0.400000 0.500000 1.000000";
			shadowColor = "0.000000 0.000000 0.150000 0.350000";
		};
		new StaticShape() {
			canSaveDynamicFields = "1";
			position = "0 0 -500";
			rotation = "1 0 0 0";
			scale = "1 1 1";
			hidden = "0";
			dataBlock = "astrolabeShape";
		};
	};
}

