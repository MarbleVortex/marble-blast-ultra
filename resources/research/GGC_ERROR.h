enum GGC_ERROR
{
  GGC_SUCCESS = 0,
  GGC_ERROR_Timeout = 10,
  GGC_ERROR_LibNotInitialized = 101,
  GGC_ERROR_LibAlreadyInitialized = 102,
  GGC_ERROR_NetworkInit = 103,
  GGC_ERROR_InvalidParam = 104,
  GGC_ERROR_InvalidQueryID = 108,
  GGC_ERROR_Communication = 111,
  GGC_ERROR_CommunicationFatal = 112,
  GGC_ERROR_Memory = 113,
  GGC_ERROR_NetworkDown = 500,
  GGC_ERROR_ConnectionFailed = 501,
  GGC_ERROR_DiskError = 502,
  GGC_ERROR_ObjectNotFound = 503,
  GGC_ERROR_CommandNotFound = 504,
  GGC_ERROR_InvalidArguments = 505,
  GGC_ERROR_Aborted = 506,
  GGC_ERROR_Max = 1004
};
