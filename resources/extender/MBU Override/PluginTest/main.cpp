#include <TGE.h>
#include <TorqueLib/QuickOverride.h>
#include <PluginLoader/PluginInterface.h>
#include "testClass.h"

#include <Windows.h>

/*TorqueOverride(void, Con::debugPrint, (int* a1, char* fmt, ...), originalDebugPrint)
{
	__asm
	{
		call originalDebugPrint
	}
	//originalDebugPrint(out, "bla");
	va_list va;
	va_start(va, fmt);

	//Con::_printf(TGE::ConsoleLogEntry::Level::Error, fmt, va);
	char buf[2048];
	vsnprintf_s(buf, 2048, fmt, va);

	//out = buf;

	Con::errorf("DEBUG: %s", buf);
}*/

/*TorqueOverrideMember(int, GGCUnknown::makeWindowGGC, (TGE::GGCUnknown* pThis), originalMakeWindowGGC)
{
	originalMakeWindowGGC(pThis);
	return 0;
}*/

/*ConsoleFunction(cool, void, 1, 1, "cool()")
{
	TGE::Con::printf("Cool Text!");
	TGE::Con::warnf("Cool Warning!");
	TGE::Con::errorf("Cool Error!");
}*/

/*ConsoleFunction(loadZip, void, 2, 2, "loadZip(zipName)")
{
	if (argc > 1)
	{
		const char* path = argv[1];

		TGE::Con::printf("Loading Zip: %s", path);
	} else {
		TGE::Con::errorf("Not enough arguments!");
	}
}*/

/*TorqueOverrideMember(void, Marble::MarblePhysics, (TGE::Marble *thisObj, const void *move, U32 delta), originalMarblePhysics)
{
	originalMarblePhysics(thisObj, move, delta);
}*/

/*VOID HookVTable(PVOID classPtr)
{
		DWORD* pdwVTable = (DWORD*)*(DWORD*)classPtr; // Aquire the VTable
		if (pdwVTable == NULL) return; // How does this happen?
		// We have to make it writable! By default this isn't writable in most (all?) compilers.
		MEMORY_BASIC_INFORMATION mbi;
		VirtualQuery((LPCVOID)pdwVTable, &mbi, sizeof(mbi));
		VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_READWRITE, &mbi.Protect);
		pdwVTable[0] = (DWORD)hook_one; // Hook!
		VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);
		printf("Hooked!\n");
}*/

/*void _msg(const char* fmt, va_list argPtr)
{
	char buffer[4096];
	vsprintf_s(buffer, 4096, fmt, argPtr);

	MessageBox(NULL, buffer, "DEBUG MSG", MB_OK | MB_ICONWARNING);
}

void msg(const char* fmt, ...)
{
	va_list argPtr;
	va_start(argPtr, fmt);
	_msg(fmt, argPtr);
	va_end(argPtr);
}*/

/*ConsoleFunction(createTestClass, void, 2, 2, "createConsole(name);")
{
	// fails on GuiConsole.isAwake();
	//void *console = new void*[4096];
	//Members::GuiConsole::_new((TestClass *)console);

	TestClass* obj = new TestClass();

	if (!Members::SimObject::registerObject(obj))
	{
		Con::errorf("Unable to create TestClass!");
		return;
	}

	Members::SimObject::assignName(obj, argv[1]);
}*/



/*TorqueOverride(void*, dynCast, (Void* a1, Void* a2, Void *a3, Void *a4, Void* a5), originalDynCast)
{
	return dynamic_cast<void*>(a1);
}*/

/*int ggcCallback(int a1, int a2, int a3)
{
	Con::printf("Received GGC Callback: %d, %d, %d", a1, a2, a3);

	return 0;
}

HWND parentWindow;

LRESULT WINAPI WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

void makeBaseWindow()
{
	const char* windowClassName = "GGConnectHWND";
	HWND hWnd;

	WNDCLASSEX wndClass;
	HMODULE appInstance = GetModuleHandle(NULL);
	HICON appIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpszClassName = windowClassName;
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WindowProc;
	wndClass.hInstance = appInstance;
	wndClass.hIcon = appIcon;
	wndClass.hIconSm = appIcon;
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndClass.lpszMenuName = NULL;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	if (!RegisterClassEx(&wndClass))
	{
		MessageBox(NULL, "Failed to register window class!", "Error!", MB_OK | MB_ICONERROR);
		PostQuitMessage(0);
	}

	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD dwStyle = WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	dwStyle |= WS_OVERLAPPEDWINDOW | WS_THICKFRAME | WS_CAPTION;

	hWnd = CreateWindowEx(dwExStyle, windowClassName, "GGConnect Window", dwStyle, CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720, NULL, NULL, appInstance, NULL);

	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);

	parentWindow = hWnd;
}*/

/*TorqueOverride(HWND, GGConnect::getParentHWND, (), originalGetParentHWND)
{
	return parentWindow;
}

void writeBytes(HANDLE pipe, char* buf)
{
	WriteFile(pipe, buf, sizeof(buf), 0, NULL);
}

const char* pipeFile;

void createGGConnectPipe()
{
	char temp[MAX_PATH];
	GetTempPath(MAX_PATH, temp);

	std::string str = (std::string(temp) + "\\GarageGames\\IAPlayer\\pipe\\");
	pipeFile = (const char*)malloc(1024);
	const char* src = str.c_str();
	strcpy((char*)pipeFile, src);

	char** pipeRef1 = reinterpret_cast<char**>(TGECODE_GGCPIPEREF1);
	char** pipeRef2 = reinterpret_cast<char**>(TGECODE_GGCPIPEREF2);
	*pipeRef1 = (char*)pipeFile;
	*pipeRef2 = (char*)pipeFile;
	//*ggcPipe = *"./pipe/";

	CreateDirectory((std::string(temp) + "\\GarageGames").c_str(), 0);
	CreateDirectory((std::string(temp) + "\\GarageGames\\IAPlayer").c_str(), 0);
	CreateDirectory((std::string(temp) + "\\GarageGames\\IAPlayer\\pipe\\").c_str(), 0);

	_SYSTEM_INFO SystemInfo;
	GetSystemInfo(&SystemInfo);
	DWORD pageSize = SystemInfo.dwPageSize;
	std::string ss = (std::string(pipeFile) + "IAPlayer.100.0");
	const char* path = ss.c_str();
	//const char* path = "\\\\.\\pipe\\IAPlayer.100.0";

	HANDLE pipe = CreateFile(path, GENERIC_WRITE, FILE_SHARE_WRITE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	//HANDLE pipe = CreateNamedPipe(path, 3 | 0x40000000, 6, 255, pageSize, pageSize, 0, 0);

	if (pipe == INVALID_HANDLE_VALUE)
	{
		int err = GetLastError();
		Con::errorf("ERROR: %d", err);
		return;
	}

	//writeBytes(pipe, new char[12]);
	writeBytes(pipe, new char[1]);
	//writeBytes(pipe, new char[12]);
	//writeBytes(pipe, new char[12]);
	//writeBytes(pipe, new char[12]);

	FlushFileBuffers(pipe);

	CloseHandle(pipe);
}*/

//GGConnect::GGCStruct GGConnectStructure;

PLUGINCALLBACK void preEngineInit(PluginInterface* plugin)
{
	//makeBaseWindow();

	//--createGGConnectPipe();

	//GGConnectStructure = GGConnect::GGCStruct();

	//GGConnect::GGCInstance = GGConnectStructure;

	//GGConnect::SetGGConnectState1(&ggcCallback, 0);
	//GGConnect::SetGGConnectState2();
	//GGConnect::GGCInitialized = true;

	//TGE::SimObject_VTABLE* customTable = new TGE::SimObject_VTABLE();
	//#ifdef MBU_DEV
	/*customTable.sub_4BD415 = 0x4BD415;
	customTable.sub_4BD0E1 = 0x4BD0E1;
	customTable.sub_4BD078 = 0x4BD078;
	customTable.sub_4BB4D2 = 0x4BB4D2;
	customTable.sub_4BAF76 = 0x4BAF76;
	customTable.sub_4BAD11 = 0x4BAD11;
	customTable.sub_4BA95D = 0x4BA95D;
	customTable.sub_4B9BF8 = 0x4B9BF8;
	customTable.sub_4B98D0 = 0x4B98D0;
	customTable.sub_4B9647 = 0x4B9647;
	customTable.sub_4B95FD = 0x4B95FD;
	customTable.sub_4B9141 = 0x4B9141;
	customTable.sub_4B907A = 0x4B907A;
	customTable.sub_4B904D = 0x4B904D;
	customTable.sub_401C42 = 0x401C4D;
	customTable.nul_4B9B0D = 0x4B9B0D;
	customTable.nul_4B9B0C = 0x4B9B0C;
	customTable.nul_4B98CD = 0x4B98CD;
	customTable.nul_4B98CA = 0x4B98CA;
	customTable.nul_4B98C7 = 0x4B98C7;
	customTable.nul_4B98C6 = 0x4B98C6;
	customTable.nul_4B98C5 = 0x4B98C5;
	customTable.nul_401C47 = 0x401C47;
	customTable.nul_401C3F = 0x401C3F;
	customTable.nul_401C3E = 0x401C3E;
	customTable.nul_401C3D = 0x401C3D;
	customTable.ano_4BD6B5 = 0x4BD6B5;*/

	/*customTable->sub_4B9BF8 = 4955128;
	customTable->sub_4BAD11 = 4959505;
	customTable->sub_4B9141 = 4952385;
	customTable->sub_4B95FD = 4953597;
	customTable->sub_4B9647 = 4953671;
	customTable->sub_4B98D0 = 4954320;
	customTable->sub_4BAF76 = 4960118;
	customTable->sub_4BA95D = 4958557;
	customTable->nul_4B98C5 = 4954309;
	customTable->nul_4B98C6 = 4954310;
	customTable->nul_4B98CA = 4954314;
	customTable->nul_4B98CD = 4954317;
	customTable->nul_4B9B0C = 4954892;
	customTable->nul_4B9B0D = 4954893;
	customTable->nul_4B98C7 = 4954311;
	customTable->nul_401C3D = 4201533;
	customTable->nul_401C3E = 4201534;
	customTable->ano_4BD6B5 = 4970165;
	customTable->sub_4B904D = 4952141;
	customTable->sub_4B907A = 4952186;
	customTable->sub_4BB4D2 = 4961490;
	customTable->sub_4BD415 = 4969493;
	customTable->sub_4BD078 = 4968568;
	customTable->sub_4BD0E1 = 4968673;
	customTable->nul_401C3F = 4201535;
	customTable->sub_401C42 = 4201538;
	customTable->nul_401C47 = 4201543;
	#endif*/

	//TGE::SimObject_VTABLE *customTable;
	//customTable = reinterpret_cast<TGE::SimObject_VTABLE *>(TGEOFF_SIMOBJECT_VTABLE);
	//FIXVTABLE(TestClass, TGEOFF_SIMOBJECT_VTABLE);
	//static_cast<void*>(operator new(sizeof(TGE::SimObject)));//
	//void *buf = new void*[sizeof(TGE::SimObject)];
	//void *z_simobj_real = TGE::Members::SimObject::_new(buf, 0);//TGE::Members::SimObject::_new(0);
	/*TestClass z_className;
	TestClass *parentClassPtr = &z_className;
	void *tablePtr = &customTable;
	memcpy(parentClassPtr, &tablePtr, sizeof(void*));
*/
//memcpy(parentClassPtr, &tablePtr, sizeof(void*));

	/*void *originalSimObjectVTable = reinterpret_cast<TGE::SimObject_VTABLE *>(TGEOFF_SIMOBJECT_VTABLE);

	class X
	{
		virtual void pizza()
		{

		}
	};
	X z_className;
	X *parentClassPtr = &z_className;
	void *tablePtr = &z_className;//&customTable;
								  //memcpy(originalSimObjectVTable, &tablePtr, sizeof(void*));
	memcpy(parentClassPtr, &tablePtr, sizeof(void*));
	//free(originalSimObjectVTable);

	//MEMORY_BASIC_INFORMATION mbi;
	//VirtualQuery((LPCVOID)originalSimObjectVTable, &mbi, sizeof(mbi));
	//VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_READWRITE, &mbi.Protect);
	TestClass z_className2;
	TestClass *parentClassPtr2 = &z_className2;
	void *tablePtr2 = &z_className2;//&customTable;
	//memcpy(originalSimObjectVTable, &tablePtr, sizeof(void*));
	memcpy(parentClassPtr2, &tablePtr2, sizeof(void*));

	TestClass z2;
	TestClass *z3 = &z2;

	X x;

	TestClass* test = dynamic_cast<TestClass*>(&x);

	if (!test)
	{
		msg("Nope :/");
	}*/
	//VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);
}

PLUGINCALLBACK void postEngineInit(PluginInterface* plugin)
{
	//TGE::Con::printf("This is a test: %s", plugin->getPath());
}

PLUGINCALLBACK void engineShutdown(PluginInterface* plugin)
{
	//DeleteFile("./pipe/IAPlayer.100.0");
	//RemoveDirectory("./pipe");
}