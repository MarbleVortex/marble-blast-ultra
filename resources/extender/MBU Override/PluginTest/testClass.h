#ifndef __TESTCLASS_H_
#define __TESTCLASS_H_

#include <TGE.h>
#include <TorqueLib\QuickOverride.h>

using namespace TGE;

class TestClass : public SimObject
{
	typedef SimObject Parent;

public:
	TestClass();
	~TestClass();

	bool onAdd();
	void onRemove();

	//static TGE::ConcreteClassRep<TestClass> dynClassRep;
	DECLARE_CONOBJECT(TestClass);
	//static Torque::ConcreteClassRep<TestClass> dynClassRep;
	//static Torque::AbstractClassRep* getParentStaticClassRep();
	//static Torque::AbstractClassRep* getStaticClassRep();
	//virtual Torque::AbstractClassRep* getClassRep() const;
};

#endif
