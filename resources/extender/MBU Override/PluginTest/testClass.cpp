#include "testClass.h"

IMPLEMENT_CONOBJECT(TestClass);

//TGE::ConcreteClassRep<TestClass> TestClass::dynClassRep("TestClass", 0, -1, 0, NULL);
//Torque::AbstractClassRep* TestClass::getClassRep() const { return &TestClass::dynClassRep; }
//Torque::AbstractClassRep* TestClass::getStaticClassRep() { return &dynClassRep; }
//Torque::AbstractClassRep* TestClass::getParentStaticClassRep() { return Parent::getStaticClassRep(); }
//Torque::ConcreteClassRep<TestClass> TestClass::dynClassRep("TestClass", 0, -1, 0, TestClass::getParentStaticClassRep());

//ClassRep classRep("TestClass", 0, -1, 0, reinterpret_cast<ClassRep*>(TGEADDR_CON_SIMOBJECT_CONCRETECLASSREP));

//IMPLEMENT_CONOBJECT(TestClass, classRep);

template<class T>
ConcreteClassRep<T>* ConcreteClassRep<T>::testRef = NULL;

/*ConsoleFunction(test, void, 1, 1, "test();")
{
	ConsoleObject *object = ConcreteClassRep<TestClass>::testRef->create();

	// Deal with failure!
	if (!object)
	{
		Con::errorf(ConsoleLogEntry::General, "%s: Unable to instantiate non-conobject class %s.", "ENGINE", "TestClass");//getFileLine(ip - 1), callArgv[1]);
		//ip = failJump;
		//break;
	}

	SimObjectT *currentNewObject = NULL;

	// Finally, set currentNewObject to point to the new one.
	currentNewObject = dynamic_cast<SimObjectT *>(object);

	// Deal with the case of a non-SimObject.
	if (!currentNewObject)
	{
		Con::errorf(ConsoleLogEntry::General, "%s: Unable to instantiate non-SimObject class %s.", "ENGINE", "TestClass");
		//delete object;
		//ip = failJump;
		//break;
	}
}*/

/*OverrideAddress(0x4AE561, originalAddr)
{
	Con::printf("Test");
}*/

/*typedef void(*MAKEUNIQUE(z_overrideAddress_ptr)) ();
static MAKEUNIQUE(z_overrideAddress_ptr) originalName = 0x4AE561;
static TorqueLib::OverrideGenerator<MAKEUNIQUE(z_overrideAddress_ptr)> MAKEUNIQUE(z_overrideAddressGen) (&originalName, MAKEUNIQUE(z_overrideAddress));
static void MAKEUNIQUE(z_overrideAddress) ()
{

}*/
//namespace { rettype (*const name) args = reinterpret_cast<rettype (*) args>(addr); }
/*namespace {
	void(*const address) () = reinterpret_cast<void(*) ()>(TGEADDR_SHUTDOWNGAME);
}*/

/*ConsoleFunction(test, void, 1, 1, "test();")
{
	const char*(*const prependDollar) (const char*) = reinterpret_cast<const char*(*) (const char*)>(0x4AFFA1);
	//void(*address) () = reinterpret_cast<void(*) ()>(TGEADDR_SHUTDOWNGAME);//(0x4AE561);

	//address();
	const char* originalText = "Pickle";
	const char* newText = prependDollar(originalText);
	Con::printf("Original: %s\nNew: %s\n", originalText, newText);
	//void *address = reinterpret_cast<void*>(0x4AE561);
	//address();*/
	/*__asm
	{
		jmp far ptr [address]
	}*/
/*
	//jmp_buf a;
	//a[0] = 0x4AE561;
	//setjmp(a);

	//longjmp(a, 0x4AE561);
	//__asm {
	//	jmp far ptr [address]
	//}
	//JMP(0x4AE561);
}*/

TestClass::TestClass()
{
	//TGE::Con::printf("Initialized Test Class!");
}

TestClass::~TestClass()
{
	//TGE::Con::printf("Destroyed Test Class!");
}

bool TestClass::onAdd()
{
	//TGE::Con::printf("Added Test Class!");
	return true;
}

void TestClass::onRemove()
{
	//TGE::Con::printf("Removed Test Class!");
}
