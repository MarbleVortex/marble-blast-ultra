#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

#include "vortexnet.h"

using namespace TGE;
using namespace Con;

bool isVortexNet(TCPObject* obj)
{
	const auto pos = std::find(gVortexTcpObjects.begin(), gVortexTcpObjects.end(), obj);
	return pos != gVortexTcpObjects.end();
}

TorqueOverrideMember(void, TCPObj::onConnectionRequest, (TCPObject* pThis, const NetAddress *addr, U32 connectId), originalOnConnectionRequest)
{
	if (isVortexNet(pThis))
		onVortexNetConnectionRequest(addr, connectId);
	else
		originalOnConnectionRequest(pThis, addr, connectId);
}

TorqueOverrideMember(bool, TCPObj::processLine, (TCPObject* pThis, UTF8* line), originalProcessLine)
{
	if (isVortexNet(pThis))
		return onVortexNetLine(line);
	else
		return originalProcessLine(pThis, line);
}

TorqueOverrideMember(void, TCPObj::onDNSResolved, (TCPObject* pThis), originalOnDNSResolved)
{
	if (isVortexNet(pThis))
		onVortexNetDNSResolved();
	else
		originalOnDNSResolved(pThis);
}

TorqueOverrideMember(void, TCPObj::onDNSFailed, (TCPObject* pThis), originalOnDNSFailed)
{
	if (isVortexNet(pThis))
		onVortexNetDNSFailed();
	else
		originalOnDNSFailed(pThis);
}

TorqueOverrideMember(void, TCPObj::onConnected, (TCPObject* pThis), originalOnConnected)
{
	if (isVortexNet(pThis))
		onVortexNetConnected();
	else
		originalOnConnected(pThis);
}

TorqueOverrideMember(void, TCPObj::onConnectFailed, (TCPObject* pThis), originalOnConnectFailed)
{
	if (isVortexNet(pThis))
		onVortexNetConnectFailed();
	else
		originalOnConnectFailed(pThis);
}

/*TorqueOverrideMember(void, TCPObj::onDisconnect, (TCPObject* pThis), originalOnDisconnect)
{
	if (isVortexNet(pThis))
		onVortexNetDisconnect();
	else
		originalOnDisconnect(pThis);
}*/

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{

}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	// Call our onClientProcess() function each tick
	plugin->onClientProcess(onVortexNetUpdate);
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{

}
