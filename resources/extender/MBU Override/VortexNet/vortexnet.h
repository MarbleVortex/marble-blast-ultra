#ifndef __VORTEXNET_H_
#define __VORTEXNET_H_

#define VORTEXNET_VERSION 1

#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

#include <vector>
#include <time.h>

class VortexNet
{
private:
	enum ConType
	{
		HOST,
		QUERY,
		JOIN,
		NONE
	};

public:
	VortexNet();
	~VortexNet();

	void stop();
	void host(const char* info, const bool openJoin);
	void query();
	void join(const char* vortexId);

	void update(U32 timeDelta);

	void onConnectionRequest(const TGE::NetAddress *addr, U32 connectId);
	bool onLine(const char* line);
	void onDNSResolved();
	void onDNSFailed();
	void onConnected();
	void onConnectFailed();
	void onDisconnect();

private:
	const char* mVortexId;
	TGE::TCPObject* mTcpObject;
	ConType mConType;
	bool mOpenJoin;
	const char* mInfo;
	const char* mJoinVortexId;

	const char* getVortexNetAddress(bool udp) const;

	bool connect();
	void send(const char* msg);
	void sendf(const char* format, ...);

	void startHeartbeat();

	void onQueryResponse(const char* response) const;
	void onBroadcastSuccess();
	void onBroadcastFailed();
	void onJoinSuccess(const char* address, const int port);
	void onJoinFailed();
	void onClientJoin(const char* address, const int port);
	void onOutdatedServer();
	void onOutdatedClient();

	void doHeartbeat();
	//std::future<void> mHeartbeatThread;
	//HANDLE hTimer;
	time_t mStartHeartbeatTime;
	bool mHeartbeating;
};

extern VortexNet* gVortexNet;
extern std::vector<TGE::TCPObject*> gVortexTcpObjects;

inline void onVortexNetConnectionRequest(const TGE::NetAddress *addr, U32 connectId)
{
	gVortexNet->onConnectionRequest(addr, connectId);
}

inline bool onVortexNetLine(const char* line)
{
	return gVortexNet->onLine(line);
}

inline void onVortexNetDNSResolved()
{
	gVortexNet->onDNSResolved();
}

inline void onVortexNetDNSFailed()
{
	gVortexNet->onDNSFailed();
}

inline void onVortexNetConnected()
{
	gVortexNet->onConnected();
}

inline void onVortexNetConnectFailed()
{
	gVortexNet->onConnectFailed();
}

inline void onVortexNetDisconnect()
{
	gVortexNet->onDisconnect();
}

inline void onVortexNetUpdate(U32 timeDelta)
{
	gVortexNet->update(timeDelta);
}

#endif // __VORTEXNET_H_