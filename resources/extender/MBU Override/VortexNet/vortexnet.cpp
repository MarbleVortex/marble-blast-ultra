#include "vortexnet.h"
#include "TorqueLib/platform/platformAssert.h"
#include <cstdarg>
#include <vector>
#include <algorithm>

using namespace TGE;
using namespace Con;

int strsplit(const char *str, char c, char ***arr)
{
	int count = 1;
	int token_len = 1;
	int i = 0;
	char *p;
	char *t;

	p = (char*)str;
	while (*p != '\0')
	{
		if (*p == c)
			count++;
		p++;
	}

	*arr = (char**)malloc(sizeof(char*) * count);
	if (*arr == NULL)
		AssertFatal(false, "split: Memory Allocation Error (1)");

	p = (char*)str;
	while (*p != '\0')
	{
		if (*p == c)
		{
			(*arr)[i] = (char*)malloc(sizeof(char) * token_len);
			if ((*arr)[i] == NULL)
				AssertFatal(false, "split: Memory Allocation Error (2)");

			token_len = 0;
			i++;
		}
		p++;
		token_len++;
	}
	(*arr)[i] = (char*)malloc(sizeof(char) * token_len);
	if ((*arr)[i] == NULL)
		AssertFatal(false, "split: Memory Allocation Error (3)");

	i = 0;
	p = (char*)str;
	t = ((*arr)[i]);
	while (*p != '\0')
	{
		if (*p != c && *p != '\0')
		{
			*t = *p;
			t++;
		}
		else
		{
			*t = '\0';
			i++;
			t = ((*arr)[i]);
		}
		p++;
	}

	return count;
}

inline bool strprefix(const char *pre, const char *str)
{
	return strncmp(pre, str, strlen(pre)) == 0;
}

inline const char* substr(const char* str, const int startIndex)
{
	if (startIndex >= strlen(str))
		return nullptr;
	char* buf = (char*)malloc(1024);
	strcpy_s(buf, 1024, str + startIndex);
	return buf;
}

VortexNet* gVortexNet = new VortexNet();
std::vector<TCPObject*> gVortexTcpObjects;

VortexNet::VortexNet() : mVortexId(nullptr), mTcpObject(nullptr), mConType(NONE), mOpenJoin(false), mInfo(nullptr), mJoinVortexId(nullptr),mHeartbeating(false), mStartHeartbeatTime(0)
{
	
}

VortexNet::~VortexNet()
{
	stop();
}

void VortexNet::host(const char* info, const bool openJoin)
{
	mConType = HOST;
	mOpenJoin = openJoin;
	mInfo = info;

	this->connect();
}

void VortexNet::query()
{
	mConType = QUERY;

	this->connect();
}

void VortexNet::join(const char* vortexId)
{
	//Con::printf("Join: %s", vortexId);
	mConType = JOIN;
	mJoinVortexId = vortexId;

	this->connect();
}

void VortexNet::stop()
{
	this->mHeartbeating = false;
	this->mStartHeartbeatTime = 0;

	if (mTcpObject == nullptr)
		return;

	auto pos = std::find(gVortexTcpObjects.begin(), gVortexTcpObjects.end(), mTcpObject);
	if (pos != gVortexTcpObjects.end())
		gVortexTcpObjects.erase(pos);

	mTcpObject->disconnect();
	//tcpObject->deleteObject();
	mTcpObject = nullptr;

	delete mTcpObject;

	mOpenJoin = false;
	mVortexId = nullptr;
	mConType = NONE;
	mInfo = nullptr;
	mJoinVortexId = nullptr;
}

void VortexNet::onConnectionRequest(const TGE::NetAddress *addr, U32 connectId)
{
	char idBuf[16];
	char addrBuf[256];
	Net::addressToString(addr, addrBuf);
	sprintf_s(idBuf, sizeof(idBuf), "%d", connectId);
	Con::executef(3, "onVortexNetConnectRequest", addrBuf, idBuf);
}

bool VortexNet::onLine(const char* line)
{
	//Con::errorf("Received: %s", line);
	const char* requestValidationCmd = "requestValidation:";
	if (strprefix(requestValidationCmd, line))
	{
		mVortexId = substr(line, strlen(requestValidationCmd));

		const char* gameType = Con::getVariable("$VortexNet::GameName");
		if (strlen(gameType) < 1)
		{
			Con::errorf("VortexNet: $VortexNet::GameName not set!");
			stop();
			return false;
		}

		if (mConType == HOST)
		{
			const char* response = "responseValidation";
			this->sendf("%s:%d:%s:%s:%s", response, VORTEXNET_VERSION, gameType, mOpenJoin ? "true" : "false", mInfo);
		} else if (mConType == QUERY)
		{
			const char* response = "requestQuery";
			this->sendf("%s:%d:%s", response, VORTEXNET_VERSION, gameType);
		} else if (mConType == JOIN)
		{
			const char* response = "requestJoin";
			this->sendf("%s:%d:%s", response, VORTEXNET_VERSION, mJoinVortexId);
		}
	} else
	{
		const char* requestUDPCmd = "requestUDP";
		if (strcmp(requestUDPCmd, line) == 0)
		{
			const char* setIdCmd = "setId";
			char buf[2048];
			sprintf_s(buf, 2048, "%s:%d:%s", setIdCmd, VORTEXNET_VERSION, mVortexId);

			BitStream *out = BStream::getPacketStream();
			NetAddress addr;
			const char* addressThing = getVortexNetAddress(true);
			if (!Net::stringToAddress(addressThing, &addr))
			{
				Con::errorf("Error converting address!");
			}

			char newAddr[256];
			Net::addressToString(&addr, newAddr);
			
			out->write(strlen(buf), buf);

			//Con::printf("Sending %s to %s", buf, newAddr);

			//Con::printf("Sending %s to %s", buf, addressThing);

			for (int i = 0; i < 10000; i++)
			{
				BStream::sendPacketStream(&addr); // failing to send???
			}

			this->sendf("responseUDP");
		}
		else {

			const char* responseQueryCmd = "responseQuery:";
			if (strprefix(responseQueryCmd, line))
			{
				const char* queryResponse = substr(line, strlen(responseQueryCmd));

				stop();
				this->onQueryResponse(queryResponse);
			}
			else {
				const char* responseBroadcast = "responseBroadcast:";
				if (strprefix(responseBroadcast, line))
				{
					bool success = strcmp(substr(line, strlen(responseBroadcast)), "true") == 0;

					if (success)
					{
						this->startHeartbeat();

						this->onBroadcastSuccess();
					} else
					{
						stop();
						this->onBroadcastFailed();
					}
				}
				else {
					const char* requestJoinUDPCmd = "requestJoinUDP:";
					if (strprefix(requestJoinUDPCmd, line))
					{
						//Con::printf("RequestJoinUDP Received: %s", line);
						
						const char* joinId = substr(line, strlen(requestJoinUDPCmd));

						const char* setIdCmd = "setJoinId";
						char buf[2048];
						sprintf_s(buf, 2048, "%s:%d:%s", setIdCmd, VORTEXNET_VERSION, mVortexId);

						BitStream *out = BStream::getPacketStream();
						NetAddress addr;
						Net::stringToAddress(getVortexNetAddress(true), &addr);

						out->write(strlen(buf), buf);

						for (int i = 0; i < 10000; i++)
						{
							BStream::sendPacketStream(&addr); // failing to send???
						}

						this->sendf("responseJoinUDP:%s", joinId);
					}
					else {
						const char* responseJoinCmd = "responseJoin:";
						if (strprefix(responseJoinCmd, line))
						{
							const char* joinResp = substr(line, strlen(responseJoinCmd));

							char** joinData = nullptr;
							int count = strsplit(joinResp, ':', &joinData);

							if (count != 3)
							{
								Con::errorf("VortexNet: Invalid Join Response");
								stop();
								this->onJoinFailed();
							}

							bool success = strcmp(joinData[0], "true") == 0;
							const char* joinAddress = joinData[1];
							int joinPort = dAtoi(joinData[2]);

							if (success)
							{
								stop();
								
								this->onJoinSuccess(joinAddress, joinPort);
							} else
							{
								stop();

								this->onJoinFailed();
							}
						} else
						{
							const char* requestClientJoinCmd = "requestClientJoin:";
							if (strprefix(requestClientJoinCmd, line))
							{
								const char* clientJoinResp = substr(line, strlen(requestClientJoinCmd));

								char** clientJoinData = nullptr;
								int count = strsplit(clientJoinResp, ':', &clientJoinData);

								if (count != 2)
								{
									Con::errorf("VortexNet: Invalid Join Response");
									stop();
									this->onJoinFailed();
									return false;
								}

								const char* clientJoinAddress = clientJoinData[0];
								int clientJoinPort = dAtoi(clientJoinData[1]);

								this->onClientJoin(clientJoinAddress, clientJoinPort);
							} else
							{
								const char* outOfDateCmd = "outOfDate:";
								if (strprefix(outOfDateCmd, line))
								{
									const char* clientOutOfDate = substr(line, strlen(outOfDateCmd));

									bool oldServer = strcmp(clientOutOfDate, "true") == 0;

									if (oldServer) // Outdated Server
									{
										this->onOutdatedServer();
									} else // Outdated Client
									{
										this->onOutdatedClient();
									}
									stop();
									return false;
								}
							}
						}
					}
				}
			}
		}
	}
	return true;
}

void VortexNet::onDNSResolved()
{
	Con::executef(1, "onVortexNetDNSResolved");
}

void VortexNet::onDNSFailed()
{
	Con::executef(1, "onVortexNetDNSFailed");
}

void VortexNet::onConnected()
{
	Con::executef(1, "onVortexNetConnected");
}

void VortexNet::onConnectFailed()
{
	Con::executef(1, "onVortexNetConnectFailed");
}

void VortexNet::onDisconnect()
{
	Con::executef(1, "onVortexNetDisconnect");
}

const char* VortexNet::getVortexNetAddress(bool udp) const
{
	const char* address = Con::getVariable("$VortexNet::Address");
	const char* tcpPort = Con::getVariable("$VortexNet::TCPPort");
	const char* udpPort = Con::getVariable("$VortexNet::UDPPort");

	if (strlen(address) < 1 || strlen(address) < 1 || strlen(udpPort) < 1)
	{
		Con::errorf("getVortexNetAddress: Invalid Address, please make sure $VortexNet::Address, $VortexNet::TCPPort, and $VortexNet::UDPPort, are set correctly!");
		return nullptr;
	}

	const char* port;

	if (udp)
		port = udpPort;
	else
		port = tcpPort;

	char* buf = (char*)malloc(1024);
	sprintf_s(buf, 1024, "%s:%s", address, port);

	return buf;
}

void VortexNet::send(const char* msg)
{
	char buf[2048];
	sprintf_s(buf, 2048, "%s\n", msg);
	//Con::errorf("Sending: %s", buf);
	mTcpObject->send((const U8*)buf, strlen(buf));
}

void VortexNet::sendf(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	char buffer[2048];
	S32 len = vsnprintf(buffer, 2048, format, args);
	if (len < 0 || len >= 2048)
	{
		buffer[2048 - 1] = '\0';

#ifdef TORQUE_DEBUG
		Platform::AlertOK("Overflow", "Buffer Overflow in VortexNet::sendf");
		Platform::debugBreak();
#endif
	}

	this->send(buffer);
}

bool VortexNet::connect()
{
	//BitStream *out = BitStream::getPacketStream();
	//BitStream::sendPacketStream(getVortexNetAddress(false));

	//NetSocket socket = Net::openConnectTo(getVortexNetAddress(false));

	if (mTcpObject != nullptr)
	{
		Con::errorf("VortexNet::connect: Attempted to broadcast before closing running VortexNet operations!");
		return false;
	}

	mTcpObject = (TCPObject*)ConObj::create("TCPObject");
	//mTcpObject->setVortexNet(true);

	gVortexTcpObjects.push_back(mTcpObject);

	//tcpObject->registerObject("VortexNet");

	const char* addr = getVortexNetAddress(false);
	if (addr == nullptr)
	{
		stop();
		return false;
	}

	mTcpObject->connect(addr);

	return true;
}

void VortexNet::startHeartbeat()
{
	this->mHeartbeating = true;
	this->mStartHeartbeatTime = time(nullptr);
}

void VortexNet::update(U32 timeDelta)
{
	if (mHeartbeating)
	{
		const double secondsPassed = difftime(time(nullptr), this->mStartHeartbeatTime);
		
		if (secondsPassed >= 10)
		{
			doHeartbeat();
			this->mStartHeartbeatTime = time(nullptr);
		}
	}
}

void VortexNet::doHeartbeat()
{
	Con::printf("Sending Heartbeat to master...");

	// TCP
	this->sendf("HEARTBEAT");

	// UDP
	const char* heartbeat = "HB";
	BitStream *out = BStream::getPacketStream();
	NetAddress addr;
	Net::stringToAddress(getVortexNetAddress(true), &addr);
	out->write(strlen(heartbeat), heartbeat);
	BStream::sendPacketStream(&addr);
}

void VortexNet::onQueryResponse(const char* response) const
{
	Con::executef(2, "onVortexNetQueryResponse", response);
}

void VortexNet::onBroadcastSuccess()
{
	Con::executef(1, "onVortexNetBroadcastSuccess");
}

void VortexNet::onBroadcastFailed()
{
	Con::executef(1, "onVortexNetBroadcastFailed");
}

void VortexNet::onJoinSuccess(const char* address, const int port)
{
	char theAddr[1024];
	sprintf_s(theAddr, 1024, "%s:%d", address, port);
	//const char* buf = "initial";
	BitStream *out = BStream::getPacketStream();
	NetAddress addr;
	Net::stringToAddress(theAddr, &addr);

	int initial = 39;
	out->write(sizeof(U8), &initial); // Initial Packet
	//out->write(dStrlen(buf), buf);

	for (int i = 0; i < 10000; i++)
	{
		BStream::sendPacketStream(&addr);
	}

	char portBuf[1024];
	sprintf_s(portBuf, 1024, "%d", port);
	Con::executef(3, "onVortexNetJoinSuccess", address, portBuf);
}

void VortexNet::onJoinFailed()
{
	Con::executef(1, "onVortexNetJoinFailed");
}

void VortexNet::onClientJoin(const char* address, const int port)
{
	char theAddr[1024];
	sprintf_s(theAddr, 1024, "%s:%d", address, port);
	//const char* buf = "initial";
	BitStream *out = BStream::getPacketStream();
	NetAddress addr;
	Net::stringToAddress(theAddr, &addr);
	
	int initial = 39;
	out->write(sizeof(U8), &initial); // Initial Packet
	//out->write(dStrlen(buf), buf);

	for (int i = 0; i < 10000; i++)
	{
		BStream::sendPacketStream(&addr);
	}

	char portBuf[1024];
	sprintf_s(portBuf, 1024, "%d", port);
	Con::executef(3, "onVortexNetClientJoin", address, portBuf);
}

void VortexNet::onOutdatedServer()
{
	Con::executef(1, "onVortexNetServerOutdated");
}

void VortexNet::onOutdatedClient()
{
	Con::executef(1, "onVortexNetClientOutdated");
}


// -------------------------------------
// Console Functions
// -------------------------------------

ConsoleFunction(hostVortexNet, void, 2, 3, "hostVortexNet(info, [openJoin])")
{
	char* info = (char*)malloc(2048);
	strcpy_s(info, 2048, argv[1]);
	info[2048 - 1] = '\0';
	
	bool openJoin = false;
	if (argc > 2)
		openJoin = dAtob(argv[2]);
	gVortexNet->host(info, openJoin);
}

ConsoleFunction(queryVortexNet, void, 1, 1, "queryVortexNet()")
{
	gVortexNet->query();
}

ConsoleFunction(joinVortexNet, void, 2, 2, "joinVortexNet(vortexId)")
{
	char* vortexId = (char*)malloc(2048);
	strcpy_s(vortexId, 2048, argv[1]);
	vortexId[2048 - 1] = '\0';

	gVortexNet->join(vortexId);
}

ConsoleFunction(stopVortexNet, void, 1, 1, "stopVortexNet()")
{
	gVortexNet->stop();
}
