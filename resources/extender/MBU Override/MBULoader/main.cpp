#include <Windows.h>

#include "SharedObject.h"

const char *const TorqueLibStandardPath = "MBUExtension.dll";
const char *const TorqueLibDevPath = "MBUExtension_Dev.dll";
const char * TorqueLibPath;

typedef bool(*initLoader_t)();

namespace {
	
	SharedObject *pluginLoader;

	bool testStringPointer(const char *testPointer, const char *testStr)
	{
		// Make sure we can actually read from the test memory location
		MEMORY_BASIC_INFORMATION memInfo;
		if (VirtualQuery(testPointer, &memInfo, sizeof(memInfo)) != sizeof(memInfo))
			return false;
		if (memInfo.Protect == 0 || (memInfo.Protect & PAGE_NOACCESS) || (memInfo.Protect & PAGE_EXECUTE))
			return false;

		// Check if the string matches
		if (memcmp(testPointer, testStr, strlen(testStr)) != 0)
			return false;
		return true;
	}

	enum GameVersion
	{
		INVALID		= 0,
		STANDARD	= 1,
		DEV			= 2,
	};

	GameVersion getGameVersion()
	{
		const char *testPointerStandard = reinterpret_cast<const char*>(0x6EF15C);
		const char *testPointerDev = reinterpret_cast<const char*>(0x6F9CDC);
		const char *testStr = "Marble Blast Ultra";

		if (testStringPointer(testPointerStandard, testStr))
			return GameVersion::STANDARD;
		else if (testStringPointer(testPointerDev, testStr))
			return GameVersion::DEV;
		else
			return GameVersion::INVALID;
	}

}

extern "C" __declspec(dllexport) bool LoadMBUExtension()
{
	GameVersion version = getGameVersion();
	if(version == GameVersion::STANDARD)
	{
		TorqueLibPath = TorqueLibStandardPath;
	} else if (version == GameVersion::DEV) {
		TorqueLibPath = TorqueLibDevPath;
	} else {
		MessageBox(NULL, "Invalid Marble Blast Ultra Exe File", "Error", MB_OK | MB_ICONERROR);
		return false;
	}

	pluginLoader = new SharedObject(TorqueLibPath);
	if (pluginLoader->loaded())
	{
		initLoader_t initFunc = reinterpret_cast<initLoader_t>(pluginLoader->getSymbol("InitMBUExtension"));
		if (initFunc)
			return initFunc();
		else
			return false;
	}
	else
	{
		if(version == GameVersion::STANDARD)
			MessageBox(NULL, "Failed to load MBUExtension.dll", "Error", MB_OK | MB_ICONERROR);
		else
			MessageBox(NULL, "Failed to load MBUExtension_Dev.dll", "Error", MB_OK | MB_ICONERROR);
		delete pluginLoader;
		pluginLoader = NULL;

		return false;
	}
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		if (!LoadMBUExtension())
			TerminateProcess(GetCurrentProcess(), 0);
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

	return true;
}