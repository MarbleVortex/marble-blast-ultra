//#include <iostream>
#include <Windows.h>

#include <vector>
#include <string>
#include <TGE.h>

#include <InterfaceMacros-win32.h>
#include "FuncInterceptor.h"
#include "BasicPluginInterface.h"
#include "SharedObject.h"
#include "Filesystem.h"
#include "StringUtil.h"

//const char *const TorqueLibStandardPath = "MBULib.dll";
//const char *const TorqueLibDevPath = "MBULib_Dev.dll";
#ifdef MBU_DEV
const char *const TorqueLibPath = "MBULib_Dev.dll";
#else
const char *const TorqueLibPath = "MBULib.dll";
#endif

//#include "plugin.h"

//Plugin *plugin;

typedef void(*initMath_t)();
typedef void(*installOverrides_t)(PluginInterface *plugin);
typedef void(*pluginCallback_t)(PluginInterface *plugin);

namespace
{

	CodeInjection::CodeAllocator *codeAlloc;
	CodeInjection::CodeInjectionStream *injectionStream;
	CodeInjection::FuncInterceptor *hook;

	SharedObject *mathLib;
	installOverrides_t installUserOverrides;

	struct LoadedPlugin
	{
		std::string path;
		SharedObject *library;
		CodeInjection::FuncInterceptor *interceptor;
		BasicTorqueFunctionInterceptor *torqueInterceptor;
		BasicPluginInterface *pluginInterface;
	};
	std::vector<LoadedPlugin> *loadedPlugins;

	void loadPlugins()
	{
#ifdef MBU_DEV
		std::string pluginDir = "plugins/dev";
#else
		std::string pluginDir = "plugins/standard";
#endif
		if (!Filesystem::Directory::exists(pluginDir))
		{
			TGE::Con::warnf("   No %s directory found!", pluginDir.c_str());
			return;
		}
		std::vector<std::string> paths;
		if (!Filesystem::Directory::enumerate(pluginDir, &paths))
		{
			TGE::Con::warnf("   Unable to enumerate the %s directory!", pluginDir.c_str());
			return;
		}
		for (size_t i = 0; i < paths.size(); i++)
		{
			std::string &path = paths[i];

			// Check if the path points to a shared object file
			if (Filesystem::Path::getExtension(path) != SharedObject::DefaultExtension)
			{
				// Check if the path is a directory, and if so, try to load a shared object file inside it with the same name
				if (!Filesystem::Directory::exists(path))
					continue;
				std::string pluginName = Filesystem::Path::getFilename(path);
				path = Filesystem::Path::combine(path, pluginName + SharedObject::DefaultExtension);
				if (!Filesystem::File::exists(path))
					continue;
			}
			
			TGE::Con::printf("   Loading %s", path.c_str());
			SharedObject *library = new SharedObject(path.c_str());
			if (library->loaded())
			{
				CodeInjection::FuncInterceptor *interceptor = new CodeInjection::FuncInterceptor(injectionStream, codeAlloc);
				BasicTorqueFunctionInterceptor *torqueInterceptor = new BasicTorqueFunctionInterceptor(interceptor);
				BasicPluginInterface *pluginInterface = new BasicPluginInterface(torqueInterceptor, path);
				LoadedPlugin info = { path, library, interceptor, torqueInterceptor, pluginInterface };
				loadedPlugins->push_back(info);
				if (installUserOverrides)
					installUserOverrides(pluginInterface);
			}
			else
			{
				TGE::Con::errorf("   Unable to load %s!", path.c_str());
				delete library;
			}
		}
	}

	bool runPluginCallback(const LoadedPlugin *plugin, const char *fnName)
	{
		pluginCallback_t func = reinterpret_cast<pluginCallback_t>(plugin->library->getSymbol(fnName));
		if (!func)
			return false;
		func(plugin->pluginInterface);
		return true;
	}

	void callPluginInit(const char *message, const char *fnName)
	{
		if (loadedPlugins->size() == 0)
			return;
		TGE::Con::printf("%s", message);
		for (size_t i = 0; i < loadedPlugins->size(); i++)
		{
			LoadedPlugin &plugin = (*loadedPlugins)[i];
			TGE::Con::printf("   Initializing %s", plugin.path.c_str());
			if (!runPluginCallback(&plugin, fnName))
				TGE::Con::warnf("   WARNING: %s does not have a %s() function!", plugin.path.c_str(), fnName);
		}
		TGE::Con::printf("");
	}

	void pluginPreInit()
	{
		callPluginInit("MBExtender: Initializing Plugins, Stage 1:", "preEngineInit");
	}

	void pluginPostInit()
	{
		callPluginInit("MBExtender: Initializing Plugins, Stage 2:", "postEngineInit");
	}

	void unloadPlugin(LoadedPlugin *plugin)
	{
		if (!runPluginCallback(plugin, "engineShutdown"))
			TGE::Con::warnf("   WARNING: %s does not have a %s() function!", plugin->path.c_str(), "engineShutdown");
		delete plugin->pluginInterface;
		delete plugin->torqueInterceptor;
		delete plugin->interceptor;
		delete plugin->library;
		plugin->pluginInterface = NULL;
		plugin->torqueInterceptor = NULL;
		plugin->interceptor = NULL;
		plugin->library = NULL;
	}

	void unloadPlugins()
	{
		TGE::Con::printf("MBExtender: Unloading Plugins:");
		for (size_t i = 0; i < loadedPlugins->size(); i++)
		{
			LoadedPlugin &plugin = (*loadedPlugins)[i];
			TGE::Con::printf("   Unloading %s", plugin.path.c_str());
			unloadPlugin(&plugin);
		}
		loadedPlugins->clear();
	}

	void setPluginVariables()
	{
		for (size_t i = 0; i < loadedPlugins->size(); i++)
		{
			LoadedPlugin &plugin = (*loadedPlugins)[i];

			// Set the Plugin::Loaded variable corresponding to the plugin
			std::string varName = Filesystem::Path::getFilenameWithoutExtension(plugin.path);
			varName = "Plugin::Loaded" + varName;
			TGE::Con::setBoolVariable(varName.c_str(), true);
		}
	}

	void loadMathLibrary()
	{
		TGE::Con::printf("   Initializing memory interface");
		mathLib = new SharedObject(TorqueLibPath);
		if (mathLib->loaded())
		{
			initMath_t initFunc = reinterpret_cast<initMath_t>(mathLib->getSymbol("init"));
			if (initFunc)
				initFunc();
			else
				TGE::Con::errorf("   TorqueLib does not have an init() function!");

			installUserOverrides = reinterpret_cast<installOverrides_t>(mathLib->getSymbol("installUserOverrides"));
			if (!installUserOverrides)
				TGE::Con::errorf("   TorqueLib does not support user overrides!");
		}
		else
		{
			TGE::Con::errorf("   Unable to load %s! Some plugins may fail to load!", TorqueLibPath);
			delete mathLib;
			mathLib = NULL;
		}
	}

	// TorqueScript function to unload a plugin given its name
	bool tsUnloadPlugin(TGE::SimObject *obj, S32 argc, const char *argv[])
	{
		std::string upperName = strToUpper(argv[1]);
		for (std::vector<LoadedPlugin>::iterator it = loadedPlugins->begin(); it != loadedPlugins->end(); ++it)
		{
			LoadedPlugin *plugin = &*it;
			if (strToUpper(Filesystem::Path::getFilenameWithoutExtension(plugin->path)) == upperName)
			{
				TGE::Con::printf("MBExtender: Unloading plugin %s", plugin->path.c_str());
				unloadPlugin(plugin);
				loadedPlugins->erase(it);
				return true;
			}
		}
		return false;
	}

	void registerFunctions()
	{
		TGE::Con::addCommand("unloadPlugin", tsUnloadPlugin, "unloadPlugin(name)", 2, 2, 0, NULL);
	}

	void (*originalNsInit)() = TGE::Members::Namespace::init;
	void newNsInit()
	{
		//originalNsInit();

		TGE::mGlobalNamespace = TGE::Members::Namespace::find(NULL, NULL);

		//MessageBox(NULL, "Namespace Init", "DEBUG", MB_OK | MB_ICONERROR);

		TGE::Con::printf("MBU Extension Init:");
		loadMathLibrary();
		loadPlugins();
		//TGE::Con::printf("   MBU Extension Initialized!");
	
		TGE::Con::printf("");
	
		pluginPreInit();

		//if (plugin)
		//	plugin->preInit();
	}

	void (*originalBadWordFilterCreate)() = TGE::BadWordFilter::create;
	void newBadWordFilterCreate()
	{
		originalBadWordFilterCreate();

		static bool initialized = false;
		if (initialized)
			return;
		initialized = true;

		registerFunctions();
		pluginPostInit();
		setPluginVariables();
	
		//if (plugin)
		//	plugin->postInit();
	}

	void (*originalShutdownGame)() = TGE::shutdownGame;
	void newShutdownGame()
	{
		originalShutdownGame();

		unloadPlugins();

		//if (plugin)
		//	delete plugin;//->unloadPlugin();

		//MessageBox(NULL, "Shutting Down Game", "DEBUG", MB_OK | MB_ICONERROR);
	}

	void (*originalClientProcess)(U32 timeDelta) = TGE::clientProcess;
	void newClientProcess(U32 timeDelta)
	{
		//if (plugin)
		//	plugin->update(timeDelta);
		BasicPluginInterface::executeProcessList(timeDelta);
		originalClientProcess(timeDelta);
		//MessageBox(NULL, "Game Process", "DEBUG", MB_OK | MB_ICONERROR);
	}

	/*void (*originalPrintf_)(int a1, int a2, char* fmt, va_list list) = TGE::Con::printf_;
	void newPrintf_(int a1, int a2, char* fmt, va_list list)
	{
		//va_list va;
		//va_start(va, fmt);
		//MessageBox(NULL, "I like pizza!!!", "Copyright Food Network", MB_OK | MB_ICONERROR);
		//originalPrintf_(0, 0, "Free Pizza!!!", va_list());
		originalPrintf_(0, 0, fmt, list);
	}*/

}

/*bool testStringPointer(const char *testPointer, const char *testStr)
{
	// Make sure we can actually read from the test memory location
	MEMORY_BASIC_INFORMATION memInfo;
	if (VirtualQuery(testPointer, &memInfo, sizeof(memInfo)) != sizeof(memInfo))
		return false;
	if (memInfo.Protect == 0 || (memInfo.Protect & PAGE_NOACCESS) || (memInfo.Protect & PAGE_EXECUTE))
		return false;

	// Check if the string matches
	if (memcmp(testPointer, testStr, strlen(testStr)) != 0)
		return false;
	return true;
}

enum GameVersion
{
	INVALID		= 0,
	STANDARD	= 1,
	DEV			= 2,
};

GameVersion getGameVersion()
{
	const char *testPointerStandard = reinterpret_cast<const char*>(0x2EF15C);
	const char *testPointerDev = reinterpret_cast<const char*>(0x2F9CDC);
	const char *testStr = "Marble Blast Ultra";

	if (testStringPointer(testPointerStandard, testStr))
		return GameVersion::STANDARD;
	else if (testStringPointer(testPointerDev, testStr))
		return GameVersion::DEV;
	else
		return GameVersion::INVALID;
}*/

/* Floating Point support not loaded fix */
int(__cdecl* original_IsNonwritableInCurrentImage)(int a1) = CInit::Engine_IsNonwritableInCurrentImage;
int __cdecl new_IsNonwritableInCurrentImage(int a1)
{
    return 1;
}

void fixFloatingPointSupport(CodeInjection::FuncInterceptor* hook)
{
    /* Floating Point support not loaded fix */
    original_IsNonwritableInCurrentImage = hook->intercept(CInit::Engine_IsNonwritableInCurrentImage, new_IsNonwritableInCurrentImage);
}

extern "C" __declspec(dllexport) bool InitMBUExtension()
{
	/*if(getGameVersion() == GameVersion::STANDARD)
	{
		TorqueLibPath = TorqueLibStandardPath;
	} else if (getGameVersion() == GameVersion::DEV) {
		TorqueLibPath = TorqueLibDevPath;
	} else {
		MessageBox(NULL, "Invalid Marble Blast Ultra Exe File", "Error", MB_OK | MB_ICONERROR);
		return false;
	}*/

	loadedPlugins = new std::vector<LoadedPlugin>();
	//MessageBox(NULL, "Initializing MBU Extension", "DEBUG", MB_OK | MB_ICONERROR);

	//TGE::Con::printf("Initializing MBU Extension");

	codeAlloc = new CodeInjection::CodeAllocator();
	injectionStream = new CodeInjection::CodeInjectionStream(reinterpret_cast<void*>(MB_TEXT_START), MB_TEXT_SIZE);
	hook = new CodeInjection::FuncInterceptor(injectionStream, codeAlloc);

    fixFloatingPointSupport(hook);

	originalNsInit = hook->intercept(TGE::Members::Namespace::init, newNsInit);
	originalBadWordFilterCreate = hook->intercept(TGE::BadWordFilter::create, newBadWordFilterCreate);

	originalShutdownGame = hook->intercept(TGE::shutdownGame, newShutdownGame);

	originalClientProcess = hook->intercept(TGE::clientProcess, newClientProcess);

	//originalPrintf_ = hook->intercept(TGE::Con::printf_, newPrintf_);

	//plugin = new Plugin();

	/*TGE::SimObject_VTABLE customTable;
#ifdef MBU_DEV
	customTable.sub_4BD415 = 0x4BD415;
	customTable.sub_4BD0E1 = 0x4BD0E1;
	customTable.sub_4BD078 = 0x4BD078;
	customTable.sub_4BB4D2 = 0x4BB4D2;
	customTable.sub_4BAF76 = 0x4BAF76;
	customTable.sub_4BAD11 = 0x4BAD11;
	customTable.sub_4BA95D = 0x4BA95D;
	customTable.sub_4B9BF8 = 0x4B9BF8;
	customTable.sub_4B98D0 = 0x4B98D0;
	customTable.sub_4B9647 = 0x4B9647;
	customTable.sub_4B95FD = 0x4B95FD;
	customTable.sub_4B9141 = 0x4B9141;
	customTable.sub_4B907A = 0x4B907A;
	customTable.sub_4B904D = 0x4B904D;
	customTable.sub_401C42 = 0x401C4D;
	customTable.nul_4B9B0D = 0x4B9B0D;
	customTable.nul_4B9B0C = 0x4B9B0C;
	customTable.nul_4B98CD = 0x4B98CD;
	customTable.nul_4B98CA = 0x4B98CA;
	customTable.nul_4B98C7 = 0x4B98C7;
	customTable.nul_4B98C6 = 0x4B98C6;
	customTable.nul_4B98C5 = 0x4B98C5;
	customTable.nul_401C47 = 0x401C47;
	customTable.nul_401C3F = 0x401C3F;
	customTable.nul_401C3E = 0x401C3E;
	customTable.nul_401C3D = 0x401C3D;
	customTable.ano_4BD6B5 = 0x4BD6B5;
#endif

	TGE::SimObject simObject;
	void *parentClassPtr = &simObject;//reinterpret_cast<void *>(TGEOFF_SIMOBJECT_VTABLE);

	TGE::SimObject_VTABLE *tablePtr = &customTable;

	memcpy(parentClassPtr, &tablePtr, sizeof(void*));

	TGE::SimObject_VTABLE customTable2;
#ifdef MBU_DEV
	customTable2.sub_4BD415 = 0x4BD415;
	customTable2.sub_4BD0E1 = 0x4BD0E1;
	customTable2.sub_4BD078 = 0x4BD078;
	customTable2.sub_4BB4D2 = 0x4BB4D2;
	customTable2.sub_4BAF76 = 0x4BAF76;
	customTable2.sub_4BAD11 = 0x4BAD11;
	customTable2.sub_4BA95D = 0x4BA95D;
	customTable2.sub_4B9BF8 = 0x4B9BF8;
	customTable2.sub_4B98D0 = 0x4B98D0;
	customTable2.sub_4B9647 = 0x4B9647;
	customTable2.sub_4B95FD = 0x4B95FD;
	customTable2.sub_4B9141 = 0x4B9141;
	customTable2.sub_4B907A = 0x4B907A;
	customTable2.sub_4B904D = 0x4B904D;
	customTable2.sub_401C42 = 0x401C4D;
	customTable2.nul_4B9B0D = 0x4B9B0D;
	customTable2.nul_4B9B0C = 0x4B9B0C;
	customTable2.nul_4B98CD = 0x4B98CD;
	customTable2.nul_4B98CA = 0x4B98CA;
	customTable2.nul_4B98C7 = 0x4B98C7;
	customTable2.nul_4B98C6 = 0x4B98C6;
	customTable2.nul_4B98C5 = 0x4B98C5;
	customTable2.nul_401C47 = 0x401C47;
	customTable2.nul_401C3F = 0x401C3F;
	customTable2.nul_401C3E = 0x401C3E;
	customTable2.nul_401C3D = 0x401C3D;
	customTable2.ano_4BD6B5 = 0x4BD6B5;
#endif

	//TGE::SimObject simObject1;
	//void *parentClassPtr1 = &simObject1;//reinterpret_cast<void *>(TGEOFF_SIMOBJECT_VTABLE);
	void *parentClassPtr1 = reinterpret_cast<void *>(TGEOFF_SIMOBJECT_VTABLE);

	TGE::SimObject_VTABLE *tablePtr1 = &customTable2;

	memcpy(parentClassPtr1, &tablePtr1, sizeof(void*));*/

	//FIXVTABLE(TGE::SimObject);//, TGEOFF_SIMOBJECT_VTABLE);

		/*void *z_simobj_real = TGE::Members::SimObject::_new(0);
		TGE::SimObject z_className;
		TGE::SimObject *parentClassPtr = &z_className;
		void *tablePtr = &z_simobj_real;
		memcpy(parentClassPtr, &tablePtr, sizeof(void*));*/

	return true;
}

/*BOOL APIENTRY DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		if (!InitMBUExtension())
			TerminateProcess(GetCurrentProcess(), 0);
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

	return true;
}*/
