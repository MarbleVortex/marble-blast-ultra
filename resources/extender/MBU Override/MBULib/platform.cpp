#include <TorqueLib\platform\platform.h>

S32 dAtoi(const char *str)
{
	return atoi(str);
}

F32 dAtof(const char *str)
{
	// metrowerks crashes when strange strings are passed in '0x [enter]' for example!
	// valid string for atof: [whitespaces][+|-][nnnnn][.nnnnn][e|E[+|-]nnnn]

	// just check for any char other than those allowed (still does not guarantee a valid float,
	// but hopefully metrowerks will catch those)
	const char * p = str;
	while (*p)
	{
		switch (*p++)
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '+':
		case '-':
		case ' ':
		case '\t':
		case 'e':
		case 'E':
		case '.':
			continue;

		default:
			return(0.f);
		}
	}
	return (F32)atof(str);
}

bool dAtob(const char *str)
{
	return !_stricmp(str, "true") || dAtof(str);
}