#ifndef __CONSOLEFIX_H_
#define __CONSOLEFIX_H_

#include <TGE.h>
#include <string>
#include <mutex>

class ConsoleFixer
{
	bool initialized;
	std::string consoleText;

	static const char* getColorString(TGE::ConsoleLogEntry::Level level);

public:
	ConsoleFixer();
	~ConsoleFixer();

	static bool isCreated();
	static bool isInitialized();
	static void create();
	static void destroy();
	static void initialize();

	void updateConsoleText(TGE::ConsoleLogEntry::Level level, const char *fmt, va_list argptr);
};

extern ConsoleFixer *ConsoleFix;

#endif
