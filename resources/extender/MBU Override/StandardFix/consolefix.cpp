#include "consolefix.h"

#include <iostream>
#include "util.h"

using namespace TGE;

ConsoleFixer *ConsoleFix = NULL;

ConsoleFunction(initializeConsoleFix, void, 1, 1, "initializeConsoleFix()")
{
	ConsoleFixer::initialize();
}

bool ConsoleFixer::isCreated()
{
	if (ConsoleFix)
		return true;

	return false;
}

bool ConsoleFixer::isInitialized()
{
	if (ConsoleFixer::isCreated())
		return ConsoleFix->initialized;

	return false;
}

void ConsoleFixer::create()
{
	ConsoleFix = new ConsoleFixer();
}

void ConsoleFixer::destroy()
{
	delete ConsoleFix;
	ConsoleFix = NULL;
}

void ConsoleFixer::initialize()
{
	if (ConsoleFixer::isCreated())
		ConsoleFix->initialized = true;
}

const char* ConsoleFixer::getColorString(ConsoleLogEntry::Level level)
{
	switch (level)
	{
	case ConsoleLogEntry::Level::Warning:
		return "<color:0000FF>";
		break;
	case ConsoleLogEntry::Level::Error:
		return "<color:FF0000>";
		break;
	default:
		return "<color:000000>";
		break;
	}
}

void ConsoleFixer::updateConsoleText(ConsoleLogEntry::Level level, const char *fmt, va_list argptr)
{
	const char* color = getColorString(level);

	char buffer[4096];
	U32 offset = 0;
	dVsprintf(buffer + offset, sizeof(buffer) - offset, fmt, argptr);

	if (!consoleText.empty())
		consoleText.append("\n");

	consoleText.append(color);
	consoleText.append(buffer);

	Con::setVariable("ConsoleFix::ConsoleText", consoleText.c_str());

	if (initialized)
		Con::executef(1, "onUpdateConsole");
}

ConsoleFixer::ConsoleFixer()
{
	initialized = false;	
}

ConsoleFixer::~ConsoleFixer()
{
	initialized = false;
}
