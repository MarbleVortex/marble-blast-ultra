#include "util.h"

#include <cstdio>
#include <string>

#include <TGE.h>

#ifdef _WIN32
#include <Windows.h>
#endif

using namespace TGE;

S32 dSprintf(char *buffer, U32 bufferSize, const char *format, ...)
{
	va_list args;
	va_start(args, format);

#if defined(TORQUE_COMPILER_CODEWARRIOR)
	S32 len = vsnprintf(buffer, bufferSize, format, args);
#else
	bufferSize;
	S32 len = vsprintf(buffer, format, args);
#endif
	return (len);
}

S32 dVsprintf(char *buffer, U32 bufferSize, const char *format, void *arglist)
{
#if defined(TORQUE_COMPILER_CODEWARRIOR)
	S32 len = vsnprintf(buffer, bufferSize, format, (char*)arglist);
#else
	bufferSize;
	S32 len = vsprintf(buffer, format, (char*)arglist);
#endif
	//   S32 len = vsnprintf(buffer, bufferSize, format, (char*)arglist);
	return (len);
}

// Based on strreplace console function from TSE Milestone 4
const char* strreplace(const char* source, const char *from, const char *to)
{
	S32 fromLen = strlen(from);
	if (!fromLen)
		return source;

	S32 toLen = strlen(to);
	S32 count = 0;
	const char *scan = source;
	while (scan)
	{
		scan = strstr(scan, from);
		if (scan)
		{
			scan += fromLen;
			count++;
		}
	}
	char *ret = TGE::Con::getReturnBuffer(strlen(source) + 1 + (toLen - fromLen) * count);
	U32 scanp = 0;
	U32 dstp = 0;
	for (;;)
	{
		const char *scan = strstr(source + scanp, from);
		if (!scan)
		{
			strcpy(ret + dstp, source + scanp);
			return ret;
		}
		U32 len = scan - (source + scanp);
		strncpy(ret + dstp, source + scanp, len);
		dstp += len;
		strcpy(ret + dstp, to);
		dstp += toLen;
		scanp += len + fromLen;
	}
	return ret;
}
