#include "winconsole.h"

#include <cstdio>
#include <string>
#include <iostream>
#include <thread>
#include <mutex>

#include <TGE.h>
#include <TorqueLib\QuickOverride.h>

#ifdef _WIN32
#include <Windows.h>
#endif

using namespace TGE;

WinConsole *WindowsConsole = NULL;

bool shouldEnable = false;

ConsoleFunction(enableWinConsole, void, 2, 2, "enableWinConsole(bool);")
{
	argc;
	shouldEnable = dAtob(argv[1]);

	//Con::printf("ShouldEnable: %d", shouldEnable);
	//Con::printf("IsEnabled: %d", WinConsole::isEnabled());
	//WindowsConsole->enable(dAtob(argv[1]));
}

void WinConsole::checkEnabled()
{
	if (shouldEnable != winConsoleEnabled)
		WindowsConsole->enable(shouldEnable);
}

void WinConsole::create()
{
	WindowsConsole = new WinConsole();
	//WindowsConsole->enable(true);
}

void WinConsole::destroy()
{
	delete WindowsConsole;
	WindowsConsole = NULL;
}

void WinConsole::enable(bool enabled)
{
	bool wasEnabled = winConsoleEnabled;
	winConsoleEnabled = enabled;
	if (winConsoleEnabled && !wasEnabled)
	{
		AllocConsole();
		const char* title = "MBU Console";
		if (title && *title)
		{
#ifdef UNICODE
			UTF16 buf[512];
			convertUTF8toUTF16((UTF8 *)title, buf, sizeof(buf));
			setConsoleTitle(buf);
#else
			SetConsoleTitle(title);
#endif
		}
		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);
		std::ios::sync_with_stdio();

		//printf("%s", Con::getVariable("Con::Prompt"));
	}
	else if (!winConsoleEnabled && wasEnabled)
	{
		closeWinConsole();
		//WinConsole::destroy();
	}
}

bool WinConsole::isCreated()
{
	if (WindowsConsole)
		return true;

	return false;
}

bool WinConsole::isEnabled()
{
	if (WinConsole::isCreated())
		return WindowsConsole->winConsoleEnabled;

	return false;
}

WinConsole::WinConsole()
{
	winConsoleEnabled = false;
	done = false;
}

WinConsole::~WinConsole()
{
	done = true;
	closeWinConsole();
}

void WinConsole::closeWinConsole()
{
	HWND console = GetConsoleWindow();
	FreeConsole();
	SendMessage(console, WM_CLOSE, 0, 0);
}

void WinConsole::setAttributes(TGE::ConsoleLogEntry::Level level)
{
#ifdef _WIN32
	WORD attributes;
	switch (level)
	{
	case TGE::ConsoleLogEntry::Warning:
		attributes = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
		break;
	case TGE::ConsoleLogEntry::Error:
		attributes = FOREGROUND_RED | FOREGROUND_INTENSITY;
		break;
	default:
		attributes = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
		break;
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), attributes);
#endif
}

void WinConsole::resetAttributes()
{
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
#endif
}

void WinConsole::printf(ConsoleLogEntry::Level level, const char *fmt, va_list argptr)//(const char *s, ...)
{
	setAttributes(level);
	vprintf(fmt, argptr);
	putchar('\n');
	resetAttributes();
}

void WinConsole::processInput()
{
	if (cmdMutex.try_lock()) // NOTE: This is a really bad way of synchronization, but it works
	{
		if (cmdString.length() > 0)
		{
			Con::evaluatef("%s", cmdString.c_str());
			cmdString = "";
		}
		cmdMutex.unlock();
	}
}

bool WinConsole::getInput()
{
	std::string str;
	std::getline(std::cin, str);

	if (done)
		return false;

	cmdMutex.lock();
	cmdString = str;
	cmdMutex.unlock();

	return true;
}

