#ifndef __UTIL_H_
#define __UTIL_H_

#include <TorqueLib\platform\platform.h>

extern S32 dSprintf(char *buffer, U32 bufferSize, const char *format, ...);

extern S32 dVsprintf(char *buffer, U32 bufferSize, const char *format, void *arglist);

// Based on strreplace console function from TSE Milestone 4
extern const char* strreplace(const char* source, const char *from, const char *to);

#endif
