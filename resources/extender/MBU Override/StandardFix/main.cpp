// Demonstration of how to use the function interception mechanism to create an external console window for the game.

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <cstdio>
#include <string>
#include <iostream>
#include <thread>
#include <mutex>
#include <PluginLoader/PluginInterface.h>
#include <TGE.h>
#include <TorqueLib/QuickOverride.h>

#ifdef _WIN32
#include <Windows.h>
#endif

#include "winconsole.h"
#include "consolefix.h"
#include "util.h"

using namespace TGE;

namespace
{
	bool waitProcess = false;

	void inputHandler()
	{
		while (true)
		{
			if (waitProcess)
				continue;
			if (WinConsole::isCreated())
			{
				if (WinConsole::isEnabled())
				{
					if (!WindowsConsole->getInput())
						break;
					waitProcess = true;
				}
				else {
					WindowsConsole->checkEnabled();
				}
			}
		}
	}

	void onClientProcess(U32 timeDelta)
	{
		if (WinConsole::isEnabled())
		{
			WindowsConsole->processInput();
			WindowsConsole->checkEnabled();
			waitProcess = false;
		}
	}
}

// _printf is fastcall on Mac. Ew.
#ifdef __APPLE__
TorqueOverrideFastcall(void, Con::_printf, (TGE::ConsoleLogEntry::Level level, TGE::ConsoleLogEntry::Type type, const char *fmt, va_list argptr), originalPrintf)
#else
TorqueOverride(void, Con::_printf, (TGE::ConsoleLogEntry::Level level,/* TGE::ConsoleLogEntry::Type type,*/ const char *fmt, va_list argptr), originalPrintf)
#endif
{
	if (WinConsole::isEnabled())
		WindowsConsole->printf(level, fmt, argptr);

	if (ConsoleFixer::isCreated())
		ConsoleFix->updateConsoleText(level, fmt, argptr);

	char buffer[4096];
	U32 offset = 0;
	dVsprintf(buffer + offset, sizeof(buffer) - offset, fmt, argptr);

	//char * retBuf = Con::getReturnBuffer(1024);
	//dSprintf(retBuf, 1024, fmt, argptr);

	// Print to CMD
	//::printf(fmt, argptr);
	::printf("%s\n", buffer);

	originalPrintf(level/*, type*/, fmt, argptr);
}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
	WinConsole::create();
	ConsoleFixer::create();
}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	// Call our onClientProcess() function each tick
	plugin->onClientProcess(onClientProcess);
	
	// Create input-handling thread
	std::thread(inputHandler).detach();
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{
	if (WinConsole::isCreated())
		WinConsole::destroy();

	if (ConsoleFixer::isCreated())
		ConsoleFixer::destroy();
}
