#ifndef __WINCONSOLE_H_
#define __WINCONSOLE_H_

#include <TorqueLib\platform\platform.h>
#include <TGE.h>
#include <string>
#include <mutex>

class WinConsole
{
	bool winConsoleEnabled;
	bool done;
	std::mutex cmdMutex;
	std::string cmdString;

	void setAttributes(TGE::ConsoleLogEntry::Level level);
	void resetAttributes();
	void closeWinConsole();

public:
	WinConsole();
	~WinConsole();

	void enable(bool);
	static void create();
	static void destroy();
	static bool isEnabled();
	static bool isCreated();

	void processInput();
	bool getInput();
	void checkEnabled();
	void printf(TGE::ConsoleLogEntry::Level level, const char *fmt, va_list argptr); //(const char *s, ...);
};

extern WinConsole *WindowsConsole;

#endif
