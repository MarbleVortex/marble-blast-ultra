#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>
#include "CodeInjectionStream.h"

using namespace TGE;

ConsoleMethod(PathedInterior, getPathPosition, F32, 2, 2, "getPathPosition()")
{
	return object->getPathPosition();
}

ConsoleMethod(PathedInterior, getTargetPosition, S32, 2, 2, "getTargetPosition()")
{
	return object->getTargetPosition();
}
