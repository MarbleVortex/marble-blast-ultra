#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>
#include "CodeInjectionStream.h"

#include <vector>

#ifdef MBU_DEV

using namespace TGE;
using namespace Con;

#define USE_SPEEDOMETER
#ifdef USE_SPEEDOMETER

TorqueOverrideMember(void, GuiSpeedometerHud::onRender, (TGE::GuiSpeedometerHud *pThis, Point2I offset, const RectI &updateRect), originalGuiSpeedometerHudOnRender)
{
	originalGuiSpeedometerHudOnRender(pThis, offset, updateRect);

	getActiveGFXDevice()->popWorldMatrix();
}

bool isZero(F32 a)
{
	return mFabs(a) < __EQUAL_CONST_F;
}

void fixRenderSpeedometer(GuiSpeedometerHud* pThis, Point2I offset)
{
	Point2F center = pThis->mCenter;
	if (isZero(center.x) && isZero(center.y))
	{
		center.x = pThis->mBounds.extent.x / 2.0f;
		center.y = pThis->mBounds.extent.y / 2.0f;
	}
	MatrixF newMat(1);

	F32 rotation = Float_Pi * (pThis->mMinAngle + (pThis->mMaxAngle - pThis->mMinAngle) * (pThis->mSpeed / pThis->mMaxSpeed)) / 180.0f;
	AngAxisF newRot(Point3F(0.0f, 0.0f, -1.0f), rotation);
	newRot.setMatrix(&newMat);
	//pThis->mBounds.point.x + 
	//pThis->mBounds.point.y + 
	newMat.setPosition(Point3F(offset.x + center.x, offset.y + center.y, 0));
	getActiveGFXDevice()->multWorld(newMat);

	//TGE::Con::errorf("Offset: (%d, %d)!", offset.x, offset.y);
}

extern "C" __declspec(dllexport) __declspec(naked) void overrideSpeedometer()
{
	__asm
	{
		// offset (Point2I)
		mov ecx, [ebp + 24d]; // offset.y
		push ecx;
		mov ecx, [ebp + 20d]; // offset.x
		push ecx;

		push ebx; // pThis
		call fixRenderSpeedometer;
		push 4629A1h; // ret address
		ret;
	}
}

void fixNeedleRender()
{
	bool solid = Con::getBoolVariable("Speedometer::SolidNeedle");
	if (solid)
		PrimBuild::begin(GFXPrimitiveType::GFXTriangleStrip, 5);
	else
		PrimBuild::begin(GFXPrimitiveType::GFXLineStrip, 5);
}

extern "C" __declspec(dllexport) __declspec(naked) void overrideNeedleRender()
{
	__asm
	{
		call fixNeedleRender;
		push 4629F9h; // ret address
		ret;
	}
}

#endif // USE_SPEEDOMETER

#endif // MBU_DEV

//void fixD3D9Reset(D3DPRESENT_PARAMETERS &d3dpp)
//{
//	GFXDeviceD3D9* device = TGE::getActiveGFXDevice();
//	LPDIRECT3DDEVICE9 mD3DDevice = device->getD3DDevice();
//
//	U32 retries = 0;
//	while(mD3DDevice->TestCooperativeLevel() == D3DERR_DEVICELOST && retries < 2)
//	{
//		Con::printf("Attempting to reset device...");
//		Sleep(100);
//		retries++;
//	}
//
//	if (retries < 2)
//	{
//		mD3DDevice->Reset(&d3dpp);
//	}
//	Con::printf("Device lost");
//}
//
//extern "C" __declspec(dllexport) __declspec(naked) void overrideResetD3D9()
//{
//	__asm
//	{
//		mov ecx, [esp + 10h]; // d3dpp
//		push ecx;
//
//		call fixD3D9Reset;
//		push 4EFAD7h; // ret address
//		ret;
//	}
//}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
#ifdef MBU_DEV
#ifdef USE_SPEEDOMETER
	// Fix Needle Rendering
	void* addr = reinterpret_cast<void*>(0x462929);
	CodeInjection::CodeInjectionStream stream(addr, 0x78);
	stream.writeRel32Jump((void*)overrideSpeedometer);

	// Patch the cast to ShapeBase from Vehicle
	void* addr2 = reinterpret_cast<void*>(0x46284B);
	CodeInjection::CodeInjectionStream stream2(addr2, 0x05);
	U8 data[5];
	/*
	// Patch to Marble
	data[0] = 0x68;
	data[1] = 0xF8;
	data[2] = 0xEA;
	data[3] = 0x7A;
	data[4] = 0x00;
	*/

	// Patch to ShapeBase
	data[0] = 0x68;
	data[1] = 0x4C;
	data[2] = 0xE0;
	data[3] = 0x7A;
	data[4] = 0x00;
	stream2.write(data, 0x05);

	void* addr3 = reinterpret_cast<void*>(0x4629F0);
	CodeInjection::CodeInjectionStream stream3(addr3, 0x09);
	stream3.writeRel32Jump((void*)overrideNeedleRender);

#endif // USE_SPEEDOMETER

	// TODO: MOVE THIS
	TGE::defLogFileName = "console_dev.log";
#endif // MBU_DEV

	void* addr4 = reinterpret_cast<void*>(TGEPATCHADDR_SECONDARYWINDOW);
	CodeInjection::CodeInjectionStream stream4(addr4, 1);
	U8 data4[1];
	data4[0] = 0xEB;
	stream4.write(data4, 1);

	
	// 4EFA9C

	/*void* addr5 = reinterpret_cast<void*>(0x4EFA9C);
	CodeInjection::CodeInjectionStream stream5(addr5, 0x3B);
	stream5.writeRel32Jump((void*)overrideResetD3D9);*/
	
}

#ifdef MBU_DEV

/*void fixRenderSpeedometer(GuiSpeedometerHud* pThis)
{
	/*Point2F center = pThis->mCenter;
	if (isZero(center.x) && isZero(center.y))
	{
		center.x = pThis->mBounds.extent.x / 2.0f;
		center.y = pThis->mBounds.extent.y / 2.0f;
	}
	MatrixF newMat(1);

	F32 rotation = pThis->mMinAngle + (pThis->mMaxAngle - mMinAngle) * (pThis->mSpeed / pThis->mMaxSpeed);
	AngAxisF newRot(Point3F(0.0f, 0.0f, -1.0f), rotation);
	newRot.setMatrix(&newMat);

	newMat.setPosition(Point3F(pThis->mBounds.point.x + center.x, pThis->mBounds.point.y + center.y, 0));
	GFX->multWorld(newMat);*/

	//Con::errorf("I am here: %d!", (int)pThis);
//}*/

/*extern "C" __declspec(dllexport) __declspec(naked) void overrideSpeedometer()
{
	__asm
	{
		push ebx;
		call fixRenderSpeedometer;
		push 4629A1h;
		ret;
	}
}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
	void* addr = reinterpret_cast<void*>(0x462929);
	CodeInjection::CodeInjectionStream stream(addr, 0x78);
	stream.writeRel32Jump((void*)overrideSpeedometer);
}*/

#endif // MBU_DEV