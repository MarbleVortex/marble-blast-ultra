#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

#include <vector>

using namespace TGE;
using namespace Con;

namespace
{
	void onClientProcess(U32 timeDelta)
	{
		//TGE::NetConn* connection = getConnectionToServer();
		//if (connection)
		char* deltaArg = Con::getIntArg(timeDelta);
		Con::executef(2, "onFrameAdvance", deltaArg);
	}
}

// -------------------------------------------------------------
// This is to fix the window for windowed mode and fullscreen
// to use the correct dwStyle
// without this the fullscreen window has invisible window
// borders that cause issues clicking on buttons
// it is possible to enable the maximize and resize options
// by defining ALLOW_RESIZE if wanted.
// -------------------------------------------------------------

#ifdef POTATO

/* -- Uncomment to enable resizing -- */
//#define ALLOW_RESIZE 

//void *FIXCRASHADDR = reinterpret_cast<void*>(0x5F19F3);

void *PARENTPUSHADDR = reinterpret_cast<void*>(0x5F2CC1);

void *WINDOWEXFLAGSADDR = reinterpret_cast<void*>(0x5F2CD1);
void *WINDOWEDSTYLEADDR = reinterpret_cast<void*>(0x5F2CB3);
void *FULLSCREENSTYLEADDR = reinterpret_cast<void*>(0x5F2CBA);

void *JMPFROMFIXWINDOW = reinterpret_cast<void*>(0x5F2C9E);
void *JMPBACKFIXWINDOW = reinterpret_cast<void*>(0x5F2CA4);
__declspec(naked) void FIXWINDOW()
{
	__asm
	{
		movzx ecx, byte ptr[eax + 10h]
		mov eax, [eax + 4]
		jmp JMPBACKFIXWINDOW
	}
}

/*struct Point2I_
{
	S32 x, y;
};
struct GFXVideoMode
{
	Point2I_ resolution;
	U32 bitDepth;
	U32 refreshRate;
	bool fullScreen;
	//bool wideScreen;
	//U32 antialiasLevel;
};


//LONG w32w;

void *JMPFROMFIXWINDOW = reinterpret_cast<void*>(0x5F2C99);
void *JMPBACKFIXWINDOW = reinterpret_cast<void*>(0x5F2CDB);
extern "C" HWND __cdecl makeWindow(GFXVideoMode* mode, HWND parent)
{
	int dwStyle = WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	int dwExStyle = 0;
	HWND hwnd;
	//__asm pusha;

	//HWND parent;

	//GFXVideoMode mode;
	//__asm mov eax, [ebp + 12];
	//__asm mov eax, [eax];
	//__asm mov[mode], eax;
	//__asm mov ecx, [edi + 12];
	//__asm mov[parent], ecx;

	int x = 0;
	int y = 0;

	if (parent) 
	{
		// Browser?
		dwStyle |= WS_VISIBLE | WS_CHILDWINDOW;
	}
	else if (mode->fullScreen) 
	{
		// Fullscreen?
		dwStyle |= WS_POPUP | WS_MAXIMIZE;
	}
	else
	{
		// Windowed?
		dwStyle |= WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
#ifdef ALLOW_RESIZE
		dwStyle |= WS_MAXIMIZEBOX | WS_THICKFRAME;
#endif
		x = CW_USEDEFAULT;
		y = CW_USEDEFAULT;
	}

	hwnd = CreateWindowExW(dwExStyle, L"Marble Blast Ultra", L"Marble Blast Ultra", dwStyle, x, y, mode->resolution.x, mode->resolution.y, parent, NULL, NULL, NULL);

	//__asm mov eax, hwnd
	//__asm mov[esi + 200], eax;
	//__asm mov w32w, esi;

	//SetWindowLongW(hwnd, GWLP_USERDATA, (LONG)w32w);

	//__asm popa;
	//__asm jmp JMPBACKFIXWINDOW;

	return hwnd;
}

__declspec(naked) void FIXWINDOW()
{
	__asm push[edi + 12]; // Parent HWND
	__asm push[ebp + 12]; // GFXVideoMode
	__asm call makeWindow; // returns hwnd inside eax
	__asm jmp JMPBACKFIXWINDOW;
}*/

#ifdef ALLOW_RESIZE
int windowedStyle = WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME;
#else
int windowedStyle = WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
#endif
int fullScreenStyle = WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_POPUP | WS_MAXIMIZE;

int windowEXFlags = 0;

// Patches a jmp call into the code
void patchJMP(void* from, void* target)
{
	const int sizeofjmp = 5;
	int jmpAddr = (int)target - ((int)from + sizeofjmp);
	((unsigned char*)from)[0] = 0xE9; // jmp
	*(int*)&((char*)from)[1] = jmpAddr; // addr
}

void patchInt(void* location, int value)
{
	int* loc = (int*)location;
	*loc = value;
}

void applyWindowFix()
{
	patchJMP(JMPFROMFIXWINDOW, &FIXWINDOW);


	patchInt(WINDOWEDSTYLEADDR, windowedStyle);
	patchInt(FULLSCREENSTYLEADDR, fullScreenStyle);
	patchInt(WINDOWEXFLAGSADDR, windowEXFlags);
	((char*)PARENTPUSHADDR)[0] = 0x53; // push ebx

	//// Prevent Crash
	//((char*)FIXCRASHADDR)[0] = 0xE9; // jmp
}

// -------------------------------------------------------------

//PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
//{
	//applyWindowFix();
//}

#endif // POTATO

/*const char* newDefLogFileName = "console_dev.log";

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
	TGE::defLogFileName = &newDefLogFileName;
}*/

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	// Call our onClientProcess() function each tick
	plugin->onClientProcess(onClientProcess);
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{

}
