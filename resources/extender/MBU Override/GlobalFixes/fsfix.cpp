#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

#include <vector>

using namespace TGE;
using namespace Con;

LPDIRECT3DDEVICE9 getD3DDevice(GFXD3D9Device* pThis)
{
	char* class_start = (char*)pThis;
	char* class_offs = class_start + 0x44BC;
	LPDIRECT3DDEVICE9 device = *((LPDIRECT3DDEVICE9*)class_offs);

	return device;
}

GFXResource *getResourceListHead(GFXD3D9Device* pThis)
{
	char* class_start = (char*)pThis;
	char* class_offs = class_start + 0x220;
	GFXResource *resourceListHead = ((GFXResource*)class_offs);

	return resourceListHead;
}

TorqueOverrideMember(bool, GFXD3D9Device::checkDevice, (GFXD3D9Device* pThis), originalCheckDevice)
{
	LPDIRECT3DDEVICE9 mD3DDevice = getD3DDevice(pThis);
	// Make sure we have a device
	HRESULT res = mD3DDevice->TestCooperativeLevel();

	S32 attempts = 0;
	const S32 MaxAttempts = 40;
	const S32 SleepMsPerAttempt = 50;

	while (res == D3DERR_DEVICELOST && attempts < MaxAttempts)
	{
		// Lost device! Just keep querying
		res = mD3DDevice->TestCooperativeLevel();

		//Con::warnf("GFXD3D9Device::checkDevice - Device lost, skipping reset (will retry later)");
		Con::warnf("GFXD3D9Device::checkDevice - Device needs to be reset, waiting on device...");

		Sleep(SleepMsPerAttempt);
		attempts++;
	}

	if (attempts >= MaxAttempts && res == D3DERR_DEVICELOST)
	{
		Con::errorf("GFXD3D9Device::checkDevice - Device lost and reset wait time exceeded, skipping reset (will retry later)");
		pThis->setCanCurrentlyRender(false);
		return false;
	}

	// Trigger a reset if we can't get a good result from TestCooperativeLevel.
	if (res == D3DERR_DEVICENOTRESET)
	{
		Con::warnf("GFXD3D9Device::checkDevice - Device needs to be reset, resetting device...");

		//originalCheckDevice(pThis);

		// Temp
		Con::errorf("GFXD3D9Device::checkDevice - Failed to reset device.");
	}

	pThis->setCanCurrentlyRender(true);

	return pThis->getCanCurrentlyRender();
}

bool isFullscreen(GuiCanvas* canvas)
{
	return canvas->getPlatformWindow()->getCurrentMode().fullScreen;
}

bool renderEnabled = true;

void checkShouldRender()
{
	GuiCanvas* canvas = (GuiCanvas*)Sim::findObject("Canvas");
	if (canvas && !isFullscreen(canvas))
	{
		renderEnabled = true;
		return;
	}

	GFXDeviceD3D9* device = TGE::getActiveGFXDevice();
	LPDIRECT3DDEVICE9 mD3DDevice = device->getD3DDevice();

	D3DDEVICE_CREATION_PARAMETERS pp;
	mD3DDevice->GetCreationParameters(&pp);

	HWND activeWindow = GetActiveWindow();

	renderEnabled = activeWindow == pp.hFocusWindow;
}

TorqueOverrideMember(bool, GFXD3D9Device::checkDeviceLost1, (GFXD3D9Device* pThis), originalCheckDeviceLost1)
{
	checkShouldRender();
	if (renderEnabled)
		return originalCheckDeviceLost1(pThis);
	else
		return false;
}

TorqueOverrideMember(bool, GFXD3D9Device::checkDeviceLost2, (GFXD3D9Device* pThis), originalCheckDeviceLost2)
{
	checkShouldRender();
	if (renderEnabled)
		return originalCheckDeviceLost2(pThis);
	else
		return false;
}

/*ConsoleFunction(setRenderEnabled, void, 3, 3, "setRenderEnabled(flag)")
{
	renderEnabled = dAtob(argv[2]);
}

ConsoleFunction(isRenderEnabled, bool, 2, 2, "isRenderEnabled()")
{
	return renderEnabled;
}*/

TorqueOverrideMember(bool, Win32Window::windowProc, (TGE::Win32Window* pThis, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam), originalWindowProc)
{
	if (Msg == WM_DISPLAYCHANGE)
	{
		Win32Window* window = (Win32Window *)GetWindowLongW(hWnd, -21);
		if (window && window->isVisible() && !window->getSupressReset() && window->getCurrentMode().bitDepth != wParam)
		{
			Con::warnf("Win32Window::WindowProc - resetting device due to display mode BPP change.");
			//window->getGFXTarget()->resetMode();
		}
		// Queue up for later and invoke the Windows default handler.
		Dispatch(DelayedDispatch, hWnd, Msg, wParam, lParam);
		return DefWindowProc(hWnd, Msg, wParam, lParam);
	}
	else {
		return originalWindowProc(pThis, hWnd, Msg, wParam, lParam);
	}
}

/*
 *
 * In windowProc the wParam != SIZE_MINIMIZED is not there in MBU for these two

if (wParam != SIZE_MINIMIZED && !Journal::IsPlaying())
	Dispatch( ImmediateDispatch, hWnd,message,wParam,lParam );

if(wParam != SIZE_MINIMIZED && window != NULL )
{
	if(!window->mVideoMode.fullScreen)
		window->mVideoMode.resolution.set(LOWORD(lParam),HIWORD(lParam));

	if(window->getGFXTarget())
	{
		Con::warnf("Win32Window::WindowProc - resetting device due to window size change.");
		window->getGFXTarget()->resetMode();
	}
}

 */

//from 5F2C9E
//  movzx   ecx, byte ptr [eax+10h]
//  mov     eax, [eax+4]
//to 5F2CA4

