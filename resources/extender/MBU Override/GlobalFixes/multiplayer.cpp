//#define MULTIFIX
#ifdef MULTIFIX

#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

#include <vector>

using namespace TGE;
using namespace Con;

char *__cdecl __RTDynamicCast(int a1, int a2, /*struct _s_RTTICompleteObjectLocator*/ void *a3, /*struct TypeDescriptor *a4*/ void * a4, int a5);

#define dStrdup(x) dStrdup_r(x)//, __FILE__, __LINE__)
char *dStrdup_r(const char *src)//, const char *fileName, U32 lineNumber)
{
	int sz = strlen(src) + 1;
	char *buffer = (char *)malloc(sz);
	strcpy_s(buffer, sz, src);
	return buffer;
}


#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

inline U32 rotlFixed(U32 x, unsigned int y)
{
	return (x >> y) | (x << (32 - y));
}

#define MD5STEP(f, w, x, y, z, data, s) w = rotlFixed(w + f(x, y, z) + data, s) + x

U32 mRandomHashData[12];
bool mRandomDataInitialized = false;

void computeNetMD5(NetInterface* pThis, const NetAddress *address, U32 connectSequence, U32 digest[4])
{
	digest[0] = 0x67452301L;
	digest[1] = 0xefcdab89L;
	digest[2] = 0x98badcfeL;
	digest[3] = 0x10325476L;


	U32 a, b, c, d;

	a = digest[0];
	b = digest[1];
	c = digest[2];
	d = digest[3];

	U32 in[16];
	in[0] = address->type;
	in[1] = (U32(address->netNum[0]) << 24) |
		(U32(address->netNum[1]) << 16) |
		(U32(address->netNum[2]) << 8) |
		(U32(address->netNum[3]));
	in[2] = address->port;
	in[3] = connectSequence;
	for (U32 i = 0; i < 12; i++)
		in[i + 4] = mRandomHashData[i];

	MD5STEP(F1, a, b, c, d, in[0] + 0xd76aa478, 7);
	MD5STEP(F1, d, a, b, c, in[1] + 0xe8c7b756, 12);
	MD5STEP(F1, c, d, a, b, in[2] + 0x242070db, 17);
	MD5STEP(F1, b, c, d, a, in[3] + 0xc1bdceee, 22);
	MD5STEP(F1, a, b, c, d, in[4] + 0xf57c0faf, 7);
	MD5STEP(F1, d, a, b, c, in[5] + 0x4787c62a, 12);
	MD5STEP(F1, c, d, a, b, in[6] + 0xa8304613, 17);
	MD5STEP(F1, b, c, d, a, in[7] + 0xfd469501, 22);
	MD5STEP(F1, a, b, c, d, in[8] + 0x698098d8, 7);
	MD5STEP(F1, d, a, b, c, in[9] + 0x8b44f7af, 12);
	MD5STEP(F1, c, d, a, b, in[10] + 0xffff5bb1, 17);
	MD5STEP(F1, b, c, d, a, in[11] + 0x895cd7be, 22);
	MD5STEP(F1, a, b, c, d, in[12] + 0x6b901122, 7);
	MD5STEP(F1, d, a, b, c, in[13] + 0xfd987193, 12);
	MD5STEP(F1, c, d, a, b, in[14] + 0xa679438e, 17);
	MD5STEP(F1, b, c, d, a, in[15] + 0x49b40821, 22);

	MD5STEP(F2, a, b, c, d, in[1] + 0xf61e2562, 5);
	MD5STEP(F2, d, a, b, c, in[6] + 0xc040b340, 9);
	MD5STEP(F2, c, d, a, b, in[11] + 0x265e5a51, 14);
	MD5STEP(F2, b, c, d, a, in[0] + 0xe9b6c7aa, 20);
	MD5STEP(F2, a, b, c, d, in[5] + 0xd62f105d, 5);
	MD5STEP(F2, d, a, b, c, in[10] + 0x02441453, 9);
	MD5STEP(F2, c, d, a, b, in[15] + 0xd8a1e681, 14);
	MD5STEP(F2, b, c, d, a, in[4] + 0xe7d3fbc8, 20);
	MD5STEP(F2, a, b, c, d, in[9] + 0x21e1cde6, 5);
	MD5STEP(F2, d, a, b, c, in[14] + 0xc33707d6, 9);
	MD5STEP(F2, c, d, a, b, in[3] + 0xf4d50d87, 14);
	MD5STEP(F2, b, c, d, a, in[8] + 0x455a14ed, 20);
	MD5STEP(F2, a, b, c, d, in[13] + 0xa9e3e905, 5);
	MD5STEP(F2, d, a, b, c, in[2] + 0xfcefa3f8, 9);
	MD5STEP(F2, c, d, a, b, in[7] + 0x676f02d9, 14);
	MD5STEP(F2, b, c, d, a, in[12] + 0x8d2a4c8a, 20);

	MD5STEP(F3, a, b, c, d, in[5] + 0xfffa3942, 4);
	MD5STEP(F3, d, a, b, c, in[8] + 0x8771f681, 11);
	MD5STEP(F3, c, d, a, b, in[11] + 0x6d9d6122, 16);
	MD5STEP(F3, b, c, d, a, in[14] + 0xfde5380c, 23);
	MD5STEP(F3, a, b, c, d, in[1] + 0xa4beea44, 4);
	MD5STEP(F3, d, a, b, c, in[4] + 0x4bdecfa9, 11);
	MD5STEP(F3, c, d, a, b, in[7] + 0xf6bb4b60, 16);
	MD5STEP(F3, b, c, d, a, in[10] + 0xbebfbc70, 23);
	MD5STEP(F3, a, b, c, d, in[13] + 0x289b7ec6, 4);
	MD5STEP(F3, d, a, b, c, in[0] + 0xeaa127fa, 11);
	MD5STEP(F3, c, d, a, b, in[3] + 0xd4ef3085, 16);
	MD5STEP(F3, b, c, d, a, in[6] + 0x04881d05, 23);
	MD5STEP(F3, a, b, c, d, in[9] + 0xd9d4d039, 4);
	MD5STEP(F3, d, a, b, c, in[12] + 0xe6db99e5, 11);
	MD5STEP(F3, c, d, a, b, in[15] + 0x1fa27cf8, 16);
	MD5STEP(F3, b, c, d, a, in[2] + 0xc4ac5665, 23);

	MD5STEP(F4, a, b, c, d, in[0] + 0xf4292244, 6);
	MD5STEP(F4, d, a, b, c, in[7] + 0x432aff97, 10);
	MD5STEP(F4, c, d, a, b, in[14] + 0xab9423a7, 15);
	MD5STEP(F4, b, c, d, a, in[5] + 0xfc93a039, 21);
	MD5STEP(F4, a, b, c, d, in[12] + 0x655b59c3, 6);
	MD5STEP(F4, d, a, b, c, in[3] + 0x8f0ccc92, 10);
	MD5STEP(F4, c, d, a, b, in[10] + 0xffeff47d, 15);
	MD5STEP(F4, b, c, d, a, in[1] + 0x85845dd1, 21);
	MD5STEP(F4, a, b, c, d, in[8] + 0x6fa87e4f, 6);
	MD5STEP(F4, d, a, b, c, in[15] + 0xfe2ce6e0, 10);
	MD5STEP(F4, c, d, a, b, in[6] + 0xa3014314, 15);
	MD5STEP(F4, b, c, d, a, in[13] + 0x4e0811a1, 21);
	MD5STEP(F4, a, b, c, d, in[4] + 0xf7537e82, 6);
	MD5STEP(F4, d, a, b, c, in[11] + 0xbd3af235, 10);
	MD5STEP(F4, c, d, a, b, in[2] + 0x2ad7d2bb, 15);
	MD5STEP(F4, b, c, d, a, in[9] + 0xeb86d391, 21);

	digest[0] += a;
	digest[1] += b;
	digest[2] += c;
	digest[3] += d;
}

// Fix Multiplayer On Dev EXE
#ifdef MBU_DEV

enum PacketTypes
{
	MasterServerGameTypesRequest = 2,
	MasterServerGameTypesResponse = 4,
	MasterServerListRequest = 6,
	MasterServerListResponse = 8,
	GameMasterInfoRequest = 10,
	GameMasterInfoResponse = 12,
	GamePingRequest = 14,
	GamePingResponse = 16,
	GameInfoRequest = 18,
	GameInfoResponse = 20,
	GameHeartbeat = 22,
	GGCPacket = 24,
	ConnectChallengeRequest = 26,
	ConnectChallengeReject = 28,
	ConnectChallengeResponse = 30,
	ConnectRequest = 32,
	ConnectReject = 34,
	ConnectAccept = 36,
	Disconnect = 38,
};

enum NetConnectionState
{
	NotConnected,
	AwaitingChallengeResponse, ///< We've sent a challenge request, awaiting the response.
	AwaitingConnectRequest,    ///< We've received a challenge request and sent a challenge response.
	AwaitingConnectResponse,   ///< We've received a challenge response and sent a connect request.
	Connected,                 ///< We've accepted a connect request, or we've received a connect response accept.
};

U32 mConnectSendCount = 0;
U32 mConnectLastSendTime = 0;
U32 mAddressDigest[4];

/*void writeByte(BitStream* out, U8 data)
{
bool(*_writeByte) (const U32, const void*) = (bool(*) (const U32, const void*))(out + 8);//reinterpret_cast<bool(*) (const U32 numBytes, const void* data)>(out + 8);
_writeByte(sizeof(U8), &data);
}*/

//U32 getInt(RawData packetData, U32& index)
//{
	/*U8 b4 = packetData.data[index++];
	U8 b3 = packetData.data[index++];
	U8 b2 = packetData.data[index++];
	U8 b1 = packetData.data[index++];

	return (b4 << 24) + (b3 << 16) + (b2 << 8) + (b1 << 0);*/

	//U32* val = (U32*)&(packetData.data[index]);
	//index += 4;

	//return *val;
//}

void sendConnectChallengeRequest(TGE::NetInterface *pThis, TGE::NetConn *conn)
{
	Con::printf("Sending Connect challenge Request");

	BitStream *out = BStream::getPacketStream();

	U8 req = (U8)ConnectChallengeRequest;

	out->write(sizeof(U8), &req);
	U32 sequence = conn->getSequence();
	out->write(sizeof(U32), &sequence);

	//conn->mConnectSendCount++;
	//conn->mConnectLastSendTime = Platform::getVirtualMilliseconds();

	mConnectSendCount++;
	mConnectLastSendTime = Platform::getVirtualMilliseconds();

	BStream::sendPacketStream(conn->getNetAddress());
}

TorqueOverrideMember(void, NetInterface::startConnection, (TGE::NetInterface *pThis, TGE::NetConn *conn), originalStartConnection)
{
	// Call original function
	originalStartConnection(pThis, conn);

	// Calls conn->setIsConnectionToServer();
	//*((int*)conn + 0x46) |= 1u;

	// Send Request
	sendConnectChallengeRequest(pThis, conn);
}

void handleInfoPacket(TGE::NetInterface *pThis, const NetAddress *address, U8 packetType, BitStream *stream)
{
	char buf[256];
	Net::addressToString(address, buf);
	Con::warnf("DEBUG: Received Info Packet from %s with type %d", buf, packetType);
}

//U32 addressDigest[4];

void initRandomData()
{
	mRandomDataInitialized = true;
	U32 seed = GetTickCount();//Platform::getRealMilliseconds();

	/*if (Journal::IsPlaying())
		Journal::Read(&seed);
	else if (Journal::IsRecording())
		Journal::Write(seed);*/

	MRandomR250 myRandom(seed);
	for (U32 i = 0; i < 12; i++)
		mRandomHashData[i] = myRandom.randI();
}

void handleConnectChallengeRequest(TGE::NetInterface *pThis, const NetAddress *addr, BitStream *stream)//, RawData packetData, U32& index)
{
	char buf[256];
	Net::addressToString(addr, buf);
	Con::printf("Got Connect challenge Request from %s", buf);
	//if (!mAllowConnections)
	//	return;

	U32 connectSequence;
	stream->read(sizeof(U32), &connectSequence);

	//if (!mRandomDataInitialized)
	//	initRandomData();

	U32 addressDigest[4];
	computeNetMD5(pThis, addr, connectSequence, addressDigest);

	// --- old start ---
	//U32 connectSequence = getInt(packetData, index);//packetData.data[index++];


	//addressDigest[0] = 0;
	//addressDigest[1] = 10;
	//addressDigest[2] = 45;
	//addressDigest[3] = 77;
	//computeNetMD5(addr, connectSequence, addressDigest);
	// --- old end ---

	BitStream *out = BStream::getPacketStream();

	U8 res = (U8)ConnectChallengeResponse;

	out->write(sizeof(U8), &res);
	out->write(sizeof(U32), &connectSequence);
	out->write(sizeof(U32), &addressDigest[0]);
	out->write(sizeof(U32), &addressDigest[1]);
	out->write(sizeof(U32), &addressDigest[2]);
	out->write(sizeof(U32), &addressDigest[3]);

	BStream::sendPacketStream(addr);
}

//bool readConnectRequest(NetConn *pThis, BitStream *stream, const char **errorString)//, RawData packetData, U32& index)
//{
//	//conn->setFailThing(true);
//	//U32 classGroup = getInt(packetData, index);
//	//U32 classCRC = getInt(packetData, index);
//	//stream->read(&classGroup);
//	//stream->read(&classCRC);
//
//	U32 classGroup, classCRC;
//	stream->read(sizeof(U32), &classGroup);
//	stream->read(sizeof(U32), &classCRC);
//
//	//index += 255;
//
//	/*if (classGroup != mNetClassGroup || classCRC != AbstractClassRep::getClassCRC(mNetClassGroup))
//	{
//		*errorString = "CHR_INVALID";
//		return false;
//	}*/
//
//	U32 currentProtocol, minProtocol;
//	char gameString[256];
//	stream->readString(gameString);
//	if (strcmp(gameString, "Marble Blast Ultra"))
//	{
//		*errorString = "CHR_GAME";
//		return false;
//	}
//
//	stream->read(sizeof(U32), &currentProtocol);
//	stream->read(sizeof(U32), &minProtocol);
//
//	char joinPassword[256];
//	stream->readString(joinPassword);
//
//	if (currentProtocol < 1000)
//	{
//		*errorString = "CHR_PROTOCOL_LESS";
//		return false;
//	}
//	if (minProtocol > 1000)
//	{
//		*errorString = "CHR_PROTOCOL_GREATER";
//		return false;
//	}
//	//pThis->setProtocolVersion(1000);
//
//	const char *serverPassword = Con::getVariable("Pref::Server::Password");
//	if (serverPassword[0])
//	{
//		if (strcmp(joinPassword, serverPassword))
//		{
//			*errorString = "CHR_PASSWORD";
//			return false;
//		}
//	}
//
//	U32 connectArgc;
//	stream->read(sizeof(U32), &connectArgc);
//	pThis->setConnectArgc(connectArgc);
//	
//	if (pThis->getConnectArgc() > 16)
//	{
//		*errorString = "CR_INVALID_ARGS";
//		return false;
//	}
//	const char *connectArgv[16 + 3];
//	for (U32 i = 0; i < pThis->getConnectArgc(); i++)
//	{
//		char argString[256];
//		stream->readString(argString);
//		pThis->getConnectArgv()[i] = dStrdup(argString);
//		connectArgv[i + 3] = pThis->getConnectArgv()[i];
//	}
//	connectArgv[0] = "onConnectRequest";
//	char buffer[256];
//	Net::addressToString(pThis->getNetAddress(), buffer);
//	connectArgv[2] = buffer;
//
//	const char *ret = Con::execute(pThis, pThis->getConnectArgc() + 3, connectArgv);
//	if (ret[0])
//	{
//		*errorString = ret;
//		return false;
//	}
//	return true;
//
//
//	//U32 currentProtocol = getInt(packetData, index);
//	//U32 minProtocol = getInt(packetData, index);
//
//	/*index += 255;
//
//	int conArgc = getInt(packetData, index);//16;//getInt(packetData, index);
//
//	char** argv = conn->getConnectArgv();
//
//	const char *connectArgv[16 + 3];
//	for (int i = 0; i < conArgc; i++)
//	{
//		//char argString[256];
//		//stream->readString(argString);
//		//char* argString = "blorp";//(char*)&packetData.data[index]; //"GameConnection";//"NetConnection";//
//		char* argString = (char*)&packetData.data[index];
//		index += 255;
//		//argv[i] = argString;//dStrdup(argString);
//		connectArgv[i + 3] = argv[i];
//	}
//	connectArgv[0] = "onConnectRequest";
//	char buffer[256];
//	Net::addressToString(conn->getNetAddress(), buffer);
//	connectArgv[2] = buffer;
//	const char *ret = Con::execute(conn, conArgc + 3, connectArgv);
//	if (ret[0])
//	{
//		*errorString = ret;
//		return false;
//	}
//	//if (classGroup == mNetClassGroup && classCRC == AbstractClassRep::getClassCRC(mNetClassGroup))
//	return true;*/
//
//	//*errorString = "CHR_INVALID";
//	//return false;
//}

void writeConnectAccept(BitStream *stream)
{
	//stream;
	U32 v = 1000;
	stream->write(sizeof(U32), &v);
}

void sendConnectAccept(NetConn *conn)
{
	BitStream *out = BStream::getPacketStream();
	U8 acc = (U8)ConnectAccept;
	out->write(sizeof(U8), &acc);
	U32 seq = conn->getSequence();
	out->write(sizeof(U32), &seq);
	writeConnectAccept(out);
	BStream::sendPacketStream(conn->getNetAddress());
}

enum NetConnectionFlags
{
	ConnectionToServer = BIT(0),
	ConnectionToClient = BIT(1),
	LocalClientConnection = BIT(2),
	NetworkConnection = BIT(3),
};

/*TorqueOverrideMember(void, Stream::writeTabs, (TGE::Stream *pThis, U32 count), originalWriteTabs)
{
	Con::warnf("Stream::writeTabs: Disabled for Debugging with MBUExtender");
}*/

/*TorqueOverrideMember(bool, SimObject::save, (TGE::SimObject *pThis, const char* pcFileName, bool bOnlySelected), originalSave)
{
	Con::warnf("SimObject::save: Disabled for Debugging with MBUExtender");
	return true;
}*/

//U32  checkString(NetStringHandle &string, bool *isOnOtherSide = NULL)
// 0x5B6A87

inline S32 dAtoi(const char *str)
{
	return strtol(str, NULL, 10);
}

void handleConnectRequest(TGE::NetInterface *pThis, const NetAddress *address, BitStream *stream)//, RawData packetData, U32& index)
{
	//if (!mAllowConnections)
	//return;
	Con::printf("Got Connect Request");
	//U32 connectSequence = getInt(packetData, index);//packetData.data[index+=32];

													// see if the connection is in the main connection table:

													/*NetConn *connect = NetConnection::lookup(address);
													if (connect && connect->getSequence() == connectSequence)
													{
													sendConnectAccept(connect);
													return;
													}*/
	U32 connectSequence;
	stream->read(sizeof(U32), &connectSequence);

	NetConn* connect = NetConnection::lookup(address);
	if (connect && connect->getSequence() == connectSequence)
	{
		Con::printf("HERE: Send");
		sendConnectAccept(connect);
		return;
	}

	U32 addressDigest[4];
	U32 computedAddressDigest[4];

	stream->read(sizeof(U32), &addressDigest[0]);
	stream->read(sizeof(U32), &addressDigest[1]);
	stream->read(sizeof(U32), &addressDigest[2]);
	stream->read(sizeof(U32), &addressDigest[3]);

	computeNetMD5(pThis, address, connectSequence, computedAddressDigest);
	if (addressDigest[0] != computedAddressDigest[0] ||
		addressDigest[1] != computedAddressDigest[1] ||
		addressDigest[2] != computedAddressDigest[2] ||
		addressDigest[3] != computedAddressDigest[3])
		return; // bogus connection attempt

	if (connect)
	{
		if (connect->getSequence() > connectSequence)
		{
			Con::printf("HERE");
			return; // the existing connection should be kept - the incoming request is stale.
		}
		else
			connect->deleteObject(); // disconnect this one, and allow the new one to be created.
	}

	//addressDigest[0] = getInt(packetData, index);
	//addressDigest[1] = getInt(packetData, index);
	//addressDigest[2] = getInt(packetData, index);
	//addressDigest[3] = getInt(packetData, index);

	/*computeNetMD5(address, connectSequence, computedAddressDigest);
	if (addressDigest[0] != computedAddressDigest[0] ||
	addressDigest[1] != computedAddressDigest[1] ||
	addressDigest[2] != computedAddressDigest[2] ||
	addressDigest[3] != computedAddressDigest[3])
	return; // bogus connection attempt*/

	/*if (connect)
	{
	if (connect->getSequence() > connectSequence)
	return; // the existing connection should be kept - the incoming request is stale.
	else
	connect->deleteObject(); // disconnect this one, and allow the new one to be created.
	}*/

	//char *connectionClass = "GameConnection";//(char*)&packetData.data[index]; //"GameConnection";//"NetConnection";//
	//index += 255;
	//(NetConn*)(co);//

	char connectionClass[255];
	stream->readString(connectionClass);

	// Pointers to variables in an extension can't be accessed from within the game process???
	// Hmmmmm...

	void *co = ConObj::create(connectionClass);
	//Con::printf("THING: %s", connectionClass);
	//NetConn *conn = (NetConn*)co;// dynamic_cast<NetConn *>(co);

	//void* simObjectClass = reinterpret_cast<void*>(0x7AE034);
	void* consoleObjectClass = reinterpret_cast<void*>(0x7AE148);
	void* netConnectionClass = reinterpret_cast<void*>(0x7AE0D8);

	NetConn *conn = (NetConn*)___RTDynamicCast((int)co, 0, consoleObjectClass, netConnectionClass, 0);
	if (!conn)// || !conn->canRemoteCreate())
	{
		Con::printf("HERE: NOPE");
		delete co;
		return;
	}

	// this crashes the game... (fixed)
	conn->registerObject();
	
	conn->setNetAddress(address); // < now this crashes T_T
	//conn->setNetworkConnection(true);

	char* class_start = (char*)conn;
	char* class_offs = class_start + 0x118;
	BitSet32 *flags = ((BitSet32*)class_offs);
	flags->set(BitSet32(NetworkConnection), true);

	conn->setSequence(connectSequence);

	const char *errorString = NULL;
	if (!conn->readConnectRequest(stream, &errorString))//, packetData, index))
	{
		//sendConnectReject(conn, errorString);
		Con::warnf("It got rejected... :(, %s", errorString);
		conn->deleteObject();
		return;
	}

	if (conn == NULL)
	{
		Con::warnf("AW CRUD!!");
		return;
	}
	//conn->setNetworkConnection(true);

	// This function causes a memory error and fails
	// to be called.
	// It's a virtual function if that's important
	conn->onConnectionEstablished(false);
	conn->setEstablished();
	//setEstablished(conn);

	//conn->setConnectSequence(connectSequence);

	Con::printf("HERE: Working");
	sendConnectAccept(conn);
}

ConsoleFunction(handleGotConnectRequest, void, 2, 2, "handleGotConnectRequest(%conn);")
{
	Con::printf("Thing: %s, %s", argv[0], argv[1]);
	NetConn* conn = (NetConn*)Sim::findObject(argv[1]);
	if (conn == NULL)
	{
		Con::warnf("AW PLOPPY");
		return;
	}

	conn->onConnectionEstablished(false);
	conn->setEstablished();

	sendConnectAccept(conn);
}

void sendConnectRequest(TGE::NetInterface *pThis, NetConn *conn)
{
	BitStream *out = BStream::getPacketStream();
	U8 req = ConnectRequest;
	out->write(sizeof(U8), &req);

	U32 seq = conn->getSequence();
	out->write(sizeof(U32), &seq);

	U32 addressDigest[4];
	//conn->getAddressDigest(addressDigest);
	addressDigest[0] = mAddressDigest[0];
	addressDigest[1] = mAddressDigest[1];
	addressDigest[2] = mAddressDigest[2];
	addressDigest[3] = mAddressDigest[3];

	Con::errorf("Sending Digest: %d::%d::%d::%d", addressDigest[0], addressDigest[1], addressDigest[2], addressDigest[3]);

	out->write(sizeof(U32), &addressDigest[0]);
	out->write(sizeof(U32), &addressDigest[1]);
	out->write(sizeof(U32), &addressDigest[2]);
	out->write(sizeof(U32), &addressDigest[3]);

	const char* className = conn->getClassName();

	out->writeString(className, 255);
	conn->writeConnectRequest(out);
	mConnectSendCount++;
	mConnectLastSendTime = Platform::getVirtualMilliseconds();

	BStream::sendPacketStream(conn->getNetAddress());
}

void handleConnectChallengeResponse(TGE::NetInterface *pThis, const NetAddress *address, BitStream *stream)//, RawData packetData, U32& index)
{
	Con::printf("Got Connect challenge Response");
	//U32 connectSequence = getInt(packetData, index);

	U32 connectSequence;
	stream->read(sizeof(U32), &connectSequence);

	NetConn *conn = pThis->findPendingConnection(address, connectSequence);

	if (!conn || conn->getConnectionState() != AwaitingChallengeResponse)
	{
		Con::warnf("WARNING: No Pending Connections? Why did we receive a connect challenge response?");
		return;
	}

	U32 addressDigest[4];
	stream->read(sizeof(U32), &addressDigest[0]);
	stream->read(sizeof(U32), &addressDigest[1]);
	stream->read(sizeof(U32), &addressDigest[2]);
	stream->read(sizeof(U32), &addressDigest[3]);
	//conn->setAddressDigest(addressDigest);
	mAddressDigest[0] = addressDigest[0];
	mAddressDigest[1] = addressDigest[1];
	mAddressDigest[2] = addressDigest[2];
	mAddressDigest[3] = addressDigest[3];

	Con::errorf("Received Digest: %d::%d::%d::%d", mAddressDigest[0], mAddressDigest[1], mAddressDigest[2], mAddressDigest[3]);

	/*addressDigest[0] = getInt(packetData, index);
	addressDigest[1] = getInt(packetData, index);
	addressDigest[2] = getInt(packetData, index);
	addressDigest[3] = getInt(packetData, index);*/

	//U32 *mConnectionState = (U32*)(conn + TGEOFF_NETCONNECTION_MCONNECTIONSTATE);

	conn->setConnectionState(AwaitingConnectResponse);
	mConnectSendCount = 0;
	Con::printf("Sending Connect Request");
	sendConnectRequest(pThis, conn);
}

//void writeConnectRequest(TGE::NetInterface *pThis, BitStream *stream)
//{
//	stream->write(mNetClassGroup);
//	stream->writeInt(U32(AbstractClassRep::getClassCRC(mNetClassGroup)));
//}

/*bool readConnectAccept(BitStream *stream, const char **errorString)//, RawData packetData, U32& index)
{
	//if (!Parent::readConnectAccept(stream, errorString))
	//	return false;

	//U32 protocolVersion = getInt(packetData, index);
	//stream->read(&protocolVersion);

	U32 protocolVersion;
	stream->read(sizeof(U32), &protocolVersion);

	if (protocolVersion < 1000 || protocolVersion > 1000)
	{
		*errorString = "CHR_PROTOCOL"; // this should never happen unless someone is faking us out.
		return false;
	}
	return true;
}*/

void handleConnectAccept(TGE::NetInterface *pThis, const NetAddress *address, BitStream *stream)//, RawData packetData, U32& index)
{
	char buf[256];
	Net::addressToString(address, buf);
	Con::warnf("DEBUG: Handle Connection Accept: %s", buf);

	//U32 connectSequence = getInt(packetData, index);


	U32 connectSequence;
	stream->read(sizeof(U32), &connectSequence);

	NetConn *conn = pThis->findPendingConnection(address, connectSequence);
	if (!conn || conn->getConnectionState() != AwaitingConnectResponse)
	{
		Con::errorf("Jeff wants more mcnuggies");
		return;
	}
	const char *errorString = NULL;
	if (!conn->readConnectAccept(stream, &errorString))//, packetData, index))
	{
		Con::errorf("Awww poop");
		//conn->handleStartupError(errorString);
		pThis->removePendingConnection(conn);
		//conn->deleteObject();
		return;
	}

	pThis->removePendingConnection(conn); // remove from the pending connection list

	char* class_start = (char*)conn;
	char* class_offs = class_start + 0x118;
	BitSet32 *flags = ((BitSet32*)class_offs);
	flags->set(BitSet32(NetworkConnection), true);
	//conn->setNetworkConnection(true);
	conn->onConnectionEstablished(true); // notify the connection that it has been established
	conn->setEstablished(); // installs the connection in the connection table, and causes pings/timeouts to happen
							//conn->setConnectSequence(connectSequence);
	conn->setSequence(connectSequence);
}

void handleDisconnect(TGE::NetInterface *pThis, const NetAddress *address, BitStream *stream)
{
	char buf[256];
	Net::addressToString(address, buf);
	Con::warnf("DEBUG: Handle Disconnect: %s", buf);
}

void handleConnectReject(TGE::NetInterface *pThis, const NetAddress *address, BitStream *stream)
{
	char buf[256];
	Net::addressToString(address, buf);
	Con::warnf("DEBUG: Handle Connection Rejected: %s", buf);
}

void handleGGCPacket(TGE::NetInterface *pThis, const NetAddress *address, U8* data, U32 dataSize)//, RawData packetData, U32& index)
{
	char buf[256];
	Net::addressToString(address, buf);
	Con::warnf("DEBUG: Handle GGC Packet: %s", buf);
}

/*TorqueOverrideMember(bool, GameConn::readConnectAccept, (TGE::NetConn *pThis, BitStream *stream, const char **errorString), originalReadConnectAccept)
{

	return true;
}*/

TorqueOverrideMember(void, NetConn2::handleNotify, (TGE::NetConn *pThis, bool recvd), originalHandleNotify)
{
	NetConn::PacketNotify *note = pThis->getNotifyQueueHead();
	if (note == nullptr)
	{
		//AssertFatal(note != NULL, "Error: got a notify with a null notify head.");
		Con::errorf("Error: got a notify with a null notify head.");
	}
	else {
		originalHandleNotify(pThis, recvd);
	}
}

// This override causes the game to crash when quitting??
// Removed until it can be fixed.
TorqueOverrideMember(void, NetInterface::processPacketReceiveEvent, (TGE::NetInterface *pThis, TGE::NetAddress srcAddress, TGE::RawData packetData), originalProcessPacket)
{
	originalProcessPacket(pThis, srcAddress, packetData);
	//TGE::NetAddress* srcAddress = (TGE::NetAddress*)src;

	U32 dataSize = packetData.size;
	BitStream* pStream = (BitStream*)malloc(41);
	pStream->__construct(packetData.data, dataSize, -1);

					  // Determine what to do with this packet:

	if (packetData.data[0] & 0x01) // it's a protocol packet...
	{
		// if the LSB of the first byte is set, it's a game data packet
		// so pass it to the appropriate connection.

		// lookup the connection in the addressTable
		NetConn *conn = NetConnection::lookup(&srcAddress);
		if (conn)
			conn->processRawPacket(pStream); //&pStream);
		//originalProcessPacket(pThis, srcAddress, packetData);
	}
	else
	{
		// Otherwise, it's either a game info packet or a
		// connection handshake packet.

		//U32 index = 0;

		U8 packetType;
		pStream->read(sizeof(U8), &packetType);
		//packetType = packetData.data[index++];

		NetAddress *addr = &srcAddress;

		int GameHeartbeat = 22;
		if (packetType <= GameHeartbeat)
			handleInfoPacket(pThis, addr, packetType, pStream);
		//#ifdef GGC_PLUGIN
		else if (packetType == GGCPacket)
		{
			handleGGCPacket(pThis, addr, (U8*)packetData.data, dataSize);//, packetData, index);
		}
		//#endif
		else
		{
			// check if there's a connection already:
			switch (packetType)
			{
			case ConnectChallengeRequest:
				handleConnectChallengeRequest(pThis, addr, pStream);//, packetData, index);
				break;
			case ConnectRequest:
				handleConnectRequest(pThis, addr, pStream);//, packetData, index);
				break;
			case ConnectChallengeResponse:
				handleConnectChallengeResponse(pThis, addr, pStream);//, packetData, index);
				break;
			case ConnectAccept:
				handleConnectAccept(pThis, addr, pStream);//, packetData, index);
				break;
			case Disconnect:
				handleDisconnect(pThis, addr, pStream);
				break;
			case ConnectReject:
				handleConnectReject(pThis, addr, pStream);
				break;
			default:
				//DEBUG: Handle (204) Packet: IP:127.0.0.1:61767 ???
				char buf[256];
				Net::addressToString(addr, buf);
				Con::warnf("DEBUG: Handle (%d) Packet: %s", packetType, buf);
				break;
			}
		}
	}
}

#endif

#endif