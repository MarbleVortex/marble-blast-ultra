#ifndef __CLASSREP_H_
#define __CLASSREP_H_


#include <TorqueLib/platform/platform.h>
#include <tVector.h>
#include <bitSet.h>
#include <TGE.h>
//#include <Addresses.h>
//#include <InterfaceMacros-win32.h>

namespace TGE
{
	class Namespace;
	class ConsoleObject;
}

namespace TGE
{

	//typedef void* Namespace;
	//typedef void* ConsoleObject;
	//class ConsoleObject;
	struct EnumTable;

	enum NetClassTypes {
		NetClassTypeObject = 0,
		NetClassTypeDataBlock,
		NetClassTypeEvent,
		NetClassTypesCount,
	};

	enum NetClassGroups {
		NetClassGroupGame = 0,
		NetClassGroupCommunity,
		NetClassGroup3,
		NetClassGroup4,
		NetClassGroupsCount,
	};

	enum NetClassMasks {
		NetClassGroupGameMask = BIT(NetClassGroupGame),
		NetClassGroupCommunityMask = BIT(NetClassGroupCommunity),
	};

	enum NetDirection
	{
		NetEventDirAny,
		NetEventDirServerToClient,
		NetEventDirClientToServer,
	};

	class SimObject;
	class TypeValidator;
	
	class AbstractClassRep;

	namespace Members
	{
		namespace AbstractClassRep
		{
			FN(void, registerClassRep, (TGE::AbstractClassRep* in_pRep), TGEADDR_CON_ABSTRACTCLASSREP_REGISTERCLASSREP);
		}
	}

	class AbstractClassRep
	{
		friend class TGE::ConsoleObject;

	public:

		/// @name 'Tructors
		/// @{

		AbstractClassRep()
		{
			VECTOR_SET_ASSOCIATION(mFieldList);
			parentClass = NULL;
		}
		virtual ~AbstractClassRep() { }

		/// @}

		/// @name Representation Interface
		/// @{

		//void *mUnknown;
		S32 mClassGroupMask;                ///< Mask indicating in which NetGroups this object belongs.
		S32 mClassType;                     ///< Stores the NetClass of this class.
		S32 mNetEventDir;                   ///< Stores the NetDirection of this class.
		S32 mClassId[NetClassGroupsCount];  ///< Stores the IDs assigned to this class for each group.
		//S32 mUnknown;

		S32                        getClassId(U32 netClassGroup)   const;
		//static U32                 getClassCRC(U32 netClassGroup);
		const char*                getClassName() const;
		//static AbstractClassRep*   getClassList();
		Namespace*                 getNameSpace();
		AbstractClassRep*          getNextClass();
		AbstractClassRep*          getParentClass();

		/// Helper class to see if we are a given class, or a subclass thereof.
		bool                       isClass(AbstractClassRep  *acr)
		{
			AbstractClassRep  *walk = this;

			//  Walk up parents, checking for equivalence.
			while (walk)
			{
				if (walk == acr)
					return true;

				walk = walk->parentClass;
			};

			return false;
		}

		virtual TGE::ConsoleObject*     create() const = 0;

	protected:
		virtual void init() const = 0;

		const char *       mClassName;
		AbstractClassRep * nextClass;
		AbstractClassRep * parentClass;
		Namespace *        mNamespace;

		/// @}


		/// @name Fields
		/// @{
	public:
		enum ACRFieldTypes
		{
			StartGroupFieldType = 0xFFFFFFFD,
			EndGroupFieldType = 0xFFFFFFFE,
			DepricatedFieldType = 0xFFFFFFFF
		};

		struct Field {
			const char* pFieldname;    ///< Name of the field.
			const char* pGroupname;      ///< Optionally filled field containing the group name.
											///
											///  This is filled when type is StartField or EndField

			const char*    pFieldDocs;    ///< Documentation about this field; see consoleDoc.cc.
			bool           groupExpand;   ///< Flag to track expanded/not state of this group in the editor.
			U32            type;          ///< A type ID. @see ACRFieldTypes
			U32            offset;        ///< Memory offset from beginning of class for this field.
			S32            elementCount;  ///< Number of elements, if this is an array.
			EnumTable *    table;         ///< If this is an enum, this points to the table defining it.
			BitSet32       flag;          ///< Stores various flags
			TypeValidator *validator;     ///< Validator, if any.
		};
		typedef Vector<Field> FieldList;

		FieldList mFieldList;

		bool mDynamicGroupExpand;

		//const Field *findField(StringTableEntry fieldName) const;
		/*const Field *findField(void* fieldName) const
		{
			Field f;
			f.pFieldname = "";
			f.pGroupname = "";
			f.pFieldDocs = "";
			f.type = DepricatedFieldType;
			f.offset = 0;
			f.elementCount = 0;
			f.table = NULL;
			f.flag = 0;
			f.validator = NULL;

			return &f;
		}*/

		/// @}

		/// @name Abstract Class Database
		/// @{

	protected:
		//static AbstractClassRep ** classTable[NetClassGroupsCount][NetClassTypesCount];
		//static AbstractClassRep *  classLinkList;
		//static U32                 classCRC[NetClassGroupsCount];
		//static bool                initialized;

		//static ConsoleObject* create(const char*  in_pClassName);
		//static ConsoleObject* create(const U32 groupId, const U32 typeId, const U32 in_classId);

	public:
		//static U32  NetClassCount[NetClassGroupsCount][NetClassTypesCount];
		//static U32  NetClassBitSize[NetClassGroupsCount][NetClassTypesCount];

		//static void registerClassRep(AbstractClassRep*);
		static void registerClassRep(AbstractClassRep *in_pRep)
		{
			TGE::Members::AbstractClassRep::registerClassRep(in_pRep);
		}
		//static void initialize(); // Called from Con::init once on startup


									/// @}
	};
	
	/*inline AbstractClassRep *AbstractClassRep::getClassList()
	{
		return classLinkList;
	}

	inline U32 AbstractClassRep::getClassCRC(U32 group)
	{
		return classCRC[group];
	}*/
	
	inline AbstractClassRep *AbstractClassRep::getNextClass()
	{
		return nextClass;
	}

	inline AbstractClassRep *AbstractClassRep::getParentClass()
	{
		return parentClass;
	}

	inline S32 AbstractClassRep::getClassId(U32 group) const
	{
		return mClassId[group];
	}

	inline const char* AbstractClassRep::getClassName() const
	{
		return mClassName;
	}

	inline Namespace *AbstractClassRep::getNameSpace()
	{
		return mNamespace;
	}

	//------------------------------------------------------------------------------
	//-------------------------------------- ConcreteClassRep
	//


	/// Helper class for AbstractClassRep.
	///
	/// @see AbtractClassRep
	/// @see ConsoleObject
	template <class T>
	class ConcreteClassRep : public AbstractClassRep
	{
	public:
		//test
		static ConcreteClassRep<T>* testRef;

		ConcreteClassRep(const char *name, S32 netClassGroupMask, S32 netClassType, S32 netEventDir/*, S32 unknown*/, AbstractClassRep *parent)
		{
			// name is a static compiler string so no need to worry about copying or deleting
			mClassName = name;

			//mUnknown = nullptr;

			// Clean up mClassId
			for (U32 i = 0; i < NetClassGroupsCount; i++)
				mClassId[i] = -1;

			// Set properties for this ACR
			mClassType = netClassType;
			mClassGroupMask = netClassGroupMask;
			mNetEventDir = netEventDir;
			//mUnknown = unknown;
			parentClass = parent;

			// Finally, register ourselves.
			registerClassRep(this);

			//test
			testRef = this;
		};

		/// Perform class specific initialization tasks.
		///
		/// Link namespaces, call initPersistFields() and consoleInit().
		void init() const
		{
			// Get handle to our parent class, if any, and ourselves (we are our parent's child).
			AbstractClassRep *parent = T::getParentStaticClassRep();
			AbstractClassRep *child = T::getStaticClassRep();

			// If we got reps, then link those namespaces! (To get proper inheritance.)
			if (parent && child)
				TGE::Con::classLinkNamespaces(parent->getNameSpace(), child->getNameSpace());

			// Finally, do any class specific initialization...
			T::initPersistFields();
			T::consoleInit();
		}

		/// Wrap constructor.
		TGE::ConsoleObject* create() const { return new T; }
	};

	// From console.h TSE Milestone 4
	struct EnumTable
	{
		/// Number of enumerated items in the table.
		S32 size;

		/// This represents a specific item in the enumeration.
		struct Enums
		{
			S32 index;        ///< Index label maps to.
			const char *label;///< Label for this index.
		};

		Enums *table;

		/// Constructor.
		///
		/// This sets up the EnumTable with predefined data.
		///
		/// @param sSize  Size of the table.
		/// @param sTable Pointer to table of Enums.
		///
		/// @see gLiquidTypeTable
		/// @see gAlignTable
		EnumTable(S32 sSize, Enums *sTable)
		{
			size = sSize; table = sTable;
		}
	};
}

#endif