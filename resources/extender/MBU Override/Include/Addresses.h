#ifndef __ADDRESSES_H_
#define __ADDRESSES_H_

#ifdef MBU_DEV
#include "Addresses-Dev.h"
#else
#include "Addresses-Standard.h"
#endif

#endif // __ADDRESSES_H_