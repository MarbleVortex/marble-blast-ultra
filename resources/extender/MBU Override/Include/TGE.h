#ifndef __TGE_H_
#define __TGE_H_

#include <TorqueLib/platform/platform.h>
#include <Addresses.h>
#include <InterfaceMacros-win32.h>
#include <d3d9.h>
#include <TorqueLib\math\mMath.h>

#include <ClassRep.h>

namespace CInit
{
    FN(int, Engine_IsNonwritableInCurrentImage, (int a1), ADDR__IsNonwritableInCurrentImage);
}

namespace TGE
{
	// Class prototypes
	class SimObject;
	class SimGroup;
	struct NetAddress;
	class NetConn;
	/*class BaseMatInstance;
	class GameConnection;
	class Camera;
	class ResourceObject;
	class NetConnection;
	class BitStream;
	struct Move;*/
}

// TGE callback types
typedef const char* (*StringCallback)(TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef S32         (*IntCallback)   (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef F32         (*FloatCallback) (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef void        (*VoidCallback)  (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef bool        (*BoolCallback)  (TGE::SimObject *obj, S32 argc, const char *argv[]);

typedef U32 SimObjectId;

namespace TGE
{
	FN(void, clientProcess, (U32 timeDelta), TGEADDR_CLIENTPROCESS);
	FN(void, shutdownGame, (), TGEADDR_SHUTDOWNGAME);

	/*FN(bool, GGCInit, (), TGEADDR_GGCONNECT_INIT);
	FN(void, GGCGetError, (), TGEADDR_GGCONNECTINTERFACE_CONNECT);
	FN(bool, GGCFunc, (), TGEADDR_GGCFUNC);
	GLOBALVAR(void*, gGGConnect, TGEADDR_GGCONNECT_VAR);
	GLOBALVAR(bool, GGConnectFailed, TGEADDR_GGCONNECT_FAILED_TO_CONNECT_VAR);
	GLOBALVAR(int, GGCVar, TGEADDR_GGCVAR);
	FN(void, SetGGC, (), TGEADDR_SETGGC);*/

	struct ColorF
	{
		F32 red, green, blue, alpha;
	};

	namespace Compiler
	{

		enum CompiledInstructions
		{
			OP_FUNC_DECL,
			OP_CREATE_OBJECT,
			OP_ADD_OBJECT,
			OP_END_OBJECT,

			OP_JMPIFFNOT,
			OP_JMPIFNOT,
			OP_JMPIFF,
			OP_JMPIF,
			OP_JMPIFNOT_NP,
			OP_JMPIF_NP,
			OP_JMP,
			OP_RETURN,
			OP_CMPEQ,
			OP_CMPGR,
			OP_CMPGE,
			OP_CMPLT,
			OP_CMPLE,
			OP_CMPNE,
			OP_XOR,
			OP_MOD,
			OP_BITAND,
			OP_BITOR,
			OP_NOT,
			OP_NOTF,
			OP_ONESCOMPLEMENT,

			OP_SHR,
			OP_SHL,
			OP_AND,
			OP_OR,

			OP_ADD,
			OP_SUB,
			OP_MUL,
			OP_DIV,
			OP_NEG,

			OP_SETCURVAR,
			OP_SETCURVAR_CREATE,
			OP_SETCURVAR_ARRAY,
			OP_SETCURVAR_ARRAY_CREATE,

			OP_LOADVAR_UINT,
			OP_LOADVAR_FLT,
			OP_LOADVAR_STR,

			OP_SAVEVAR_UINT,
			OP_SAVEVAR_FLT,
			OP_SAVEVAR_STR,

			OP_SETCUROBJECT,
			OP_SETCUROBJECT_NEW,
			OP_SETCUROBJECT_INTERNAL,

			OP_SETCURFIELD,
			OP_SETCURFIELD_ARRAY,

			OP_LOADFIELD_UINT,
			OP_LOADFIELD_FLT,
			OP_LOADFIELD_STR,

			OP_SAVEFIELD_UINT,
			OP_SAVEFIELD_FLT,
			OP_SAVEFIELD_STR,

			OP_STR_TO_UINT,
			OP_STR_TO_FLT,
			OP_STR_TO_NONE,
			OP_FLT_TO_UINT,
			OP_FLT_TO_STR,
			OP_FLT_TO_NONE,
			OP_UINT_TO_FLT,
			OP_UINT_TO_STR,
			OP_UINT_TO_NONE,

			OP_LOADIMMED_UINT,
			OP_LOADIMMED_FLT,
			OP_TAG_TO_STR,
			OP_LOADIMMED_STR,
			OP_DOCBLOCK_STR,
			OP_LOADIMMED_IDENT,

			OP_CALLFUNC_RESOLVE,
			OP_CALLFUNC,

			OP_ADVANCE_STR,
			OP_ADVANCE_STR_APPENDCHAR,
			OP_ADVANCE_STR_COMMA,
			OP_ADVANCE_STR_NUL,
			OP_REWIND_STR,
			OP_TERMINATE_REWIND_STR,
			OP_COMPARE_STR,

			OP_PUSH,
			OP_PUSH_FRAME,

			OP_BREAK,

			OP_INVALID
		};
	}

	/// Extended information about a console function.
	struct ConsoleFunctionHeader
	{
		/// Return type string.
		const char* mReturnString;

		/// List of arguments taken by the function.  Used for documentation.
		const char* mArgString;

		/// List of default argument values.  Used for documentation.
		const char* mDefaultArgString;

		/// Whether this is a static method in a class.
		bool mIsStatic;

		ConsoleFunctionHeader(
			const char* returnString,
			const char* argString,
			const char* defaultArgString,
			bool isStatic = false)
			: mReturnString(returnString),
			mArgString(argString),
			mDefaultArgString(defaultArgString),
			mIsStatic(isStatic) {}
	};

	class Namespace
	{
	public:

	};

	class ConsoleObject
	{
	public:
		/*VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);
		//uint32_t z_vtableLookup_(int index) \
		{ \
			return (*reinterpret_cast<uint32_t**>(reinterpret_cast<uint32_t>(this) + TGEOFF_CONSOLEOBJECT_VTABLE))[index]; \
		} //\
			//virtual void z_foo_() = 0

		//UNDEFVIRT(getClassRep);
		//VIRTDTOR(~ConsoleObject, TGEVIRT_CONSOLEOBJECT_DESTRUCTOR);*/

		static void initPersistFields()
		{
		}

		static void consoleInit()
		{
		}

		const AbstractClassRep::FieldList& getFieldList() const;
		AbstractClassRep::FieldList& getModifiableFieldList();

		// Get the abstract class information for this class.
		static AbstractClassRep *getStaticClassRep() { return NULL; }

		/// Get the abstract class information for this class's superclass.
		static AbstractClassRep *getParentStaticClassRep() { return NULL; }

		virtual AbstractClassRep* getClassRep() const
		{
			return NULL;
		}
	};

	inline const AbstractClassRep::FieldList& ConsoleObject::getFieldList() const
	{
		return getClassRep()->mFieldList;
	}

	inline AbstractClassRep::FieldList& ConsoleObject::getModifiableFieldList()
	{
		return getClassRep()->mFieldList;
	}

	class SimFieldDictionary
	{
		friend class SimFieldDictionaryIterator;

	public:
		struct Entry
		{
			StringTableEntry slotName;
			char *value;
			Entry *next;
		};
	private:
		enum
		{
			HashTableSize = 19
		};
		Entry *mHashTable[HashTableSize];

		static Entry *mFreeList;
		//static void freeEntry(Entry *entry);
		//static Entry *allocEntry();

		/// In order to efficiently detect when a dynamic field has been
		/// added or deleted, we increment this every time we add or
		/// remove a field.
		U32 mVersion;

	public:
		const U32 getVersion() const { return mVersion; }

		//SimFieldDictionary();
		//~SimFieldDictionary();
		//void setFieldValue(StringTableEntry slotName, const char *value);
		//const char *getFieldValue(StringTableEntry slotName);
		//void writeFields(SimObject *obj, Stream &strem, U32 tabStop);
		//void printFields(SimObject *obj);
		//void assignFrom(SimFieldDictionary *dict);
	};

	class SimFieldDictionaryIterator
	{
		SimFieldDictionary *          mDictionary;
		S32                           mHashIndex;
		SimFieldDictionary::Entry *   mEntry;

	public:
		//SimFieldDictionaryIterator(SimFieldDictionary*);
		//SimFieldDictionary::Entry* operator++();
		//SimFieldDictionary::Entry* operator*();
	};

	struct SimObject_VTABLE
	{
#ifdef MBU_DEV
		int sub_4B9BF8; //0x4B9BF8
		int sub_4BAD11; //0x4BAD11
		int sub_4B9141; //0x4B9141
		int sub_4B95FD; //0x4B95FD
		int sub_4B9647; //0x4B9647
		int sub_4B98D0; //0x4B98D0
		int sub_4BAF76; //0x4BAF76
		int sub_4BA95D; //0x4BA95D
		int nul_4B98C5; //0x4B98C5
		int nul_4B98C6; //0x4B98C6
		int nul_4B98CA; //0x4B98CA
		int nul_4B98CD; //0x4B98CD
		int nul_4B9B0C; //0x4B9B0C
		int nul_4B9B0D; //0x4B9B0D
		int nul_4B98C7; //0x4B98C7
		int nul_401C3D; //0x401C3D
		int nul_401C3E; //0x401C3E
		int ano_4BD6B5; //0x4BD6B5 // may be wrong
		int sub_4B904D; //0x4B904D
		int sub_4B907A; //0x4B907A
		int sub_4BB4D2; //0x4BB4D2
		int sub_4BD415; //0x4BD415
		int sub_4BD078; //0x4BD078
		int sub_4BD0E1; //0x4BD0E1
		int nul_401C3F; //0x401C3F
		int sub_401C42; //0x401C42
		int nul_401C47; //0x401C47
#endif
	};

	class SimObject : public ConsoleObject
	{
		typedef ConsoleObject Parent;
	private:
		/// Flags for use in mFlags
		enum {
			Deleted = BIT(0),   ///< This object is marked for deletion.
			Removed = BIT(1),   ///< This object has been unregistered from the object system.
			Added = BIT(3),   ///< This object has been registered with the object system.
			Selected = BIT(4),   ///< This object has been marked as selected. (in editor)
			Expanded = BIT(5),   ///< This object has been marked as expanded. (in editor)
			ModStaticFields = BIT(6),    ///< The object allows you to read/modify static fields
			ModDynamicFields = BIT(7)     ///< The object allows you to read/modify dynamic fields
		};
	public:
		SimObject(const U8 namespaceLinkMask = 0)
		{

		}

		/// @name Notification
		/// @{
		struct Notify {
			enum Type {
				ClearNotify,   ///< Notified when the object is cleared.
				DeleteNotify,  ///< Notified when the object is deleted.
				ObjectRef,     ///< Cleverness to allow tracking of references.
				Invalid        ///< Mark this notification as unused (used in freeNotify).
			} type;
			void *ptr;        ///< Data (typically referencing or interested object).
			Notify *next;     ///< Next notification in the linked list.
		};

		/// @}

		enum WriteFlags {
			SelectedOnly = BIT(0) ///< Passed to SimObject::write to indicate that only objects
								  ///  marked as selected should be outputted. Used in SimSet.
		};

	private:
		// dictionary information stored on the object
		StringTableEntry objectName;
		SimObject*       nextNameObject;
		SimObject*       nextManagerNameObject;
		SimObject*       nextIdObject;

		SimGroup*   mGroup;  ///< SimGroup we're contained in, if any.
		BitSet32    mFlags;

		/// @name Notification
		/// @{
		void*     mNotifyList;
		/// @}

	public:
		SimObjectId mId;         ///< Id number for this object.
		Namespace*  mNameSpace;
		U32         mTypeMask;

		/*protected:
			/// @name Notification
			/// Helper functions for notification code.
			/// @{

			static SimObject::Notify *mNotifyFreeList;

			/// @}*/

	private:
		SimFieldDictionary *mFieldDictionary;    ///< Storage for dynamic fields.
	protected:
		bool	mCanSaveFieldDictionary; ///< true if dynamic fields (added at runtime) should be saved, defaults to true
		StringTableEntry mInternalName; ///< Stores object Internal Name

										// Namespace linking
		StringTableEntry mClassName;     ///< Stores the class name to link script class namespaces
		StringTableEntry mSuperClassName;   ///< Stores super class name to link script class namespaces

		enum SimObjectNSLinkType
		{
			LinkClassName = BIT(0),
			LinkSuperClassName = BIT(1)
		};
		U8 mNSLinkMask;

	public:
		//static DLLSPEC /*TGE::ConcreteClassRep<TGE::SimObject>*/ int* &dynClassRep;
		//MEMBERFNSTATICSIMP(TGE::AbstractClassRep*, getStaticClassRep, TGEADDR_CON_SIMOBJECT_GETSTATICCLASSREP);

		static TGE::AbstractClassRep *getParentStaticClassRep()
		{
			return NULL;
		}

		static AbstractClassRep *getStaticClassRep() {
			//return (AbstractClassRep*)&dynClassRep;
			return (TGE::AbstractClassRep*)&*reinterpret_cast<int*>(TGEADDR_CON_SIMOBJECT_CONCRETECLASSREP);
		}

		virtual TGE::AbstractClassRep *getClassRep()
		{
			//return (AbstractClassRep*)&SimObject::dynClassRep;

			return (TGE::AbstractClassRep*)&*reinterpret_cast<int*>(TGEADDR_CON_SIMOBJECT_CONCRETECLASSREP);
		}

		GETTERFNSIMP(SimObjectId, getId, TGEOFF_SIMOBJECT_ID);

		MEMBERFN(bool, registerObject, (), (), TGEADDR_SIMOBJECT_REGISTEROBJECT);
#ifdef MBU_DEV
		MEMBERFN(void, deleteObject, (), (), TGEADDR_SIMOBJECT_DELETEOBJECT);
#endif

		SimGroup* getGroup() {

			char* class_start = (char*)this;
			char* class_offs = class_start + 0x14;
			SimGroup* group = *((SimGroup**)class_offs);

			return group;//mConnectionState;
		}

		void setGroup(SimGroup* group) {

			char* class_start = (char*)this;
			char* class_offs = class_start + 0x14;
			*((SimGroup**)class_offs) = group;
		}
	};

	namespace NetConnection
	{
		FN(void, checkMaxRate, (), TGEADDR_NETCONNECTION_CHECKMAXRATE);
#ifdef MBU_DEV
		FN(NetConn*, lookup, (const NetAddress *addr), TGEADDR_NETCONNECTION_LOOKUP);
#endif
	}

	// Console enums
	namespace ConsoleLogEntry
	{
		enum Level
		{
			Normal = 0,
			Warning,
			Error,
			NUM_CLASS
		};

		enum Type
		{
			General = 0,
			Assert,
			Script,
			GUI,
			Network,
			NUM_TYPE
		};
	}

	class NetObject: public SimObject
	{
	public:
		UNDEFVIRT(getUpdatePriority);
		UNDEFVIRT(packUpdate);
		UNDEFVIRT(unpackUpdate);
		UNDEFVIRT(onCameraScopeQuery);
	};

	class SceneObject: public NetObject
	{
	public:
		UNDEFVIRT(isDisplacable);
		UNDEFVIRT(getMomentum);
		UNDEFVIRT(setMomentum);
		UNDEFVIRT(getMass);
		UNDEFVIRT(displaceObject);
		UNDEFVIRT(setTransform);
		UNDEFVIRT(setScale);
		UNDEFVIRT(setRenderTransform);
		UNDEFVIRT(buildConvex);
		UNDEFVIRT(buildPolyList);
		UNDEFVIRT(buildCollisionBSP);
		UNDEFVIRT(castRay);
		UNDEFVIRT(collideBox);
		UNDEFVIRT(getOverlappingZones);
		UNDEFVIRT(getPointZone);
		UNDEFVIRT(renderShadowVolumes);
		UNDEFVIRT(renderObject);
		UNDEFVIRT(prepRenderImage);
		UNDEFVIRT(scopeObject);
		UNDEFVIRT(getMaterialProperty);
		UNDEFVIRT(onSceneAdd);
		UNDEFVIRT(onSceneRemove);
		UNDEFVIRT(transformModelview);
		UNDEFVIRT(transformPosition);
		UNDEFVIRT(computeNewFrustum);
		UNDEFVIRT(openPortal);
		UNDEFVIRT(closePortal);
		UNDEFVIRT(getWSPortalPlane);
		UNDEFVIRT(installLights);
		UNDEFVIRT(uninstallLights);
		UNDEFVIRT(getLightingAmbientColor);
	};

	class GameBase : public SceneObject
	{
	public:
		UNDEFVIRT(onNewDataBlock);
		UNDEFVIRT(processTick);
		UNDEFVIRT(interpolateTick);
		UNDEFVIRT(advanceTime);
		UNDEFVIRT(advancePhysics);
		UNDEFVIRT(getVelocity);
		UNDEFVIRT(getForce);
		UNDEFVIRT(writePacketData);
		UNDEFVIRT(readPacketData);
		UNDEFVIRT(getPacketDataChecksum);
	};

	class ShapeBase : public GameBase
	{
	public:
		UNDEFVIRT(setImage);
		UNDEFVIRT(onImageRecoil);
		UNDEFVIRT(ejectShellCasing);
		UNDEFVIRT(updateDamageLevel);
		UNDEFVIRT(updateDamageState);
		UNDEFVIRT(blowUp);
		UNDEFVIRT(onMount);
		UNDEFVIRT(onUnmount);
		UNDEFVIRT(onImpact_SceneObject_Point3F);
		UNDEFVIRT(onImpact_Point3F);
		UNDEFVIRT(controlPrePacketSend);
		UNDEFVIRT(setEnergyLevel);
		UNDEFVIRT(mountObject);
		UNDEFVIRT(mountImage);
		UNDEFVIRT(unmountImage);
		UNDEFVIRT(getMuzzleVector);
		UNDEFVIRT(getCameraParameters);
		UNDEFVIRT(getCameraTransform);
		UNDEFVIRT(getEyeTransform);
		UNDEFVIRT(getRetractionTransform);
		UNDEFVIRT(getMountTransform);
		UNDEFVIRT(getMuzzleTransform);
		UNDEFVIRT(getImageTransform_uint_MatrixF);
		UNDEFVIRT(getImageTransform_uint_int_MatrixF);
		UNDEFVIRT(getImageTransform_uint_constchar_MatrixF);
		UNDEFVIRT(getRenderRetractionTransform);
		UNDEFVIRT(getRenderMountTransform);
		UNDEFVIRT(getRenderMuzzleTransform);
		UNDEFVIRT(getRenderImageTransform_uint_MatrixF);
		UNDEFVIRT(getRenderImageTransform_uint_int_MatrixF);
		UNDEFVIRT(getRenderImageTransform_uint_constchar_MatrixF);
		UNDEFVIRT(getRenderMuzzleVector);
		UNDEFVIRT(getRenderMuzzlePoint);
		UNDEFVIRT(getRenderEyeTransform);
		UNDEFVIRT(getDamageFlash);
		UNDEFVIRT(setDamageFlash);
		UNDEFVIRT(getWhiteOut);
		UNDEFVIRT(setWhiteOut);
		UNDEFVIRT(getInvincibleEffect);
		UNDEFVIRT(setupInvincibleEffect);
		UNDEFVIRT(updateInvincibleEffect);
		UNDEFVIRT(setVelocity);
		UNDEFVIRT(applyImpulse);
		UNDEFVIRT(setControllingClient);
		UNDEFVIRT(setControllingObject);
		UNDEFVIRT(getControlObject);
		UNDEFVIRT(setControlObject);
		UNDEFVIRT(getCameraFov);
		UNDEFVIRT(getDefaultCameraFov);
		UNDEFVIRT(setCameraFov);
		UNDEFVIRT(isValidCameraFov);
		UNDEFVIRT(renderMountedImage);
		UNDEFVIRT(renderImage);
		UNDEFVIRT(calcClassRenderData);
		UNDEFVIRT(onCollision);
		UNDEFVIRT(getSurfaceFriction);
		UNDEFVIRT(getBounceFriction);
		UNDEFVIRT(setHidden);
	};

	//-----------------------------------------------------------------------------
	/// \brief List of parsers for the compiler
	//-----------------------------------------------------------------------------
	struct ConsoleParser
	{
		struct ConsoleParser *next;       //!< Next object in list or NULL

		char *ext;                        //!< Filename extension handled by this parser

		/*fnGetCurrentFile*/ void* getCurrentFile;  //!< GetCurrentFile lexer function
		/*fnGetCurrentLine*/ void* getCurrentLine;  //!< GetCurrentLine lexer function
		/*fnParse*/ void*          parse;           //!< Parse lexer function
		/*fnRestart*/ void*        restart;         //!< Restart lexer function
		/*fnSetScanBuffer*/ void*  setScanBuffer;   //!< SetScanBuffer lexer function
	};

	class CodeBlock
	{
	private:
		static CodeBlock* smCodeBlockList;
		static CodeBlock* smCurrentCodeBlock;

	public:
		static U32                       smBreakLineCount;
		static bool                      smInFunction;
		static ConsoleParser * smCurrentParser;

		static CodeBlock* getCurrentBlock()
		{
			return smCurrentCodeBlock;
		}

		static CodeBlock *getCodeBlockList()
		{
			return smCodeBlockList;
		}

		static StringTableEntry getCurrentCodeBlockName();
		static StringTableEntry getCurrentCodeBlockFullPath();
		static StringTableEntry getCurrentCodeBlockModName();
		static CodeBlock *find(StringTableEntry);

		CodeBlock();
		~CodeBlock();

		StringTableEntry name;
		StringTableEntry fullPath;
		StringTableEntry modPath;

		char *globalStrings;
		char *functionStrings;

		U32 functionStringsMaxLen;
		U32 globalStringsMaxLen;

		F64 *globalFloats;
		F64 *functionFloats;

		U32 codeSize;
		U32 *code;

		U32 refCount;
		U32 lineBreakPairCount;
		U32 *lineBreakPairs;
		U32 breakListSize;
		U32 *breakList;
		CodeBlock *nextFile;
		StringTableEntry mRoot;


		void addToCodeList();
		void removeFromCodeList();
		void calcBreakList();
		void clearAllBreaks();
		void setAllBreaks();

		/// Returns the first breakable line or 0 if none was found.
		/// @param lineNumber The one based line number.
		U32 findFirstBreakLine(U32 lineNumber) { return -1; }

		void clearBreakpoint(U32 lineNumber) {};

		/// Set a OP_BREAK instruction on a line. If a break 
		/// is not possible on that line it returns false.
		/// @param lineNumber The one based line number.
		bool setBreakpoint(U32 lineNumber) { return false; }

		void findBreakLine(U32 ip, U32 &line, U32 &instruction) {}
		void getFunctionArgs(char buffer[1024], U32 offset) {}
		const char *getFileLine(U32 ip) { return "X"; }

		//bool read(StringTableEntry fileName, Stream &st) { return true; }
		bool compile(const char *dsoName, StringTableEntry fileName, const char *script) { return true; }

		void incRefCount() {}
		void decRefCount() {}

		/// Compiles and executes a block of script storing the compiled code in this
		/// CodeBlock. If there is no filename breakpoints will not be generated and 
		/// the CodeBlock will not be added to the linked list of loaded CodeBlocks. 
		/// Note that if the script contains no executable statements the CodeBlock
		/// will delete itself on return an empty string. The return string is any 
		/// result of the code executed, if any, or an empty string.
		///
		/// @param fileName The file name, including path and extension, for the 
		/// block of code or an empty string.
		/// @param script The script code to compile and execute.
		/// @param noCalls Skips calling functions from the script.
		/// @param setFrame A zero based index of the stack frame to execute the code 
		/// with, zero being the top of the stack. If the the index is
		/// -1 a new frame is created. If the index is out of range the
		/// top stack frame is used.
		const char *compileExec(StringTableEntry fileName, const char *script,
			bool noCalls, int setFrame = -1) {
			return "";
		}

		/// Executes the existing code in the CodeBlock. The return string is any 
		/// result of the code executed, if any, or an empty string.
		///
		/// @param offset The instruction offset to start executing from.
		/// @param fnName The name of the function to execute or null.
		/// @param ns The namespace of the function to execute or null.
		/// @param argc The number of parameters passed to the function or
		/// zero to execute code outside of a function.
		/// @param argv The function parameter list.
		/// @param noCalls Skips calling functions from the script.
		/// @param setFrame A zero based index of the stack frame to execute the code 
		/// with, zero being the top of the stack. If the the index is
		/// -1 a new frame is created. If the index is out of range the
		/// top stack frame is used.
		/// @param packageName The code package name or null.
		/*const char *exec(U32 offset, const char *fnName, Namespace *ns, U32 argc,
			const char **argv, bool noCalls, StringTableEntry packageName,
			S32 setFrame = -1) {
			return Members::CodeBlock::exec(this, offset, fnName, ns, argc, argv, noCalls, packageName, setFrame);
		}*/
		//MEMBERFN(TGE::CodeBlock, const char *, exec, (U32 ip, const char *functionName, Namespace *thisNamespace, U32 argc, const char **argv, bool noCalls, StringTableEntry packageName, S32 setFrame), (ip, functionName, thisNamespace, argc, argv, noCalls, packageName, setFrame), TGEADDR_CODEBLOCK_EXEC);
		//FN(const char *, exec, (TGE::CodeBlock, U32 ip, const char *functionName, TGE::Namespace *thisNamespace, U32 argc, const char **argv, bool noCalls, StringTableEntry packageName, S32 setFrame), TGEADDR_CODEBLOCK_EXEC);

		GETTERFNSIMP(U32, getCodeSize, 36);
		GETTERFNSIMP(U32*, getCode, 40);
	};

	class Marble: public ShapeBase
	{
	};

	// CodeReview: This number is used for the declaration of variables, but it
	// should *not* be used for any run-time purposes [7/2/2007 Pat]
#define TEXTURE_STAGE_COUNT 16
	// Matrix stuff
#define WORLD_STACK_MAX 24
	// Light define
#define LIGHT_STAGE_COUNT 8

	/*class GFXTarget : public StrongRefBase, public GFXResource
	{
	public:
		S32 mChangeToken;
		S32 mLastAppliedChange;

		/// Called whenever a change is made to this target.
		inline void invalidateState()
		{
			mChangeToken++;
		}

		/// Called when the device has applied pending state.
		inline void stateApplied()
		{
			mLastAppliedChange = mChangeToken;
		}

		/// Constructor to initialize the state tracking logic.
		GFXTarget() : mChangeToken(0),
			mLastAppliedChange(0)
		{
		}
		virtual ~GFXTarget() {}

		/// Called to check if we have pending state for the device to apply.
		inline const bool isPendingState() const
		{
			return (mChangeToken != mLastAppliedChange);
		}

		/// Returns the size in pixels of this rendering target.
		virtual const void* getSize() { return size; }

		// GFXResourceInterface
		/// The resource should put a description of itself (number of vertices, size/width of texture, etc.) in buffer
		virtual const void* describeSelf() {}

		/// This is called when the target is not being used anymore.
		virtual void activate() { }

		/// This is called when the target is not being used anymore.
		virtual void deactivate() { }
	};

	class GFXWindowTarget : public GFXTarget
	{
	public:
		void *mWindow;
		GFXWindowTarget() : mWindow(NULL) {};
		GFXWindowTarget(void *windowObject)
		{
			mWindow = windowObject;
		}
		virtual ~GFXWindowTarget() {}

		/// Returns a pointer to the window this target is bound to.
		inline void *getWindow() { return mWindow; };

		/// Present latest buffer, if buffer swapping is in effect.
		virtual bool present() {}

		/// Notify the target that the video mode on the window has changed.
		virtual void resetMode() {}
	};*/

	/*class GFXD3D9WindowTarget// : public GFXWindowTarget
	{
	public:
		/// Our depth stencil buffer, if any.
		IDirect3DSurface9 *mDepthStencil;

		/// Our backbuffer
		IDirect3DSurface9 *mBackbuffer;

		/// Maximum size we can render to.
		void* mSize;

		/// Our swap chain, potentially the implicit device swap chain.
		IDirect3DSwapChain9 *mSwapChain;

		/// D3D presentation info.
		D3DPRESENT_PARAMETERS mPresentationParams;

		/// Owning d3d device.
		GFXD3D9Device  *mDevice;

		/// Is this the implicit swap chain?
		bool mImplicit;

		/// Internal interface that notifies us we need to reset our video mode.
		void resetMode() {}

		GFXD3D9WindowTarget();
		~GFXD3D9WindowTarget();

		virtual const void* getSize() { return mSize; }
		virtual bool present() { return false; }

		void initPresentationParams() {}
		void setImplicitSwapChain() {}
		void createAdditionalSwapChain() {}

		virtual void activate() {}

		void zombify() {}
		void resurrect() {}
	};*/

	/*class GFXDevice
	{
	private:
		U32 mDeviceIndex;
		void* mAdapter;

	public:
		void* mVideoModes;
		void* mCardProfiler;
		void* mResourceListHead;
		bool mCanCurrentlyRender;
		bool mAllowRender;
		bool mInitialized;
		void* mDeviceStatistics;
		bool mStateDirty;

		enum TexDirtyType
		{
			GFXTDT_Normal,
			GFXTDT_Cube
		};

		void* mCurrentTexture[TEXTURE_STAGE_COUNT];
		void* mNewTexture[TEXTURE_STAGE_COUNT];
		void* mCurrentCubemap[TEXTURE_STAGE_COUNT];
		void* mNewCubemap[TEXTURE_STAGE_COUNT];

		TexDirtyType   mTexType[TEXTURE_STAGE_COUNT];
		bool           mTextureDirty[TEXTURE_STAGE_COUNT];
		bool           mTexturesDirty;
		void* mCurrentStateBlocks;
		bool  mStateBlockDirty;
		void* mCurrentStateBlock;
		void* mNewStateBlock;
		void* mCurrentShaderConstBuffer;
		bool  mSamplerAddressModeOverride[TEXTURE_STAGE_COUNT];
		void* mSamplerAddressModeOverrides[TEXTURE_STAGE_COUNT];
		bool  mSamplerMipLODBiasOverride[TEXTURE_STAGE_COUNT];
		F32   mSamplerMipLODBiasOverrides[TEXTURE_STAGE_COUNT];
		void*  mCurrentLight[LIGHT_STAGE_COUNT];
		bool          mCurrentLightEnable[LIGHT_STAGE_COUNT];
		bool          mLightDirty[LIGHT_STAGE_COUNT];
		bool          mLightsDirty;
		void*        mGlobalAmbientColor;
		bool          mGlobalAmbientColorDirty;
		void* mCurrentLightMaterial;
		bool mLightMaterialDirty;
		void *mDeviceSwizzle32;
		void *mDeviceSwizzle24;
		void* mWorldMatrix[WORLD_STACK_MAX];
		bool    mWorldMatrixDirty;
		S32     mWorldStackSize;
		void* mProjectionMatrix;
		bool    mProjectionMatrixDirty;
		void* mViewMatrix;
		bool    mViewMatrixDirty;
		void* mTextureMatrix[TEXTURE_STAGE_COUNT];
		bool    mTextureMatrixDirty[TEXTURE_STAGE_COUNT];
		bool    mTextureMatrixCheckDirty;
		F32 mFrustLeft, mFrustRight;
		F32 mFrustBottom, mFrustTop;
		F32 mFrustNear, mFrustFar;
		bool mFrustOrtho;
		void* mCurrentVertexBuffer;
		bool mVertexBufferDirty;
		void* mCurrentPrimitiveBuffer;
		bool mPrimitiveBufferDirty;
		void* mSfxBackBuffer;
		bool         mUseSfxBackBuffer;
		void* mFrontBuffer[2];
		U32 mCurrentFrontBufferIdx;
		void* mRTStack;
		void* mCurrentRT;
		void* mTextureManager;
	public:

		enum GenericShaderType
		{
			GSColor = 0,
			GSTexture,
			GSModColorTexture,
			GSAddColorTexture,
			GS_COUNT
		};
	protected:
		void* mDrawer;
	};

	class GFXD3D9Device : public GFXDevice
	{
		typedef GFXDevice Parent;

	public:
		void* mTempMatrix;    ///< Temporary matrix, no assurances on value at all
		D3DVIEWPORT9 mViewport; ///< Because setViewport gets called a lot, don't want to allocate/unallocate a lot
		void* mViewportRect;
		void* mClipRect;
		void* mVolatileVBList;

		IDirect3DSurface9 *mDeviceBackbuffer;
		IDirect3DSurface9 *mDeviceDepthStencil;
		IDirect3DSurface9 *mDeviceColor;

		void* mCurrentOpenAllocVB;
		void* mCurrentVB;
		void *mCurrentOpenAllocVertexData;
		//-----------------------------------------------------------------------
		void* mDynamicPB;
		void* mCurrentOpenAllocPB;
		void* mCurrentPB;

		IDirect3DVertexShader9 *mLastVertShader;
		IDirect3DPixelShader9 *mLastPixShader;

		S32 mCreateFenceType;

		LPDIRECT3D9       mD3D;        ///< D3D Handle
		LPDIRECT3DDEVICE9 mD3DDevice;  ///< Handle for D3DDevice

		U32  mAdapterIndex;            ///< Adapter index because D3D supports multiple adapters

		void* mShaderMgr;    ///< D3D Shader Manager
		F32 mPixVersion;
		U32 mNumSamplers;               ///< Profiled (via caps)

		D3DMULTISAMPLE_TYPE mMultisampleType;
		DWORD mMultisampleLevel;

#ifdef TORQUE_DEBUG
		/// @name Debug Vertex Buffer information/management
		/// @{

		///
		U32 mNumAllocatedVertexBuffers; ///< To keep track of how many are allocated and freed
		void* *mVBListHead;
		/// @}
#endif
		void* mCurrentConstBuffer;
	public:
		void* mGenericShader[GS_COUNT];

		//virtual LPDIRECT3DDEVICE9 getDevice()
		//{
		//	return reinterpret_cast<LPDIRECT3DDEVICE9>(TGEOFF_GFXD3D9DEVICE_VTABLE + 53);
		//}
	};*/

	/// Generic network address
	///
	/// This is used to represent IP addresses.
struct NetAddress
{
	int type;        ///< Type of address (IPAddress currently)

					 /// Acceptable NetAddress types.
	enum
	{
		IPAddress,
	};

	U8 netNum[4];    ///< For IP:  sin_addr<br>
	U8 nodeNum[6];   ///< For IP:  Not used.<br>
	U16  port;       ///< For IP:  sin_port<br>
};

class RawData
{
private:
	bool ownMemory;

public:
	char *data;
	U32 size;

	/*RawData(const RawData &rd)
	{
		data = rd.data;
		size = rd.size;
		ownMemory = false;
	}

	RawData()
		: ownMemory(false), data(NULL), size(0)
	{
	}

	~RawData()
	{
		reset();
	}

	void reset()
	{
		if (ownMemory)
			delete[] data;
		data = NULL;
		ownMemory = false;
		size = 0;
	}

	void alloc(const U32 newSize)
	{
		reset();

		ownMemory = true;
		size = newSize;
		data = new char[newSize];
	}

	void operator =(const RawData &rd)
	{
		data = rd.data;
		size = rd.size;
		ownMemory = false;
	}*/
};

namespace Net
{
	FN(void, addressToString, (const NetAddress *address, char addressString[256]), TGEADDR_NET_ADDRESSTOSTRING);

#ifdef MBU_DEV
	FN(bool, compareAddresses, (const NetAddress *a1, const NetAddress *a2), TGEADDR_NET_COMPAREADDRESSES);
#endif
}

class BitStream
{
public:
	MEMBERFN(bool, read, (const U32 in_numBytes, void* out_pBuffer), (in_numBytes, out_pBuffer), TGEADDR_BITSTREAM_READ);
	MEMBERFN(bool, write, (const U32 numBytes, const void* buf), (numBytes, buf), TGEADDR_BITSTREAM_WRITE);

#ifdef MBU_DEV
	MEMBERFN(void, writeInt, (U32 data), (data), TGEADDR_BITSTREAM_WRITE_INT);
	MEMBERFN(void, writeString, (const char* string, S32 maxLen), (string, maxLen), TGEADDR_BITSTREAM_WRITESTRING);
	MEMBERFN(void, readString, (char stringBuf[256]), (stringBuf), TGEADDR_BITSTREAM_READSTRING);

	/*void writeString(const char *string, S32 maxLen)
	{

	}*/

	/*void readString(char buf[256])
	{

	}*/

	// These are the Stream methods (BitStream extends Stream)
	void writeString_Stream(const char *string, S32 maxLen)
	{
		S32 len = string ? strlen(string) : 0;
		if (len > maxLen)
			len = maxLen;

		U8 l = (U8)len;
		write(sizeof(U8), &l);
		if (len)
			write(len, string);
	}
	void readString_Stream(char buf[256])
	{
		U8 len;
		read(sizeof(U8), &len);
		read(S32(len), buf);
		buf[len] = 0;
	}
	MEMBERFN(BitStream*, __construct, (void *bufPtr, S32 bufSize, S32 maxWriteSize), (bufPtr, bufSize, maxWriteSize), TGEADDR_BITSTREAM_CONSTRUCTOR);
#endif
};

template <typename T, typename V>
inline int MemberOffsetOf(V T::* v)
{
	return ((int)&(((A*)0)->*v));
}

class SimSet : public SimObject
{
	void *vftbl;
	typedef SimObject Parent;
protected:
	void* objectList;
	void *mMutex;
};

class SimNameDictionary
{
	void *vftbl;
	enum
	{
		DefaultTableSize = 29
	};

	SimObject **hashTable;  // hash the pointers of the names...
	S32 hashTableSize;
	S32 hashEntryCount;

	void *mutex;
};

#ifdef MBU_DEV
class Dictionary
{
public:
	MEMBERFN(void, insert, (SimObject* sim), (sim), TGEADDR_DICTIONARY_INSERT);
};
#endif

#ifdef MBU_DEV
class ObjectList
{
public:
	MEMBERFN(void, push_back, (SimObject* obj), (obj), TGEADDR_OBJECTLIST_PUSHBACK);
};
#endif

class SimGroup : public SimSet
{
public:
	VTABLE(0x0);
private:
	typedef SimSet Parent;
	SimNameDictionary nameDictionary;
public:
#ifdef MBU_DEV
	MEMBERFN(void, addObject, (SimObject* sim), (sim), TGEADDR_SIMGROUP_ADDOBJECT);
	MEMBERFN(void, removeObject, (SimObject* sim), (sim), TGEADDR_SIMGROUP_REMOVEOBJECT);

	Dictionary* getNameDictionary() {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x60;
		Dictionary* dict = ((Dictionary*)class_offs);

		return dict;
	}

	ObjectList* getObjectList() {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x50;
		ObjectList* list = ((ObjectList*)class_offs);

		return list;
	}
#endif
};

class ConnectionProtocol
{
	void* vftbl;
protected:
	U32 mLastSeqRecvdAtSend[32];
	U32 mLastSeqRecvd;
	U32 mHighestAckedSeq;
	U32 mLastSendSeq;
	U32 mAckMask;
	U32 mConnectSequence;
	U32 mLastRecvAckAck;
	bool mConnectionEstablished;
};

template <class T> class SimObjectPtr
{
private:
	SimObject *mObj;

public:
	SimObjectPtr() { mObj = 0; }
	SimObjectPtr(T* ptr)
	{
		mObj = ptr;
		if (mObj)
			mObj->registerReference(&mObj);
	}
	SimObjectPtr(const SimObjectPtr<T>& rhs)
	{
		mObj = const_cast<T*>(static_cast<const T*>(rhs));
		if (mObj)
			mObj->registerReference(&mObj);
	}
	/*SimObjectPtr<T>& operator=(const SimObjectPtr<T>& rhs)
	{
		if (this == &rhs)
			return(*this);
		if (mObj)
			mObj->unregisterReference(&mObj);
		mObj = const_cast<T*>(static_cast<const T*>(rhs));
		if (mObj)
			mObj->registerReference(&mObj);
		return(*this);
	}*/
	~SimObjectPtr()
	{
		if (mObj)
			mObj->unregisterReference(&mObj);
	}
	SimObjectPtr<T>& operator= (T *ptr)
	{
		if (mObj != (SimObject *)ptr)
		{
			if (mObj)
				mObj->unregisterReference(&mObj);
			mObj = (SimObject *)ptr;
			if (mObj)
				mObj->registerReference(&mObj);
		}
		return *this;
	}
#if defined(__MWERKS__) && (__MWERKS__ < 0x2400)
	// CW 5.3 seems to get confused comparing SimObjectPtrs...
	bool operator == (const SimObject *ptr) { return mObj == ptr; }
	bool operator != (const SimObject *ptr) { return mObj != ptr; }
#endif
	bool isNull() const { return mObj == 0; }
	T* operator->() const { return static_cast<T*>(mObj); }
	T& operator*() const { return *static_cast<T*>(mObj); }
	operator T*() const { return static_cast<T*>(mObj) ? static_cast<T*>(mObj) : 0; }
	T* getObject() const { return static_cast<T*>(mObj); }
};

#ifdef MBU_DEV
class MoveList
{
	MEMBERFN(void, init, (), (), TGEADDR_MOVELIST_INIT);
};

class NetStringTable
{
public:
	//void        incStringRef(U32 id);
	MEMBERFN(void, incStringRef, (U32 id), (id), TGEADDR_NETSTRINGTABLE_INCSTRINGREF);
	MEMBERFN(void, removeString, (U32 id, bool script), (id, script), TGEADDR_NETSTRINGTABLE_INCSTRINGREF);
};

GLOBALVAR(NetStringTable*, gNetStringTable, TGEADDR_NETSTRINGTABLE_GLOBAL);

#endif

class GuiSpeedometerHud
{
public:

	U8 bla[144];
	RectI mBounds;
	U8 bla_[70];
	F32 mSpeed;
	F32 mMaxSpeed;
	F32 mMaxAngle;
	F32 mMinAngle;
	Point2F mCenter;
	ColorF mColor;
	F32 mNeedleLength;
	F32 mNeedleWidth;
	F32 mTailLength;

#ifdef MBU_DEV
	MEMBERFN(void, onRender, (Point2I offset, const RectI &updateRect), (offset, updateRect), TGEADDR_GUISPEEDOMETERHUD_ONRENDER);
#endif
};

enum GFXPrimitiveType
{
	GFXPT_FIRST = 0,
	GFXPointList = 0,
	GFXLineList,
	GFXLineStrip,
	GFXTriangleList,
	GFXTriangleStrip,
	GFXTriangleFan,
	GFXPT_COUNT
};

namespace PrimBuild
{
#ifdef MBU_DEV
	FN(void, begin, (GFXPrimitiveType type, U32 maxVerts), TGEADDR_PRIMBUILD_BEGIN);
#endif
}

class GFXDeviceD3D9
{
public:
#ifdef MBU_DEV
	MEMBERFN(void, popWorldMatrix, (), (), TGEADDR_GFXDEVICED3D9_POPWORLDMATRIX);
	MEMBERFN(void, multWorld, (const MatrixF &mat), (mat), TGEADDR_GFXDEVICE_MULTWORLD);
#endif

	GETTERFNSIMP(LPDIRECT3DDEVICE9, getD3DDevice, TGEOFF_GFXD3D9DEVICE_D3DDEVICE)
};

#ifdef MBU_DEV
class NetStringHandle
{
	U32 index;
public:

	NetStringHandle(U32 initIndex)
	{
		index = initIndex;
		if (index)
			gNetStringTable->incStringRef(index);
	}

	/*NetStringHandle(const char *string) {
		index = 0;//gNetStringTable->addString(string);
	}*/

	//bool operator==(const NetStringHandle &s) const { return index == s.index; }
	//bool operator!=(const NetStringHandle &s) const { return index != s.index; }

	/*NetStringHandle &operator=(const NetStringHandle &s)
	{
		if (index)
			gNetStringTable->removeString(index);
		index = s.index;
		if (index)
			gNetStringTable->incStringRef(index);
		return *this;
	}*/

	U32 getIndex() { return index; }
};
#endif

class NetConn;
class NetStringHandle;

/*class NetStringEvent : public NetEvent
{
	NetStringHandle mString;
	U32 mIndex;
public:
	NetStringEvent(U32 index = 0, NetStringHandle string = NetStringHandle())
	{
		mIndex = index;
		mString = string;
	}
	virtual void pack(NetConn* , BitStream *bstream)
	{
		bstream->writeInt(mIndex, ConnectionStringTable::EntryBitSize);
		bstream->writeString(mString.getString());
	}
	virtual void write(NetConn* , BitStream *bstream)
	{
		bstream->writeInt(mIndex, ConnectionStringTable::EntryBitSize);
		bstream->writeString(mString.getString());
	}
	virtual void unpack(NetConn* , BitStream *bstream)
	{
		char buf[256];
		mIndex = bstream->readInt(ConnectionStringTable::EntryBitSize);
		bstream->readString(buf);
		mString = NetStringHandle(buf);
	}
	virtual void notifyDelivered(NetConn *ps, bool madeit)
	{
		if (madeit)
			ps->confirmStringReceived(mString, mIndex);
	}
	virtual void process(NetConn *connection)
	{
		Con::printf("Mapping string: %s to index: %d", mString.getString(), mIndex);
		connection->mapString(mIndex, mString);
	}
	//DECLARE_CONOBJECT(NetStringEvent);
};*/

class NetStringEvent
{
public:
};

class ConnectionStringTable;

class NetInterface;

class NetConn;

#ifdef MBU_DEV
GLOBALVAR(NetConn*, daHashTable, TGEADDR_NETCONNECTION_HASHTABLE);
#endif

class GFXResource
{

};

class GFXD3D9WindowTarget : public GFXResource
{
public:
	MEMBERFNSIMP(void, resetMode, TGEADDR_GFXD3D9WINDOWTARGET_RESETMODE);
};

class TCPObject
{
public:
#ifndef MBU_DEV
	MEMBERFN(void, connect, (const char *address), (address), TGEADDR_TCPOBJECT_CONNECT);
	MEMBERFNSIMP(void, disconnect, TGEADDR_TCPOBJECT_DISCONNECT);
	MEMBERFN(void, send, (const U8 *buffer, U32 len), (buffer, len), TGEADDR_TCPOBJECT_SEND);
#endif // MBU_DEV
};

class NetConn : public ConnectionProtocol, public SimGroup
{
	//VTABLE(0x0);
	friend class NetInterface;

	typedef SimGroup Parent;

public:
	struct GhostRef
	{
		U32 mask;                  ///< States we transmitted.
		U32 ghostInfoFlags;        ///< Flags from GhostInfo::Flags
		void *ghost;          ///< Reference to the GhostInfo we're from.
		GhostRef *nextRef;         ///< Next GhostRef in this packet.
		GhostRef *nextUpdateChain; ///< Next update we sent for this ghost.
	};

	enum Constants
	{
		HashTableSize = 127,
	};
private:
	///
	NetConn *mNextConnection;        ///< Next item in list.
	NetConn *mPrevConnection;        ///< Previous item in list.
private:
	BitSet32 mTypeFlags;

	U32 mNetClassGroup;  ///< The NetClassGroup of this connection.

						 /// @name Statistics
						 /// @{

	U32 mLastUpdateTime;
	F32 mRoundTripTime;
	F32 mPacketLoss;
	U32 mSimulatedPing;
	F32 mSimulatedPacketLoss;

	/// @}

	/// @name State
	/// @{

	U32 mProtocolVersion;
	U32 mSendDelayCredit;
	U32 mConnectSequence;
	U32 mAddressDigest[4];

	bool mEstablished;
	bool mMissionPathsSent;

	struct NetRate
	{
		U32 updateDelay;
		S32 packetSize;
		bool changed;
	};

	NetRate mCurRate;
	NetRate mMaxRate;

	SimObjectPtr<NetConn> mRemoteConnection;

	NetAddress mNetAddress;

	U32 mPingSendCount;
	U32 mPingRetryCount;
	U32 mLastPingSendTime;

	NetConn *mNextTableHash;

	U32 mConnectSendCount;
	U32 mConnectLastSendTime;

public:
	U32 mConnectionSendCount;  ///< number of connection messages we've sent.
	U32 mConnectionState;      ///< State of the connection, from NetConnectionState.

	struct PacketNotify
	{
		bool rateChanged;       ///< Did the rate change on this packet?
		bool maxRateChanged;    ///< Did the max rate change on this packet?
		U32  sendTime;          ///< Timestampe, when we sent this packet.

		void *eventList;    ///< Linked list of events sent over this packet.
		GhostRef *ghostList;    ///< Linked list of ghost updates we sent in this packet.
		void *subList;  ///< Defined by subclass - used as desired.

		PacketNotify *nextPacket;  ///< Next packet sent.
								   //PacketNotify();
	};

	PacketNotify *mNotifyQueueHead;  ///< Head of packet notify list.
	PacketNotify *mNotifyQueueTail;  ///< Tail of packet notify list.
private:
	void *mSendEventQueueHead;
	void *mSendEventQueueTail;
	void *mUnorderedSendEventQueueHead;
	void *mUnorderedSendEventQueueTail;
	void *mWaitSeqEvents;
	void *mNotifyEventList;

	bool mSendingEvents;

	S32 mNextSendEventSeq;
	S32 mNextRecvEventSeq;
	S32 mLastAckedEventSeq;

	enum NetEventConstants {
		InvalidSendEventSeq = -1,
		FirstValidSendEventSeq = 0
	};

private:
	bool mTranslateStrings;
	void *mStringTable;

	enum GhostStates
	{
		GhostAlwaysDone,
		ReadyForNormalGhosts,
		EndGhosting,
		GhostAlwaysStarting,
		SendNextDownloadRequest,
		FileDownloadSizeMessage,
		NumConnectionMessages,
	};
	void **mGhostArray;    ///< Linked list of ghostInfos ghosted by this side of the connection

	U32 mGhostZeroUpdateIndex;  ///< Index in mGhostArray of first ghost with 0 update mask.
	U32 mGhostFreeIndex;        ///< Index in mGhostArray of first free ghost.

	U32 mGhostsActive;			///- Track actve ghosts on client side

	bool mGhosting;             ///< Am I currently ghosting objects?
	bool mScoping;              ///< am I currently scoping objects?
	U32  mGhostingSequence;     ///< Sequence number describing this ghosting session.

	NetObject **mLocalGhosts;  ///< Local ghost for remote object.
							   ///
							   /// mLocalGhosts pointer is NULL if mGhostTo is false

	void *mGhostRefs;           ///< Allocated array of ghostInfos. Null if ghostFrom is false.
	void **mGhostLookupTable;   ///< Table indexed by object id to GhostInfo. Null if ghostFrom is false.

	void* mScopeObject;
public:
	/// Some configuration values.
	enum GhostConstants
	{
		GhostIdBitSize = 12,
		MaxGhostCount = 1 << GhostIdBitSize, //4096,
		GhostLookupTableSize = 1 << GhostIdBitSize, //4096
		GhostIndexBitSize = 4 // number of bits GhostIdBitSize-3 fits into
	};

protected:
	/// List of files missing for this connection.
	///
	/// The currently downloading file is always first in the list (ie, [0]).
	Vector<char *> mMissingFileList;

	/// Stream for currently uploading file (if any).
	void *mCurrentDownloadingFile;

	/// Storage for currently downloading file.
	void *mCurrentFileBuffer;

	/// Size of currently downloading file in bytes.
	U32 mCurrentFileBufferSize;

	/// Our position in the currently downloading file in bytes.
	U32 mCurrentFileBufferOffset;

	/// Number of files we have downloaded.
	U32 mNumDownloadedFiles;

	/// Error storage for file transfers.
	char* mLastFileErrorBuffer;

	/// Structure to track ghost-always objects and their ghost indices.
	struct GhostSave {
		NetObject *ghost;
		U32 index;
	};

	/// List of objects to ghost-always.
	Vector<void*> mGhostAlwaysSaveList;
private:
	void *mDemoWriteStream;
	void *mDemoReadStream;
	U32 mDemoNextBlockType;
	U32 mDemoNextBlockSize;

	U32 mDemoWriteStartTime;
	U32 mDemoReadStartTime;
	U32 mDemoLastWriteTime;

	U32 mDemoRealStartTime;
public:
	enum DemoBlockTypes {
		BlockTypePacket,
		BlockTypeSendPacket,
		NetConnectionBlockTypeCount
	};

	enum DemoConstants {
		MaxNumBlockTypes = 16,
		MaxBlockSize = 0x1000,
	};
public:
#ifdef MBU_DEV
	GETTERFNSIMP(PacketNotify*, getNotifyQueueHead, TGEOFF_NETCONNECTION_MNOTIFYQUEUEHEAD);


	//MEMBERFN(void, getAddressDigest, (U32* addressDigest), (addressDigest), TGEADDR_NETCONNECTION_GETADRESSDIGEST);

	/*uint32_t z_vtableLookup_(int index)
	{
		int offset = 0x0;
		return (*reinterpret_cast<uint32_t**>(reinterpret_cast<uint32_t>(this) + offset))[index];
	}
	virtual void z_foo_() = 0;*/
	//VTABLE(0x0);
	MEMBERFN(void, setSequence, (U32 seq), (seq), TGEADDR_NETCONNECTION_SETSEQUENCE);
	MEMBERFN(U32, getSequence, (), (), TGEADDR_NETCONNECTION_GETSEQUENCE);
	MEMBERFN(NetAddress*, getNetAddress, (), (), TGEADDR_NETCONNECTION_GETNETADDRESS);
	//MEMBERFN(const char*, getClassName, (), (), TGEADDR_NETCONNECTION_GETCLASSNAME);
	const char* getClassName()
	{
		return "GameConnection";
	}
	MEMBERFN(void, writeConnectRequest, (BitStream* stream), (stream), TGEADDR_NETCONNECTION_WRITECONNECTREQUEST);
	//VIRTFN(void, onConnectionEstablished, (bool isInitiator), (isInitiator), 0x34);
	MEMBERFN(void, onConnectionEstablished, (bool isInitiator), (isInitiator), TGEADDR_GAMECONNECTION_ONCONNECTIONESTABLISHED);//z_vtableLookup_(0x34))
	/*void onConnectionEstablished(bool isInitiator)                                                                                                       
	{          
		int addr = z_vtableLookup_(0x34);
		//reinterpret_cast<void(__thiscall *) (void*, bool)>(addr)(this, isInitiator);
		//reinterpret_cast<void(__thiscall *) (void*, bool)>(addr)(this, isInitiator);
	}*/
	//VIRTFN(void, setEstablished, (), (), 0x4C);
	//MEMBERFN(void, onConnectionEstablished, (bool isInitiator), (isInitiator), TGEADDR_GAMECONNECTION_ONCONNECTIONESTABLISHED);
	MEMBERFN(void, setEstablished, (), (), TGEADDR_NETCONNECTION_SETESTABLISHED);

	MEMBERFN(void, setGhostFrom, (bool value), (value), TGEADDR_GAMECONNECTION_SETGHOSTFROM);
	MEMBERFN(void, setGhostTo, (bool value), (value), TGEADDR_GAMECONNECTION_SETGHOSTTO);
	MEMBERFN(void, setSendingEvents, (bool value), (value), TGEADDR_GAMECONNECTION_SETSENDINGEVENTS);
	MEMBERFN(void, setTranslatesStrings, (bool value), (value), TGEADDR_GAMECONNECTION_SETTRANSLATESSTRINGS);

	MEMBERFN(bool, postNetEvent, (void *theEvent), (theEvent), TGEADDR_NETCONNECTION_POSTNETEVENT);

	MEMBERFN(void, checkPacketSend, (bool force), (force), TGEADDR_NETCONNECTION_CHECKPACKETSEND);

	MEMBERFN(void, processRawPacket, (BitStream *pstream), (pstream), TGEADDR_NETCONNECTION_PROCESSRAWPACKET);

	MEMBERFN(bool, readConnectAccept, (BitStream *stream, const char **errorString), (stream, errorString), TGEADDR_GAMECONNECTION_READCONNECTACCEPT);
	MEMBERFN(bool, readConnectRequest, (BitStream *stream, const char **errorString), (stream, errorString), TGEADDR_GAMECONNECTION_READCONNECTREQUEST);

	//MEMBERVAR(U32, mConnectionState_, TGEOFF_NETCONNECTION_MCONNECTIONSTATE);

	void setConnectionState(U32 connectionState) {
		//mConnectionState = connectionState;

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x19C;

		U32* u = ((U32*)class_offs);
		u = &connectionState;
	}//U32 state = connectionState; this->mConnectionState = &state; }

	U32 getConnectionState() {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x19C;
		U32 connectionState = *((U32*)class_offs);

		return connectionState;//mConnectionState;
	}//U32 state = *mConnectionState; return state; }

	/*void setNetAddress(const NetAddress* addr)
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + 0x170;
		NetAddress* ad = ((NetAddress*)class_offs);
		ad = (NetAddress*)addr;
	}*/

	SETTERFN(const NetAddress*, setNetAddress, TGEOFF_NETCONNECTION_NETADDRESS);

	char** getConnectArgv()
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + (0x1C + 0xA0) * 2;
		return ((char**)class_offs);
	}

	U32 getConnectArgc()
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + (0x18 + 0xA0) * 2;
		return *((U32*)class_offs);
	}

	void setConnectArgc(U32 val)
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + (0x18 + 0xA0) * 2;
		*((U32*)class_offs) = val;
	}

	ConnectionStringTable* getStringTable()
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + 0x1D4;
		return ((ConnectionStringTable*)class_offs);
	}

	void setFailThing(U32 val)
	{
		char* class_start = (char*)this;
		char* class_offs = class_start + 0x1D4;
		//*((U32*)class_offs) = val;
	}

	bool getIsEstablished() {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x150;
		bool established = *((bool*)class_offs);

		return established;//mConnectionState;
	}//U32 state = *mConnectionState; return state; }

	void setIsEstablished(bool val) {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x150;
		bool* b = ((bool*)class_offs);
		b = &val;
	}

	void setPrevConnection(NetConn* val) {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x114;
		NetConn* n = ((NetConn*)class_offs);
		n = val;
	}

	void setNextConnection(NetConn* val) {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x110;
		NetConn* n = ((NetConn*)class_offs);
		n = val;
	}

	static inline U32 HashNetAddress(const NetAddress *addr)
	{
		return *((U32 *)addr->netNum) % NetConn::HashTableSize;
	}

	/*void getHashTable(NetConn* val) {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x110;
		*((NetConn*)class_offs) = *val;
	}*/

	void setNextTableHash(NetConn* val) {

		char* class_start = (char*)this;
		char* class_offs = class_start + 0x18C;
		NetConn* n = ((NetConn*)class_offs);
		n = val;
	}

	void netAddressTableInsert()
	{
		U32 hashIndex = HashNetAddress(this->getNetAddress());
		setNextTableHash(&(daHashTable)[hashIndex]);
		NetConn* c = &(daHashTable)[hashIndex];
		c = this;
	}

	/*SimGroup* getClientGroup()
	{
	char* class_start = (char*)this;
	char* class_offs = class_start + 0xA0;
	return ((SimGroup*)class_offs);
	}*/
#endif
};

class Stream
{
	void* bla;
};

	class PathedInterior : public SimObject
	{
	public:
		GETTERFNSIMP(F64, getPathPosition, TGEOFF_PATHEDINTERIOR_PATHPOSITION);
		GETTERFNSIMP(S32, getTargetPosition, TGEOFF_PATHEDINTERIOR_TARGETPOSITION);
	};

#ifdef MBU_DEV
namespace NSE
{
	FN(NetStringEvent*, __ctor, (void* mem, int index, NetStringHandle string)/*, (mem, index, string)*/, TGEADDR_NETSTRINGTABLE_CTOR);
}
#endif

#ifdef MBU_DEV
class ConnectionStringTable
{
public:
	enum Constants {
		EntryCount = 32,
		EntryBitSize = 5,
		InvalidEntryId = 32,
	};
private:
	struct Entry {
		NetStringHandle string;   ///< Global string table entry of this string
								  ///  will be 0 if this string is unused.

		U32 index;             ///< index of this entry
		Entry *nextHash;       ///< the next hash entry for this id
		Entry *nextLink;       ///< the next in the LRU list
		Entry *prevLink;       ///< the prev entry in the LRU list
		bool receiveConfirmed; ///< The other side now has this string.
	};

	Entry mEntryTable[EntryCount];
	Entry *mHashTable[EntryCount];
	NetStringHandle mRemoteStringTable[EntryCount];
	Entry mLRUHead, mLRUTail;

	/// Connection over which we are maintaining this string table.
	NetConn *mParent;
public:

	inline void pushBack(Entry *entry) // pushes an entry to the back of the LRU list
	{
		entry->prevLink->nextLink = entry->nextLink;
		entry->nextLink->prevLink = entry->prevLink;
		entry->nextLink = &mLRUTail;
		entry->prevLink = mLRUTail.prevLink;
		entry->nextLink->prevLink = entry;
		entry->prevLink->nextLink = entry;
	}
#ifdef MBU_DEV
	MEMBERFN(int, checkString, (NetStringHandle& string, bool* isOnOtherSide), (string, isOnOtherSide), TGEADDR_NETCONNECTION_CHECKSTRING);
#endif
	/*U32 checkString(NetStringHandle &string, bool *isOnOtherSide)
	{
		// see if the entry is in the hash table right now
		U32 hashIndex = string.getIndex() % EntryCount;
		for (Entry *walk = mHashTable[hashIndex]; walk; walk = walk->nextHash)
		{
			if (walk->string == string)
			{
				pushBack(walk);
				if (isOnOtherSide)
					*isOnOtherSide = walk->receiveConfirmed;
				return walk->index;
			}
		}

		// not in the hash table, means we have to add it
		Entry *newEntry;

		// pull the new entry from the LRU list.
		newEntry = mLRUHead.nextLink;
		pushBack(newEntry);

		// remove the string from the hash table
		Entry **hashWalk;
		for (hashWalk = &mHashTable[newEntry->string.getIndex() % EntryCount]; *hashWalk; hashWalk = &((*hashWalk)->nextHash))
		{
			if (*hashWalk == newEntry)
			{
				*hashWalk = newEntry->nextHash;
				break;
			}
		}

		newEntry->string = string;
		newEntry->receiveConfirmed = false;
		newEntry->nextHash = mHashTable[hashIndex];
		mHashTable[hashIndex] = newEntry;

		void* mem = alloca(0x18);

		NetStringEvent* netStringEvent = NSE::__ctor(mem, newEntry->index, string);

		mParent->postNetEvent(netStringEvent);
		if (isOnOtherSide)
			*isOnOtherSide = false;
		return newEntry->index;
	}*/
};

#endif

namespace Bla
{
	//FN(void*, allowMulti, (void * a1), TGEADDR_ALLOWMULTI);
}

namespace Con {
	//void printf(const char *fmt, ...);

	FN(void, printf, (const char *fmt, ...), TGEADDR_CON_PRINTF);

	OVERLOAD_PTR{
		OVERLOAD_FN(void, (const char *fmt, ...),                             TGEADDR_CON_WARNF_1V);
	OVERLOAD_FN(void, (ConsoleLogEntry::Type type, const char *fmt, ...), TGEADDR_CON_WARNF_2V);
	} warnf;
}

class NetInterface
{
private:
	//unsigned char* blargh;

	//MEMBERVAR(Vector<NetConn*>, mPendingConnections, TGEOFF_NETINTERFACE_MPENDINGCONNECTIONS);

	//MEMBERVAR(Vector<NetConn*>, mPendingConnections, 0x0);

	void *vftbl;
	Vector<NetConn*>		mPendingConnections;
	U32                     mLastTimeoutCheckTime;  ///< Last time all the active connections were checked for timeouts.
	U32                     mRandomHashData[12];    ///< Data that gets hashed with connect challenge requests to prevent connection spoofing.
	bool                    mRandomDataInitialized; ///< Have we initialized our random number generator?
	bool                    mAllowConnections;      ///< Is this NetInterface allowing connections at this time?

public:
#ifdef MBU_DEV
	MEMBERFN(void, addPendingConnection, (NetConn* connection), (connection), TGEADDR_NETINTERFACE_ADDPENDINGCONNECTION);
	MEMBERFN(void, removePendingConnection, (NetConn* connection), (connection), TGEADDR_NETINTERFACE_REMOVEPENDINGCONNECTION);
	MEMBERFN(NetConn*, findPendingConnection, (const NetAddress *address, U32 connectSequence), (address, connectSequence), TGEADDR_NETINTERFACE_FINDPENDINGCONNECTION);

	/*NetConn *findPendingConnection(const NetAddress *address, U32 connectSequence)
	{
		for (S32 i = 0; i < mPendingConnections.size(); i++)
		{
			NetConn *conn = mPendingConnections[i];
			if (Net::compareAddresses(address, conn->getNetAddress()) && connectSequence == conn->getSequence())
			{
				return conn;
			}
			else {
				char buf1[256];
				Net::addressToString(conn->getNetAddress(), buf1);

				char buf2[256];
				Net::addressToString(address, buf2);
				Con::warnf("DEBUG: Connection[%d]: (%s, %d) != (%s, %d)", i, buf1, conn->getSequence(), buf2, connectSequence);
			}
		}
		return NULL;
	}
	void NetInterface::removePendingConnection(NetConn *connection)
	{
		//for (U32 i = 0; i < mPendingConnections.size(); i++)
			//if (mPendingConnections[i] == connection)
				//mPendingConnections.erase(i);
	}*/
#endif
};

class GFXD3D9Device
{
public:
	//int field_0;
	//char gap1[5];
	//bool mCanCurrentlyRender;
	char gap4[540];//-sizeof(bool) - 5];
	int field_544;
	char gap224[17048];
	LPDIRECT3DDEVICE9 mD3DDevice;

	GETTERFNSIMP(bool, getCanCurrentlyRender, TGEOFF_GFXD3D9DEVICE_CANCURRENTLYRENDER);
	SETTERFN(bool, setCanCurrentlyRender, TGEOFF_GFXD3D9DEVICE_CANCURRENTLYRENDER);
};

class GuiControl : public SimObject
{

};

struct GFXVideoMode
{
	Point2I resolution;
	U32 bitDepth;
	U32 refreshRate;
	bool fullScreen;
	bool wideScreen;
	// When this is returned from GFX, it's the max, otherwise it's the desired AA level.
	U32 antialiasLevel;
};

class Win32Window
{
public:
	//MEMBERFNSIMP(TGE::GFXVideoMode, getCurrentMode, TGEADDR_WIN32WINDOW_GETCURRENTMODE);
	GETTERFNSIMP(TGE::GFXVideoMode, getCurrentMode, TGEOFF_WIN32WINDOW_CURRENTMODE);
	GETTERFNSIMP(bool, getSupressReset, TGEOFF_WIN32WINDOW_SUPRESSRESET);
	GETTERFNSIMP(TGE::GFXD3D9WindowTarget*, getGFXTarget, TGEOFF_WIN32WINDOW_GFXTARGET);
	GETTERFNSIMP(HWND, getWindowHandle, TGEOFF_WIN32WINDOW_WINDOWHANDLE);

	bool isVisible()
	{
		// Is the window open and visible, ie. not minimized?

		if (!getWindowHandle())
			return false;

		return IsWindowVisible(getWindowHandle())
			&& !IsIconic(getWindowHandle())
			&& !isScreenSaverRunning();
	}

	static bool isScreenSaverRunning()
	{
#ifndef SPI_GETSCREENSAVERRUNNING
#define SPI_GETSCREENSAVERRUNNING 114
#endif
		// Windows 2K, and higher. It might be better to hook into
		// the broadcast WM_SETTINGCHANGE message instead of polling for
		// the screen saver status.
		BOOL sreensaver = false;
		SystemParametersInfo(SPI_GETSCREENSAVERRUNNING, 0, &sreensaver, 0);
		return sreensaver;
	}
};

enum DispatchType {
	DelayedDispatch,
	ImmediateDispatch,
};

FN(void, Dispatch, (DispatchType type, HWND hWnd, UINT message, WPARAM wParam, WPARAM lParam), TGEADDR_WIN32WINDOW_DISPATCH);

class GuiCanvas
{
public:
	MEMBERFNSIMP(TGE::Win32Window*, getPlatformWindow, TGEADDR_GUICANVAS_GETPLATFORMWINDOW);
};

class GuiArrayCtrl : public GuiControl
{

};

class GuiConsole : public GuiArrayCtrl
{
public:
	GuiConsole()
	{

	}
	bool onWake();
	S32 GuiConsole::getMaxWidth(S32 startIndex, S32 endIndex);
	void GuiConsole::onPreRender();
	void GuiConsole::onRenderCell(Point2I offset, Point2I cell, bool /*selected*/, bool /*mouseOver*/);
};

class GGCUnknown
{
public:
	void *data;
};

//#ifdef MBU_DEV
FN(GFXDeviceD3D9*, getActiveGFXDevice, (), TGEADDR_GFXDEVICE_GETACTIVEDEVICE);
//#endif

	namespace Members
	{
		namespace Namespace
		{
			FN(void, init, (), TGEADDR_NAMESPACE_INIT);
			FN(void*, find, (const char *name, const char *package), TGEADDR_NAMESPACE_FIND);
		}

		namespace NetInterface
		{
			RAWMEMBERFN(TGE::NetInterface, void, processPacketReceiveEvent, (TGE::NetAddress srcAddress, TGE::RawData packetData), TGEADDR_NETINTERFACE_PROCESSPACKETRECEIVEEVENT);
#ifdef MBU_DEV
			RAWMEMBERFN(TGE::NetInterface, void, startConnection, (TGE::NetConn *conn), TGEADDR_NETINTERFACE_STARTCONNECTION);
			RAWMEMBERFN(TGE::NetInterface, void, processClient, (), TGEADDR_NETINTERFACE_PROCESSCLIENT);
			RAWMEMBERFN(TGE::NetInterface, void, processServer, (), TGEADDR_NETINTERFACE_PROCESSSERVER);
#endif
		}

#ifdef MBU_DEV
		namespace NetConn2
		{
			RAWMEMBERFN(TGE::NetConn, bool, postNetEvent, (void *theEvent), TGEADDR_NETCONNECTION_POSTNETEVENT);
			RAWMEMBERFN(TGE::NetConn, void, validateSendString, (const char* str), TGEADDR_NETCONNECTION_VALIDATESENDSTRING);
			RAWMEMBERFN(TGE::NetConn, void, handleNotify, (bool recvd), TGEADDR_NETCONNECTION_HANDLENOTIFY);
		}

		namespace GameConn
		{
			RAWMEMBERFN(TGE::NetConn, bool, readConnectAccept, (BitStream *stream, const char **errorString), TGEADDR_GAMECONNECTION_READCONNECTACCEPT);
		}

		namespace NetStringTBL
		{
			RAWMEMBERFN(TGE::NetStringTable, void, removeString, (U32 id, bool script), TGEADDR_NETSTRINGTABLE_INCSTRINGREF);
		}
#endif

		namespace TCPObj
		{
#ifndef MBU_DEV
			RAWMEMBERFN(TGE::TCPObject, void, onConnectionRequest, (const NetAddress *addr, U32 connectId), TGEADDR_TCPOBJECT_ONCONNECTIONREQUEST);
			RAWMEMBERFN(TGE::TCPObject, bool, processLine, (UTF8* line), TGEADDR_TCPOBJECT_PROCESSLINE);
			RAWMEMBERFNSIMP(TGE::TCPObject, void, onDNSResolved, TGEADDR_TCPOBJECT_ONDNSRESOLVED);
			RAWMEMBERFNSIMP(TGE::TCPObject, void, onDNSFailed, TGEADDR_TCPOBJECT_ONDNSFAILED);
			RAWMEMBERFNSIMP(TGE::TCPObject, void, onConnected, TGEADDR_TCPOBJECT_ONCONNECTED);
			RAWMEMBERFNSIMP(TGE::TCPObject, void, onConnectFailed, TGEADDR_TCPOBJECT_ONCONNECTFAILED);
			RAWMEMBERFNSIMP(TGE::TCPObject, void, onDisconnect, TGEADDR_TCPOBJECT_ONDISCONNECT);
#endif // MBU_DEV
		}

		namespace BitStream
		{

		}

		namespace SimGroup
		{
#ifdef MBU_DEV
			RAWMEMBERFN(TGE::SimGroup, void, addObject, (SimObject* obj), TGEADDR_SIMGROUP_ADDOBJECT);
#endif
		}

		namespace SimObject
		{
#ifdef MBU_DEV
			RAWMEMBERFN(TGE::SimObject, void, unregisterObject, (), TGEADDR_SIMOBJECT_UNREGISTEROBJECT);
			RAWMEMBERFN(TGE::SimObject, bool, save, (const char* pcFileName, bool bOnlySelected), TGEADDR_SIMOBJECT_SAVE);
#endif
			//RAWMEMBERFN(TGE::SimObject, void, assignName, (const char* name), TGEADDR_SIMOBJECT_ASSIGNNAME);
			//RAWMEMBERFN(TGE::SimObject, bool, registerObject, (), TGEADDR_SIMOBJECT_REGISTEROBJECT);
			//FN(void*, _new, (void* buf, const U8 namespaceLinkMask), TGEADDR_SIMOBJECT_CONSTRUCTOR);
			//GLOBALVAR(TGE::ConcreteClassRep<TGE::SimObject>, dynClassRep, TGEADDR_SIMOBJECT_CONCRETECLASSREP);
		}

		namespace Marble
		{
			//RAWMEMBERFN(TGE::Marble, void, MarblePhysics, (const void *move, U32 delta), TGEADDR_MARBLE_ADVANCEPHYSICS);
		}

		namespace CodeBlock
		{
			RAWMEMBERFN(TGE::CodeBlock, const char *, exec, (U32 ip, const char *functionName, TGE::Namespace *thisNamespace, U32 argc, const char **argv, bool noCalls, StringTableEntry packageName, S32 setFrame), TGEADDR_CODEBLOCK_EXEC);
		}

#ifdef MBU_DEV
		namespace Stream
		{
			RAWMEMBERFN(TGE::Stream, void, writeTabs, (U32 count), TGEADDR_STREAM_WRITETABS);
		}
#endif

		namespace GFXD3D9Device
		{
			//RAWMEMBERFN(TGE::GFXD3D9Device, bool, beginSceneInternal, (), TGEADDR_GFXD3D9DEVICE_BEGINSCENEINTERNAL);
//#ifdef MBU_DEV // TODO: Implement on standard
			RAWMEMBERFNSIMP(TGE::GFXD3D9Device, bool, checkDevice, TGEADDR_GFXD3D9DEVICE_CHECKDEVICE);

			RAWMEMBERFNSIMP(TGE::GFXD3D9Device, bool, checkDeviceLost1, TGEADDR_CHECKDEVICELOST1);
			RAWMEMBERFNSIMP(TGE::GFXD3D9Device, bool, checkDeviceLost2, TGEADDR_CHECKDEVICELOST2);
//#endif
		}

		namespace Win32Window
		{
			RAWMEMBERFN(TGE::Win32Window, bool, windowProc, (HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam), TGEADDR_WIN32WINDOW_WINDOWPROC);
		}

		namespace GuiSpeedometerHud
		{
#ifdef MBU_DEV
			RAWMEMBERFN(TGE::GuiSpeedometerHud, void, onRender, (Point2I offset, const RectI &updateRect), TGEADDR_GUISPEEDOMETERHUD_ONRENDER);
#endif
		}
		namespace GuiConsole
		{
			/*namespace
			{
				void* GuiConsole_VTABLE = reinterpret_cast<void*>(TGEOFF_GUICONSOLE_VTABLE);
			}
			RAWMEMBERFN(TGE::GuiConsole, void, _new, (), TGEADDR_GUICONSOLE_CONSTRUCTOR);
			RAWMEMBERFN(TGE::GuiConsole, bool, onWake, (), TGEADDR_GUICONSOLE_ONWAKE);
			RAWMEMBERFN(TGE::GuiConsole, void, onPreRender, (), TGEADDR_GUICONSOLE_ONPRERENDER);
			RAWMEMBERFN(TGE::GuiConsole, void, onRenderCell, (Point2I offset, Point2I cell, bool selected, bool mouseOver), TGEADDR_GUICONSOLE_ONRENDERCELL);*/
		}

		namespace GGCUnknown
		{
			//RAWMEMBERFN(TGE::GGCUnknown, int, GGCAddCallback, (const char* name, void(*callback)(), void* unknown), TGEADDR_GGCONNECT_ADDVAR);
			//RAWMEMBERFN(TGE::GGCUnknown, int, GGCLibConnect, (), TGEADDR_LIBGGC_CONNECT);
			//RAWMEMBERFN(TGE::GGCUnknown, bool, GGCBadFood, (), TGEADDR_GGCONNECT_BADFOOD);

			//RAWMEMBERFN(TGE::GGCUnknown, int, makeWindowGGC, (), TGEADDR_MAKEWINDOWGGC);
		}

		//namespace AbstractClassRep
		//{
		//	RAWMEMBERFN(TGE::AbstractClassRep, void, registerClassRep, (TGE::AbstractClassRep* in_pRep), TGEADDR_CON_ABSTRACTCLASSREP_REGISTERCLASSREP);
		//	//RAWMEMBERFN(TGE::ClassRep, void, registerClassRep, (TGE::ClassRep* in_pRep), TGEADDR_CON_ABSTRACTCLASSREP_REGISTERCLASSREP);
		//}

		//namespace ConcreteClassRep
		//{
		//	
		//}

	}

#ifdef MBU_DEV
	FN(char*, ___RTDynamicCast, (int a1, int a2, void *a3, void *a4, int a5), TGEADDR___RTDYNAMICCAST);
#endif

	namespace Net
	{
#ifndef MBU_DEV
		FN(bool, stringToAddress, (const char *addressString, NetAddress *address), TGEADDR_NET_STRINGTOADDRESS);
#endif // MBU_DEV
	}

	namespace ConObj
	{
		FN(ConsoleObject*, create, (const char* name), TGEADDR_CONSOLEOBJECT_CREATE);
	}

#ifdef MBU_DEV
	namespace AbstractClassRep2
	{
		FN(U32, getClassCRC, (U32 index), TGEADDR_ABSTRACTCLASSREP_GETCLASSCRC);
	}

	namespace Platform
	{
		FN(U32, getVirtualMilliseconds, (void), TGEADDR_PLATFORM_GETVIRTUALMILLISECONDS);
	}

#endif

	namespace BStream
	{
//#ifdef MBU_DEV
		FN(TGE::BitStream*, getPacketStream, (void), TGEADDR_BITSTREAM_GETPACKETSTREAM);
		FN(void, sendPacketStream, (const NetAddress *addr), TGEADDR_BITSTREAM_SENDPACKETSTREAM);
//#endif
	}

	class Void
	{
		void* v;
		virtual void* getV()
		{
			return v;
		}
	};

	FN(void*, dynCast, (Void* a1, Void* a2, Void *a3, Void *a4, Void* a5), 0x692A67);

	namespace Sim
	{
		//FN(SimObject*, findObject, (const char* name), TGEADDR_SIM_FINDOBJECT);

/*#ifdef MBU_DEV
		FN(SimObject*, findObject, (const char* name), TGEADDR_SIM_FINDOBJECT_1);
#endif*/

		FN(SimObject*, findObject, (const char* name), TGEADDR_SIM_FINDOBJECT);
	}

	namespace GGConnect
	{
		enum GGC_ERROR
		{
			GGC_SUCCESS = 0x0,
			GGC_ERROR_Timeout = 0xA,
			GGC_ERROR_LibNotInitialized = 0x65,
			GGC_ERROR_LibAlreadyInitialized = 0x66,
			GGC_ERROR_NetworkInit = 0x67,
			GGC_ERROR_InvalidParam = 0x68,
			GGC_ERROR_InvalidQueryID = 0x6C,
			GGC_ERROR_Communication = 0x6F,
			GGC_ERROR_CommunicationFatal = 0x70,
			GGC_ERROR_Memory = 0x71,
			GGC_ERROR_NetworkDown = 0x1F4,
			GGC_ERROR_ConnectionFailed = 0x1F5,
			GGC_ERROR_DiskError = 0x1F6,
			GGC_ERROR_ObjectNotFound = 0x1F7,
			GGC_ERROR_CommandNotFound = 0x1F8,
			GGC_ERROR_InvalidArguments = 0x1F9,
			GGC_ERROR_Aborted = 0x1FA,
			GGC_ERROR_Max = 0x3EC,
		};

		class GGCStruct
		{
			virtual int bla(int x)
			{
				Con::printf("Bla");
				return 0;
			}
			virtual int blorph(int ga)
			{
				Con::printf("Blorph");
				return 0;
			}
			int potato;
			int pickle;
		};

		typedef int(__cdecl *GGConnectCallback)(int, int, int);
#ifdef MBU_DEV
		STDFN(GGC_ERROR, SetGGConnectState1, (GGConnectCallback callback, int a2), TGEADDR_SETGGCSTATE_1);
		FN(GGC_ERROR, SetGGConnectState2, (), TGEADDR_SETGGCSTATE_2);

		GLOBALVAR(bool, GGCInitialized, TGEVAR_GGCINITIALIZED);
		GLOBALVAR(GGCStruct, GGCInstance, TGEVAR_GGCSTRUCT);

		FN(HWND, getParentHWND, (), TGEADDR_PARENTHWND);
#endif
	}

	namespace Con
	{
		//FN(void, debugPrint, (int *a1, char* fmt, ...), TGEADDR_DEBUG_PRINT);

		// Logging
		//FN(void, printf, (const char *fmt, ...), TGEADDR_CON_PRINTF); // Moved above for testing
		FN(void, _printf, (ConsoleLogEntry::Level level/*, ConsoleLogEntry::Type type*/, const char *fmt, va_list argptr), TGEADDR_CON__PRINTF);

		/*OVERLOAD_PTR { // Moved above for testing
			OVERLOAD_FN(void, (const char *fmt, ...),                             TGEADDR_CON_WARNF_1V);
			OVERLOAD_FN(void, (ConsoleLogEntry::Type type, const char *fmt, ...), TGEADDR_CON_WARNF_2V);
		} warnf;*/
		OVERLOAD_PTR {
			OVERLOAD_FN(void, (const char *fmt, ...),                             TGEADDR_CON_ERRORF_1V);
			OVERLOAD_FN(void, (ConsoleLogEntry::Type type, const char *fmt, ...), TGEADDR_CON_ERRORF_2V);
		} errorf;

		// addCommand()
		OVERLOAD_PTR {
			OVERLOAD_FN(void, (const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),                     TGEADDR_CON_ADDCOMMAND_5_STRING);
			OVERLOAD_FN(void, (const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),                       TGEADDR_CON_ADDCOMMAND_5_VOID);
			OVERLOAD_FN(void, (const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),                        TGEADDR_CON_ADDCOMMAND_5_INT);
			OVERLOAD_FN(void, (const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),                      TGEADDR_CON_ADDCOMMAND_5_FLOAT);
			OVERLOAD_FN(void, (const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),                       TGEADDR_CON_ADDCOMMAND_5_BOOL);
			OVERLOAD_FN(void, (const char *nsName, const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header), TGEADDR_CON_ADDCOMMAND_6_STRING);
			OVERLOAD_FN(void, (const char *nsName, const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),   TGEADDR_CON_ADDCOMMAND_6_VOID);
			OVERLOAD_FN(void, (const char *nsName, const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),    TGEADDR_CON_ADDCOMMAND_6_INT);
			OVERLOAD_FN(void, (const char *nsName, const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),  TGEADDR_CON_ADDCOMMAND_6_FLOAT);
			OVERLOAD_FN(void, (const char *nsName, const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs, bool isToolOnly, ConsoleFunctionHeader *header),   TGEADDR_CON_ADDCOMMAND_6_BOOL);
		} addCommand;

		// Variables
		FN(void, setVariable,      (const char *name, const char *value), TGEADDR_CON_SETVARIABLE);
		FN(void, setLocalVariable, (const char *name, const char *value), TGEADDR_CON_SETLOCALVARIABLE);
		FN(void, setBoolVariable,  (const char *name, bool value),        TGEADDR_CON_SETBOOLVARIABLE);
		FN(void, setIntVariable,   (const char *name, S32 value),         TGEADDR_CON_SETINTVARIABLE);
		FN(void, setFloatVariable, (const char *name, F32 value),         TGEADDR_CON_SETFLOATVARIABLE);

		FN(const char*, getVariable, (const char *name), TGEADDR_CON_GETVARIABLE);
		FN(const char*, getLocalVariable, (const char *name), TGEADDR_CON_GETLOCALVARIABLE);
		FN(bool, _getBoolVariable, (const char *name, bool def), TGEADDR_CON_GETBOOLVARIABLE);
		inline bool getBoolVariable(const char* name, bool def = false) { return _getBoolVariable(name, def); }
		FN(S32, _getIntVariable, (const char *name, S32 def), TGEADDR_CON_GETINTVARIABLE);
		inline S32 getIntVariable(const char* name, S32 def = 0) { return _getIntVariable(name, def); }
		FN(F32, _getFloatVariable, (const char *name, F32 def), TGEADDR_CON_GETFLOATVARIABLE);
		inline F32 getFloatVariable(const char* name, F32 def = .0f) { return _getFloatVariable(name, def); }

#ifdef MBU_DEV
		FN(const char*, execute, (SimObject *object, S32 argc, const char *argv[]), TGEADDR_CON_EXECUTE);
#endif
		FN(const char*, executef, (S32 argc, ...), TGEADDR_CON_EXECUTEF);
		FN(const char*, evaluatef,            (const char* string, ...),                             TGEADDR_CON_EVALUATEF);
		FN(char*, getIntArg, (S32 arg), TGEADDR_CON_GETINTARG);
		FN(char*, getReturnBuffer, (U32 bufferSize), TGEADDR_CON_GETRETURNBUFFER);

		FN(bool, classLinkNamespaces, (TGE::Namespace *parent, TGE::Namespace *child), TGEADDR_CON_CLASSLINKNAMESPACES);

#ifdef MBU_DEV
		FN(bool, unlinkNamespaces, (const char *parent, const char *child), TGEADDR_CON_UNLINKNAMESPACES);
#endif
	}

	FN(TGE::NetConn*, getConnectionToServer, (), TGEADDR_GAMECONNECTION_GETCONNECTIONTOSERVER);

	namespace BadWordFilter
	{
		FN(void, create, (), TGEADDR_BADWORDFILTER_CREATE);
	}
	GLOBALVAR(void*, mGlobalNamespace, TGEADDR_GLOBALNAMESPACE);

#ifdef MBU_DEV
	GLOBALVAR(SimGroup*, gClientGroup, TGEADDR_SIM_CLIENTGROUP);

	GLOBALVAR(NetConn*, mConnectionList, TGEADDR_NETCONNECTION_CONLIST);

	GLOBALVAR(const char*, defLogFileName, TGEADDR_DEFLOGFILENAME);
#endif

}

// ConsoleFunction() can't be used from inside PluginLoader.dll without crashes
#ifndef IN_PLUGIN_LOADER

namespace TGE
{
	// Psuedo class used to implement the ConsoleFunction macro.
	class _ConsoleConstructor
	{
	public:
		_ConsoleConstructor(const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *nsName, const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(nsName, name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *nsName, const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(nsName, name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *nsName, const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(nsName, name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *nsName, const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(nsName, name, cb, usage, minArgs, maxArgs, 0, NULL);
		}

		_ConsoleConstructor(const char *nsName, const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			Con::addCommand(nsName, name, cb, usage, minArgs, maxArgs, 0, NULL);
		}
	};
}

// Defines a console function.
#define ConsoleFunction(name, returnType, minArgs, maxArgs, usage)                         \
	static returnType c##name(TGE::SimObject *, S32, const char **argv);                   \
	static TGE::_ConsoleConstructor g##name##obj(#name, c##name, usage, minArgs, maxArgs); \
	static returnType c##name(TGE::SimObject *, S32 argc, const char **argv)

// Hacks to handle properly returning different value types from console methods
#define CONMETHOD_NULLIFY(val)
#define CONMETHOD_RETURN_const return (const
#define CONMETHOD_RETURN_S32   return (S32
#define CONMETHOD_RETURN_F32   return (F32
#define CONMETHOD_RETURN_void  CONMETHOD_NULLIFY(void
#define CONMETHOD_RETURN_bool  return (bool

// Defines a console method.
#define ConsoleMethod(className, name, type, minArgs, maxArgs, usage)                                                                \
	static type c##className##name(TGE::className *, S32, const char **argv);                                                        \
	static type c##className##name##caster(TGE::SimObject *object, S32 argc, const char **argv) {                                    \
		if (!object)                                                                                                                 \
			TGE::Con::warnf("Object passed to " #name " is null!");                                                                  \
		CONMETHOD_RETURN_##type ) c##className##name(static_cast<TGE::className*>(object), argc, argv);                              \
	};                                                                                                                               \
	static TGE::_ConsoleConstructor g##className##name##obj(#className, #name, c##className##name##caster, usage, minArgs, maxArgs); \
	static type c##className##name(TGE::className *object, S32 argc, const char **argv)
	
#endif // IN_PLUGIN_LOADER

#endif