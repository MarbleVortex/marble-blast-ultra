#include <PluginLoader/PluginInterface.h>
#include <TGE.h>
#include <TorqueLib/QuickOverride.h>

TorqueOverride(void, NetConnection::checkMaxRate, (), originalCheckMaxRate)
{
	//TGE::Con::warnf("NetConnection::CheckMaxRate: Overridden!");
	//originalCheckMaxRate();
}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	//TGE::Con::printf("Overriding NetConnection::CheckMaxRate");
}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{

}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{

}

