#include <TorqueLib\platform\platform.h>
#include <TGE.h>
#include <TorqueLib\QuickOverride.h>
#include <PluginLoader\PluginInterface.h>

using namespace std;
using namespace TGE;
using namespace Con;

ConsoleFunction(createCanvas, bool, 2, 2, "createCanvas();")
{
	string cmd;
	cmd += "%canvas = new GuiCanvas(Canvas);";
	cmd += "%canvas.setWindowTitle(\"";
	cmd += argv[1];
	cmd += "\");";
	//cmd += "initRenderInstManager();";
	evaluatef("%s", cmd.c_str());

	return true;
}

ConsoleFunction(onWindowCreated, void, 1, 1, "onWindowCreated();")
{
	/*string cmd;
	cmd += "%canvas = new GuiCanvas(Canvas);";
	cmd += "%canvas.setWindowTitle(\"";
	cmd += argv[1];
	cmd += "\");";
	cmd += "initRenderInstManager();";
	evaluatef("%s", cmd.c_str());

	return true;*/

	executef(1, "initRenderInstManager");
}

//$BKLoader::loadPct
//$BKLoader::decompressPct
//$BKLoader::addPct
//%s/%s loaded from %s - %s %s
//Finished BSA %s

bool resourceBGLoaded = false;
bool isSinglePlayerMode = false;
const char* zipName;

ConsoleFunction(setSinglePlayerMode, void, 2, 2, "setSinglePlayerMode(spmode)")
{
	isSinglePlayerMode = dAtob(argv[1]);
}

ConsoleFunction(isSinglePlayerMode, bool, 1, 1, "isSinglePlayerMode()")
{
	return isSinglePlayerMode;
}

ConsoleFunction(getResolution, const char*, 1, 1, "getResolution()")
{
	return evaluatef("Canvas.getVideoMode();");
}

ConsoleFunction(getRes, const char*, 1, 1, "getRes()")
{
	/*const char* result = Con::evaluatef("Canvas.getResolution();");

	char* buf = Con::getReturnBuffer(strlen(result) + 1);
	strcpy_s(buf, strlen(result) + 1, result);

	return buf;*/
	return evaluatef("Canvas.getVideoMode();");
}

ConsoleFunction(loadZip, void, 2, 2, "loadZip(zipName);")
{
	zipName = argv[1];
	/*for (int i = 0; i < 101; i++)
	{
		setIntVariable("$BKLoader::loadPct", i);
		setIntVariable("$BKLoader::decompressPct", i);
		setIntVariable("$BKLoader::addPct", i);
		if (i == 50)
		{
			resourceBGLoaded = true;
		}
		Con::printf("%d/%d loaded from %s - %s %s", i*312, 100*312, argv[1], i, "%");
	}*/
	resourceBGLoaded = true;
	Con::printf("Finished BSA %s", argv[1]);
	executef(2, "onZipLoaded", argv[1], false);//argv[1], true);
}

ConsoleFunction(isResourceBGLoaded, bool, 2, 2, "isResourceBGLoaded(resource)")
{
	bool result = resourceBGLoaded;
	//Con::printf("Finished BSA %s", zipName);
	//executef(2, "onZipLoaded", zipName, false);
	return result;
}

std::string string_replace(std::string s, std::string find, std::string replace)
{
	std::string result = s;
	int index = -1;
	while (index < (int)result.size())
	{
		index = result.find(find, index + 1);
		if (index == -1)
			break;
		result = result.replace(index, find.size(), replace);
		index += replace.size();
	}
	return result;
}

ConsoleFunction(filePath, const char*, 2, 2, "filePath(fileName)")
{
	const char* result = Con::executef(2, "filxPath", argv[1]);

	std::string s = result;
	HMODULE mod = GetModuleHandle(NULL);
	char path_[2048];
	GetModuleFileName(mod, path_, sizeof(path_));
	std::string path = path_;
	path = string_replace(path, "\\", "/");
	int index = path.find_last_of('/');
	path = path.substr(0, index + 1);
	//Con::errorf("P: %s", path.c_str());
	size_t i = path.length();
	if (i >= s.length() || s.find(path) == -1)
	{
		return result;
	}
	//int i = s.find(path.c_str());
	//errorf("S: %s", path.c_str());
	//errorf("Len: %d", i);
	std::string x = s.substr(i);//, s.length() - index);
	//errorf("New: %s", x.c_str());

	char* buf = Con::getReturnBuffer(x.length()+1);
	strcpy_s(buf, x.length()+1, x.c_str());

	return buf;
}

#ifdef MBU_DEV
TorqueOverride(bool, Con::unlinkNamespaces, (const char *parent, const char *child), originalUnlinkNamespaces)
{
	return true;
}
#endif

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
	
}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{
	
}
