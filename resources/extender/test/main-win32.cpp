#include <iostream>
#include <cstring>
#include "PEFile.h"

const char *DefaultSuffix = "-patched";        // EXE suffix to append when only one argument is passed
const char *DefaultSection = ".text";

#define KB 1024
#define MB 1024 * KB
#define GB 1024 * MB

int DefaultExpansionSize = 10 * KB;

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cout << "TestPatcher: Modifies an exe file to expand a section" << std::endl;
		std::cout << "Usage: TestPatcher <path to exe file> [path to output EXE]" << std::endl;

		system("pause");
		return 1;
	}

	//if (argc > 1)
//	{
		//int expansionSize = ;
	//}

	char *inputPath = argv[1];
	std::string outputPath;
	if (argc == 3)
	{
		// Take the output path from the command line
		outputPath = argv[2];
	}
	else
	{
		// Append the suffix onto the end of the EXE name
		outputPath = inputPath;
		size_t extensionStart = outputPath.find('.');
		if (extensionStart != std::string::npos)
			outputPath.insert(extensionStart, DefaultSuffix);
		else
			outputPath += DefaultSuffix;
	}

	// Load the input file
	PEFile inputFile;
	bool result = inputFile.loadFromFile(inputPath);
	if (!result)
	{
		std::cerr << "Error: invalid EXE file!" << std::endl;

		system("pause");
		return 2;
	}

	// Make sure the DLL isn't already imported
	/*PE_IMPORT_DLL *importTable = &inputFile.importTable;
	while (importTable)
	{
		if (strcmp(importTable->DllName, DllName) == 0)
		{
			std::cerr << "Error: the input EXE file already loads " << DllName << "!" << std::endl;
			return 3;
		}
		importTable = importTable->Next;
	}

	// Add the import and save the file
	inputFile.addImport(DllName, DllFunctions, sizeof(DllFunctions) / sizeof(DllFunctions[0]));
	if (!inputFile.saveToFile(&outputPath[0]))
	{
		std::cerr << "Error: failed to save the output file!" << std::endl;
		return 4;
	}*/

	for (int i = 0; i < MAX_SECTION_COUNT; i++)
	{
		PE_SECTION_HEADER section = inputFile.sectionTable[i];
		std::string name((char*)section.Name);
		
		if (name.length() == 0)
			continue;

		if (name == DefaultSection)
		{
			printf("Expanding %s Section by %d bytes\n", name.c_str(), DefaultExpansionSize);

			//inputFile.sections[i].Size += DefaultExpansionSize;
			if (!inputFile.saveToFile(&outputPath[0]))
			{
				std::cerr << "Error: failed to save the output file!" << std::endl;
				return 4;
			}
			break;
		}

	}

	//inputFile.sectionTable-> += DefaultExpansionSize;
	
	std::cout << "EXE patched successfully!" << std::endl;

	system("pause");
	return 0;
}