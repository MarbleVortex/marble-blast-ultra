/// Triggers the process by which the client begins the attempt to connect to a
/// multiplayer server.
function Game::clientInit()
{
    ggcConsoleLog("Game::clientInit()");
    setNetPort(0, 0);
    ggcSetIPDiscoveryServers();
}

function Game::clientStart()
{
    ggcConsoleLog("Game::clientStart()");
    $naturalEnd = 0;
}

/// The client should actually join the host server in this function.  After
/// successfully completing this process, the client is no longer  in control of

/// their own state. Their game is slaved to the server and level start/end
/// commands will come from the host's game. Client remains in this state until
/// they call game.client.end (see below).
///
/// \param %hostIp (string) The host formatted as 255.255.255.255
/// \param %hostPort (int) The host port
function Game::clientRegistrationComplete(%result, %hostIp, %hostPort)
{
    ggcConsoleLog("Game::clientRegistrationComplete(" @ %result @ ", " @ %hostIp @ ", " @ %hostPort @ ")");
    if (%result == 0)
    {
        $state = $state::client;
        GgcSetLoginInfo();
        %hostAddress = %hostIp @ ":" @ %hostPort;
        connectToServer(%hostAddress, 0);
    }
    else
    {
        onGgcConnectionComplete(%result, "", 1);
    }
}

function clientCmdDoEndHandshake()
{
    if (State::isClient())
    {
        $naturalEnd = 1;
        ggcSendRpc("game.client.end");
    }
}

function clientCmdHostEnded()
{
    if (State::isClient())
    {
        $naturalEnd = 0;
        ggcSendRpc("game.client.end");
    }
}

function Game::clientEnd()
{
    ggcConsoleLog("Game::clientEnd()");
    if ($naturalEnd)
    {
        escapeFromGame();
    }
    if (!$GameEndHandshakeWaiting)
    {
        $SessionRunning = 0;
        sendGGCMessage($ggcMessageNameSessionKilled, $ggcSuccess);
    }
}

/// The client will disconnect from the server.
function Game::clientShutdown()
{
    ggcConsoleLog("Game::clientShutdown()");
    if (!$GameEndHandshakeWaiting)
    {
        $GameEndHandshakeWaiting = 1;
        escapeFromGame();
        $GameEndHandshakeWaiting = 0;
    }
    cursorOn();
    $state = $state::none;
}