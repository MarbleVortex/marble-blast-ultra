/// Transitions the game from the "none" state to the "loaded" state.
///
/// \param %videoMode (string) The video resolution to init as "width height"
function Game::init(%videoMode)
{
    ggcConsoleLog("Game::init(" @ %videoMode @ ")");
    if (!mbuInit2(%videoMode))
    {
        %ret = getReturnError($ggcMessageVidInitFailed, "Failed to init video");

        quit();
        return %ret;
    }
    else
    {
        mbuInit3();
    }
    GGConnectRefreshUserInfo();
    $Player::name = GGConnectGetUserName();
    $Player::XBLiveId = GGConnectGetUserID();
    return $ggcSuccess;
}

function Game::shutdown()
{
    ggcConsoleLog("Game::shutdown()");
    return $ggcSuccess;
}

/// Forces the game to grab focus from whatever might currently have it. This
/// has no effect if the game is already focused.
function Game::setFocus()
{
    ggcConsoleLog("Game::setFocus()");
    Canvas.setFocus();
    if ($gamePauseWanted)
    {
        GamePauseGui.doUnpause();
    }
    Canvas.checkCursor();
    GGCSignalSetFocus();
    return $ggcSuccess;
}

/// Attempts to pause the game. The game will only pause if it is in a state
/// where it is reasonable to do so.
function Game::pause()
{
    if (((!$gamePauseWanted && $Client::connectedMultiplayer) || $Game::Running)
 && $SessionRunning)
    {
        ggcConsoleLog("Game::pause()");
        pauseToggle(0);
    }
    GGCSignalBlurSession();
    return $ggcSuccess;
}

/// Attempts to unpause the game. The game only unpauses if it is in a state
/// where it is reasonable to do so.
function Game::resume()
{
    ggcConsoleLog("Game::resume()");
    if ($gamePauseWanted)
    {
        GamePauseGui.doUnpause();
    }
    return $ggcSuccess;
}

function Game::checkOwnership(%gameMode, %levelId)
{
    %code = GGConnectCheckContentOwnership("mode", %gameMode);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    %code = GGConnectCheckContentOwnership("level", %levelId);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    return $ggcSuccess;
}

function Game::setIPDiscoveryServers(%server1, %server2)
{
    ggcConsoleLog("Game::setIPDiscoveryServers(" SPC %server1 @ "," @ %server2 @ ")");
    $GGC::IPDiscoveryServer1 = %server1;
    $GGC::IPDiscoveryServer2 = %server2;
    return $ggcSuccess;
}

function Game::setForceTURN(%force)
{
    $GGC::ForceTurn = %force;
    return $ggcSuccess;
}

function Game::setTURNServer(%server)
{
    $GGC::TURNServer = %server;
    $GGC::UsingTurn = 1;
    return $ggcSuccess;
}

function Game::installContent()
{
    hotContentInstall();
    return $ggcSuccess;
}

function Game::saveJournal(%fileName)
{
    saveJournal(%fileName);
}

function Game::playJournal(%fileName)
{
    playJournal(%fileName, 0);
}