function onGCCSessionStarted()
{
    $SessionRunning = 1;
    sendGGCMessage($GGCMessageNameSessionStarted, $ggcSuccess);
}

function onGGCSessionResumed()
{
    if (!$SessionRunning)
    {
        sendGGCMessage($GGCMessageNameSessionResumed, $ggcSuccess);
    }
}

function onGGCSessionPaused()
{
    if (!$SessionRunning)
    {
        sendGGCMessage($GGCMessageNameSessionPaused, $ggcSuccess);
    }
}

/// Called when the game connection is complete with any result (success,
/// failure, etc.).
///
/// \param %status The resulting code of the function.
/// \param %message An optional, additional message.
/// \param %force Force sending ConnectionComplete even if client does not
/// appear to be connected multiplayer (default false)
function onGgcConnectionComplete(%status, %message, %force)
{
    if (!$Client::connectedMultiplayer && !%force)
    {
        sendGGCMessage("ConnectionComplete", %status, %message);
    }
}

function onGgcHostRegistrationComplete(%status, %message)
{
    sendGGCMessage("HostRegistrationComplete", %status, %message);
}

function onGgcNetworkAccess(%status, %message)
{
    sendGGCMessage("NetworkAccess", %status, %message);
}

function GgcSetLoginInfo()
{
    $Player::name = GGConnectGetUserName();
    $Player::XBLiveId = GGConnectGetUserID();
}

function ggcSetIPDiscoveryServers()
{
    if ($GGC::IPDiscoveryServer1 $= "")
    {
        $GGC::IPDiscoveryServer1 = "207.162.215.210:27999";
        error("$GGC::IPDiscoveryServer1 not set! Hardcoding to:" SPC $GGC::IPDiscoveryServer1);
    }
    if ($GGC::IPDiscoveryServer2 $= "")
    {
        $GGC::IPDiscoveryServer2 = "207.162.215.209:27999";
        error("$GGC::IPDiscoveryServer2 not set! Hardcoding to:" SPC $GGC::IPDiscoveryServer2);
    }
}

function Game::initiateGameEndHandshake()
{
    $GameEndHandshakeAcknowledged = 0;
    sendGGCMessage($ggcMessageNameSessionEnded, $ggcSuccess);
    $SessionRunning = 0;
    moveMap.pop();
    if (RootGui.contentGui.getId() $= PlayGui.getId())
    {
        RootGui.removeContent();
    }
    cursorOn();
    $GameEndHandshakeWaiting = 1;
}

function clientCmdDoAckEnd()
{
    if (State::isClient())
    {
        ggcSendRpc("game.client.ackEnd");
    }
}

/// Signal from the browser indicating it is safe to kill the play gui.
function Game::acknowledgeEndSession()
{
    ggcConsoleLog("Game::acknowledgeEndSession()");
    if (!$GameEndHandshakeWaiting)
    {
        return getReturnError($ggcGameEndHandshakeNotWaiting, "Game end handshake not initialized");
    }
    $GameEndHandshakeAcknowledged = 1;
    fadeOutAudio();
    if (!State::isClient())
    {
        call($GameEndHandshakeCaller);
    }
    if (State::isHosting())
    {
        endMission();
        %i = 0;
        while (%i < ClientGroup.getCount())
        {
            %client = ClientGroup.getObject(%i);
            commandToClient(%client, 'DoAckEnd');
            %i = %i + 1;
        }
    }
    sendGGCMessage($ggcMessageNameSessionKilled, $ggcSuccess);
    schedule(100, 0, GGCStatsSaveStats);
    $naturalEnd = 0;
    $GameEndHandshakeWaiting = 0;
    return $ggcSuccess;
}