$state::none = 0;
$state::host = 1;
$state::client = 2;
$state::sp = 3;
$state = $state::none;

function State::isSinglePlayer()
{
    %result = $state == $state::sp;
    return %result;
}

function State::isHosting()
{
    %result = $state == $state::host;
    return %result;
}

function State::isClient()
{
    %result = $state == $state::client;
    return %result;
}

$GgcToken::NULL = "";
$GgcToken::SP = "sp";
$GgcToken::HOST = "host";
$GgcToken::CLIENT = "client";
$GgcToken::SPLeaderboardType = "single";

/// Gets the token to be used for the current game state. This token is useful
/// for sending rpc to the library. The returned string will be one of:
/// $GgcToken:: SP, HOST, CLIENT, NULL
///
/// \return string A string useful as an rpc token, or an empty string if the
/// game is not in an appropriate state.
function getSpHostClientToken()
{
    if ($state == $state::sp)
    {
        return $GgcToken::SP;
    }
    else
    {
        if ($state == $state::host)
        {
            return $GgcToken::HOST;
        }
        else
        {
            if ($state == $state::client)
            {
                return $GgcToken::CLIENT;
            }
            else
            {
                return $GgcToken::NULL;
            }
        }
    }
}