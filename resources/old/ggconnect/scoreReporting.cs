$json::name = "\"name\":";
$json::score = "\"score\":";
$json::bestScore = "\"bestScore\":";
$json::bestRating = "\"bestRating\":";
$json::overallRating = "\"overallRating\":";
$jsonDelimiter = "|";

/// Gets the currently populated client scores (from the ClientRanks object) as
/// a JSON formatted string. Each client's score is one object in an array.
///
/// Example: [{"name":"Jim"|"score":22}|{"name":"Bob"|"score":38}]
///
/// \param %playerList (GuiTextListCtrl) The list of players where each row has
/// the player name as word 1 and their score as word 2 (zero indexed).
/// \return (string) JSON formatted string of client scores.
function getJsonScores(%unused)
{
    %json = "[";
    %i = 0;
    while (%i < ClientGroup.getCount())
    {
        if (%i > 0)
        {
            %json = %json @ "|";
        }
        %client = ClientGroup.getObject(%i);
        %json = %json @ getJsonScore(%client);
        %i = %i + 1;
    }
    %json = %json @ "]";
    return %json;
}

/// Gets the json formatted score for the given client.
///
/// \return string The json formatted score as
/// {"name":"%client.nameBase"|"score":"%client.points|%client.getMarbleTime()"}
function getJsonScore(%client)
{
    %name = "\"" @ %client.nameBase @ "\"";
    if (State::isSinglePlayer())
    {
        %score = "\"" @ %client.Player.score @ "\"";
        %bestScore = "\"" @ %client.Player.bestScore @ "\"";
        %bestRating = "\"" @ %client.Player.bestRating @ "\"";
        %overallRating = "\"" @ %client.Player.overallRating @ "\"";
    }
    else
    {
        %score = "\"" @ %client.points @ "\"";
        %bestScore = "\"\"";
        %bestRating = "\"\"";
        %overallRating = "\"\"";
    }
    %json = "{" @ $json::name @ %name @ $jsonDelimiter @ $json::score @ %score @ $jsonDelimiter @ $json::bestScore @ %bestScore @ $jsonDelimiter @ $json::bestRating @ %bestRating @ $jsonDelimiter @ $json::overallRating @ %overallRating @ "}";
}

/// Broadcasts the final scores to the clients.
function broadcastScores()
{
    if (State::isClient())
    {
        error("ERROR: broadcastScores(): client can\'t broadcast scores");
    }
    %i = 0;
    while (%i < ClientGroup.getCount())
    {
        %client = ClientGroup.getObject(%i);
        %jsonScores = getJsonScores();
        commandToClient(%client, 'setClientScores', %jsonScores);
        %i = %i + 1;
    }
}

/// Receives scores from the host and broadcasts them to the browser.
function clientCmdSetClientScores(%jsonScores)
{
    if (%jsonScores $= "")
    {
        %jsonScores = "[]";
    }
    sendGGCMessage($ggcMessageNameSessionScores, $ggcSuccess, %jsonScores);
}