new GuiControl(ggcMarblePicker)
{
    Profile = "GuiDefaultProfile";
    HorizSizing = "right";
    VertSizing = "bottom";
    position = "0 0";
    Extent = "640 480";
    MinExtent = "8 2";
    Visible = "1";
};

/// Shows the marble picker.
function MarblePicker::show(%this)
{
    ggcConsoleLog("MarblePicker::show()");
    RootGui.setContent(ggcMarblePicker);
    RootGui.setTitle($Text::About0Title);
    %picker = MarblePicker::getSingleton();
    GameMissionInfo.setMode(SpecialMode);
    GameMissionInfo.selectMission(0);
    commandToServer('SetMarble', $pref::marbleIndex);
    commandToServer('SpawnMarblePickerMarble');
    return $ggcSuccess;
}

/// Exists the marble picker. The current marble is auto-saved as the selected choice.
function MarblePicker::exit(%this)
{
    ggcConsoleLog("MarblePicker::exit()");
    commandToServer('DestroyMarblePickerMarble');
    %string = CurrentGamerProfile.packProfile();
    CurrentGamerProfile.saveProfile(%string);
    RootGui.setContent(ESRBGui);
    return $ggcSuccess;
}

function MarblePicker::getSingleton(%this)
{
    if (!isObject(MarblePicker))
    {
        new ScriptObject(MarblePicker);
        MarblePicker.numMarbles = 35;
        if ((!(($pref::marbleIndex $= "")) && ($pref::marbleIndex > 0)) && ($pref::marbleIndex <= MarblePicker.numMarbles))
        {
            MarblePicker.current = $pref::marbleIndex;
        }
        else
        {
            MarblePicker.current = 1;
        }
    }
    return MarblePicker;
}

/// Advances marble selection to the next marble. Wraps at the extents.
function MarblePicker::nextMarble(%this)
{
    ggcConsoleLog("MarblePicker::nextMarble()");
    if (%this $= "")
    {
        %this = MarblePicker::getSingleton();
    }
    %this.current = %this.current + 1;
    if (%this.current > %this.numMarbles)
    {
        %this.current = 1;
    }
    %this.updateDisplay();
    return "$pref::marbleIndex = " @ $pref::marbleIndex;
}

/// Advances marble selection to the previous marble. Wraps at the extents.
function MarblePicker::prevMarble(%this)
{
    ggcConsoleLog("MarblePicker::prevMarble()");
    if (%this $= "")
    {
        %this = MarblePicker::getSingleton();
    }
    %this.current = %this.current - 1;
    if (%this.current <= 0)
    {
        %this.current = %this.numMarbles;
    }
    %this.updateDisplay();
    return "$pref::marbleIndex = " @ $pref::marbleIndex;
}

function MarblePicker::updateDisplay(%this)
{
    $pref::marbleIndex = %this.current;
    commandToServer('SetMarble', $pref::marbleIndex);
    commandToServer('SpawnMarblePickerMarble');
}