function declareGGCError(%errVar, %errCode)
{
    if (!($ggcErrors[%errCode] $= ""))
    {
        error("error! error code already defined:" SPC %errCode);
    }
    else
    {
        $ggcErrors[%errCode] = %errVar;
        $ScriptError = "";
        %evalStr = %errVar @ " = " @ %errCode @ ";";
        eval(%evalStr);
        if (!($ScriptError $= ""))
        {
            error("Error defining error code:" SPC %evalStr SPC %errVar);
        }
    }
}

declareGGCError("$ggcSuccess", 0);
declareGGCError("$ggcMessageNotEnoughSections", 1);
declareGGCError("$ggcMessageNoReqId", 2);
declareGGCError("$ggcMessageNoCommand", 3);
declareGGCError("$ggcMessageParamError", 4);
declareGGCError("$ggcMessageUnknownCommand", 5);
declareGGCError("$ggcMessageVidInitFailed", 6);
declareGGCError("$ggcMessageDupMsgError", 7);
declareGGCError("$ggcMessageVidNotInitted", 8);
declareGGCError("$ggcMessageLevelNotFound", 9);
declareGGCError("$ggcMessageTimeout", 10);
declareGGCError("$ggcMessageCannotRestart", 11);
declareGGCError("$ggcMessageSessionNotActive", 12);
declareGGCError("$ggcMessageSessionInProgress", 13);
declareGGCError("$ggcMessageSessionCreateFailed", 14);
declareGGCError("$ggcMessageGameNotLoaded", 15);
declareGGCError("$ggcConnectionTimeout", 16);
declareGGCError("$ggcConnectionDropped", 17);
declareGGCError("$ggcConnectionError", 18);
declareGGCError("$ggcConnectionRejected", 19);
declareGGCError("$ggcConnectionRequestTimedOut", 20);
declareGGCError("$ggcMessageInvalidHostIp", 21);
declareGGCError("$ggcErrorUnableToRestart", 22);
declareGGCError("$ggcConnectionNATNoAddress", 24);
declareGGCError("$ggcConnectionNATLANDetected", 31);
declareGGCError("$ggcConnectionNATTimeout", 32);
declareGGCError("$ggcGameEndHandshakeNotWaiting", 33);
declareGGCError("$GgcContentNotOwned", 120);

$ggcMessageNameGameLoaded = "GameLoaded";
///< Sent when game assets are fully loaded

$GGCMessageNameGameReady = "GameReady";
///< Sent when the engine is started and ready to receive commands

$ggcMessageNameMessageError = "MessageError";
$ggcMessageNameSessionEnded = "SessionEnded";
$GGCMessageNameSessionStarted = "SessionStarted";
$ggcMessageNameSessionLoading = "SessionLoading";
$ggcMessageNameSessionKilled = "SessionKilled";
///< Video is no longer rendering (returned to title screen)

$GGCMessageNameSessionPaused = "SessionBlurred";
$GGCMessageNameSessionResumed = "SessionFocused";
$ggcMessageNameShowLeaderboard = "ShowLeaderboard";
$ggcMessageNameConnection = "Connection";
///< messages about the connection

$ggcMessageNameSessionScores = "SessionScores";
///< this message accompanies the scores from the last session
///

$gameReqId = 0;

function onGGCMessageError(%msg, %errorCode, %errorData)
{
    error("GGCMessageError:" SPC %msg SPC %errorCode SPC %errorData);
    sendGGCMessage($ggcMessageNameMessageError, %errorCode);
}

function doGGConnectMessage(%msg)
{
    ggcConsoleLog("GGCrecv:" SPC %msg);
    %delims = ",";
    %sectionCount = getSectionCount(%msg, %delims);
    if (%sectionCount < 2)
    {
        onGGCMessageError(%msg, $ggcMessageNotEnoughSections);
    }
    %reqId = trim(getSection(%msg, 0, %delims));
    if (%reqId $= "")
    {
        onGGCMessageError(%msg, $ggcMessageNoReqId);
    }
    %command = trim(getSection(%msg, 1, %delims));
    if (%command $= "")
    {
        onGGCMessageError(%msg, $ggcMessageNoCommand);
    }
    %argString = "";
    %i = 2;
    while (%i < %sectionCount)
    {
        %pair = trim(getSection(%msg, %i, %delims));
        %pairCount = getSectionCount(%pair, "=");
        if (%pairCount != 2)
        {
            onGGCMessageError(%msg, $ggcMessageParamError, %i);
        }
        %val = trim(getSection(%pair, 1, "="));
        %evalStr = "%arg" @ %i - 2 @ " = \"" @ %val @ "\";";
        $ScriptError = "";
        eval(%evalStr);
        if (!($ScriptError $= ""))
        {
            onGGCMessageError(%msg, $ggcMessageParamError, %i);
        }
        %i = %i + 1;
    }
    %funcName = "GGCMessage" @ %command;
    if (!isFunction(%funcName))
    {
        onGGCMessageError(%msg, $ggcMessageUnknownCommand);
    }
    call(%funcName, %reqId, %command, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
}

function buildGGCMessage(%reqId, %msgName, %errorCode)
{
    %msg = %reqId @ "," @ %msgName @ "," @ %errorCode;
    return %msg;
}

function sendGGCMessage(%msgName, %errorCode, %arg0, %arg1, %arg2)
{
    %reqId = $gameReqId = $gameReqId + 1;
    %msg = buildGGCMessage(%reqId, %msgName, %errorCode);
    if (!(%arg0 $= ""))
    {
        %msg = %msg @ "," @ %arg0;
    }
    if (!(%arg1 $= ""))
    {
        %msg = %msg @ "," @ %arg1;
    }
    if (!(%arg2 $= ""))
    {
        %msg = %msg @ "," @ %arg2;
    }
    if (%errorCode != $ggcSuccess)
    {
        %errorCodeStr = $ggcErrors[%errorCode];
        error("GGCsenderror, " @ %errorCodeStr @ ":" SPC %msg);
        if (!(%errorCodeStr $= ""))
        {
            %msg = %msg @ "," @ %errorCodeStr;
        }
    }
    else
    {
        ggcConsoleLog("GGCsend:" SPC %msg);
    }
    GGConnectSendMessage(%msg);
}

function setGGCVariable(%varName, %val)
{
    ggcConsoleLog("GGCsetVar:" SPC %varName SPC %val);
    GGConnectSetVar(%varName, %val);
}

$gameMsgFlag = "_____ ts:ggc _____";

function getReturnError(%errorKey, %errorMsg)
{
    error($gameMsgFlag SPC %errorMsg);
    return %errorKey @ ", " @ %errorMsg;
}

function ggcConsoleLog(%message)
{
    echo($gameMsgFlag SPC %message);
}

function ggcErrorLog(%message)
{
    error($gameMsgFlag SPC %message);
    if (!isShippingBuild())
    {
        sendGGCMessage("ERROR", %message);
    }
}