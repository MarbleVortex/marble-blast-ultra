function GgcMessageGetGameVersion(%reqId, %command)
{
    %version = getBuildNumber();
    sendGGCMessage(%reqId, %command, $ggcSuccess, %version);
}

function onGGCShowLeaderboard(%leaderboardId)
{
    sendGGCMessage($ggcMessageNameShowLeaderboard, 0, %leaderboardId);
}

function onInputActivated()
{
    
}

function onInputDeactivated()
{
    if ($noPauseOnLostInput)
    {
        return ;
    }
    if (PlayGui.isAwake() && !$gamePauseWanted)
    {
        pauseToggle(0);
    }
    cursorOn();
}