/// Initializes the host so clients can connect.
function Game::hostInit()
{
    ggcConsoleLog("Game::hostInit()");
    $state = $state::host;
    $Server::UsingLobby = 1;
    startMultiplayerServer();
}

/// Actually starts the level.
///
/// \param %mode (unused) You MUST pass a garbage string here to circumvent a
/// bug in the library that thinks empty args are missing.
/// \param %level (int) The id of a multiplayer level. Range [61-80]
function Game::hostStart(%gameMode, %levelId)
{
    ggcConsoleLog("Game::hostStart(" @ %gameMode @ ", " @ %levelId @ ")");
    %code = GGConnectCheckContentOwnership("mode", %gameMode);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    %code = GGConnectCheckContentOwnership("level", %levelId);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    $ggc::gameMode = %gameMode;
    $ggc::levelid = %levelId;
    $naturalEnd = 0;
    $Game::SPGemHunt = 0;
    GameMissionInfo.setMode(GameMissionInfo.MPMode);
    if (!MissionLoadingGui.isAwake())
    {
        RootGui.setContent(MissionLoadingGui);
    }
    ServerConnection.joinImmediately = 1;
    %mis = GameMissionInfo.findMissionById(%levelId);
    if (!isObject(%mis))
    {
        return getReturnError($ggcMessageLevelNotFound, "Level not found");
    }
    GameMissionInfo.setCurrentMission(%mis.File);
    GameMissionInfo.selectMission(GameMissionInfo.getCurrentIndex());
    $disconnectGui = ESRBGui;
    return $ggcSuccess;
}

/// Ends the current multiplayer level. Clients remain connected
function Game::hostEnd()
{
    ggcConsoleLog("Game::hostEnd()");
    escapeFromGame();
    if (!$GameEndHandshakeWaiting)
    {
        $SessionRunning = 0;
        sendGGCMessage($ggcMessageNameSessionKilled, $ggcSuccess);
        informAllClients('HostEnded');
    }
    else
    {
        informAllClients('DoEndHandshake');
    }
}

/// Sends the specified command to all clients (including self).
///
/// \param tag %command Tagged string representing the command to send.
function informAllClients(%command)
{
    %i = 0;
    while (%i < ClientGroup.getCount())
    {
        %client = ClientGroup.getObject(%i);
        commandToClient(%client, %command);
        %i = %i + 1;
    }
}

/// Ends the host's session. All clients will be kicked.
function Game::hostShutdown(%errorCode)
{
    ggcConsoleLog("Game::hostShutdown(" @ %errorCode @ ")");
    enterPreviewMode();
    $state = $state::none;
    return $ggcSuccess;
}

function Game::hostAddPlayer(%userId)
{
    ggcConsoleLog("Game::hostAddPlayer(" @ %userId @ ")");
    return $ggcSuccess;
}

function Game::hostRemovePlayer(%userId)
{
    ggcConsoleLog("Game::hostRemovePlayer(" @ %userId @ ")");
    return $ggcSuccess;
}

function Game::hostRegistrationComplete(%errorCode)
{
    ggcConsoleLog("Game::hostRegistrationComplete(" @ %errorCode @ ")");
    onGgcHostRegistrationComplete(%errorCode);
    return $ggcSuccess;
}

function Game::networkAccess(%accessLevel)
{
    ggcConsoleLog("Game::networkAccess(" @ %accessLevel @ ")");
    onGgcNetworkAccess(%accessLevel);
    return $ggcSuccess;
}