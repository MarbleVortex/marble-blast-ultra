function Game::spInit()
{
    ggcConsoleLog("Game::spInit()");
    $state = $state::sp;
}

/// Actually start the single player game with the specified level.
///
/// \param %gameMode (string) The mode to play in, either "race" or "gemhunt"
/// \param %leveId (int) The id of the level to play. Range [1, 60]
function Game::spStart(%gameMode, %levelId)
{
    ggcConsoleLog("Game::spStart(" @ %levelId @ ", " @ %gameMode @ ")");
    %code = GGConnectCheckContentOwnership("mode", %gameMode);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    %code = GGConnectCheckContentOwnership("level", %levelId);
    if (%code > 0)
    {
        ggcConsoleLog("content not owned");
        return %code;
    }
    GGCStatsSetMode($GgcToken::SPLeaderboardType, %gameMode);
    GGCStatsLoadStats(%levelId);
    GGCStatsLoadStats($Leaderboard::SPOverall);
    $naturalEnd = 0;
    $Game::SPGemHunt = ((%gameMode $= "scrum") || (%gameMode $= "gemhunt")) || (%gameMode $= "gem hunt");
    if ($Game::SPGemHunt)
    {
        GameMissionInfo.setMode(GameMissionInfo.MPMode);
    }
    else
    {
        GameMissionInfo.setMode(GameMissionInfo.SPMode);
    }
    if (!MissionLoadingGui.isAwake())
    {
        RootGui.setContent(MissionLoadingGui);
    }
    ServerConnection.joinImmediately = 1;
    %mis = GameMissionInfo.findMissionById(%levelId);
    if (!isObject(%mis))
    {
        return getReturnError($ggcMessageLevelNotFound, "Level not found");
    }
    GameMissionInfo.setCurrentMission(%mis.File);
    GameMissionInfo.selectMission(GameMissionInfo.getCurrentIndex());
    $disconnectGui = ESRBGui;
    return $ggcSuccess;
}

/// Ends the current single player level.
function Game::spEnd()
{
    ggcConsoleLog("Game::spEnd()");
    escapeFromGame();
    if (!$GameEndHandshakeWaiting)
    {
        $SessionRunning = 0;
        sendGGCMessage($ggcMessageNameSessionKilled, $ggcSuccess);
    }
}

function Game::spShutdown()
{
    ggcConsoleLog("Game::spShutdown()");
    GGCStatsClearMode();
    $state = $state::none;
    return $ggcSuccess;
}

/// Restarts the single player level.
///
/// \return $ggcSuccess if the game was restarted successfully or 22 if there
/// was an error.
function Game::spRestart()
{
    ggcConsoleLog("Game::spRestart()");
    if (GamePauseGui.doRestart())
    {
        return $ggcSuccess;
    }
    else
    {
        return getReturnError($ggcErrorUnableToRestart, "Unable to restart the game.");
    }
}