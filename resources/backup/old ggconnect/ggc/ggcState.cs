$state::none = 0;
$state::host = 1;
$state::client = 2;
$state::sp = 3;
$state = "0";

function State::isSinglePlayer()
{
    %result = false;
    if ($state == $state::sp)
        %result = true;
    return %result;
}

function State::isHosting()
{
    %result = false;
    if ($state == $state::host)
        %result = true;
    return %result;
}

function State::isClient()
{
    %result = false;
    if ($state == $state::client)
        %result = true;
    return %result;
}

$GgcToken::NULL = "";
$GgcToken::SP = "sp";
$GgcToken::HOST = "host";
$GgcToken::CLIENT = "client";
$GgcToken::SPLeaderboardType = "single";

function getSpHostClientToken()
{
    if ($state == $state::sp)
    {
        return $GgcToken::SP;
    } else if ($state == $state::host)
    {
        return $GgcToken::HOST;
    } else if ($state == $state::client)
    {
        return $GgcToken::CLIENT;
    }
    return $GgcToken::NULL;
}
