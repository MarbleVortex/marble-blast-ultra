function registerGgcCallbacks()
{
    GGConnectRegisterCommandCallback("game.setFocus", "", Game::setFocus);
    GGConnectRegisterCommandCallback("game.blurSession", "", Game::pause);
    GGConnectRegisterCommandCallback("game.focusSession", "", Game::resume);
    GGConnectRegisterCommandCallback("game.sp.restart", "", Game::spRestart);
    GGConnectRegisterCommandCallback("game.sp.ackEnd", "", Game::acknowledgeEndSession);
    GGConnectRegisterCommandCallback("game.host.ackEnd", "", Game::acknowledgeEndSession);
    GGConnectRegisterCommandCallback("game.client.ackEnd", "", Game::acknowledgeEndSession);
    GGConnectRegisterCommandCallback("video.showAstrolabe", "show", VideoOptions::showAstrolabe);
    GGConnectRegisterCommandCallback("video.invertCameraX", "invertX", VideoOptions::invertCameraX);
    GGConnectRegisterCommandCallback("video.invertCameraY", "invertY", VideoOptions::invertCameraY);
    GGConnectRegisterCommandCallback("video.setResolution", "width height", VideoOptions::setResolution);
    GGConnectRegisterCommandCallback("video.setDevice", "device", VideoOptions::setDevice);
    GGConnectRegisterCommandCallback("video.setTextureQuality", "quality", VideoOptions::setTextureQuality);
    GGConnectRegisterCommandCallback("input.setMouseSensitivity", "sensitivity", InputOptions::setMouseSensitivity);
    GGConnectRegisterCommandCallback("game.setIPDiscoveryServers", "server1 server2", Game::setIPDiscoveryServers);
    GGConnectRegisterCommandCallback("game.setTURNServer", "server", Game::setTURNServer);
    GGConnectRegisterCommandCallback("game.setForceTURN", "force", Game::setForceTURN);
    GGConnectRegisterCommandCallback("game.installContent", "", Game::installContent);
    GGConnectRegisterCommandCallback("prefs.player.setMarble", "marbleIndex", Prefs::setMarble);
    GGConnectRegisterCommandCallback("prefs.player.displayMPHelpText", "display", Prefs::setDisplayMPHelpText);
    GGConnectRegisterCommandCallback("prefs.audio.setMasterVolume", "volume", Prefs::setMasterVolume);
    GGConnectRegisterCommandCallback("prefs.audio.setEffectsVolume", "volume", Prefs::setEffectsVolume);
    GGConnectRegisterCommandCallback("prefs.audio.setMusicVolume", "volume", Prefs::setMusicVolume);
    GGConnectRegisterCommandCallback("game.saveJournal", "filename", Game::saveJournal);
    GGConnectRegisterCommandCallback("game.playJournal", "filename", Game::playJournal);
}
