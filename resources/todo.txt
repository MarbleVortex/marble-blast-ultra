=================================================================================================================
			    			Marble Blast Ultra
-----------------------------------------------------------------------------------------------------------------
						    To-Do List
=================================================================================================================
 This document is best viewed with Microsoft Notepad with Word Wrap disabled using the Consolas font at size 11.
=================================================================================================================

[Marble Blast Ultra Team]
  - Matthieu Parizeau		Leader		Bugfixing, Coding, Research, and Level Building
  - Anthony Kleine		Co-Leader	Bugfixing, Coding, Testing, Research, and Level Building
  - Erik Martin			Member		Level Building, Testing
  - James Rudolph		Member		Level Building
  - David Orel			Member		Level Building, Graphic Design

=================================================================================================================

This document is CONFIDENTIAL and must not be shared with anyone outside of the Marble Blast Ultra Team
without written permission from the leader, or co-leader.

Failure to comply may result in removal from the team.

=================================================================================================================

[Possible Statuses]
  - N/A		Not Yet Started
  - WIP		Work In Progress
  - Done	Complete and Implemented

[Severity Levels]
  - Minor	This can wait
  - Important   Probably should be worked on soon
  - Critical	Extrememly Severe and should be worked on ASAP

[Types]
  - Bug		Something unintentional that needs to be fixed
  - Feature	Something gameplay related or utility related that is not currently implemented in any way
  - Idea	Something that could be done but is not yet set in stone

=================================================================================================================
  Description								Status		Severity	Type
-----------------------------------------------------------------------------------------------------------------
- Fix pause menu (B or Backspace) visual bug				Done		Minor		Bug
- Fix window contents not resizing					Done		Important	Bug
- Remake all known Marble Blast Evolved levels including
  Multiplayer Levels shown in the intermediate levels showcase video	WIP		Important	Feature
- Remake Marble Blast Mobile Levels (Singleplayer and Multiplayer)	WIP		Important	Feature
- Remappable key binds in the options menu				WIP		Important	Feature
- Fix Full-Screen Minimize Lock Up (If Possible)			N/A		Critical	Bug
- Ability to select supported resolutions in game. It would
  detect system resolutions.						N/A		Minor		Feature
- Sort Multiplayer Levels (Similar To Custom and Game Sorting)		N/A		Important	Feature
- Level Search (Keyword/Difficulty/Type/Game)				N/A		Minor		Feature
- Automatic alignment button in editor (OCD Align) which would
  automatically align selected objects to a specified grid.		N/A		Minor		Feature
- Option to use MBO shaders instead of MBU shaders			N/A		Minor		Feature
- Automatically pause game if the window is not in
  focus (Alt-Tab, etc)							N/A		Minor		Feature
- Fix window contents not resizing while paused due to schedule
  not working with timescale at a low number				N/A		Important	Bug
- Look into the ability to hook into the game's exe
  by running a custom program with TorqueScript's ShellExecute
  for extra functionality/bugfixes as an engine hack alternative	Done		Minor		Idea
- Attempt to find information on beta MBU level (Marble Tower?)
  and remake it. (Or find the level itself)				N/A		Minor		Idea
- Fix Secret Sound option only working if the host selects it but
  applying to everyone.							Done		Minor		Bug
- Fix resizing window breaking multiplayer lobby, and timer.		Done		Critical	Bug
=================================================================================================================

[Marble Blast Ultra Team]
  - Matthieu Parizeau		Leader		Bugfixing, Coding, Research, and Level Building
  - Anthony Kleine		Co-Leader	Bugfixing, Coding, Testing, Research, and Level Building
  - Erik Martin			Member		Level Building, Testing
  - James Rudolph		Member		Level Building
  - David Orel			Member		Level Building, Graphic Design

=================================================================================================================

This document is CONFIDENTIAL and must not be shared with anyone outside of the Marble Blast Ultra Team
without written permission from the leader, or co-leader.

Failure to comply may result in removal from the team.

=================================================================================================================