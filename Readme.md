# Marble Blast Ultra

## PC Port

#### Description
This project is the continuation of Marble Blast Ultra from where [GarageGames](http://www.garagegames.com) left off. After getting the game to run on PC, we immediately decided to start adding new content.

#### Features

Some things we've added or are planning to add are:

* New Levels:
	* Bonus Levels
	* Gold Levels
	* Mobile Levels [WIP]
	* Troll Levels
	* Evolved Levels
	* Loads of Custom Levels
* New PowerUps:
	* Anvil
* New Decorations:
	* Trees
	* Flowers
	* Tall Grass
* New Textures:
	* Dirt
	* Grass
	* *Various Tiles
	* And More
* New GameModes: 
	* Sumo
	* Race
	* Color [WIP]
	* Tag [PLANNED]
	* Side Scroller
	* King of the Hill [PLANNED]
* New Environments:
	* Nature
	* Troll
	* Dark [MAYBE]
	* Dreamy [MAYBE]
	* Toy [MAYBE]
  
This is a Major Project and has currently been going on for about 2 1/2 years. We don't want Marble Blast Ultra to be lost forever and thus are keeping the project alive as long as possible.

#### MBU Team
* [Matthieu Parizeau](http://www.bitbucket.org/mparizeau) Leader
* [Anthony Kleine](http://www.bitbucket.org/tomysshadow) Co-Leader
* [Erik Martin](http://www.bitbucket.org/marterik231) Website
* [James Rudolph](http://www.bitbucket.org/danger229) Level Builder
* [David Orel](http://www.youtube.com/user/roblox184) 3D Graphics Artist and Level Builder

#### License
Please keep in mind that this is a fan-made project and we are not in contact with the current license holder in any way.

Marble Blast was created by [GarageGames](http://www.garagegames.com/) but is now owned by [IAC](http://www.iac.com/).

If there are any problems with us hosting this project on [GitLab](http://www.gitlab.com/) contact me immediately.
If we are asked by the license owners to stop this project, we will do so immediately.